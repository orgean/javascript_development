
//Versionstring is supposed to be defined in main.html
//It is used to differentiate different versions, preventing them from being cached
if (typeof versionString == 'undefined')
    alert('Fatal error: versionString is missing');

//Configuration parameters for require.js
require.config({
    baseUrl: "scripts",
    paths: {
        jquery: "DQX/Externals/jquery",
        d3: "DQX/Externals/d3",
        handlebars: "DQX/Externals/handlebars",
        markdown: "DQX/Externals/markdown",
        DQX: "DQX"
    },
    shim: {
        d3: {
            exports: 'd3'
        },
        handlebars: {
            exports: 'Handlebars'
        }
    },
    waitSeconds: 15,
    urlArgs: "version="+versionString
});





 require(["jquery", "DQX/Application", "DQX/Framework", "DQX/Msg", "DQX/Utils","Views/DataBase","MRC/client","Views/SequenceViewer"],
    function ($, Application, Framework, Msg, DQX, DataBase,  Client, SequenceViewer) {
       $(function () {

		Application.DbObjects=[];
		Application.PjObjects=[];
		Application.ActiveDbObject=null;
		Application.ActivePjObject=null;
		
        Application.SetMessage=function(code)
		{
			console.log(code);
		}
        Application.GetDbObject=function(code)
		{
			for (var i=0;i<Application.DbObjects.length;i++) if (Application.DbObjects[i].GetCode()===code) return Application.DbObjects[i];
			Application.SetMessage("No valid dbobject returned from dbobject code");
			return null;
		}
        Application.RegisterDbObject=function(_code)
		{
			var dbobject=new MDbObject.DbObject(_code);
			dbobject.clientConnection=Application.ClientConnection;
			Application.DbObjects.push(dbobject);
			return dbobject;
		}
		Application.GetPjObject=function(code)
		{
			for (var i=0;i<Application.PjObjects.length;i++) if (Application.PjObjects[i].GetCode()===code) return Application.PjObjects[i];
			Application.SetMessage("No valid pjobject returned from pjobject code");
			return null;
		}
        Application.RegisterPjObject=function(_code)
		{
			var pjobject=new MPjObject.PjObject(_code);
			pjobject.clientConnection=Application.ClientConnection;
			Application.PjObjects.push(pjobject);
			return pjobject;
		}
		Application.SetActiveDbObject=function(_code)
		{
			Application.ActiveDbObject=Application.GetDbObject(_code);
		}
		Application.GetActiveDbObject=function()
		{
			return Application.ActiveDbObject;
		}
		Application.SetActivePjObject=function(_code)
		{
			Application.ActivePjObject=Application.GetPjObject(_code);
		}
		Application.GetActivePjObject=function()
		{
			return Application.ActivePjObject;
		}
		Application.FindClassObject=function(code)
		{
			for (var i=0;i<Application.DbObjects.length;i++) if (Application.DbObjects[i].FindClassObject(code)!=null) return Application.DbObjects[i].FindClassObject(code);
			return null;
		}
		Application.DecodePath=function(_pathcode)
		{
			for (var i=0;i<Application.DbObjects.length;i++) if (Application.DbObjects[i].DecodePath(_pathcode)!="") return Application.DbObjects[i].DecodePath(_pathcode);
			return "";
		}

		Application.Client=Client.init();
			Application.UpdateTracks=null;
			Application.GetTrackCount=function()
			{
				return Application.DbTracks.GetTrackCount();
			}
 			Application.GetTrack=function(index)
			{
				return Application.DbTracks.GetTrack[index];
			}
			Application.GetLastTrack=function()
			{
				return Application.DbTracks.GetLastTrack();
			}
			Application.CreateTrack=function(description,view)
			{
				var track=Application.DbTracks.AddTrack();
				track.SetDefiner(description);
				track.SetView(view);
				if (Application.UpdateTracks!=null) Application.UpdateTracks();
			}

            // Initialise all the views in the application
			Application.CreateTrack("Client initiated");
			
			

             //Initialise the application
            Application.init('Software Marc');
			Application.RegisteredWindows=[];
			Application.RegisteredSequenceBoxes=[];
			Application.RegisteredSeqPlots=[];
			
			DataBase.init();
			Application.CreateTrack("Database initiated");

			Application.RegisterWindow=function(window)
			{
				Application.RegisteredWindows.push(window);
			}
			Application.GetRegisteredWindow=function(id)
			{
				for (var i=0;i<Application.RegisteredWindows.length;i++)
				{
					if (Application.RegisteredWindows[i].Identifier==id) return Application.RegisteredWindows[i];
				}
				return null;
			}
			Application.RegisterSeqBox=function(seqbox)
			{
				Application.RegisteredSequenceBoxes.push(seqbox);
			}
			Application.GetRegisteredSeqBox=function(id)
			{
				for (var i=0;i<Application.RegisteredSequenceBoxes.length;i++)
				{
					if (Application.RegisteredSequenceBoxes[i].GetIdentifier()==id) return Application.RegisteredSequenceBoxes[i];
				}
				return null;
			}
			Application.RegisterSeqPlot=function(seqplot)
			{
				Application.RegisteredSeqPlots.push(seqplot);
			}
 			Application.GetRegisteredSeqPlot=function(id)
			{
				for (var i=0;i<Application.RegisteredSeqPlots.length;i++)
				{
					if (Application.RegisteredSeqPlots[i].GetIdentifier()==id) return Application.RegisteredSeqPlots[i];
				}
				return null;
			}
			Application.GetRegisteredTrack=function(id)
			{
				for (var i=0;i<Application.DbTracks.length;i++)
				{
					if (Application.DbTracks[i].GetCode()==id) return Application.DbTracks[i];
				}
				return null;
			}
      });
    });
