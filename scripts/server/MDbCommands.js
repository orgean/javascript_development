(function(exports)
{

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

exports.MCommand=function()
{
	var that=this;
	that.Commands=[];
	that.parent=null;
	that.definition="";
	that.action="?";
	that.expand=true;
	that.mapped=true;
	that.uniquecode="";
	that.endnode=0;

	that.AddCommand=function()
	{
		var command=new exports.MCommand();
		command.parent=that;
		that.Commands.push(command);
		return command;
	}
	that.InsertCommand=function(_datafile,_json)
	{
		var definer=_datafile;
		var definer_l="";
		var definer_r="";
//		var index=definer.indexOf('|');
		var index=-1;
		for (var i=0;i<definer.length;i++) if (definer[i]=='|') {index=i;break;}
		if (index>-1)
		{
			definer_l=definer.substring(0,index);
			definer_r=definer.substring(index+1,definer.length);
		}
		else definer_l=definer;
		var sub=null;
		var item=null;
		for (var i=0;i<that.Commands.length;i++)
		{
			item=that.Commands[i];
			if (item.GetDefiner()==definer_l) sub=item;
		}
		if (sub==null) 
		{
			sub=that.AddCommand();
			sub.SetDefiner(definer_l);
			if (_json!=null) 
			{
				sub.action=_json;
			}
		}
		if (definer_r.length>0) return sub.InsertCommand(definer_r,_json);
		return sub;
	}
	that.HasChildren=function()
	{
		return that.Commands.length;
	}
	that.IsChildFrom=function(command)
	{
		var p_parent=that.parent;
		while (p_parent!=null)
		{
			if (p_parent==command) return true;
			p_parent=p_parent.parent;
		}
		return false;
	}

	that.GetCommandsCnt=function()
	{
		return that.Commands.length;
	}

	that.GetCommand=function(nr)
	{
		if (nr<0 || nr>=that.Commands.length) return null;
		return that.Commands[nr];
	}

	that.SetDefiner=function(def)
	{ 
		if (def==="") return;
		that.definition=def;
	}

	that.GetDefiner=function()
	{
		return that.definition;
	}
	that.GetFullDefiner=function()
	{
		var _fulldef="";
		var p_parent=that;
		while (p_parent!=null)
		{
			if (_fulldef!="") _fulldef=" | "+_fulldef;
			_fulldef=p_parent.GetDefiner()+_fulldef;
			p_parent=p_parent.parent;
		}
		return _fulldef;
	}
	that.SetAction=function(json)
	{ 
		that.action=json;
	}

	that.GetDepth=function()
	{
		var p_parent=that.parent;
		var k=0;
		while (p_parent!=null)
		{
			p_parent=p_parent.parent;
			k++;
		}
		return k;
	}	
	that.GetMapped=function() {return that.mapped;};
	that.SetMapped=function(how) {that.mapped=how;};
	that.IsExpand=function() {return that.expand;};
	that.SetExpand=function(how) {that.expand=how;};
	that.SetUniqueCode=function(code) {that.uniquecode=code;};
	that.GetUniqueCode=function() {return that.uniquecode;};

	that.ExpandChildren=function(state)
	{
		for (var i=0;i<that.Commands.length;i++)
		{
			var _command=that.Commands[i];
			_command.SetExpand(state);
		}
	}
	return that;
};

exports.DbCommands=function()
{
	var that={};
	that.clientConnection=null;
	that.Commands=[];
	that.CommandsMap=[];
	that.DbCells=[];
	that.maxdepth=0;
	that.DataLinksOnly=false;
//**********************************
	that.InsertCommand=function(_datapath,json)
	{
		console.log("Command added: "+_datapath);
		var definer=_datapath;
		var definer_l="";
		var definer_r="";
//		var index=definer.indexOf('|');
		var index=-1;
		for (var i=0;i<definer.length;i++) if (definer[i]=='|') {index=i;break;}
		if (index>-1)
		{
			definer_l=definer.substring(0,index);
			definer_r=definer.substring(index+1,definer.length);
		}
		else definer_l=definer;
		var sub=null;
		var item=null;
		for (var i=0;i<that.Commands.length;i++)
		{
			item=that.Commands[i];
			if (item.GetDefiner()==definer_l) sub=item;
		}
		if (sub==null) 
		{
			sub=that.AddCommand();
			sub.SetDefiner(definer_l);
			sub.SetAction(json);
		}
		if (definer_r.length>0) return sub.InsertCommand(definer_r,json);
		return sub;
	}
	that.AddCommand=function()
	{
		var command=new exports.MCommand();
		that.Commands.push(command);
		return that.Commands[that.Commands.length-1];
	}
	that.FindCommand=function(uniquecode)
	{
		for (var i=0;i<that.Commands.length;i++)
		{
			var pair=that.CommandsMap[i];
			if (pair.code===uniquecode) return pair.object;
		}
		return null;
	}
	that.BuildTree=function()
	{
		that.DbCells=[];
		that.maxdepth=0;
		for (var i=0;i<that.Commands.length;i++)
		{
			var command=that.Commands[i];
			var _endnode=0;
			if (i==0) _endnode=1;
			if (i==that.Commands.length-1) _endnode=2;
			if (that.Commands.length==1) _endnode=3;
			that.MapCommand(command,_endnode);
		}
		that.maxdepth++;
	}

	that.MapCommand=function(rootcommand,endnode)
	{
		var mapping=true;
		var filter=1;
		filter=1;
		var linksonly=that.DataLinksOnly;
		mapping=rootcommand.GetMapped();
		if (linksonly==true && rootcommand.HasChildren()>0) mapping=false;
		if (filter==0) mapping=false;
		if (mapping==true)
		{
			that.DbCells.push(rootcommand);
			rootcommand.endnode=endnode;
			var j=rootcommand.GetDepth();
			if (j>that.maxdepth) that.maxdepth=j;
		}
		if (rootcommand.IsExpand()==true  || linksonly)
		{
			for (var i=0;i<rootcommand.GetCommandCnt();i++) 
			{
				child=rootcommand.GetCommand(i);
				if (child==null) continue;
				var _endnode=0;
				if (i==0) _endnode=1;
				if (i==rootcommand.GetCommandCnt()-1) _endnode=2;
				if (rootcommand.GetCommandCnt()==1) _endnode=3;
				that.MapCommand(child,_endnode);
			}
		}
	}
	console.log("DbCommands module loaded...");
	that.GetCellCommandCnt=function(index)
	{
		if (index>=0 && index<that.DbCells.length) return that.DbCells[index].GetCommandCnt();
		return 0;
	}
	that.GetCommand=function(index)
	{
		if (index>=0 && index<that.DbCells.length) return that.DbCells[index];
		return null;
	}
	that.GetCellCount=function()
	{
		return that.DbCells.length;
	}
	that.ExpandCells=function(state,depth)
	{
		for (var i=0;i<that.DbCells.length;i++)
		{
			var command=that.DbCells[i];
			if (command.GetDepth()==depth) command.SetExpand(state);
		}
		that.BuildTree();
	}
	return that;
}

})(typeof exports === 'undefined'? this['MDbCommands']={}: exports);
