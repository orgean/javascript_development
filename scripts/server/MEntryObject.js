
(function(exports)
{

Array.prototype.swap = function (x,y)
 {
  var b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
}
Array.prototype.GetLast = function ()
{
	if (length>0) return this[length-1];
	return null;
}


exports.MHeader=function()
{
	var that=this;
	that.lines=[];
	that.identifier="";
	that.Reset=function()
	{
		that.lines.length=0;
		that.identifier="";
	}
	that.InsertLine=function(_key)
	{
		var line={'key':_key,'item':""};
		that.lines.push(line);
	}

	that.stringtoline=function(css)
	{
		if (css=="") return;
		if (that.lines.length<1) return;
		var line=that.lines[that.lines.length-1];
		var lkey=line.key;
		if (line.item!="" && lkey!="AC" && lkey!="ACCESSION") line.item+=" ";
		if (lkey==="ID" || lkey==="LOCUS")
		{
			for (var i=0;i<css.length;i++) if (css[i]==';' || css[i]==' ') css=css.substring(0,i);
		}

		if (lkey==="AC" || lkey==="ACCESSION")
		{
			if (line.item!="") return;
			for (var i=0;i<css.length;i++) if (css[i]==';' || css[i]==' ') css=css.substring(0,i);
			that.identifier=css;
		}
		line.item+=css;
	}
	that.GetAccession=function()
	{
		return that.identifier;
	}
	return that;
}
//********************************seqblk-functionaliteit*******************************

exports.MSeqBlk=function()
{
	var that=this;
	that.seq="";
	that.offset=0;
	that.seqlen=0;

	that.toJSON=function()
	{
		return JSON.stringify({'seq':"",'offset':that.offset,'seqlen':that.seqlen});
	}
	that.cleanmemory=function()
	{
		that.seq.length=0;
		that.seqlen=0;
	}

	that.addseq=function(iseq)
	{
		that.seq+=iseq;
		that.seqlen+=iseq.length;
	}

/*	that.GetBase=function(_ps)
	{
		var ps=_ps-that.offset;
		if (ps<0 || ps>=that.seqlen) return ' ';
		return that.seq[ps];
	}*/
	that.GetLength=function()
	{
		return that.seqlen;
	}

	return that;
};

exports.MSequence=function()
{
	var that=this;
	that.seqlen=0;
	that.seqblocks=[];
	that.seqblocksize=1000000;
	that.Reset=function()
	{
		that.seqlen=0;
		that.seqblocks.length=0;
	}
	that.AddSequence=function(iseq)
	{
		var css="";
		var sarr=[];
		for (var i=0,k=0;i<iseq.length;i++) if (iseq[i]>='a' && iseq[i]<='z') sarr.push(iseq[i]);
		css=sarr.join("");
		var oldlen=that.seqlen;
		that.seqlen+=css.length;
		var seqblk=null;
		if (that.seqblocks.length==0)
		{
			seqblk=new exports.MSeqBlk();
			that.seqblocks.push(seqblk);
			seqblk.offset=0;
		}
		else
		{
			seqblk=that.seqblocks[that.seqblocks.length-1];
			if (seqblk.GetLength()>=that.seqblocksize)
			{
				seqblk=new exports.MSeqBlk();
				that.seqblocks.push(seqblk);
				seqblk.offset=oldlen;
			}
		}
		if (seqblk!=null) seqblk.addseq(css);
	}
	that.AddSequenceFasta=function(iseq)
	{
		var css="";
		var sarr=[];
		for (var i=0,k=0;i<iseq.length;i++) if (iseq[i]>='a' && iseq[i]<='z') sarr.push(iseq[i]);
		css=sarr.join("");

		var oldlen=that.seqlen;
//		that.seqlen+=iseq.length;
		that.seqlen+=css.length;
		var seqblk=null;
		if (that.seqblocks.length==0)
		{
			seqblk=new exports.MSeqBlk();
			that.seqblocks.push(seqblk);
			seqblk.offset=0;
		}
		else 
		{
			seqblk=that.seqblocks[that.seqblocks.length-1];
			if (seqblk.GetLength()>=that.seqblocksize)
			{
				seqblk=new exports.MSeqBlk();
				that.seqblocks.push(seqblk);
				seqblk.offset=oldlen;
			}
		}
//		if (seqblk!=null) seqblk.addseq(iseq);
		if (seqblk!=null) seqblk.addseq(css);
	}
	that.GetLength=function()
	{
		return that.seqlen;
	}
	that.GetBase=function(pos)
	{
		var k=~~(pos/that.seqblocksize);
		if (k>=0 && k<that.seqblocks.length)
		{
			var seqblk=that.seqblocks[k];
			var ps=pos-seqblk.offset;
			if (ps>=0 && ps<seqblk.GetLength()) return seqblk.seq[ps]; 
		}
		for (var i=0;i<that.seqblocks.length;i++)
		{
			var seqblk=that.seqblocks[i];
			var ps=pos-seqblk.offset;
			if (ps>=0 && ps<seqblk.GetLength()) return seqblk.seq[ps]; 
		}
		return ' ';
	}
	that.getseq=function(start,end)
	{
		var buffer="";
		if (start<end)//direct orientation
		{		
			for (var i=start;i<=end;i++)	buffer+=that.GetBase(i);
		}
		if (start>end)//direct orientation
		{		
			for (var i=end;i<=start;i++) buffer+=that.GetBase(i);
		}
		return buffer;
	}
	return that;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

exports.MQlf=function(inkey)
{
	var that=this;
	that.key=inkey;
	that.item="";
	that.AddItem=function(css)
	{
		that.item+=css;
	}
	that.SetItem=function(css)
	{
		that.item=css;
	}
	that.GetKey=function()
	{
		return that.key;
	}
	that.GetItem=function()
	{
		return that.item;
	}
	that.FillQualifier=function(inqlf)
	{
		that.key=inqlf.key;
		that.item=inqlf.item;
	}
	return that;
};

exports.MExon=function()
{
	var that=this;
	that.start=0;
	that.stop=0;
	return that;
};


exports.MFts=function(_key)
{
	var that=this;
	that.dir=0;
	that.key=_key;
	that.start=0;
	that.stop=0;
	that.exons=[];
	that.qlfs=[];
	that.joins="";
	that.code=0;
	that.size=0;

	that.Reset=function()
	{
		that.qlfs.length=0;
		that.exons.length=0;
		that.start=0;
		that.stop=0;
		that.dir=0;
		that.joins="";
		that.lastqlf=null;
	}
	
	that.SetKey=function(css)
	{
		that.key=css;
	}
	
	that.GetKey=function()
	{
		return that.key;
	}
	that.SetDir=function(_dir)
	{
		that.dir=_dir;
	}
	that.GetDir=function()
	{
		return that.dir;
	}

	that.GetExon=function(index)
	{
		return that.exons[index];
	}
	that.AddExon=function(start,end,sqln)
	{
		var p=0;
		if (start>end) {p=start;start=end;end=p;}
		if (start<0) start=0;
		if (end>=sqln) end=sqln-1;
		var exon=new exports.MExon();
		exon.start=start;
		exon.stop=end;
		that.exons.push(exon);
		that.ReCalcStartStop();
	}
	
	that.ReCalcStartStop=function()
	{
		var last=that.exons.length-1;
		if (last<0) return;
		var exon1=that.exons[0];
		var exon2=that.exons[last];
		that.start=exon1.start;
		that.stop=exon2.stop;
		that.size=0;
		for (var i=0;i<that.exons.length;i++) that.size+=that.exons[i].stop-that.exons[i].start;
	}
	that.SetExonStart=function(nr,p,sqln)
	{
		var last=that.exons.length-1;
		if (last<0) return;
		if (p<0) p=0;
		if (p>=sqln) p=sqln-1;
		var exon=that.GetExon(nr);
		asert(exon);
		if (exon==null) return;
		exon.start=p;
		if (exon.stop<p) exon.stop=p;
		that.start=that.exons[0].start;
		that.stop=that.exons[last].stop;
		if (that.stop<that.start) that.stop+=sqln;
		that.ReCalcStartStop();
	}
	that.SetExonEnd=function(nr,p,sqln)
	{
		if (p<0) p=0;
		if (p>=sqln) p=sqln-1;
		var exon=that.GetExon(nr);
		asert(exon!=null);
		if (exon==null) return;
		exon.stop=p;
		if (exon.start>p) exon.start=p;
		that.ReCalcStartStop();
	}
	that.GetExonCnt=function()
	{
		return that.exons.length;
	}
	
	that.GetExonStart=function(index)
	{
		if (that.exons.length<1) return -1;
		return that.exons[index].start;
	}
	that.GetExonStop=function(index)
	{
		if (that.exons.length<1) return -1;
		return that.exons[index].stop;
	}
	that.SetJoins=function(css)
	{
		if (css=="") return;
		that.joins+=css;
	}
	that.checkexons=function(seqlen)
	{
	}
	that.ParseLocations=function(seqlen)
	{
		var possen=[];
		var ex_init1=[];
		var ex_init2=[];
		var ex_stop1=[];
		var ex_stop2=[];
		var p_joins=that.joins;
		if (that.joins=="") return;
		var n=that.joins.search("complement");
		if (that.key=="CDS") that.dir=1;
		if (that.key=="CDS" && n!=-1) that.dir=-1;
		var invert=0;
		var llen=that.joins.length;
		for (var i=0;i<that.joins.length;i++)
		{
			if (p_joins[i]=='/') break;
			if (p_joins[i]>='0' && p_joins[i]<='9' || p_joins[i]==',' || p_joins[i]=='.' || p_joins[i]=='^')
			{
				possen+=that.joins[i];
				if (i<that.joins.length-1 && p_joins[i]=='.' && i>0 && i<that.joins.length-1 && p_joins[i-1]!='.' && p_joins[i+1]!='.') possen[possen.length-1]='|';
			}
		}
		that.exons.length=0;
		var exoncnt=0;
		var typ1=0;var typ2=0;
		var p1=0;var p2=0;
		llen=possen.length;
		ex_init1.push("");
		ex_stop1.push("");
		ex_init2.push("");
		ex_stop2.push("");
		for (var i=0;i<llen;i++)
		{
			if (possen[i]>='0' && possen[i]<='9')
			{
				if (typ1==0 && typ2==0)
				{
					ex_init1[exoncnt]+=possen[i];
					ex_init2[exoncnt]+=possen[i];
				}
				if (typ1==0 && typ2>0) 
				{
					ex_init2[exoncnt]+=possen[i];
				}
				if (typ1==2 && typ2==0)
				{
					ex_stop1[exoncnt]+=possen[i];
					ex_stop2[exoncnt]+=possen[i];
				}
				if (typ1==2 && typ2>0)
				{
					ex_stop2[exoncnt]+=possen[i];
				}
			}
			if (possen[i]=='.')	{typ1++;p1=0;p2=0;}
			if (possen[i]=='|')	{typ2++;p1=0;p2=0;}
			if (possen[i]=='^')	{typ1=2;p1=0;p2=0;}
			if (possen[i]==',')	
			{
				exoncnt++;typ1=0;typ2=0;p1=0;p2=0;
				ex_init1.push("");
				ex_stop1.push("");
				ex_init2.push("");
				ex_stop2.push("");
			}
		}
		exoncnt++;
		for (var i=0;i<exoncnt;i++)
		{
			if (ex_stop1[i]=="") ex_stop1[i]=ex_init1[i].substring(0,10);
			if (ex_stop2[i]=="") ex_stop2[i]=ex_init2[i].substring(0,10);
		}
		for (var i=0;i<exoncnt;i++)	
		{
			var exon=new exports.MExon();
			var init1=parseInt(ex_init1[i])-1;
			var stop1=parseInt(ex_stop1[i])-1;
			var init2=parseInt(ex_init2[i])-1;
			var stop2=parseInt(ex_stop2[i])-1;
			if (init1>seqlen) init1=seqlen;
			if (stop1>seqlen) stop1=seqlen;	
			if (init1<0) init1=0;
			if (stop1<0) stop1=0;	
			if (init2>seqlen) init2=seqlen;
			if (stop2>seqlen) stop2=seqlen;	
			if (init2<0) init2=0;
			if (stop2<0) stop2=0;	
			if (init1<init2) exon.start=init1;else exon.start=init2;
			if (stop2>stop1) exon.stop=stop2;else exon.stop=stop1;	
			that.exons.push(exon);
		}
		for (var i=0;i<that.exons.length-1;i++)
		{
			var exon1=that.exons[i];
			var exon2=that.exons[i+1];
			if (exon1.start>exon2.start) invert++;
		}
/*		if (invert>0)	//join(a..b,c..d) als (a..b)>(c..d) ->omdraaien
		{
			for (var i=0;i<that.exons.length/2;i++)
			{
				var exon1=that.exons[i];
				var exon2=that.exons[that.exons.length-1-i];
				var p1=exon1.start;
				var p2=exon1.stop;
				exon1.start=exon2.start;
				exon1.stop=exon2.stop;
				exon2.start=p1;
				exon2.stop=p2;
			}
		}*/
		that.ReCalcStartStop();
	}
	
	that.GetQualifierCnt=function()
	{
		return that.qlfs.length;
	}
//wordt alleen gebruikt voor het inlezen!!! Geen controle op volgorde
	that.AddQualifier=function(inkey)//returns MQlf
	{
		var qlf=new exports.MQlf(inkey);
		that.qlfs.push(qlf);
		that.lastqlf=qlf;
		return that.qlfs[that.qlfs.length-1];
	}
	that.AddAndCheckQualifier=function(inkey)//returns MQlf
	{
		var shift=0;
		var last=that.qlfs.length-1;
		if (last!=-1)
		{
			if (that.qlfs[last].GetKey()=="/translation=") shift=1;
		}
		var qlf=new exports.MQlf(inkey);
		that.qlfs.push(qlf);
		if (shift==1) 
		{
			var newlast=that.qlfs.length-1;
			that.qlfs.swap(last,newlast);
		}
		return that.qlfs[newlast];
	}
	that.GetQualifier=function(index)
	{
		if (index>=that.qlfs.length) return null;
		return that.qlfs[index];
	}
	that.InsertQualifier=function(string,inkey)
	{
		var qlf=that.AddAndCheckQualifier(inkey);
		qlf.AddItem(string);
	}
	that.DeleteQualifier=function(nr)
	{
		that.qlfs.splice(nr,1);
	}
	that.DeleteQualifier=function(inkey)
	{
		var k=that.FindQualifierIndex(inkey);
		if (k!=-1) that.DeleteQualifier(k);
	}
	that.FindQualifierIndex=function(inkey)
	{
		for (var i=0;i<that.qlfs.length;i++)
		{
			var qlf=that.qlfs[i];
			if (qlf.GetKey()===inkey) return i;
		}
		return -1;
	}
	that.SetQualifierItem=function(string,nr)
	{
		var qlf=that.GetQualifier(nr);
		asert(qlf!=null);
		if (qlf==null) return 0;
		qlf.SetItem(string);
		if (qlf.GetKey()==="/transl_table=") return 1;
		if (qlf.GetKey()==="/codon_start=") return 1;
		return 0;
	}
	that.GetQualifierItem=function(inkey)
	{
		var k=that.FindQualifierIndex(inkey);
		var qlf=that.GetQualifier(k);
		if (qlf==null) return "";
		return qlf.GetItem(); 
	}
	that.GetQualifierItem=function(nr)
	{
		var qlf=that.GetQualifier(nr);
		asert(qlf!=null);
		if (qlf==null) return "";
		return qlf.GetItem(); 
	}
	that.GetQualifierKey=function(nr)
	{
		var qlf=that.GetQualifier(nr);
		asert(qlf!=null);
		if (qlf==null) return "";
		return qlf.GetKey(); 
	}
	that.FillQualifier=function(css)//wordt alleen bij lezen gebruikt
	{
		if (css=="") return;
		if (that.lastqlf==null) return;
		if (that.lastqlf.item!="" && that.lastqlf.key!="/translation=") css=" "+css;
		that.lastqlf.item+=css;
	}
	that.FillFts=function(infts)
	{
		that.key=infts.key;
		that.code=infts.code;
		that.dir=infts.dir;
		that.start=infts.start;
		that.stop=infts.stop;
		
		that.exons.length=0;
		for (var i=0;i<infts.exons.length;i++)
		{
			in_exon=infts.exons[i];
			var exon=new exports.MExon();
			exon.start=in_exon.start;
			exon.stop=in_exon.stop;
			that.exons.push(exon);
		}
		that.qlfs.length=0;
		for (var i=0;i<infts.qlfs.length;i++)	
		{
			var in_qlf=infts.qlfs[i];
			var qlf=new exports.MQlf("");
			qlf.FillQualifier(in_qlf);
			that.qlfs.push(qlf);
		}
	}
	that.MoveTranslation=function()
	{
		var q=that.FindQualifierIndex("/translation=");
		if (q<0) return;
		if (q==that.qlfs.length-1) return;
		var _qlf=that.qlfs[q];
		that.qlfs.splice(q,1); 
		that.qlfs.push(_qlf);
	}
	
	that.GetStart=function()
	{
		return that.start;
	}
	that.GetStop=function() 
	{
		return that.stop;
	}
	return that;
};

exports.MFeatureTable=function(entry_accession)
{
	var that=this;
	that.entryAccession=entry_accession;
	that.ftrs=[];
	var cnt=0;
	that.Reset=function()
	{
		that.entryAccession="";
		that.ftrs.length=0;
		cnt=0;
	}
	that.push=function(fts)
	{
		that.ftrs.push(fts);
	}
	that.AddFeature=function(_ftskey)
	{
		var fts=new exports.MFts(_ftskey);
		that.ftrs.push(fts);
		return fts;
	}
	that.FeatureFillQualifier=function(css)
	{
		if (that.ftrs.length<1) return;
		var fts=that.ftrs[that.ftrs.length-1];
		fts.FillQualifier(css);
	}

	that.FeatureAddQualifier=function(inkey)
	{
		if (that.ftrs.length<1) return;
		var fts=that.ftrs[that.ftrs.length-1];
		fts.AddAndCheckQualifier(inkey);
	}

	that.FeatureSetJoins=function(css)
	{
		if (that.ftrs.length<1) return;
		var fts=that.ftrs[that.ftrs.length-1];
		fts.SetJoins(css);
	}
	that.ParseLocations=function(seqlength)
	{
		for (var i=0;i<that.ftrs.length;i++)
		{
			that.ftrs[i].ParseLocations(seqlength);
		}
	}
	return that;
};


exports.MEntry=function()
{
	var that={};
	that.identifier="";
	that.format=-1;
	that.changed=false;
	that.blocklen=0;
	
	that.sequence=new exports.MSequence();
	that.featuretable=new exports.MFeatureTable();
	that.header=new exports.MHeader();
	that.filelocation="";
	that.direct_sequence_access=false;
	that.files=null;
	that.callback=null;
	that.offset=0;
	that.returns=1;
	that.extension=false;


	that.GetAccession=function()
	{
		that.header.identifier;
	}
	that.ReadEntryFileBatch=function(files,index,callback)
	{
		that.callback=callback;
		that.fileindex=index;
		that.files=files;
		that.ReadEntryFile(null,null);
	}
	that.ReadEntryFile=function(connection,json)
	{
		that.direct_sequence_access=false;
		that.sequence.Reset();
		that.featuretable.Reset();
		that.header.Reset();
		if (that.files!=null) that.filelocation=that.files[that.fileindex];
		if (json!=null)	that.filelocation=json.filelocation;
		console.log(that.filelocation);
		that.direct_sequence_access=false;
		that.direct_sequence_access=that.CheckFastaOffset(1);
		
		var fs=require('fs');
		var buffer=new Buffer(100);
		var process_header=false;
		var process_features=true;
		var process_sequence=true;
		if (that.direct_sequence_access==true) process_sequence=false;
		fs.open(that.filelocation,'r',function(err,fd)
		{
			if (fd)
			{
				fs.read(fd,buffer,0,100,0,function(err,bytesRead,buffer)
				{
					var txt=buffer.toString('utf8', 0,100);
					if (txt[0]=='>') that.OpenFastaStream(connection,json,process_sequence);
					else that.OpenEmblGnbkStream(connection,json,process_header,process_features,process_sequence);
				});
				fs.close(fd);
			}
			else
			{
				if (connection!=null)
				{
					var result=JSON.stringify( {'type':'TransferEntry', 'processing':err,'sequence_box_code':json.sequence_box_code});
					connection.send(result);
				}
			}
		});

	}
	that.CheckFastaOffset=function()
	{
		var fs=require('fs');
		var ext=that.filelocation.substr((~-that.filelocation.lastIndexOf(".") >>> 0) + 2);
		var path="";
		if (ext!="fasta") path=that.filelocation+".fasta";else path=that.filelocation;
		if (ext!="fasta") that.extension=true;
		var txt="";
		var fd=fs.openSync(path,'r');
		if (fd)
		{
			var size=500;
			var buffer=new Buffer(size+1);
			fs.readSync(fd,buffer,0,size,0);
			for (var i=0;i<buffer.length;i++) if (buffer[i]==13) {that.offset=i;break;}
			if (buffer[that.offset+1]==10) {that.offset++;that.returns=2;}
			that.offset++;
			txt=buffer.toString('utf8');
			txt=txt.slice(0,size);
			var line=txt.split('\n');
			var tabs=line[0].split('\t');
			that.identifier=tabs[0];
			that.header.identifier=that.identifier;
			that.featuretable.entryAccession=that.identifier;
			that.sequence.seqlen=parseInt(tabs[1]);
			console.log(that.identifier+"----"+that.sequence.seqlen);
			fs.closeSync(fd);
			return true;
		}
		return false;
	}
	

/*	that.GetSequenceFromFile=function(start,stop)
	{
		var fs=require('fs');
		var path=that.filelocation+".pur";
		var txt="";
		var fd=fs.openSync(path,'r')
		if (fd)
		{
			if (start>stop) console.log("start position is greater than stop position");
			else
			{
				var size=stop-start+1;
				var p=start;
				var buffer=new Buffer(size+100);
				fs.readSync(fd,buffer,0,size,p);
				txt=buffer.toString('utf8');
				txt=txt.slice(0,size);
			}
			fs.closeSync(fd);
		}
		return txt.toLowerCase();
	}*/

	that.GetSequenceFromFile=function(_start,_stop)
	{
		var fs=require('fs');
		var path=that.filelocation;
		if (that.extension==true) path=path+".fasta";
		var txt="";
		var res="";
		var fd=fs.openSync(path,'r');
		start=Math.floor(_start/100)*(100+that.returns)+_start%100+that.offset;
		stop=Math.floor(_stop/100)*(100+that.returns)+_stop%100+that.offset;
		if (fd)
		{
			if (start>stop) console.log("start position is greater than stop position");
			else
			{
				var size=stop-start+1;
				var p=start;
				var buffer=new Buffer(size+100);
				fs.readSync(fd,buffer,0,size,p);
				txt=buffer.toString('utf8');
				txt=txt.slice(0,size);
	//			txt.replace(/(\r\n|\n|\r)/gm,"");
				res=txt.replace(/[^\x20-\x7E]/gmi, "");
			}
			fs.closeSync(fd);
		}
		return res.toLowerCase();
	}

	that.GetLength=function()
	{
		if (that.sequence!=null) return that.sequence.GetLength();
		return 0;
	}
	that.readlength=function(css)
	{
		var _readlen=0;
		var strarray=[];
		if (that.format==1)
		{
			var spatie=0;
			for (var i=css.length-1;i>=0;i--) 
			{
				if (css[i]>='0' && css[i]<='9') strarray.push(css[i]);
				if (css[i]==' ') spatie++;
				if (spatie==2) break;
			}
			strarray.reverse();
			var str=strarray.join("");
			_readlen=parseInt(str);
		}
		if (that.format==2)
		{
			var spatie=0;
			for (var i=0;i<css.length;i++) 
			{
				if (i>0 && css[i-1]!=' ' && css[i]==' ') spatie++;
				if (spatie==2 && css[i]>='0' && css[i]<='9') strarray.push(css[i]);
				if (spatie==3) break;
			}
			var str=strarray.join("");
			_readlen=parseInt(str);
		}
		return _readlen;
	}
	that.readlengthfasta=function(css)
	{
		var _readlen=0;
		var strarray=[];
		for (var i=css.length-1;i>=0;i--) 
		{
			if (css[i]>='0' && css[i]<='9') strarray.push(css[i]);
			if (css[i]=='\t') break;
		}
		strarray.reverse();
		var str=strarray.join("");
		_readlen=parseInt(str);
		return _readlen;
	}
	
	that.OpenEmblGnbkStream=function(connection,json,process_header,process_features,process_sequence)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		instream=fs.createReadStream(that.filelocation);//fs.createReadStream('sample.txt', {start: 90, end: 99});
		instream.on('error', function(err) {console.log(err);});
		instream.on('open', function(err)  {that.ReadEmblGnbkStream(instream,connection,json,process_header,process_features,process_sequence);});
	}

	that.ReadEmblGnbkStream=function(instream,connection,json,process_header,process_features,process_sequence)
	{
		// processing==1 HEADER 
		// processing==2 FEATURES 
		// processing==3 SEQUENCE 

		var path = pathHome;
		MEmblGnbkObject=require(path+'scripts/server/MEmblGnbkObject.js');
		emblgnbk=MEmblGnbkObject.EmblGnbkObject();
		that.format=-1;
		if (process_features && that.featuretable==null) throw "no feature container defined";
		if (process_sequence && that.sequence==null) throw "no sequence container defined";
		if (process_header && that.header==null) throw "no header container defined";

		var processing=1;
		var read_lines=1;
		var feature_table=0;
		var read_sequence=0;
		var inconsistent_file=0;
		var readlen=0;
		var qualifier=-1;
		var field=-1;
		var check_process=0;
		var lkey="";
		var fkey="";
		var qkey="";
		var oldqkey="";
		var oldlkey="";
		var previous_lkey="";
		var mess="";
		var css="";
		var curfts=null;


		var es=require("event-stream");
		var linecnt=0;
		var css="";
		var len=0;
		var skip=false;
		
		instream.pipe(es.split()).pipe(
			es.mapSync(function(css)
			{
				(function()
				{
					if (linecnt==0)
					{
						if (css.substring(0,5)==="ID   ") {that.format=1;readlen=that.readlength(css);}
						if (css.substring(0,5)==="LOCUS") {that.format=2;readlen=that.readlength(css);}
						if (that.format<0) 
						{
							console.log(that.filelocation+": no standard EMBL/GENBANK format detected");
							skip=true;
						}
						if (process_header==false && process_features==false && process_sequence==false) skip=true;
					}
					if (!skip)
					{
						if (that.format==1)
						{
							if (css[0]=='F' && css[1]=='H')	feature_table=1;
							if (feature_table==1){if (css[0]=='F' && css[1]=='T') processing=2;else processing=0;}
							if (css[0]=='S' && css[1]=='Q') {processing=3;feature_table=0;}
							if (css[0]=='/' && css[1]=='/') {processing=0;}
							var input={'state':processing,'line':css,'linekey':lkey,'ftskey':fkey,'qlfkey':qkey}
							emblgnbk.emblprocessline(input);
							processing=input.state;css=input.line;lkey=input.linekey;fkey=input.ftskey;qkey=input.qlfkey;
							if (lkey==="")	lkey=previous_lkey;else previous_lkey=lkey;
						}
						if (that.format==2)
						{
							if (css.substring(0,8)==="FEATURES") {feature_table=1;}
							if (css.substring(0,6)==="ORIGIN") {processing=3;feature_table=0;}
							if (feature_table==1){if (css[0]==' ' && css[1]==' ') processing=2;else processing=0;}
							if (css[0]=='/' && css[1]=='/') {processing=0;}
							var input={'state':processing,'line':css,'linekey':lkey,'ftskey':fkey,'qlfkey':qkey}
							emblgnbk.gnbkprocessline(input);
							processing=input.state;css=input.line;lkey=input.linekey;fkey=input.ftskey;qkey=input.qlfkey;
							if (lkey==="")	lkey=previous_lkey;else previous_lkey=lkey;
						}
						if (processing==2 && process_features==false) {processing=0;}
						if (processing==3 && process_sequence==false) {processing=0;}
						if (processing==1 && process_header==false)
						{
							if (lkey==="AC" || lkey==="ACCESSION") 
							{
								if (oldlkey!=lkey) {that.header.InsertLine(lkey);oldlkey=lkey;}
								that.header.stringtoline(css);
							}
						}
						if (processing==1 && process_header==true)
						{
							if (read_lines==1)
							{
//								if (lkey!="" && oldlkey!=lkey) {that.header.InsertLine(lkey);oldlkey=lkey;}
//								that.header.stringtoline(css);
							}
						}
						if (processing==2 && process_features==true)
						{
							if (fkey!="")
							{
								curfts=that.featuretable.AddFeature(fkey,that.header.identifier);
								oldqkey="";
							}
							if (oldqkey==="" && qkey==="" && curfts!=null) curfts.SetJoins(css);
							if (qkey!="" && qkey!=oldqkey) 
							{
								oldqkey=qkey;
								if (curfts!=null) curfts.AddQualifier(qkey);
							}
							if (oldqkey!="" && curfts!=null) curfts.FillQualifier(css);
						}
						if (read_sequence==1)	
						{
							that.sequence.AddSequence(css);
						}
						if (processing==3 && process_sequence==true) read_sequence=1;
					}
					linecnt++;
				})();
			}).on('end', function()
				{
					if (process_sequence && that.sequence.GetLength()!=readlen)
					{
						inconsistent_file=1;
						console.log("Inconsistent sequence length detected: parsed length=" +that.sequence.GetLength()+" <----> "+readlen+ "(given length)");
					}
					if (that.sequence.GetLength()==0) that.sequence.seqlen=readlen;
					that.featuretable.ParseLocations(readlen);
					if (that.header!=null) 
					{
						that.identifier=that.header.identifier;
						that.featuretable.entryAccession=that.header.identifier;
					}
					that.changed=false;
					if (connection!=null)
					{
//						var result=JSON.stringify( {'type':'TransferEntry', 'processing':'finished', 'featuretable': that.featuretable, 'header': that.header, 'sequence': that.sequence, 'sequence_box_code':json.sequence_box_code,'classobjectcode':json.classobjectcode,'color':""});
						var result=JSON.stringify( {'type':'TransferEntry', 'processing':'finished', 'featuretable': null, 'header': that.header, 'sequence': that.sequence, 'sequence_box_code':json.sequence_box_code,'classobjectcode':json.classobjectcode,'color':""});
						connection.send(result);
					}
					if (that.files!=null)
					{
						console.log("entry '"+that.files[that.fileindex]+" "+that.GetLength()+"bp");
						if (that.callback!=null) that.callback(that.fileindex,that.sequence.seqlen);
						that.fileindex++;
						if (that.fileindex<that.files.length) that.ReadEntryFile(null,null);
					}
				})
		);
	}

	that.OpenFastaStream=function(connection,json,process_sequence)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		instream=fs.createReadStream(that.filelocation);
		instream.on('error', function(err) {console.log(err);});
		instream.on('open', function(err)  {that.ReadFastaStream(instream,connection,json,process_sequence);});
	}
	that.ReadFastaStream=function(instream,connection,json,process_sequence)
	{
		var es=require("event-stream");
		var linecnt=0;
		var css="";
		var len=0;
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					if (linecnt==0)
					{
						that.identifier=line;
						that.header.identifier=that.identifier;
						that.featuretable.entryAccession=that.identifier;
//						if (process_sequence) that.sequence.seqlen=that.readlengthfasta(line);
					}
					else
					{
						len+=css.length;
						if (process_sequence) 
						{
							css=line.toLowerCase();
							that.sequence.AddSequenceFasta(css);
						}
					}
					linecnt++;
					if (process_sequence==false) 
					{
						instream.emit('end');
					}
				})();
			}).on('end', function()
				{
					if (that.sequence.GetLength()==0) that.sequence.seqlen=len;
					that.changed=false;
					if (connection!=null)
					{
						var result=JSON.stringify( {'type':'TransferEntry', 'processing':'finished', 'featuretable': that.featuretable, 'header': that.header, 'sequence': that.sequence,'sequence_box_code':json.sequence_box_code,'classobjectcode':json.classobjectcode, 'color':""});
						connection.send(result);
					}
					if (that.files!=null)
					{
						console.log("entry '"+that.files[that.fileindex]+" "+that.GetLength()+"bp");
						if (that.callback!=null) that.callback(that.fileindex,that.sequence.seqlen);
						that.fileindex++;
						if (that.fileindex<that.files.length) that.ReadEntryFile(null,null);
					}
				})
		);
	}

	that.getSequence=function(start,end)
	{
		if (that.direct_sequence_access) return that.GetSequenceFromFile(start,end);
		return that.sequence.getseq(start,end);
	}
	that.GetBase=function(pos)
	{
		return that.sequence.GetBase(pos);
	}

	return that;
}
})(typeof exports === 'undefined'? this['MEntryObject']={}: exports);
