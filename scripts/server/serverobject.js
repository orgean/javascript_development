﻿//if (typeof define !== 'function') {
//    var define = require('amdefine')(module);
//}
//define(function (require) {

var local=true;

pathHome="/home/ubuntu/javascript_development/";
serverUrl='http://ec2-52-208-60-246.eu-west-1.compute.amazonaws.com:1337';
webSocketUrl='ws://ec2-52-208-60-246.eu-west-1.compute.amazonaws.com:1337';
dataPath="/home/ubuntu/data_javascript/"

if (local)
{
	pathHome="d:/javascript_development/";
	serverUrl='http://localhost:1337';
	webSocketUrl='ws://127.0.0.1:1337';
	dataPath='d:/orgean_database/';
}


pathSripts=pathHome+"scripts/";
pathServer=pathHome+"scripts/server/";


function startserver()
{

	var that={};

	var clc=require('cli-color');
	that.webSocketServer=require('websocket').server;
	that.webSocketsServerPort=1337;
	that.entries=[];
	
	require(pathServer + "ImageDataObject.js");
	MEmblGnbkObject=require(pathServer + "MEmblGnbkObject.js");
	that.emblgnbk=new MEmblGnbkObject.EmblGnbkObject();
	that.emblgnbk.Init();

	ServerProjects=require(pathServer + "serverprojects.js");
	that.serverprojecttraffic=new ServerProjects.Traffic();
	
	function OnRequest(request,response)
	{
		var path=require("path");
		var url=require("url");
		var uri=url.parse(request.url).pathname,filename=path.join(process.cwd(),uri);
		var fs=require("fs");
		fs.readFile(filename, function(err, file) 
		{
			if(err) 
			{
				response.writeHead(500, {"Content-Type": "text/plain"});
				response.write(err + "\n");
				response.end();
				return;
			}
			response.writeHead(200);
			response.write(file);
			response.end();
		});
	}

	var http=require('http');
	var server=http.createServer(OnRequest);
	
	that.GetConnection=function()
	{
		return that.connection;
	}
	
	server.listen(that.webSocketsServerPort,function()
	{
		console.log((new Date()) + " Server is listening on port " + that.webSocketsServerPort);
	});

	var wsServer=new that.webSocketServer(
	{
		httpServer: server // WebSockets zijn een onderdeel van HTTP server.
	});

	that.originIsAllowed=function(origin) 
	{
	  // check functie komt hier
		return true;
	}

	wsServer.on('request', function(request) 
	{
		if (!that.originIsAllowed(request.origin)) //later in te vullen: checken of request van gevaildeerde website komt
		{
			request.reject();
			console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			return;
		}
		console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
		that.connection=request.accept(null,request.origin); 
		console.log((new Date()) + ' Connection accepted.');
		that.connection.send(JSON.stringify( {type: 'connection', data: "Connected............."} ));
		that.connection.on('message',function(message)
		{
			if (message.type==='utf8')
			{ 
				var json=JSON.parse(message.utf8Data);
				if (json!=null)
				{
					if (json.type==='RequestEmblGnbkData') that.RequestEmblGnbkData();
					
					if (json.type==='OpenDataBase') that.OpenDataBaseFile(json);
					if (json.type==='OpenDataBaseProjects') that.OpenDataBaseProjects(json);
					if (json.type==='ReScanDataBaseProjects') that.OpenDataBaseProjects(json);
				
					if (json.type==='ServerCreateDataBaseProject') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerCreateDataBaseProject(json);}
					if (json.type==='ServerUpdateDataBaseProject') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerUpdateDataBaseProject(json);}
					if (json.type==='ServerReadProject') {that.ReadProject(json);}

					if (json.type==='SequenceDataLoad') that.LoadSequenceData(json);
					if (json.type==='GetSelectedSequence') that.GetSelectedSequence(json);
					if (json.type==='SequenceFrameLoad') that.SequenceFrameMappingRequest(json);
					if (json.type==='OpenSequenceFile') that.OpenSequenceFile(json);
					if (json.type==='FeatureSearchRequest') that.FeatureSearchRequest(json);
					if (json.type==='SequenceSearchRequest') that.SequenceSearchRequest(json);
					
					if (json.type==='ReadDotPlotInput') that.ReadDotPlotInput(json);
					if (json.type==='ReadDotPlotSettings') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ReadDotPlotSettings(json);}
					if (json.type==='RequestDPCellCanvasData') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ReadDPCellCanvasData(json);}
					if (json.type==='RequestDPCellStretchData') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ReadDPCellStretchData(json);}
					if (json.type==='RequestDPCellAlign') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.OpenCellAlignFile(json);}
					if (json.type==='RequestDPCellVariations') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.OpenCellVariationsFile(json);}
					
					if (json.type==='RequestSeedCompSettingsFile') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.CreateSeedCompSettingsFile(json);}
					if (json.type==='ServerRequestSeedCompCalculation') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerStartSeedCompCalculation(json);}
					if (json.type==='ServerRequestStretchAlignment') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerStartStretchAlignment(json);}
					if (json.type==='ServerRequestMultiAlignment') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerOpenMultiAlignFile(json,false);}
					if (json.type==='ServerRequestMultiAlignScore') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerCheckMultiAlignFile(json,false);}
					
					
					if (json.type==='RequestFindVariations') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.StartSearchVariations(json);}
					if (json.type==='ServerRequestStretchFindBestHits') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerStartStretchFindBestHits(json);}
					if (json.type==='ServerRequestStretchFindNoHits') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.ServerStartStretchFindNoHits(json);}
					
					if (json.type==='RequestDPDeleteCurrentCell') {that.serverprojecttraffic.SetConnection(that.connection);that.serverprojecttraffic.DPDeleteCurrentCell(json);}
				}
//				console.log(clc.red.bgWhite.underline('Received Message: ' + message.utf8Data));
			}
		});
		that.connection.on('finish',function()
		{
		});
		that.connection.on('close',function()
		{
			console.log((new Date())+" Peer "+ that.connection.remoteAddress + " disconnected.");
		});
	});
	that.OpenDataBaseFile=function(json)
	{
		var fs=require('fs');
		var path=json.dbpath+"database.txt";
		console.log(clc.greenBright("Opening database file "+path+"...."));
		var instream=fs.createReadStream(path);
		that.readLinesDataBase(instream,json);
	}
	that.readLinesDataBase=function(input,json) 
	{
		var chunck='';
		var linecounter={linecnt:0}
		var _dbpath=json.dbpath;
		input.on('data', function(data)
		{
			chunck+=data;
			chunck=that.ParseBlockDataBase(chunck,linecounter,json);
		});
		input.on('end', function() 
		{
			if (chunck.length>0) 
			{
				that.ParseBlockDataBase(chunck,linecounter);
			}
			that.OpenDataBaseFieldCodes(json);
			that.OpenDataBaseLayers(json);
			that.OpenDataBasePathCodes(json);
			var result=JSON.stringify( {type:'DataBaseLoaded',dbpath:_dbpath,status:'finished',code:json.code});
			that.connection.send(result);
		});
		
	}
	that.ParseBlockDataBase=function(block,linecounter,json)
	{
		var lines=[];
		var curpos=0;
		var index=block.indexOf('\n');
		while (index > -1) 
		{
			var line=block.substring(curpos,index-1);
//			block=block.substring(index+1);
			curpos=index+1;
			var obj={linenr: linecounter.linecnt, linedata: line};
			lines.push(obj);
			index=block.indexOf('\n',curpos);
			linecounter.linecnt++;
		}
		var result=JSON.stringify( {type:'LoadDataBase', dbpath:json.dbpath, code:json.code, status:"linefeed", data:lines});
		that.connection.send(result);
		var rest=block.substring(curpos);
		block.length=0;
		return rest;
	}
	that.OpenDataBaseFieldCodes=function(json)
	{
		var fs=require('fs');
		console.log(clc.greenBright("Opening database field codes...."));
		var path=json.dbpath+"fieldcodes.txt";
		var instream=fs.createReadStream(path);
		var readline=require('readline');
		instream.on('error', function(err) {console.log(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		var fieldcodes=[];
		rl.on('line', function(line)
		{
			var tabs=line.split('\t');
			if (tabs.length==4)
			{
				var foo={'code':'','label':'','dataId':0,'color':0};
				foo.code=tabs[0];
				foo.label=tabs[1];
				foo.dataId=tabs[2];
				foo.color=tabs[3];
				fieldcodes.push(foo);
			}
		});

		rl.on('close', function() 
		{
			var result=JSON.stringify( {type:'LoadDataBaseFieldCodes', code:json.code, data: fieldcodes});
			that.connection.send(result);
		});
	}
	that.OpenDataBaseLayers=function(json)
	{
		var fs=require('fs');
		var path=json.dbpath+"datalayers.txt";
		console.log(clc.greenBright("Opening database layers...."+path));
		var instream=fs.createReadStream(path);
		var readline=require('readline');
		instream.on('error', function(err) {console.log(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		var layers=[];
		rl.on('line', function(line)
		{
			var tabs=line.split('\t');
			var foo={'name':'','fieldcodes':[]};
			foo.name=tabs[0];
			for (var i=1;i<tabs.length;i++)
			{
				foo.fieldcodes.push(tabs[i]);
			}
			layers.push(foo);
		});

		rl.on('close', function() 
		{
			var result=JSON.stringify( {type:'LoadDataBaseLayers', code:json.code, data: layers});
			that.connection.send(result);
		});
	}
	that.OpenDataBasePathCodes=function(json)
	{
		var fs=require('fs');
		
		console.log(clc.greenBright("Opening path codes...."));
		var path=json.dbpath+"pathcodes.txt";
		var instream=fs.createReadStream(path);
		var readline=require('readline');
		instream.on('error', function(err) {console.log(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		var pathcodes=[];
		rl.on('line', function(line)
		{
			var tabs=line.split('\t');
			var foo={'code':'','path':''};
			foo.code=tabs[0];
			foo.path=tabs[1];
			pathcodes.push(foo);
//			console.log(foo);
		});

		rl.on('close', function() 
		{
			var result=JSON.stringify( {type:'LoadDataBasePathCodes', code:json.code, data: pathcodes});
			that.connection.send(result);
		});
	}
	that.walk=function(dir,suffix,callback)
	{
		var fs=require('fs');
		var results=[];
		var file="";
		fs.readdir(dir,function(err,list)
		{
			if (err) return done(err);
			var pending=list.length;
			if (!pending) return callback(null,results);
			list.forEach(function(file)
			{
				var sub=file;
				file=dir+'/'+file;
				fs.stat(file, function(err, stat)
				{
					if (stat && stat.isDirectory())
					{
						if (sub!="sequence_data")
						{
							that.walk(file,suffix,function(err,res)
							{
								results=results.concat(res);
								if (!--pending) callback(null, results);
							});
						}
						else if (!--pending) callback(null, results);
					}
					else
					{
						if (file.indexOf(suffix,file.length-suffix.length)!==-1) 
						{
							results.push(file);
//							console.log("project file   --------->"+file);
						}
						if (!--pending) callback(null,results);
					}
				});
			});
		});
	}	
	that.OpenDataBaseProjects=function(json)
	{
		var err="";
		var files=[];
		var path=json.dbpath+"projects";
		console.log(clc.greenBright("Opening projects at "+path));
//		console.log(json);
		that.walk(path,"setup.txt",function(err,files)
		{
			for (var i=0;i<files.length;i++) 
			{
				var _json={};
				_json.projectfilelocation=files[i];
				_json.pjobjectcode=json.code;
				console.log(_json);
				that.ReadProject(_json);
			}
		});
	}
	that.ReadProject=function(json)
	{
		var _path=json.projectfilelocation;
		console.log(clc.greenBright("Opening project at path "+_path));
		var fs=require('fs');
		var instream=fs.createReadStream(_path);
		var readline=require('readline');
		instream.on('error', function(err) {console.log(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		var projectitems=[];
		var _code="?";
		var _displayname="";
		rl.on('line', function(line)
		{
			var tabs=line.split('\t');
			if (tabs[0]==='UniqueCode') json.projectcode=tabs[1];
			if (tabs[0]==='Data')
			{
				var item={'dbcode':tabs[1],'classobjectcode':tabs[2]};
				projectitems.push(item);
			}
			if (tabs[0]==='DisplayName') _displayname=tabs[1];
		});
		rl.on('close', function() 
		{
			json.type='ClientLoadDataBaseProject';
			json.displayname=_displayname;
			json.data=projectitems;
			that.connection.send(JSON.stringify(json));
		});
	}
	that.ReadDotPlotInput=function(json)
	{
		console.log('++++++++++++++++++'+json.projectpath);
		var path=json.projectpath+"sequence_data/dotplot/dpinput.txt";
		console.log(clc.greenBright("Reading dot plot input at "+path));
		var fs=require('fs');
		var instream=fs.createReadStream(path);
		var readline=require('readline');
		instream.on('error', function(err) {console.log(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		var dataArray=[];
		var _reflexcalc=1;
		rl.on('line', function(line)
		{
			var tabs=line.split('\t');
			if (tabs[0]==='Reflexive calculation')
			{
				_reflexcalc=parseInt(tabs[1]);
				_reflexcalc=0;
			}
			if (tabs.length>4 && tabs[0]==='DataCluster')
			{
/*				tabs[1];//cluster
				tabs[2];//classobject code
				tabs[3];//fastafile
				tabs[4];//sequence length
				tabs[5];//if (strcmp(curpos,"self=true")==0) self=true;else self=false;*/
				var pair={'classObjectCode':tabs[2],'sequenceLength':parseInt(tabs[4])};
				dataArray.push(pair);
			}
		});

		rl.on('close', function() 
		{
			json.type='LoadDotPlotInput';
			json.data=dataArray;
			json.reflexcalc=_reflexcalc;
			that.connection.send(JSON.stringify(json));
		});
	}
	that.RequestEmblGnbkData=function()
	{
		that.connection.send(JSON.stringify( {type:'FetchEmblGnbkData', object: that.emblgnbk}));
		console.log(clc.greenBright("Sending EMBL GNBK data..."));
	}
	that.OpenSequenceFile=function(json)
	{
		for (var i=0;i<that.entries.length;i++)
		{
			var entry=that.entries[i];
			if (entry.path==json.filelocation) 
			{
				var result=JSON.stringify( {'type':'TransferEntry', 'processing':'finished', 'featuretable': entry.featuretable, 'header': entry.header, 'sequence': entry.sequence, 'sequence_box_code':json.sequence_box_code,'classobjectcode':json.classobjectcode,'color':""});
				that.connection.send(result);
				console.log("entry '"+entry.path+"' is already opened");
				return;
			}
		}
		var MEntryObject=require(pathServer+"/MEntryObject.js");
		var entry=new MEntryObject.MEntry();
		entry.ReadEntryFile(that.connection,json);
		console.log(json.filelocation);
		that.entries.push(entry);
	}
	that.SetProgress=function(label,_sequence_viewer_accession)
	{
//		var result=JSON.stringify( {type:'TransferEntry', processing:label, sequence_viewer_accession: _sequence_viewer_accession});
//		that.connection.sendUTF(result);
	}
	that.DecodePath=function(_pathcode)
	{
		for (var i=0;i<that.pathcodes.length;i++) if (that.pathcodes[i].code==_pathcode) return that.pathcodes[i].path;
		return "";
	}
	that.LoadSequenceData=function(json)
	{
		var locators=[];
		for (var i=0;i<json.data.length;i++)
		{
			var locator=json.data[i];
			for (var j=0;j<that.entries.length;j++)
			{
				if (that.entries[j].identifier==locator.accession) 
				{
					locator.sequence=that.entries[j].getSequence(locator.start,locator.end);
					if (locator.inverted==true) locator.sequence=locator.sequence.split('').reverse().join('');
//					console.log(locator);
				}
			}
			locators.push(locator);
		}
		var result=JSON.stringify( {type:'SequenceDataLoad', 'seqplot_code': json.seqplot_code, 'data': locators});
		that.connection.send(result);
	}
	that.GetSelectedSequence=function(json)
	{
		var locator=json.locator;
		for (var j=0;j<that.entries.length;j++)
		{
			if (that.entries[j].identifier==locator.accession) 
			{
				locator.sequence=that.entries[j].getSequence(locator.start,locator.end);
				if (locator.inverted==true) locator.sequence=locator.sequence.split('').reverse().join('');
			}
		}
		var result=JSON.stringify( {type:'GetSelectedSequence', 'seqplot_code': json.seqplot_code, 'data': locator});
		that.connection.send(result);
	}
	that.FeatureSearchRequest=function(json)
	{ 
		var MEntryObject=require(pathServer+"/MEntryObject.js");
		var ftssearch=[];
		for (var i=0;i<json.sequence_accessions.length;i++)
		{
			var _accession=json.sequence_accessions[i];
			for (var j=0;j<that.entries.length;j++)
			{
				if (that.entries[j].identifier===_accession)
				{
					var entryftssearch={ftrs:[],accession:_accession}
					for (var k=0;k<that.entries[j].featuretable.ftrs.length;k++)
					{
						if (json.features.indexOf(that.entries[j].featuretable.ftrs[k].key)!=-1) entryftssearch.ftrs.push(that.entries[j].featuretable.ftrs[k]);
					}
					ftssearch.push(entryftssearch);
				}
			}
		}
		var result=JSON.stringify( {type:'FeatureSearchRequest', 'sequence_box_code': json.sequence_box_code, 'feature_search_accession': json.feature_search_accession, 'featuretable': ftssearch});
		that.connection.send(result);
	}
	that.SequenceSearchRequest=function(json)
	{ 
		var seqsearch=[];
		for (var i=0;i<json.sequence_accessions.length;i++)
		{
			var _accession=json.sequence_accessions[i];
			for (var j=0;j<that.entries.length;j++)
			{
				if (that.entries[j].identifier===_accession)
				{
					var entryseqsearch={seqfnd:[],accession:_accession}
					var nn=parseInt(that.entries[j].sequence.seqlen/3000);
					for (var n=0;n<nn;n++)
//					var nn=that.entries[j].sequence.seqlen;
//					for (var n=0;n<nn;n=n+1000)
					{
						var k=~~(Math.random()*that.entries[j].sequence.seqlen);
//						var k=n;
						var obj={'start':k,'stop':k+6}
						entryseqsearch.seqfnd.push(obj);
					}
					entryseqsearch.seqfnd.sort(function(a,b) { return a.start>b.start} );
					seqsearch.push(entryseqsearch);
				}
			}
		}
		var result=JSON.stringify( {type:'SequenceSearchRequest', 'sequence_box_code': json.sequence_box_code, 'sequence_search_accession': json.sequence_search_accession, 'foundhits': seqsearch});
		that.connection.send(result);
	}
	that.SequenceFrameMappingRequest=function(json)
	{ 
/*		var PDFDocument=require('pdfkit');                      
		var fs=require('fs');
		var doc=new PDFDocument();

		doc.pipe( fs.createWriteStream('d:\\temp\\out.pdf'));

		// rest of the code goes here...
		var toffset=10;
		var basesperline=90;
		var loffset=20;
		var linespace=35;
		var css1="";
		var css2="";
		var css3="";
		
		for (var i=0;i<basesperline;i++) css1=css1+"a";
		doc.font('Courier',9);
		var ll=doc.widthOfString(css1)/basesperline;
		css1="";
		function dumpString()
		{
			var n=1;
			doc.font('Courier',6);
			while (n<=basesperline)
			{
				if ((i-basesperline+n)%10==0)
				{
					doc.rect(loffset+(n-1)*ll+2,l-5,1,3).fill('red');
					doc.fill('black');
					doc.text(i-basesperline+n,loffset+n*ll,l-5);
				}
				n++;
			}
			doc.font('Courier',9);
			doc.text(css1,loffset,l);
			doc.text(css3,loffset,l+7);
			doc.text(css2,loffset,l+16);
			css1="";css2="";css3="";
			k=0;
			l+=linespace;
		}
		var i=0;
		var k=0;
		var l=toffset;
		while (i<2000)
		{
			if (k==basesperline) dumpString();
			if (l>toffset+680)
			{
				doc.addPage();
				l=toffset;
			}
			css1=css1+"a";
			css2=css2+"t";
			css3=css3+"|";
			
			i++;k++;
		}
		dumpString();
		doc.end();
		return;*/
		
		var MFrameObject=require(pathServer+'/MFrameObject.js');
		for (var i=0;i<json.data.length;i++)
		{
			var frameobject=new MFrameObject.Frames();
			var seqfnd=[];
			var locator=json.data[i];
//			console.log(locator);
			for (var l=0;l<locator.seqblocks.length;l++)
			{
				var seqblock=locator.seqblocks[l];
				for (var j=0;j<that.entries.length;j++)
				{
					if (that.entries[j].identifier==seqblock.accession) 
					{
						var sequence=that.entries[j].getSequence(seqblock.startbase,seqblock.endbase);
//						console.log("sequence length: "+sequence.length);
						var offset=seqblock.startbase;
						if (seqblock.inverted) offset=seqblock.endbase;
						frameobject.create_exons_(sequence,offset,that.entries[j].GetLength(),seqblock.accession);
						var frames=frameobject.frames;
//						console.log("frame count: "+frames.length);
						for (var k=0;k<frames.length;k++) seqfnd.push(frames[k]);
					}
				}
			}
			var result=JSON.stringify( {type:'SequenceFrameLoad', 'sequence_box_code': json.sequence_box_code, 'feature_search_accession': locator.accession,'channel': locator.accession, 'foundhits': seqfnd});
			that.connection.send(result);
		}
	}
	return that;
}
startserver();
//});
