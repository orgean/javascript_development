
(function(exports)
{


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
exports.Ttransltable=function()
{
	that=this;
	that.AA="FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG";
	that.ST="-----------------------------------M----------------------------";
	that.definition="Standard Code";
	that.number=1;

	that.adddefinition=function(css)
	{
		that.definition=css;
	}
	return that;
}

exports.Tqualifier=function(inkey)
{
	that=this;
	that.key=inkey;
	that.definition="";
	that.format="";
	that.example="";
	that.comment="";

	that.adddefinition=function(css)
	{
		that.definition+=css;
	}

	that.addformat=function(css)
	{
		that.format+=css;
	}

	that.addexample=function(css)
	{
		that.example+=css;
	}
	that.addcomment=function(css)
	{
		that.comment+=css;
	}

};




exports.Tfeature=function(inkey)
{
	var that={};
	that.key=inkey;
	that.qualifiers=[];
	that.definition="";
	that.comment="";
	that.orgscope="";
	that.molscope="";

	that.adddefinition=function(css)
	{
		that.definition+=css;
	}

	that.addcomment=function(css)
	{
		that.comment+=css;
	}

	that.addqualifier=function(_key)
	{
		that.qualifiers.push(_key);
	}

	that.addmolscope=function(scope)
	{
		that.molscope=scope;
	}

	that.addorgscope=function(scope)
	{
		that.orgscope=scope;
	}

	that.GetKey=function()
	{
		return that.key;
	}
	return that;
};

exports.EmblGnbkObject=function()
{
//	var EmblGnbk={};
//	that=EmblGnbk;
	var that={};
	var EmblGnbk=that;
	that.features=[];
	that.qualifiers=[];
	that.transltables=[];
	that.embllines=[];
	that.gnbklines=[];
	that.conversions=[];
	
	that.AddLogMessage=function(mess)
	{
		var clc=require('cli-color');
		console.log(clc.greenBright(mess+"...."));
//		console.log(mess);
	}
	that.Init=function()
	{
		var installpath=pathHome;
		if (that.read_lines(installpath) && that.read_features(installpath) && that.read_translation_tables(installpath)) that.AddLogMessage("Embl/Gnbk data object created");
		else that.AddLogMessage("Embl/Gnbk data reading incomplete!");
	}
	that.read_lines=function(installpath)
	{
		var fs=require('fs');
		var filename=installpath+"system/formats/header_format.txt";
		var log="Reading EMBL/Gnbk line definitions from: "+filename;
		that.AddLogMessage(log);
		that.embllines=[];
		that.gnbklines=[];
		var addemblline=0;
		var addgnbkline=0;
		var addconversion=0;

		var instream=fs.createReadStream(filename);
		var readline=require('readline');
		instream.on('error', function(err) {that.AddLogMessage(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		rl.on('line', function(line)
		{
			if (line==="[EMBL lines]") {addemblline=1;addgnbkline=0;addconversion=0;}
			if (line==="[GENBANK lines]") {addemblline=0;addgnbkline=1;addconversion=0;}
			if (line==="[conversion EMBL - GENBANK]") {addemblline=0;addgnbkline=0;addconversion=1;}
			var tabs=line.split('\t');
			if (line[0]!=0 && line[0]!='[')
			{
				if (addemblline)
				{
					var hdline={'key':'','description':'','tabulation':0};
					hdline.key=tabs[0];
					if (tabs.length>1) hdline.description=tabs[1];
					that.embllines.push(hdline);
				}
				if (addgnbkline)
				{
					var hdline={'key':'','description':'','tabulation':0};
					if (tabs[0][0]==' ' && tabs[0][1]==' ')
					{
						hdline.key=tabs[0].substring(2,key);
						hdline.tabulation=2;
					}
					else 
					{
						hdline.key=tabs[0];
						hdline.tabulation=0;
					}
					if (tabs.length>1) hdline.description=tabs[1];
					that.gnbklines.push(hdline);
				}
				if (addconversion)
				{
					var conv={'embl':'','gnbk':''};
					conv.embl=tabs[0];
					conv.gnbk=tabs[1];
					that.conversions.push(conv);
				}
			}
		});

		rl.on('close', function() 
		{
		});
		return true;
	}
	that.read_features=function(installpath)
	{
		var fs=require('fs');
		var filename=installpath+"system/formats/feature_table_format.txt";
		var log="Reading EMBL/Gnbk feature definitions from: "+filename;
		that.AddLogMessage(log);
		
		that.features=[];
		that.qualifiers=[];
		var oldkey="";
		var key="";
		var offset=22;
		var read_feature_table=0;
		var read_qualifier_table=0;
		
		var instream=fs.createReadStream(filename);
		var readline=require('readline');
		instream.on('error', function(err) {that.AddLogMessage(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		rl.on('line', function(line)
		{
			key=line.substring(0,offset);
			if (key=="                      ") key=oldkey;
			if (key!="                      ") oldkey=key;
			if (key=="[FEATURES]") {read_feature_table=1;read_qualifier_table=0;}
			if (key=="[QUALIFIERS]") {read_feature_table=0;read_qualifier_table=1;}
			key=key.toLowerCase();
			if (read_feature_table==1)
			{
				if (key=="feature key           ")
				{
					var css=line.substring(offset);
					var _fts=new exports.Tfeature(css);
					that.features.push(_fts);
				}
				if (key=="definition            ")
				{
					var css=line.substring(offset);
					if (that.features.length>0)
					{
						var fts=that.features[that.features.length-1];
						fts.adddefinition(css);
					}
				}
				if (key=="comment               ")
				{
					var css=line.substring(offset);
					if (that.features.length>0)
					{
						var fts=that.features[that.features.length-1];
						fts.addcomment(css);
					}
				}
				if (key=="optional qualifiers   " || key=="mandatory qualifiers  ")
				{
					var stop=0;
					var css="";
					for (var i=offset;i<line.length;i++) 
					{
						if (line[i]=='=') stop=1;
						if (line[i]==' ') continue;
						css+=line[i];
						if (stop) break;
					}
					if (that.features.length>0)
					{
						var fts=that.features[that.features.length-1];
						fts.addqualifier(css);
					}
				}
				if (key=="molecule scope        ") 
				{
					var css=line.substring(offset);
					if (that.features.length>0)
					{
						var fts=that.features[that.features.length-1];
						fts.addmolscope(css);
					}
				}
				if (key=="organism scope        ") 
				{
					var css=line.substring(offset);
					if (that.features.length>0)
					{
						var fts=that.features[that.features.length-1];
						fts.addorgscope(css);
					}
				}
			}
			if (read_qualifier_table==1)
			{
				if (key=="qualifier             ")
				{
					var stop=0;
					var css="";
					for (var i=offset;i<line.length;i++) 
					{
						if (line[i]=='=') stop=1;
						if (line[i]==' ') continue;
						css+=line[i];
						if (stop) break;
					}
					var _qlf=new exports.Tqualifier(css);
					that.qualifiers.push(_qlf);
				}
				if (key=="definition            ")
				{
					var css=line.substring(offset);
					if (that.qualifiers.length>0)
					{
						var qlf=that.qualifiers[that.qualifiers.length-1];
						qlf.adddefinition(css);
					}
				}
				if (key=="value format          ")
				{
					var css=line.substring(offset);
					if (that.qualifiers.length>0)
					{
						var qlf=that.qualifiers[that.qualifiers.length-1];
						qlf.addformat(css);
					}
				}
				if (key=="example               ")
				{
					var css=line.substring(offset);
					if (that.qualifiers.length>0)
					{
						var qlf=that.qualifiers[that.qualifiers.length-1];
						qlf.addexample(css);
					}
				}
				if (key=="comment               ")
				{
					var css=line.substring(offset);
					if (that.qualifiers.length>0)
					{
						var qlf=that.qualifiers[that.qualifiers.length-1];
						qlf.addcomment(css);
					}
				}
			}
		});

		rl.on('close', function() 
		{
		});
		return true;
	}
	that.read_translation_tables=function(installpath)
	{
		var fs=require('fs');
		var filename=installpath+"system/formats/translation_tables.txt";
		var log="Reading translation tables at: "+filename;
		that.AddLogMessage(log);

		var table=null;
		that.transltables=[];
		var offset=11;
		
		var instream=fs.createReadStream(filename);
		var readline=require('readline');
		instream.on('error', function(err) {that.AddLogMessage(err);});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		rl.on('line', function(line)
		{
			if (line.substring(0,12)=="Genetic Code")
			{
				var _table=new exports.Ttransltable();
				var css="";
				for (var i=0;i<line.length;i++) if (line[i]>='0' && line[i]<='9') {css+=line[i];}
				table=_table;
				table.number=parseInt(css);
				that.transltables.push(table);
			}
			if (line[0]!=' ' && line[1]!=' ' && table!=null) table.adddefinition(line);
			if (line.substring(0,11)=="    AAs  = " && table!=null)
			{
				var css=line.substring(offset);
				table.AA=css;
			}
			if (line.substring(0,11)=="  Starts = " && table!=null)
			{
				var css=line.substring(offset);
				table.ST=css;
			}
		});

		rl.on('close', function() 
		{
		});
		return true;
	}
	that.emblprocessline=function(input)
	{
		input.filtered_str="";
		input.linekey="";
		input.ftskey="";
		input.qlfkey="";
		if (input.line=="") return;

		var feature="";
		var qualifier="";
		if (input.state==1 && input.line.length>=5)
		{
			var line="";
			for (var i=0;i<2;i++) if (input.line[i]!=' ') line+=input.line[i];
			if (line.length>0)
			{
				var fnd=0;
				for (var i=0;i<that.embllines.length;i++)
				{
					var hdline=that.embllines[i];
					if (line===hdline.key) {input.linekey=hdline.key;fnd=1;break;}
				}
				if (fnd==0) //if an undefined line occurs, check it
				{
					input.linekey=line;
					var hdline={'key':'','description':'','tabulation':0};
					hdline.key=input.linekey;
					hdline.description="to be updated";
					that.embllines.push(hdline);
				}
				input.filtered_str=input.line.substring(5); //wegfilteren eerste 5 karakter
			}
		}
		if (input.state==2) that.process_commonblock(input);
		if (input.state!=3) input.line=input.filtered_str;
	}
	that.gnbkprocessline=function(input)
	{
		input.filtered_str="";
		input.linekey="";
		input.ftskey="";
		input.qlfkey="";
		if (input.line=="") return;
		if (input.state==1 && input.line.length>12)
		{
			var line="";
			for (var i=0;i<12;i++) if (input.line[i]!=' ')	line+=input.line[i];
			if (line.length>0)
			{
				var fnd=0;
				for (var i=0;i<that.gnbklines.length;i++)
				{
					var hdline=that.gnbklines[i];
					if (line===hdline.key) {input.linekey=hdline.key;fnd=1;break;}
				}
				if (fnd==0) 
				{
					input.linekey=line;//if an undefined line occurs, check it
					var hdline={'key':'','description':'','tabulation':0};
					hdline.key=input.linekey;
					hdline.description="to be updated";
					that.gnbklines.push(hdline);
				}
			}
			input.filtered_str=input.line.substring(12); //wegfilteren eerste 12 karakter
		}
		if (input.state==2) that.process_commonblock(input);
		if (input.state!=3) input.line=input.filtered_str;
	}
	that.process_commonblock=function(input)//embl en gnbk feature table en sequence block
	{
		var feature="";
		input.linekey="FT";
		for (var i=5;i<21;i++) if (input.line[i]!=' ')	feature+=input.line[i];
		if (feature.length>0)
		{
			var fnd=0;
			for (var i=0;i<that.features.length;i++)
			{
				var fts=that.features[i];
				if (fts.key===feature) {input.ftskey=fts.key;fnd=1;break;}
			}
			if (fnd==0) 
			{
				input.ftskey=feature;// if an undefined feature occurs, check it 
				var _fts=new exports.Tfeature(feature);
				that.features.push(_fts);
				_fts.adddefinition("to be updated");
			}
		}
		if (input.line.length>21 && input.line[21]=='/')
		{
			var qualifier="";
			for (var i=21;i<input.line.length;i++)
			{
				qualifier+=input.line[i];
				if (input.line[i]==='=' || input.line[i]===' ') break;
			}
			var fnd=0;
			for (var i=0;i<that.qualifiers.length;i++)
			{
				var qlf=that.qualifiers[i];
				if (qlf.key===qualifier) {input.qlfkey=qlf.key;fnd=1;break;}	
			}
			if (fnd==0)	//if an undefined qualifier occurs, check it.
			{
				var newqlf="";
				for (var i=21;i<input.line.length;i++)
				{
					newqlf+=input.line[i];
					if (input.line[i]==='=' || input.line[i]===' ') break;
				}
				if (newqlf.length<=20) 
				{
					input.qlfkey=newqlf;//only if qualifier is shorter than 20 characters, accept.
					var _qlf=new exports.Tqualifier(newqlf);
					that.qualifiers.push(_qlf);
					_qlf.adddefinition("to be updated");
				}
			}
		}
		var len=input.line.length;
		var j=21+input.qlfkey.length;
		if (j<len && input.line[j]==='"') j++;
		if (input.line[len-1]=='"') len--; 
		for (var i=j;i<len;i++) input.filtered_str+=input.line[i]; //wegfilteren eerste 21 karakter
	}
	return that;
}

})(typeof exports === 'undefined'? this['MEmblGnbkObject']={}: exports);
