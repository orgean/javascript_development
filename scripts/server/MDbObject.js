(function(exports)
{

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

exports.MClassObjectField=function()
{
	var that=this;
	that.definition="";
	that.fieldcode="";
	
	that.SetDefiner=function(def)
	{
		that.definition=def;
	}

	that.GetDefiner=function()
	{
		return that.definition;
	}

	that.SetFieldCode=function(_code)
	{
		that.fieldcode=_code;
	}

	that.GetFieldCode=function()
	{
		return that.fieldcode;
	}
	return that;
}

MDataLayer=function()
{
	var that=this;
	that.name="";
	that.fieldcodes=[];
	that.HasFieldCode=function(fieldcode)
	{
		for (var i=0;i<that.fieldcodes.length;i++) if (that.fieldcodes[i]==fieldcode) return true;
		return false;
	}
	return that;
}



exports.MClassObject=function()
{
	var that=this;
	that.ClassObjects=[];
	that.Fields=[];
	that.parent=null;
	that.definition="";
	that.expand=true;
	that.mapped=true;
	that.organism=false;
	that.uniquecode="";
	that.fieldcode="";
	that.fielddata="";
	that.pathcode="";
	that.filename="";
	that.endnode=0;
	that.overwriteparent=false;
	that.toJSON=function()
	{
		return JSON.stringify({filename:that.filename,definition:that.definition,uniquecode:that.uniquecode,fielddata:that.fielddata,pathcode:that.pathcode});
	}

	that.AddClassObject=function()
	{
		var classobj=new exports.MClassObject();
		classobj.parent=that;
		that.ClassObjects.push(classobj);
		return classobj;
	}
	that.InsertClassObject=function(_datafile)
	{
		var definer=_datafile;
		var definer_l="";
		var definer_r="";
//		var index=definer.indexOf('|');
		var index=-1;
		for (var i=0;i<definer.length;i++) if (definer[i]=='|') {index=i;break;}
		if (index>-1)
		{
			definer_l=definer.substring(0,index);
			definer_r=definer.substring(index+1,definer.length);
		}
		else definer_l=definer;
		var sub=null;
		var item=null;
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			item=that.ClassObjects[i];
			if (item.GetDefiner()==definer_l) sub=item;
		}
		if (sub==null) 
		{
			sub=that.AddClassObject();
			sub.SetDefiner(definer_l);
//			console.log("    subclassobject added to database: '"+definer_l+"'");
//			console.log("              subclassobject added to database: '"+definer_r+"'");
		}
		if (definer_r.length>0) return sub.InsertClassObject(definer_r);
		return sub;
	}
	that.HasChildren=function()
	{
		return that.ClassObjects.length;
	}
	that.IsChildFrom=function(classobj)
	{
		var p_parent=that.parent;
		if (that.overwriteparent) return false;
		while (p_parent!=null)
		{
			if (p_parent==classobj) return true;
			p_parent=p_parent.parent;
		}
		return false;
	}

	that.GetOrganismParent=function()
	{
		if (that.IsOrganism()==true) return this;
		var p_parent=that.parent;
		while (p_parent!=null)
		{
			if (p_parent.IsOrganism()==true) return p_parent;
			p_parent=p_parent.parent;
		}
		return null;
	}
	that.GetClassObjectCnt=function()
	{
		return that.ClassObjects.length;
	}

	that.GetClassObject=function(nr)
	{
		if (nr<0 || nr>=that.ClassObjects.length) return null;
		return that.ClassObjects[nr];
	}

	that.SetDefiner=function(def)
	{ 
		if (def==="") return;
		that.definition=def;
	}

	that.GetDefiner=function()
	{
		return that.definition;
	}
	that.GetFullDefiner=function()
	{
		var _fulldef="";
		var concatenate=false;
		var p_parent=that;
		while (p_parent!=null)
		{
			if (_fulldef!="") _fulldef=" | "+_fulldef;
			if (p_parent.IsOrganism()) concatenate=true;
			if (concatenate) _fulldef=p_parent.GetDefiner()+_fulldef;
			p_parent=p_parent.parent;
		}
		return _fulldef;
	}
	that.GetDataFile=function()
	{
		return that.GetPathCode()+"/"+that.GetFileName();
	}
	that.GetDepth=function()
	{
		var p_parent=that.parent;
		var k=0;
		if (that.overwriteparent) return k;
		while (p_parent!=null)
		{
			p_parent=p_parent.parent;
			k++;
			if (p_parent!=null && p_parent.overwriteparent) return k;
		}
		return k;
	}	
	that.GetText=function(obj)
	{
		if (obj.fieldcode!='index') that.AccumulateFieldData(obj);
		if (obj.fieldcode==='index') obj.fielddata=that.GetDefiner();
		return obj;
	}
	that.GetText2=function(fielddata,fieldcode)
	{
		if (fieldcode!='index') that.AccumulateFieldData2(fielddata,fieldcode);
		if (fieldcode==='index') fielddata=that.GetDefiner();
		return fielddata;
	}
	that.GetMapped=function() {return that.mapped;};
	that.SetMapped=function(how) {that.mapped=how;};
	that.IsExpand=function() {return that.expand;};
	that.SetExpand=function(how) {that.expand=how;};
	that.IsOrganism=function() {return that.organism;};
	that.SetOrganism=function(how) {that.organism=how;};
	that.SetUniqueCode=function(code) {that.uniquecode=code;};
	that.GetUniqueCode=function() {return that.uniquecode;};
	
	that.SetFieldCode=function(_code) {that.fieldcode=_code;};
	that.GetFieldCode=function() {return that.fieldcode;}

	that.SetFieldData=function(_data) {that.fielddata=_data;}
	that.GetFieldData=function() {return that.fielddata;}
	that.SetPathCode=function(_path) {that.pathcode=_path;}
	that.GetPathCode=function() {return that.pathcode;}
	that.SetFileName=function(_name) {that.filename=_name;}
	that.GetFileName=function() {return that.filename;}

	that.AddField=function()
	{
		var field=new exports.MClassObjectField();
		that.Fields.push(field);
		return field;
	}
	that.GetField=function(fieldcode)
	{
		for (var i=0;i<that.Fields.length;i++)
		{
			var field=that.Fields[i];
			if (field.GetFieldCode()===fieldcode) return field.GetDefiner();
		}
		return "";
	}
	that.AccumulateFieldData=function(obj)
	{
		if (that.GetFieldCode()===obj.fieldcode) 
		{
			if (obj.fielddata!='') obj.fielddata=obj.fielddata+"|"+that.GetFieldData();else obj.fielddata=that.GetFieldData();
		}
		if (that.IsOrganism()==true)
		{
			for (var i=0;i<that.ClassObjects.length;i++)
			{
				var _classobject=that.ClassObjects[i];
				_classobject.AccumulateFieldData(obj);
			}
		}
	}
	that.AccumulateFieldData2=function(fielddata,fieldcode)
	{
		if (that.GetFieldCode()===fieldcode) 
		{
			if (fielddata!='') fielddata=fielddata+"|"+that.GetFieldData();else fielddata=that.GetFieldData();
		}
		if (that.IsOrganism()==true)
		{
			for (var i=0;i<that.ClassObjects.length;i++)
			{
				var _classobject=that.ClassObjects[i];
				_classobject.AccumulateFieldData(fielddata,fieldcode);
			}
		}
	}
	that.CountFieldData=function(cnt,fieldcode)
	{
		if (that.GetFieldCode()===fieldcode) cnt++;
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var _classobject=that.ClassObjects[i];
			cnt=_classobject.CountFieldData(cnt,fieldcode);
		}
		return cnt;
	}
	that.GetClassObjects=function(scopebox,field)
	{
		if (that.GetFieldCode()==field) 
		{
			scopebox.push(this);
		}
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var _classobject=that.ClassObjects[i];
			_classobject.GetClassObjects(scopebox,field);
		}
	}
	that.FillTrack=function(track,field)
	{
		if (that.GetFieldCode()==field) 
		{
			track.AddClassObject(this);
		}
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var _classobject=that.ClassObjects[i];
			_classobject.FillTrack(track,field);
		}
	}
	that.ExpandChildren=function(state)
	{
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var _classobject=that.ClassObjects[i];
			_classobject.SetExpand(state);
		}
	}
	that.FindClassObject=function(uniquecode)
	{
		if (that.uniquecode===uniquecode) return that;
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var _classobject=that.ClassObjects[i].FindClassObject(uniquecode);
			if (_classobject!=null) return _classobject;
		}
		return null;
	}
	return that;
};

exports.MDbScope=function()
{
	var that=this;
	that.Items=[];
	that.Organisms=[];
	that.Clear=function()
	{
		that.Items.length=0;
		that.Organisms.length=0;
	}
	that.AddClassObject=function(_classobject)
	{
		var parent_organism=_classobject.GetOrganismParent();
		var organism_definer="";
		var col="rgb(0,0,0)";
		if (parent_organism!=null)
		{
			var fnd=-1;
			for (var i=0;i<that.Organisms.length;i++) if (that.Organisms[i].organism===parent_organism.GetDefiner()) fnd=i;
			if (fnd==-1) 
			{
				col=('#'+Math.floor(Math.random()*16777215).toString(16));
				var obj={organism:parent_organism.GetDefiner(),color:col};
				that.Organisms.push(obj);
			}
			else col=that.Organisms[fnd].color;
			organism_definer=parent_organism.GetDefiner();
		}
		var obj={classobject:_classobject,organism:organism_definer,color:col};
		that.Items.push(obj);
	}
	that.GetItemCnt=function()
	{
		return that.Items.length;
	}
	that.GetItem=function(index)
	{
		if (index>=0 && index<that.Items.length) return that.Items[index];
		return null;
	}
	return that;
}

exports.DbObject=function(_code)
{
	var that={};
	that.code=_code;
	that.clientConnection=null;
	that.ClassObjects=[];
	that.DbCells=[];
	that.ScopeBox=[];
	that.FieldCodes=[];
	that.maxdepth=0;
	that.DataLinksOnly=false;
	that.OrganismsOnly=false;
	that.DataLayers=[];
	that.PathCodes=[];
	that.Projects=[];
	that.CurrentLayer=null;
	that.databaselist=null;
//********* database field codes	
	that.FieldCodes.push({'code':'index','label':'index','size':400,'dataId':0,'color':0});	
	that.FindFieldCode=function(code)
	{
		for (var i=0;i<that.FieldCodes.length;i++) if (that.FieldCodes[i].code==code) return i;
		return -1;
	}
	that.FindFieldCodeDataId=function(code)
	{
		for (var i=0;i<that.FieldCodes.length;i++) 
		{
			var fld=that.FieldCodes[i];
			if (fld.code==code) return fld.dataId;
		}
		return -1;
	}
	that.GetFieldCodes=function()
	{
		return that.FieldCodes;
	}
	that.GetFieldCode=function(index)
	{
		return that.FieldCodes[index];
	}
//**********************************
	that.SetCode=function(_code){that.code=_code;}
	that.GetCode=function() {return that.code;};
	
	that.ReadDataBase=function(DataBasePath)
	{
		var file=DataBasePath;
		file=file+"/database.txt";

		var root=that.InsertClassObject("root");
		
		var fs=require('fs');
		var readline=require('readline');
		var stream=require('stream');
		
		console.log("Opening database file " +file);
		var instream=fs.createReadStream(file);
		instream.on('error', function(err) {
			console.log(err);
		});
	
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);

		rl.on('line', function(line)
		{
			that.ParseDataBaseLine(line,DataBasePath);
		});

		rl.on('close', function() 
		{
			that.BuildTree();
		  // do something on finish here
		});
	}
	that.ParseDataBaseLine=function(line,DataBasePath)
	{
		var tabs=line.split('\t');
		if (tabs.length>1)
		{
			var item=DataBasePath;
			item=item+"|"+tabs[0];
			var definition=item;
			var classobj1=that.InsertClassObject(item);
			if (classobj1!=null)
			{
				if (tabs.length==3) //vrije velden voor organisme
				{
					var field=classobj1.AddField();
					field.SetFieldCode(tabs[1]);
					field.SetDefiner(tabs[2]);
					if (that.FindFieldCode(tabs[1])==-1) 
					{
						var foo={'code':tabs[1],'label':'','size':100,'dataId':0,'color':0}
						that.FieldCodes.push(foo);
					}
				}
				if (tabs.length>3)
				{
					classobj1.SetOrganism(true);
					classobj1.SetExpand(false);
//					definition=definition+"|"+tabs[1];
//					var classobj2=that.InsertClassObject(definition);
					var classobj2=classobj1.InsertClassObject(tabs[1]);
					if (classobj2!=null)
					{
						classobj2.SetUniqueCode(tabs[1]);
						classobj2.SetExpand(true);
						if (tabs.length==6) // datatypes
						{
							classobj2.SetFieldCode(tabs[2]);
							classobj2.SetFieldData(tabs[3]);
							classobj2.SetPathCode(tabs[4]);
							classobj2.SetFileName(tabs[5]);
							if (that.FindFieldCode(tabs[2])==-1) 
							{
								var foo={'code':tabs[2],'label':'','size':200,'dataId':0,'color':0};
								that.FieldCodes.push(foo);
							}
						}
						if (tabs.length==4) //vrije velden voor datatypes
						{
							var field=classobj2.AddField();
							field.SetFieldCode(tabs[2]);
							field.SetDefiner(tabs[3]);
							if (that.FindFieldCode(tabs[2])==-1) 
							{
								var foo={'code':tabs[2],'label':'','size':100,'dataId':0,'color':0};
								that.FieldCodes.push(foo);
							}
						}
					}
				}
			}
		}
	  // process line here
	}
	that.InsertClassObject=function(_datapath)
	{
		var definer=_datapath;
		var definer_l="";
		var definer_r="";
//		var index=definer.indexOf('|');
		var index=-1;
		for (var i=0;i<definer.length;i++) if (definer[i]=='|') {index=i;break;}
		if (index>-1)
		{
			definer_l=definer.substring(0,index);
			definer_r=definer.substring(index+1,definer.length);
		}
		else definer_l=definer;
		var sub=null;
		var item=null;
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			item=that.ClassObjects[i];
			if (item.GetDefiner()==definer_l) sub=item;
		}
		if (sub==null) 
		{
			sub=that.AddClassObject();
			sub.SetDefiner(definer_l);
		}
		if (definer_r.length>0) return sub.InsertClassObject(definer_r);
		return sub;
	}
	that.AddClassObject=function()
	{
		classobj=new exports.MClassObject();
		that.ClassObjects.push(classobj);
		return that.ClassObjects[that.ClassObjects.length-1];
	}
	that.FindClassObject=function(uniquecode)
	{
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var classobj=that.ClassObjects[i].FindClassObject(uniquecode);
			if (classobj!=null) return classobj;
		}
		return null;
	}
	that.BuildTree=function()
	{
		that.DbCells=[];
		that.maxdepth=0;
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var classobj=that.ClassObjects[i];
			var _endnode=0;
			if (i==0) _endnode=1;
			if (i==that.ClassObjects.length-1) _endnode=2;
			if (that.ClassObjects.length==1) _endnode=3;
			that.MapClassObject(that.DbCells,classobj,_endnode);
		}
		that.maxdepth++;
	}

	that.MapClassObject=function(cells,rootclassobj,endnode)
	{
		var mapping=true;
		var filter=1;
	//	if (CurrentLayer!=NULL)	for (k=0,filter=0;k<CurrentLayer->GetFieldCodeCount();k++) filter=root->CountClassObjects(filter,CurrentLayer->GetFieldCode(k));
	//	if (show_always_all_organisms) filter=1;
		filter=1;
		var linksonly=that.DataLinksOnly;
		if (that.OrganismsOnly==true) linksonly=true;
		if (that.OrganismsOnly==true && rootclassobj.IsOrganism()==true) linksonly=false;
		mapping=rootclassobj.GetMapped();
		if (linksonly==true && rootclassobj.HasChildren()>0) mapping=false;
		if (filter==0) mapping=false;
		if (that.DataLinksOnly || that.OrganismsOnly) rootclassobj.overwriteparent=true;
		if (mapping==true)
		{
			cells.push(rootclassobj);
			rootclassobj.endnode=endnode;
			var j=rootclassobj.GetDepth();
			if (j>that.maxdepth) that.maxdepth=j;
			if (that.OrganismsOnly && rootclassobj.parent!=null && rootclassobj.parent.IsOrganism()) rootclassobj.overwriteparent=false;
		}
		if (rootclassobj.IsExpand()==true  || linksonly)
		{
			for (var i=0;i<rootclassobj.GetClassObjectCnt();i++) 
			{
				var child=rootclassobj.GetClassObject(i);
				if (child==null) continue;
				var _endnode=0;
				if (i==0) _endnode=1;
				if (i==rootclassobj.GetClassObjectCnt()-1) _endnode=2;
				if (rootclassobj.GetClassObjectCnt()==1) _endnode=3;
				that.MapClassObject(cells,child,_endnode);
			}
		}
	}
	console.log("DbObject module "+that.GetCode()+ " loaded...");
	that.GetCellClassObjectCnt=function(index)
	{
		if (index>=0 && index<that.DbCells.length) return that.DbCells[index].GetClassObjectCnt();
		return 0;
	}
	that.GetClassObject=function(index)
	{
		if (index>=0 && index<that.DbCells.length) return that.DbCells[index];
		return null;
	}
	that.GetCellCount=function()
	{
		return that.DbCells.length;
	}
	that.AddClassObjectScopeBox=function(classobj)
	{
		for (var i=0;i<that.ScopeBox.length;i++) if (that.ScopeBox[i]==classobj) return;
		that.ScopeBox.push(classobj);
	}
	that.AddDataScopeBox=function(index,field)
	{
		var classobj=that.GetClassObject(index);
		if (classobj!=null) classobj.GetClassObjects(that.ScopeBox,field);
	}
	that.AddDataTrack=function(index,field,track)
	{
		var classobj=that.GetClassObject(index);
		if (classobj!=null) classobj.FillTrack(track,field);
	}
	that.GetScopeBoxCount=function()
	{
		return that.ScopeBox.length;
	}
	that.ClearScopeBox=function()
	{
		that.ScopeBox.length=0;
	}
	that.GetScopeBoxClassObject=function(index)
	{
		return that.ScopeBox[index];
	}
//DataLayers
	that.AddDbDataLayer=function(_layer)
	{
		//var foo={'name':'','fieldcodes':[]};
		for (var i=0;i<that.DataLayers.length;i++)
		{
			var layer=that.DataLayers[i];
			if (layer.name==_layer.name) return layer;
		}
		var layer=new MDataLayer();
		layer.name=_layer.name;
		layer.fieldcodes=_layer.fieldcodes;
		that.DataLayers.push(layer);
		return layer;
	}
	that.GetLayers=function()
	{
		return that.DataLayers;
	}
	that.ImportDataLayers=function()
	{
/*		ASSERT(_mainwin);
		if (_mainwin==NULL) return;
		int i;
		for (i=0;i<that.DataLayers.GetItemCnt();i++)
		{
			MDataLayer *layer=that.DataLayers.GetItem(i);
			_mainwin->layer_select->AddDropDown(layer->GetLayerName(),CreateLinkCommand(_mainwin,&TMainWindow::LayerChanged));
		}
		SetDataLayer("All");*/
	}

	that.SetDataLayer=function(layername)
	{
		that.CurrentLayer=null;
		for (var i=0;i<that.DataLayers.length;i++)
		{
			var layer=that.DataLayers[i];
			if (layer.name==layername) that.CurrentLayer=layer;
		}
		that.BuildTree();
	}
	that.GetCurrentLayer=function()
	{
		return that.CurrentLayer;
	}
//Pathcodes
	that.AddDbPathCode=function(_pathcode)
	{
		//var foo={'code':'','path':[]};
		for (var i=0;i<that.PathCodes.length;i++) if (that.PathCodes[i].code==_pathcode.code) return;
		that.PathCodes.push(_pathcode);
	}
	that.DecodePath=function(_pathcode)
	{
		for (var i=0;i<that.PathCodes.length;i++) if (that.PathCodes[i].code==_pathcode) return that.PathCodes[i].path;
		return "";
	}
	that.ExpandCells=function(state,depth)
	{
		for (var i=0;i<that.DbCells.length;i++)
		{
			var classobj=that.DbCells[i];
			if (classobj.GetDepth()==depth) classobj.SetExpand(state);
		}
		that.BuildTree();
	}
	that.SaveDataBase=function()
	{
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var classobj=that.ClassObjects[i];
		}
	}
	that.CreateEntryFile=function()
	{
		var _DataLinksOnly=that.DataLinksOnly;
		that.DataLinksOnly=true;
		var cells=[];
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var classobj=that.ClassObjects[i];
			var _endnode=0;
			if (i==0) _endnode=1;
			if (i==that.ClassObjects.length-1) _endnode=2;
			if (that.ClassObjects.length==1) _endnode=3;
			that.MapClassObject(cells,classobj,_endnode);
		}
		for (var i=0;i<that.DbCells.length;i++)
		{
			var classobj=cells[i];
		}
		that.DataLinksOnly=_DataLinksOnly;
		
		for (var i=0;i<that.ClassObjects.length;i++)
		{
			var classobj=that.ClassObjects[i];
			classobj.GetClassObjects(scopebox,field.fieldcode);
		}
	}
	return that;
}

})(typeof exports === 'undefined'? this['MDbObject']={}: exports);
