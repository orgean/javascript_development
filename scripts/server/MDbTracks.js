(function(exports)
{

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

exports.MTrack=function()
{
	var that=this;
	that.definition="";
	that.isView=false;
	that.tabnumber=-1;
	that.classobjects=[];
	that.action="";
	that.code="";

	that.SetCode=function(_code){that.code=_code;}
	that.GetCode=function() {return that.code;};
	that.SetDefiner=function(def)
	{ 
		if (def==="") return;
		that.definition=def;
	}

	that.GetDefiner=function()
	{
		return that.definition;
	}
	that.GetView=function() {return that.isView;}
	that.SetView=function(how) {that.isView=how;};
	that.AddClassObject=function(classobj)
	{
		that.classobjects.push(classobj);
	}
	that.GetClassObjectCount=function()
	{
		return that.classobjects.length;
	}
	that.GetClassObject=function(index)
	{
		if (index>=0 && index<that.classobjects.length) return that.classobjects[index];
		return null;
	}
	that.ViewFunction=null;
	that.execute=function()
	{
		if (that.ViewFunction!=null) that.ViewFunction(this);
	}
	return that;
};

exports.DbTracks=function()
{
	var that={};
	that.clientConnection=null;
	that.Tracks=[];
	that.NewUUID=function()
	{
		var d = new Date().getTime();
		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) 
		{
			var r = (d + Math.random()*16)%16 | 0;
			d = Math.floor(d/16);
			return (c=='x' ? r : (r&0x7|0x8)).toString(16);
		});
		return uuid;
	}
//**********************************
	that.AddTrack=function()
	{
		track=new exports.MTrack();
		track.uniquecode=that.NewUUID();
		that.Tracks.push(track);
		return that.Tracks[that.Tracks.length-1];
	}
	console.log("DbTracks module loaded...");
	that.GetTrackCount=function()
	{
		return that.Tracks.length;
	}
	that.GetTrack=function(index)
	{
		if (index>=0 && index<=that.GetTrackCount()) return that.Tracks[index];
		return null;
	}
	that.FindTrack=function(code)
	{
		for (var i=0;i<that.GetTrackCount();i++) if (that.Tracks[i].code==code) return that.Tracks[i];
		return null;
	}
	that.GetLastTrack=function()
	{
		if (that.Tracks.length>0) return that.Tracks[that.Tracks.length-1];
		return null;
	}
	return that;
}

})(typeof exports === 'undefined'? this['MDbTracks']={}: exports);
