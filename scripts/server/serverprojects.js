(function(exports)
{


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
exports.Traffic=function()
{
	var that={};
	that.connection=null;

	that.mkdirSync=function(path)
	{
		var fs=require('fs');
		try 
		{
			console.log(path);
			fs.mkdirSync(path);
		} 
		catch(e) 
		{
			if ( e.code != 'EEXIST' ) throw e;
		}
	}
	that.mkdirpSync=function(dirpath) 
	{
		var path=require('path');
		var parts=dirpath.split(path.sep);
		for (var i=1;i<=parts.length;i++) 
			that.mkdirSync(path.join.apply(null,parts.slice(0,i)));
	}
	that.SetConnection=function(connection)
	{
		that.connection=connection;
	}
	that.ReadDotPlotSettings=function(json)
	{
	console.log(json);
		var seedcompsettings=[];
		var alignsettings=[];
		var filtersettings=[];
		var fs=require('fs');
		var path=json.projectpath+'sequence_data/dotplot/dpsettings.txt';
		console.log("Reading dot plot settings at :"+path);
		var instream=fs.createReadStream(path);
		var readline=require('readline');
		instream.on('error', function(err) 
		{
			json.type='LoadDotPlotSettings';
			json.seedcompsettings=seedcompsettings;
			json.alignsettings=alignsettings;
			json.filtersettings=filtersettings;
//			result=JSON.stringify( {type:'LoadDotPlotSettings', 'projectcode':json.projectcode, 'seedcompsettings':seedcompsettings, 'alignsettings':alignsettings, 'filtersettings':filtersettings});
			that.connection.send(JSON.stringify(json));
//			console.log(err);
			return;
		});
		var stream=require('stream');
		var outstream=new stream;
		var rl=readline.createInterface(instream,outstream);
		rl.on('line', function(line)
		{
			var tabs=line.split('\t');
			if (tabs.length==3)
			{
				if (tabs[0]=="Seeding")
				{
					var foo={key:"",value:""};
					foo.key=tabs[1];
					foo.value=tabs[2];
					if (tabs[2]=="true") foo.value=true;
					if (tabs[2]=="false") foo.value=false;
					seedcompsettings.push(foo);
				}
				if (tabs[0]=="Aligning")
				{
					var foo={key:"",value:""};
					foo.key=tabs[1];
					foo.value=tabs[2];
					if (tabs[2]=="true") foo.value=true;
					if (tabs[2]=="false") foo.value=false;
					alignsettings.push(foo);
				}
				if (tabs[0]=="Filtering")
				{
					var foo={key:"",value:""};
					foo.key=tabs[1];
					foo.value=tabs[2];
					if (tabs[2]=="true") foo.value=true;
					if (tabs[2]=="false") foo.value=false;
					filtersettings.push(foo);
				}
			}
		});

		rl.on('close', function() 
		{
			json.type='LoadDotPlotSettings';
			json.seedcompsettings=seedcompsettings;
			json.alignsettings=alignsettings;
			json.filtersettings=filtersettings;
			that.connection.send(JSON.stringify(json));
		});
	}
	that.ReadDPCellCanvasData=function(json)
	{
		that.OpenDotPlotCellFile(json,false);
	}
	that.ReadDPCellStretchData=function(json)
	{
		if (json.stretchdatatype=='synteny') that.OpenDotPlotCellStretchFile(json,false);
		if (json.stretchdatatype=='uniqueX' || json.stretchdatatype=='uniqueY') that.OpenDotPlotCellUniqueStretches(json,false);
	}
	that.OpenDotPlotCellFile=function(json,invert)
	{
		var fs=require('fs');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".txt";
		if (invert==true) path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".txt";
		fs.stat(path, function(err, stat)
		{
			if (stat && stat.isFile()) go_on();
			else if (invert==false) that.OpenDotPlotCellFile(json,true);
			else if (invert==true) failed();
		});
		function go_on()
		{
			var path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".dts";
			if (invert==true) path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".dts";
			instream=fs.createReadStream(path);
			instream.on('error', function(err) {failed();});
			instream.on('open', function(err) {that.ReadStreamStretchCanvas(instream,json);});
		}
		function failed()
		{
			json.type='LoadDotPlotCellData';
			that.connection.send(JSON.stringify(json));
		}
	}
	that.OpenDotPlotCellStretchFile=function(json,invert)
	{
		console.log(json);
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".txt";
		if (invert==true) path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".txt";
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			if (invert==false) that.OpenDotPlotCellStretchFile(json,true);
			else console.log("Could not open file "+path);
		});
		instream.on('open', function(err) 
		{
			console.log("File is successfully opened: "+path);
			var es=require("event-stream");
			var linecnt=0;
			var len1=0;
			var len2=0;
			var xpossen=[];
			var ypossen=[];
			var lens=[];
			var oris=[];
			var obj={'xpossen':[],'ypossen':[],'lens':[],'oris':[],'includes':[],'scores':[]}
			var data=0;
			instream.pipe(es.split()).pipe(
				es.mapSync(function(line)
				{
					(function()
					{
						var tabs=line.split(' ');
						if (data==0 && tabs[0]=="DATA") data=1;
						if (data==0)
						{
							if (tabs[0]=="LENGTHS")
							{
								len1=parseInt(tabs[1]);
								len2=parseInt(tabs[2]);
							}
						}
						if (data==1 && tabs.length==6)
						{
							obj.xpossen.push(parseInt(tabs[0]));
							obj.ypossen.push(parseInt(tabs[1]));
							obj.lens.push(parseInt(tabs[2]));
							obj.scores.push(parseInt(tabs[3]));
							obj.oris.push(parseInt(tabs[4]));
							obj.includes.push(parseInt(tabs[5]));
						}
						linecnt++;
					})();
				}).on('end', function()
					{
						json.type='ClientLoadDotPlotCellStretchData';
						json.length1=len1;
						json.length2=len2;
						json.stretches=obj;
						that.connection.send(JSON.stringify(json));
					})
			);
		});
	}

	that.OpenDotPlotCellUniqueStretches=function(json,invert)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".hit";
		if (invert==true) path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".hit";
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			if (invert==false) that.OpenDotPlotCellUniqueStretches(json,true);
			else console.log("Could not open file "+path);
		});
		instream.on('open', function(err) 
		{
			var es=require("event-stream");
			var obj={'lens':[],'copyscores':[],'xcover':0,'xscore_full':0,'xscore_overlap':0,'ycover':0,'yscore_full':0,'yscore_overlap':0}
			var introread=true,xcoverread=false,ycoverread=false;
			instream.pipe(es.split()).pipe(
				es.mapSync(function(line)
				{
					(function()
					{
						var tabs=line.split(' ');
						var readX=true;
						if (json.stretchdatatype=='uniqueY') readX=false;
						if (ycoverread==true)
						{
							if (tabs.length==2)
							{
								obj.lens.push(parseInt(tabs[0]));
								obj.copyscores.push(parseInt(tabs[1]));
							}
						}
						if (xcoverread==true)
						{
							if (tabs.length==1 && tabs[0]=='y_cover_overview') {xcoverread=false;if (readX==false) ycoverread=true;}
							if (readX==true && tabs.length==2)
							{
								obj.lens.push(parseInt(tabs[0]));
								obj.copyscores.push(parseInt(tabs[1]));
							}
						}
						if (introread)
						{
							if (tabs[0]=='xcover') obj.xcover=tabs[1];
							if (tabs[0]=='xscore_full') obj.xscore_full=tabs[1];
							if (tabs[0]=='xscore_overlap') obj.xscore_overlap=tabs[1];
							if (tabs[0]=='ycover') obj.ycover=tabs[1];
							if (tabs[0]=='yscore_full') obj.yscore_full=tabs[1];
							if (tabs[0]=='yscore_overlap') obj.yscore_overlap=tabs[1];
							if (tabs[0]=='x_cover_overview') {introread=false;xcoverread=true;}
						}
					})();
				}).on('end', function()
					{
						json.type='ClientLoadDotPlotCellUniqueStretches';
						json.data=obj;
						that.connection.send(JSON.stringify(json));
					})
			);
		});
	}
	
	that.OpenDotPlotBestHits=function(json)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/best_hits.mpp';
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
//			console.log("Could not open file "+path);
			return 0;
		});
		instream.on('open', function(err) 
		{
			var es=require("event-stream");
			var linecnt=0;
			var data=[];
			var lastpair=null;
			instream.pipe(es.split()).pipe(
				es.mapSync(function(line)
				{
					(function()
					{
						var tabs=line.split(' ');
						if (tabs[0]=="+")
						{
							var pair={'code1':'','code2':'','besthits':[]}
							pair.code1=tabs[1];
							pair.code2=tabs[2];
							data.push(pair);
							lastpair=pair;
						}
						if (tabs[0]=='>')
						{
							var obj={'x':0,'y':0,'len':0,'ori':0,'pdf':0}
							obj.x=parseInt(tabs[1]);
							obj.y=parseInt(tabs[2]);
							obj.len=parseInt(tabs[3]);
							obj.ori=parseInt(tabs[4]);
							obj.pdf=parseInt(tabs[5]);
							lastpair.besthits.push(obj);
						}
						linecnt++;
					})();
				}).on('end', function()
					{
						json.type='ClientLoadDotPlotBestHits';
						json.data=data;
						json.pjobjectcode=json.code;
						that.connection.send(JSON.stringify(json));
						return 1;
					})
			);
		});
	}

	that.OpenDotPlotNoHits=function(json)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/no_hits.mpp';
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
//			console.log("Could not open file "+path);
			return 0;
		});
		instream.on('open', function(err) 
		{
			var es=require("event-stream");
			var linecnt=0;
			var data=[];
			var lastseq=null;
			instream.pipe(es.split()).pipe(
				es.mapSync(function(line)
				{
					(function()
					{
						var tabs=line.split(' ');
						if (tabs[0]=="+")
						{
							var seq={'code':'','cover':0,nohits:[]}
							seq.code=tabs[1];
							seq.cover=parseInt(tabs[2]);
							data.push(seq);
							lastseq=seq;
						}
						if (tabs[0]=='>')
						{
							var obj={'pos':0,'len':0}
							obj.pos=parseInt(tabs[1]);
							obj.len=parseInt(tabs[2]);
							lastseq.nohits.push(obj);
						}
						linecnt++;
					})();
				}).on('end', function()
					{
						json.type='ClientLoadDotPlotNoHits';
						json.data=data;
						json.pjobjectcode=json.code;
						that.connection.send(JSON.stringify(json));
						return 1;
					})
			);
		});
	}

	that.convert=function(x)
	{
		return Math.floor(x/10);
	}
	
	that.ReadStreamStretchCanvas=function(instream,json)
	{
		var es=require("event-stream");
		var len1=0;
		var len2=0;
		var lines=[];
		var start=0;
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					var tabs=line.split(' ');
					if (start==0)
					{
						if (tabs[0]==='length1') len1=parseInt(tabs[1]);
						if (tabs[0]==='length2') len2=parseInt(tabs[1]);
					}
					if (start==1 && tabs.length>1)	
					{
						var obj={x:parseInt(tabs[0]),y:parseInt(tabs[1]),val:parseInt(tabs[2])};
						lines.push(obj);
					}
					if (tabs[0]==='data') start=1;
				})();
			}).on('end', function()
				{
					json.type='LoadDotPlotCellData';
					json.status='ok';
					json.length1=len1;
					json.length2=len2;
					json.lines=lines;
					json.objectcode=json.code;
					that.connection.send(JSON.stringify(json));
				})
		);
	}

/*	that.ReadStreamStretchIncludeData=function(instream,json)
	{
		var es=require("event-stream");
		var includes="";
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					includes+=line;
				})();
			}).on('end', function()
				{
					json.type='LoadDotPlotCellStretchIncludeData';
					json.includes=includes;
					that.connection.send(JSON.stringify(json));
				})
		);
	}*/
	
	that.OpenCellAlignFile=function(json,invert)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".aln";
		if (invert)	path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".aln";
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			if (invert==false) that.OpenCellAlignFile(json,true);
			else console.log("no valid alignment file at "+path);
		});
		instream.on('open', function(err) 
		{
			that.ReadAlignStream(instream,json,path);
		});
	}

	that.ReadAlignStreamFilepos=function(instream,json,path)
	{
		console.log("Reading cell alignment data at :"+path);
		var es=require("event-stream");
		var blocks=[];
		var k=0;
		var filepos=0;
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					if (k%1000==0)
					{
						var tabs=line.split(' ');
						if (tabs.length==6)
						{
							var obj={'startbasegaps':parseInt(tabs[0]),'filepos':filepos};
							blocks.push(obj);
						}
					}
					k++;
					filepos=filepos+line.length;
				})();
			}).on('end', function()
				{
					json.type='LoadCellAlignData';
					json.blocks=blocks;
//					var result=JSON.stringify( {type:'LoadCellAlignData', 'projectcode':json.projectcode, 'Txymatrix': json.Txymatrix, 'classobjcode1': json.classobjcode1, 'classobjcode2': json.classobjcode2,'blocks':blocks, objectcode:json.code});
					that.connection.send(JSON.stringify(json));
//					console.log("Cell alignment file positions : "+blocks.cell);
				})
		);
	}

	that.ReadAlignStream=function(instream,json,path)
	{
		console.log("Reading cell alignment data at :"+path);
		var es=require("event-stream");
		var obj={'startbasegaps':[],'startbases':[],'endbases':[],'oris':[],'cluster':[]}
		var superstretch={'xpossen1':[],'xpossen2':[],'ypossen1':[],'ypossen2':[],'oris':[],'scores':[]}
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					var tabs=line.split(' ');
					if (tabs.length>0)
					{
						if (tabs[0]=='+')
						{
							superstretch.xpossen1.push(parseInt(tabs[1]));
							superstretch.xpossen2.push(parseInt(tabs[2]));
							superstretch.ypossen1.push(parseInt(tabs[3]));
							superstretch.ypossen2.push(parseInt(tabs[4]));
							superstretch.oris.push(parseInt(tabs[5]));
							superstretch.scores.push(parseInt(tabs[6]));
						}
						if (tabs[0]=='>')
						{
							obj.startbasegaps.push(parseInt(tabs[1]));
							obj.startbases.push(parseInt(tabs[2]));
							obj.endbases.push(parseInt(tabs[3]));
							obj.oris.push(parseInt(tabs[4]));
						}
					}
				})();
			}).on('end', function()
				{
					json.type='LoadCellAlignData';
					json.blocks=obj;
					json.superstretches=superstretch;
					that.connection.send(JSON.stringify(json));
				})
		);
	}

	that.ServerCheckMultiAlignFile=function(json,invert)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.code1+'_'+json.code2+'.aln';
		if (invert)	path=json.projectpath+'/sequence_data/dotplot/'+json.code2+"_"+json.code1+".aln";
		var score=0;
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			if (invert==false) that.ServerCheckMultiAlignFile(json,true);
			else
			{
				console.log("no valid alignment file at "+path);
				json.type='ClientCheckMultiAlignData';
				json.pjobjectcode=json.code;
				that.connection.send(JSON.stringify(json));
			}
		});
		instream.on('open', function(err) 
		{
			var es=require("event-stream");
			instream.pipe(es.split()).pipe(
				es.mapSync(function(line)
				{
					(function()
					{
						var tabs=line.split(' ');
						if (tabs.length>0 && tabs[0]=='<') score=1.0*tabs[1];
					})();
				}).on('end', function()
					{
						json.type='ClientCheckMultiAlignData';
						json.score=score;
						json.invertdata=invert;
						json.pjobjectcode=json.code;
						that.connection.send(JSON.stringify(json));
					})
			);
		});
	}
	
	that.ServerOpenMultiAlignFile=function(json,invert)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.code1+'_'+json.code2+'.aln';
		if (invert)	path=json.projectpath+'/sequence_data/dotplot/'+json.code2+"_"+json.code1+".aln";
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			if (invert==false) that.ServerOpenMultiAlignFile(json,true);
			else
			{
				console.log("no valid alignment file at "+path);
				json.type='ClientLoadMultiAlignData';
				json.pjobjectcode=json.code;
				that.connection.send(JSON.stringify(json));
			}
		});
		instream.on('open', function(err) 
		{
			that.ReadMultiAlignStream(instream,json,path);
		});
	}

	that.ReadMultiAlignStream=function(instream,json,path)
	{
		var blocks=[];
		var es=require("event-stream");
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					var tabs=line.split(' ');
					if (tabs.length>0)
					{
						if (tabs[0]=='+')
						{
							var block={xpos1:0,xpos2:0,ypos1:0,ypos2:0,ori:0,score:0}
							block.xpos1=parseInt(tabs[1]);
							block.xpos2=parseInt(tabs[2]);
							block.ypos1=parseInt(tabs[3]);
							block.ypos2=parseInt(tabs[4]);
							block.ori=parseInt(tabs[5]);
							block.score=parseInt(tabs[6]);
							blocks.push(block);
						}
					}
				})();
			}).on('end', function()
				{
					json.blocks=blocks;
					json.type='ClientLoadMultiAlignData';
					json.pjobjectcode=json.code;
					that.connection.send(JSON.stringify(json));
				})
		);
	}
/*	that.ServerOpenMultiAlignFile=function(json)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path=json.projectpath+'/sequence_data/dotplot/multialign.txt';
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			if (invert==false) that.OpenCellAlignFile(json,true);
			else console.log("no valid multialignment file at "+path);
		});
		instream.on('open', function(err) 
		{
			that.ReadMultiAlignStream(instream,json,path);
		});
	}
	that.ReadMultiAlignStream=function(instream,json,path)
	{
		console.log("Reading multialignment data at :"+path);
		var current_cluster=null;
		var current_template=null;
		var current_alignment=null;
		var clusters=[];
		var es=require("event-stream");
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					var tabs=line.split(' ');
					if (tabs.length>0)
					{
						if (tabs[0]=='{')
						{
							var cluster={templates:[]};
							current_cluster=cluster;
							clusters.push(cluster);
						}
						if (tabs[0]=='+')
						{
							var template={key:tabs[1],alignments:[]};
							current_template=template;
							if (current_cluster!=null) current_cluster.templates.push(template);
						}
						if (tabs[0]=='-')
						{
							var alignment={key:tabs[1],startbasegaps:[],startbases:[],lengths:[],orientations:[]};
							current_alignment=alignment;
							if (current_template!=null) current_template.alignments.push(alignment);
						}
						if (tabs[0]=='&')
						{
							if (current_alignment!=null)
							{
								current_alignment.startbasegaps.push(tabs[1]);
								current_alignment.startbases.push(tabs[2]);
								current_alignment.lengths.push(tabs[3]);
								current_alignment.orientations.push(tabs[4]);
							}
						}
					}
				})();
			}).on('end', function()
				{
					json.type='ClientLoadMultiAlignData';
					json.multialignment=clusters;
					json.pjobjectcode=json.code;
					that.connection.send(JSON.stringify(json));
				})
		);
	}*/
	
	that.ServerUpdateDataBaseProject=function(json)
	{
//		console.log(json);
		var fs=require('fs');
		var elements=json.elements;
		var files=[];
		for (var i=0;i<elements.length;i++) files.push(elements[i].file);
		function create_setup()
		{
			var _path=json.projectfilelocation;
			var stream1=fs.createWriteStream(_path);
			stream1.on('open', function(fd) 
			{
				stream1.write("ProjectFormat\t1\n");
				stream1.write("UniqueCode\t"+json.projectcode+"\n");
				stream1.write("DisplayName\t"+json.projectdisplayname+"\n");
				for (var i=0;i<elements.length;i++) stream1.write("Data\t?\t"+elements[i].classobjectcode+"\n");
				stream1.write("Dotplot\n");
				stream1.end();
			});	
			stream1.on('finish', function(fd) 
			{
				_path=json.projectpath+"/sequence_data/dotplot/dpinput.txt";
				var stream2=fs.createWriteStream(_path);
				stream2.on('open', function(fd) 
				{
					for (var i=0;i<elements.length;i++) stream2.write("DataCluster\t"+elements[i].clusternumber+'\t'+elements[i].classobjectcode+'\t'+elements[i].file+'\t'+elements[i].seqlength+'\tself=true'+"\n");
					stream2.end();
				});	
				stream2.on('finish', function(fd) 
				{
					json.type='ClientUpdateDataBaseProject';
					that.connection.send(JSON.stringify(json));
				});
			});
		}
		function callback(index,seqlen)
		{
			console.log("lengths " +seqlen);
			if (index>=0 && index<=files.length-1) elements[index].seqlength=seqlen;
			if (index==files.length-1)	create_setup();
		}
		var index=0;
		if (files.length>0)
		{
			var MEntryObject=require(pathServer+"/MEntryObject.js");
			var entry=new MEntryObject.MEntry();
			entry.ReadEntryFileBatch(files,index,callback);
		}
		else create_setup();
	}

	that.ServerCreateDataBaseProject=function(json)
	{
		var elements=json.elements;
		var files=[];
		for (var i=0;i<elements.length;i++) files.push(elements[i].file);
		var path=json.projectpath;that.mkdirSync(path);
		console.log("Creating new project at :"+path);
		var fs=require('fs');
		if (!fs.existsSync(path))
		{
			fs.mkdirSync(path,0766,function(err){if (err) console.log(err);	});   
		}
		else {}
		var _path=path+"/sequence_data";that.mkdirSync(_path);
		_path=_path+"/dotplot";that.mkdirSync(_path);

		function create_setup()
		{
			var _path=json.filelocation;
			var stream1=fs.createWriteStream(_path);
			stream1.on('open', function(fd) 
			{
				stream1.write("ProjectFormat\t1\n");
				stream1.write("UniqueCode\t"+json.projectcode+"\n");
				stream1.write("DisplayName\t"+json.displayname+"\n");
				for (var i=0;i<elements.length;i++) stream1.write("Data\t?\t"+elements[i].classobjectcode+'\t'+elements[i].file+'\t'+elements[i].seqlength+"\n");
				stream1.write("Dotplot\n");
				stream1.end();
			});	
			stream1.on('finish', function(fd) 
			{
				_path=json.projectpath+"/sequence_data/dotplot/dpinput.txt";
				var stream2=fs.createWriteStream(_path);
				stream2.on('open', function(fd) 
				{
					for (var i=0;i<elements.length;i++) stream2.write("DataCluster\t"+elements[i].clusternumber+'\t'+elements[i].classobjectcode+'\t'+elements[i].file+'\t'+elements[i].seqlength+'\tself=true'+"\n");
					if (elements.length==0)  stream2.write("\n");
					stream2.end();
				});	
				stream2.on('finish', function(fd) 
				{
					json.type='ClientCreateDataBaseProject';
					that.connection.send(JSON.stringify(json));
				});
			});
		}
		function callback(index,seqlen)
		{
			if (index>=0 && index<=files.length-1) elements[index].seqlength=seqlen;
			if (index==files.length-1) create_setup();
		}
		var index=0;
		if (files.length>0)
		{
			var MEntryObject=require(pathServer+"/MEntryObject.js");
			var entry=new MEntryObject.MEntry();
			entry.ReadEntryFileBatch(files,index,callback);
		}
		else create_setup();
	}

	that.OpenCellVariationsFile=function(json)
	{
		var fs=require('fs');
		var util=require('util');
		var stream=require('stream');
		var path="";
		path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".var";
		instream=fs.createReadStream(path);
		instream.on('error', function(err) 
		{
			path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".var";
			instream=fs.createReadStream(path);
			instream.on('error', function(err) 
			{
			});
			instream.on('open', function(err) 
			{
				that.ReadVariationsStream(instream,json,path);
			});
		});
		instream.on('open', function(err) 
		{
			that.ReadVariationsStream(instream,json,path);
		});
	}

	that.ReadVariationsStream=function(instream,json,path)
	{
		console.log("Reading cell variation data at :"+path);
		var es=require("event-stream");
		var variations=[];
		instream.pipe(es.split()).pipe(
			es.mapSync(function(line)
			{
				(function()
				{
					var tabs=line.split(' ');
					if (tabs.length>0)
					{
						var obj={'start':tabs[0]*1,'accession':json.entryaccession}
						variations.push(obj);
//						variations.push(tabs[0]*1);
					}
				})();
			}).on('end', function()
				{
					var result=JSON.stringify( {type:'LoadCellVariationsData', 'projectcode':json.projectcode, 'Txymatrix': json.Txymatrix, 'classobjcode1': json.classobjcode1, 'classobjcode2': json.classobjcode2,'variations':variations, 'objectcode':json.code,'seqboxcode':json.seqboxcode});
					that.connection.send(result);
				})
		);
	}
	that.DPDeleteCurrentCell=function(json)//enkele cell berekend
	{
		var fs=require('fs');
		var path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode1+"_"+json.classobjcode2+".txt";
		console.log('Deleting file '+path);
		fs.unlink(path,function(err)
		{
			if (err) 
			{
				path=json.projectpath+'/sequence_data/dotplot/'+json.classobjcode2+"_"+json.classobjcode1+".txt";
				console.log('Deleting file '+path);
				fs.unlink(path,function(err)
				{
					if (err) console.log(err);else console.log('File successfully deleted: '+path);
				});
			}
			else console.log('File successfully deleted: '+path);
		});
		var _path=json.projectpath+"/sequence_data/dotplot/";
		var spawn=require('child_process').spawn;
		var seedcomp=spawn(pathHome+'/executables/seedcomp.exe',[_path], function (error, stdout, stderr) {
			if (error !== null) console.log('exec error: ' + error);
		});
		seedcomp.on('close', function (code) {
			console.log('child process seedcomp.exe exited with code ' + code);
			var result=JSON.stringify( {type:'ReLoadDotPlot', 'projectcode':json.projectcode, objectcode:json.code});
			that.connection.send(result);
		});
	}
	that.CreateDotPlotInputFile=function(json)
	{
		var fs=require('fs');
		var path=json.projectpath+"/sequence_data/dotplot/dp_input.txt"
		fs.writeFileSync(path,json.stream);
		console.log('DotPlot Input file created at ' + path);
	}
	that.CreateSeedCompSettingsFile=function(json)
	{
		var fs=require('fs');
		var path=json.projectpath+"/sequence_data/dotplot/dpsettings.txt"
		var buffer="";
		for (var key in json.seedcompsettings) buffer+='Seeding\t'+key+"\t"+json.seedcompsettings[key]+"\n";
		for (var key in json.alignsettings) buffer+='Aligning\t'+key+"\t"+json.alignsettings[key]+"\n";
		for (var key in json.filtersettings) buffer+='Filtering\t'+key+"\t"+json.filtersettings[key]+"\n";
		for (var key in json.multialignsettings) if (key!='Templates') buffer+='MultiAligning\t'+key+"\t"+json.multialignsettings[key]+"\n";
		for (var i=0;i<json.multialignsettings.Templates.length;i++) buffer+='MultiAligning\tTemplate\t'+json.multialignsettings.Templates[i]+"\n";
		fs.writeFileSync(path,buffer);
	}
	that.ServerStartSeedCompCalculation=function(json)//volledige matrix
	{
		var fs=require('fs');
		var _path=json.projectpath+"/sequence_data/dotplot/";
		var spawn=require('child_process').spawn;
		var seedcomp=spawn(pathHome+'/executables/seedcomp.exe',[_path,"reset"],{stdio:[1,'pipe']});
		seedcomp.on('exit', function (code) 
		{
			console.log('child process seedcomp.exe exited with code ' + code);
			var result=JSON.stringify( {type:'DotPlotUpdateRun', 'projectcode':json.projectcode, 'pjobjectcode':json.code, 'action':'StretchCalculationFinished'});
			that.connection.send(result);
		});
		seedcomp.stdout.on('data',function (data) 
		{
			var result=JSON.stringify( {type:'DotPlotUpdateRun', 'projectcode':json.projectcode, 'pjobjectcode':json.code, 'action':'StretchCalculationStatus','status':data.toString()});
			that.connection.send(result);
		});
	}
	that.ServerStartStretchAlignment=function(json)
	{
		var fs=require('fs');
		var _path=json.projectpath+"/sequence_data/dotplot/";
		var spawn=require('child_process').spawn;
		var location="Location\t"+_path;
		var action="Action\tAlign";
		var align=spawn(pathHome+'/executables/align.exe',[location,action],{stdio:[1,'pipe']});
		align.on('exit', function (code) 
		{
			console.log('child process align.exe (Stretch Alignment) exited with code ' + code);
			var result=JSON.stringify( {type:'DotPlotUpdateRun', 'projectcode':json.projectcode, 'pjobjectcode':json.code, 'action':'StretchAlignmentFinished'});
			that.connection.send(result);
		});
		align.stdout.on('data', function (data) 
		{
			var result=JSON.stringify( {type:'DotPlotUpdateRun', 'projectcode':json.projectcode, 'pjobjectcode':json.code, 'action':'StretchAlignStatus','status':data.toString()});
			that.connection.send(result);
		});
	}
	that.ServerStartStretchFindNoHits=function(json)
	{
		var _path=json.projectpath+"/sequence_data/dotplot/no_hits.mpp";
		var fs=require('fs');
		if (fs.existsSync(_path)) {that.OpenDotPlotNoHits(json);return;}

		var spawn=require('child_process').spawn;
		_path=json.projectpath+"/sequence_data/dotplot/";
		var location="Location\t"+_path;
		var action="Action\tFindUniqueSequences";
		var align=spawn(pathHome+'/executables/align.exe',[location,action], function (error, stdout, stderr) {
			if (error !== null) console.log('exec error: ' + error);
			console.log('executable finished');
		});
		align.on('close', function (code) {
			console.log('child process align.exe (Find No Hits) exited with code ' + code);
			that.OpenDotPlotNoHits(json);
//			var result=JSON.stringify( {type:'ReLoadDotPlot', 'projectcode':json.projectcode, 'pjobjectcode':json.code,'action':'StretchFindUniqueSequences'});
//			that.connection.send(result);
		});
	}
	that.ServerStartStretchFindBestHits=function(json)
	{
		var _path=json.projectpath+"/sequence_data/dotplot/best_hits.mpp";
		var fs=require('fs');
		if (fs.existsSync(_path)) {that.OpenDotPlotBestHits(json);return;}
		
		var spawn=require('child_process').spawn;
		_path=json.projectpath+"/sequence_data/dotplot/";
		var location="Location\t"+_path;
		var action="Action\tFindBestHits";
		var align=spawn(pathHome+'/executables/align.exe',[location,action], function (error, stdout, stderr) {
			if (error !== null) console.log('exec error: ' + error);
			console.log('executable finished');
		});
		align.on('close', function (code) {
			console.log('child process align.exe (Find Best Hits) exited with code ' + code);
			that.OpenDotPlotBestHits(json);
//			var result=JSON.stringify( {type:'DotPlotUpdateRun', 'projectcode':json.projectcode, 'pjobjectcode':json.code,'action':'StretchFindBestHits'});
//			that.connection.send(result);
		});
	}
	that.StartSearchVariations=function(json)
	{
		var fs=require('fs');
		var _path=json.projectpath+"/sequence_data/dotplot/";
		var spawn=require('child_process').spawn;
		var location="Location\t"+_path;
		var action="Action\tVariations";
		var align=spawn(pathHome+'/executables/align.exe',[location,action], function (error, stdout, stderr) {
			if (error !== null) console.log('exec error: ' + error);
			console.log('executable finished');
		});
		align.on('close', function (code) {
			console.log('child process align.exe (Find Variations) exited with code ' + code);
			var result=JSON.stringify( {type:'ReLoadDotPlot', 'projectcode':json.projectcode, 'pjobjectcode':json.code});
			that.connection.send(result);
		});
	}
	that.ReadDotPlotImage=function(json)
	{
		var img = fs.readFileSync('./logo.gif');
		res.writeHead(200, {'Content-Type': 'image/gif' });
		res.end(img, 'binary');
	}
	return that;
}


})(typeof exports === 'undefined'? this['ServerProjects']={}: exports);
