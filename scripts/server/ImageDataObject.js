
(function(exports)
{


exports.ImageData=function()
{
	var that={};
	that.context=null;
	that.width=10;
	that.height=10;
	that.ImageData=null;
	that.length=0;
	that.SetImageData=function(context,width,height)
	{
		if (that.width==width && that.height==height && context!=null) return;
		if (width>0 && height>0)
		{
			that.width=width;
			that.height=height;
		}
		that.ImageData=context.getImageData(0,0,that.width,that.height);
		that.length=that.ImageData.data.length;
		that.context=context;
		that.data=that.ImageData.data;
	}
	that.DrawLine=function(px1,py1,px2,py2,r,g,b,a)
	{
		if (px1>that.width && px2>that.width) return;
		if (px1<0 && px2<0) return;
		if (py1>that.height && py2>that.height) return;
		if (py1<0 && py2<0) return;
		var data=that.data;
		if (px1==px2 && py1==py2 && a==255) 
		{
			var p=py1*that.width*4+px1*4;
			if (p<0 || p+4>that.length) return;
			data[p]=r;p++;
			data[p]=g;p++
			data[p]=b;p++
			data[p]=255;
			return;
		}
		if (px1==px2 && py1==py2 && a<255) {that.MergePixel(px1,py1,r,g,b,a);return;}
		var dist=Math.sqrt((px2-px1)*(px2-px1)+(py2-py1)*(py2-py1));
		if (a<255)
		{
			for (var i=0;i<dist;i++)
			{
				var x=Math.round(px1+i*(px2-px1)/dist);
				var y=Math.round(py1+i*(py2-py1)/dist);
				if (x<0) continue;
				if (x>that.width) return;
				if (y<0) continue;
				if (y>that.height) return;
				that.MergePixel(x,y,r,g,b,a);
			}
		}
		else
		{
			var p=0;
			for (var i=0;i<dist;i++)
			{
				var x=Math.round(px1+i*(px2-px1)/dist);
				var y=Math.round(py1+i*(py2-py1)/dist);
				if (x<0) continue;
				if (x>that.width) return;
				if (y<0) continue;
				if (y>that.height) return;
				p=y*that.width*4+x*4;
				data[p+0]=r;
				data[p+1]=g;
				data[p+2]=b;
				data[p+3]=255;
			}
		}
	}
	that.DrawStretch=function(px1,py1,px2,py2,len,r,g,b,a)
	{
		var dist=(px2-px1)/(py2-py1);
		var lx=that.width;
		var i=0;
		var j=0;
		var p=0;
		var aa=0;
		var floor=Math.floor;
		var data=that.data;
		var ll=that.length;
		for (i=py1;i<=py2;i++)
		{
			var x=floor(px1+(i-py1)*dist);
			for (j=0;j<=len;j++) 
			{
				if (j+x<0) continue;
				if (j+x>lx) break;
				p=i*lx*4+(j+x)*4;
				if (p>=0 && p+3<ll)
				{
					aa=data[p+3]/255
					data[p]=data[p]+r*(1-aa);p++;
					data[p]=data[p]+g*(1-aa);p++;
					data[p]=data[p]+b*(1-aa);p++;
					data[p]=data[p]+a*(1-aa);
				}
			}
		}
	}
/*	that.DrawStretch=function(px1,py1,px2,py2,len,r,g,b,a)
	{
		var dist=(px2-px1)/(py2-py1);
		var x1=px1+(i-py1)*dist;
		var x2=px1+(i-py1)*dist;
		var x_x=Math.floor(px1);
		var y_y=py1;
		x_x=Math.max(x_x,0);x_x=Math.min(x_x,that.width);
		var aa=a;
		var k=0;
		var ll=Math.floor(len);//if (ll<dist) ll=dist;
		for (var i=py1;i<=py2;i++)
		{
			var s1=Math.max(x,0);s1=Math.min(s1,that.width);
			var s2=Math.min(x+ll,that.width);s2=Math.max(s2,0);
			var d=0;
			for (var j=s1,h=0;j<=s2;j++) {that.MergePixel(j,i,r,g,b,a+k+50*h/ll);h++;}
			if (i%2==0) k++;
			if (x_x!=s1) {x_x=s1;y_y=i;}
		}
	}*/
/*	that.DrawBlock=function(px1,py1,px2,py2,r,g,b,a)
	{
		var round=Math.round;
		var mergepixel=that.MergePixel;
		for (var j=py1;j<=py2;j++)
		{
			var s1=Math.max(px1,0);s1=Math.min(s1,that.width);
			var s2=Math.min(px2,that.width);s2=Math.max(s2,0);
			for (var i=s1;i<=s2;i++)
			{
				mergepixel(i,j,r,g,b,a);
			}
		}
	}*/
	that.DrawBlock=function(_px1,_py1,_px2,_py2,r,g,b,_a2)
	{
		var py1=~~_py1;
		var py2=~~_py2;
		var px1=~~_px1;
		var px2=~~_px2;

		var data=that.data;
		var p=0;var j=0;var i=0;
//		if (_a2==255)
		{
			for (j=py1;j<=py2;j++)
			{
//				for (i=s1;i<=s2;i++)
				for (i=px1;i<=px2;i++)
				{
					p=j*that.width*4+i*4;
					if (p<0 || p+4>that.length) return;
					data[p]=r;p++;
					data[p]=g;p++;
					data[p]=b;p++;
					data[p]=255;
				}
			}
			return;
		}
		var a1=0,a2=0,aa=0;
		a2=_a2/255;
		
		for (j=py1;j<=py2;j++)
		{
			for (i=px1;i<=px2;i++)
			{
				p=j*that.width*4+i*4;
				if (p<0 || p+4>that.length) return;
				a1=data[p+3]/255;
				aa=a1+a2*(1-a1);
				data[p]=(data[p]*a1+r*a2*(1-a1))/aa;p++;
				data[p]=(data[p]*a1+g*a2*(1-a1))/aa;p++;
				data[p]=(data[p]*a1+b*a2*(1-a1))/aa;p++;
				data[p]=255*aa;
			}
		}
	}
	that.DrawBackGround=function(r,g,b,a)
	{
		that.context.clearRect(0,0,that.width,that.height);
		that.ImageData=that.context.getImageData(0,0,that.width,that.height);
		that.length=that.ImageData.data.length;
		that.data=that.ImageData.data;
		return;
		var k=that.length;
		while (k--)	that.data[k]=0;
	}
	that.SetPixel=function(px,py,r,g,b)
	{
		var p=py*that.width*4+px*4;
		if (p<0 || p+4>that.length) return;
		var data=that.ImageData.data;
		data[p]=r;p++;
		data[p]=g;p++
		data[p]=b;p++
		data[p]=255;
	}
	that.MergePixel=function(px,py,r,g,b,_a2)
	{
		var p=py*that.width*4+px*4;
		if (p<0 || p+4>that.length) return;
		var data=that.data;
		var a1=data[p+3]/255;
		var a2=_a2/255;
		var aa=a1+a2*(1-a1);
		data[p]=(data[p]*a1+r*a2*(1-a1))/aa;p++;
		data[p]=(data[p]*a1+g*a2*(1-a1))/aa;p++;
		data[p]=(data[p]*a1+b*a2*(1-a1))/aa;p++;
		data[p]=255*aa;
	}
	that.ResolveImageData=function()
	{
		if (that.context==null) return;
		that.context.putImageData(that.ImageData,0,0);
	}
	that.rgbaSum=function(r1,r2)
	{
       var a = c1.a + c2.a*(1-c1.a);
       return {
         r: (c1.r * c1.a  + c2.r * c2.a * (1 - c1.a)) / a,
         g: (c1.g * c1.a  + c2.g * c2.a * (1 - c1.a)) / a,
         b: (c1.b * c1.a  + c2.b * c2.a * (1 - c1.a)) / a,
         a: a
       }
     } 
	return that;
}

})(typeof exports === 'undefined'? this['ImageDataObject']={}: exports);
