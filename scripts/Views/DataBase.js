define(["require","DQX/Application", "DQX/Framework", "DQX/Controls", "DQX/Msg", "DQX/DocEl", "DQX/Utils","DQX/FrameTree","DQX/Popup","DQX/FramePanel", "DQX/FrameCanvas", "MRC/MarcTable","MRC/ScopeBoxList",
"MRC/ProjectList","Views/DataBase","MRC/MFieldBar","MRC/MSequenceBox","MRC/SeqPlot","Views/SequenceViewer","Views/DotPlotViewer","MRC/NewProjectWizard","MRC/pdfkit","MRC/blob-stream"],
    function (require,Application, Framework, Controls, Msg, DocEl, DQX, FrameTree, Popup, FramePanel, FrameCanvas, MarcTable, ScopeBoxList, 
	ProjectList, DataBase, MFieldBar, MSequenceBox, SeqPlot, SequenceViewer, DotPlotViewer,NewProjectWizard,PDFDocument,blobStream) 
	{

        var DataBaseModule = 
		{

            init: function () 
			{
                var that=Application.View
				(
//                   'database',     // View ID
//                    'Database'     // View title
                'database','rarara freche kleine Rabe'//schrijf hier tekst als een header moet verschijnen
				);

 				that.createFrames = function(rootFrame) 
				{
					that.root=rootFrame;
					rootFrame.makeGroupHor().setSeparatorSize(8);//Declare the root frame as a vertically divided set of subframes

					var commandStack=rootFrame.addMemberFrame(Framework.FrameGroupVert('',0.2)).setDisplayTitle('Commands').setSeparatorSize(0);
					that._commandList=commandStack.addMemberFrame(Framework.FrameFinal('', 1.0));

					that.viewStack=rootFrame.addMemberFrame(Framework.FrameGroupStack('', 1));////
//					that.viewStack=rootFrame.addMemberFrame(Framework.FrameGroupTab('', 1));////als vensters als tab moeten verschijnen

					var trackListStack=rootFrame.addMemberFrame(Framework.FrameGroupVert('',0.2)).setDisplayTitle('Actions').setSeparatorSize(0);
					this._trackListButtons=trackListStack.addMemberFrame(Framework.FrameFinal('', 0.3));
					this._trackListButtons.setFixedSize(Framework.dimY,32);
					this._trackList=trackListStack.addMemberFrame(Framework.FrameFinal('', 0.5));
				}


				
				that.createPanels = function() 
				{
					that.createCommandForm();
					this.finaliseCommandForm();

					that.createTrackForm();
					this.finaliseTrackForm();

					var _id=that.NewUUID();
					var track=Application.DbTracks.AddTrack();
					track.SetCode(_id);
					track.SetDefiner("Intro");
					track.SetView(true);
					track.ViewFunction=that.OpenIntro;
					that.finaliseTrackForm();

//					that.OpenIntro(track);
				}
				
				that.createCommandForm = function() 
				{
					// Create a form-type panel and attach it to the frame this.frame1
					var form=Framework.Form(that._commandList);
					form.setPadding(10);

					that.CommandForm=FrameTree.Tree(that._commandList);
					var dbcommands=Application.DbCommands.Commands;
					for (var i=0;i<dbcommands.length;i++) that.AddCommandBranch(that.CommandForm.root,dbcommands[i],i);
				}
				that.AddCommandBranch=function(branch,rootcommand,index)
				{
					if (rootcommand.HasChildren()==false)
					{
						var check=Controls.Hyperlink(null,{width:"100%",content:rootcommand.GetDefiner()});
						check.setOnChanged(function(controlID,theControl) 
						{
							console.log(rootcommand);
							var track=that.evaluateTrack(rootcommand,true);
							if (track!=null) track.execute();//voer actie ook direct uit
						//set info in info screen
						})
						var bt1 = Controls.Button(null, { buttonClass: 'DQXToolButtonBar0', bitmap:'bitmaps/open.png', width:16, height:16 });
						bt1.setOnChanged(function() 
						{
							console.log(rootcommand);
							var track=that.evaluateTrack(rootcommand,true);
							if (track) track.execute();//voer actie ook direct uit
						});
						var comp=Controls.CompoundHor([bt1,check]);
//						branch.addItem(FrameTree.Control(omp)).setCanSelect(true).setCssClass('DQXTreeItem');
						branch.addItem(FrameTree.Control(check)).setCanSelect(true).setCssClass('DQXTreeItemNoSelect');;
					}
					else
					{
						var _branch=branch.addItem(FrameTree.Branch(rootcommand.GetDefiner(), DocEl.StyledText(rootcommand.GetDefiner(), 'DQXSemiLarge'))).setCanSelect(false).setCollapsed(false);
						for (var i=0;i<rootcommand.GetCommandsCnt();i++) 
						{
							child=rootcommand.GetCommand(i);
							if (child==null) continue;
							that.AddCommandBranch(_branch,child,i);
						}
					}
				};
				that.finaliseCommandForm=function()
				{
					that.CommandForm.render();
				};

				that.createTrackForm = function() 
				{
					var form=Framework.Form(this._trackList);
					form.setPadding(10);
					that.TrackForm=FrameTree.Tree(this._trackList);
					Application.UpdateTracks=that.finaliseTrackForm;
				}
				that.AddTrackBranch=function(branch,track,index)
				{
					var check=Controls.Hyperlink(null,{content:track.GetDefiner()});
					track.check=check;
					if (track.GetView()==true)
					{
						var bt1 = Controls.Button(null, { buttonClass: 'DQXToolButtonBar0', bitmap:'bitmaps/open.png', width:16, height:16 });
						bt1.setOnChanged(function() {track.execute()});
						var bt2 = Controls.Button(null, { buttonClass: 'DQXToolButtonBar0', bitmap:'bitmaps/delete.png', width:16, height:16 });
						var comp=Controls.CompoundHor([check,bt1,bt2]);
						branch.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem2');
					}
					else branch.addItem(FrameTree.Control(check)).setCanSelect(true).setCssClass('DQXTreeItem2');
				};
				that.finaliseTrackForm=function()
				{
					var dbtracks=Application.DbTracks;
					that.TrackForm.root.clear();
					for (var i=dbtracks.GetTrackCount()-1;i>=0;i--)	that.AddTrackBranch(that.TrackForm.root,dbtracks.GetTrack(i),i);
					that.TrackForm.render();
				};
				that.FindTrackFromBranchItem=function(id)
				{
					var dbtracks=Application.DbTracks;
					for (var i=0;i<dbtracks.GetTrackCount();i++)
					{
						var track=dbtracks.GetTrack(i);
						var _id=track.check.getID();
						if (_id==id) return track;
					}
					return null;
				};
				that.CreateButtons = function() 
				{
                    var examples = [];
                    
					sep=Controls.HorizontalSeparator(10);
					examples.push(sep);

					var comboStates=[];
                    that.LayerSelect=Controls.Combo(null,{label:'Layers',states: comboStates,value:'state1',hint:'Database Layers'});
                    that.LayerSelect.setOnChanged(function(controlID, theControl) 
					{
						var dbobject=Application.GetActiveDbObject();
						if (dbobject==null) return;
						var layername=theControl._selectedState;
						dbobject.SetDataLayer(layername);
						dbobject.databaselist.ImportDbInfoFields();
					})
                    examples.push(that.LayerSelect);

					sep=Controls.HorizontalSeparator(20);
					examples.push(sep);

					
					var bt = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Add selection to scope data', bitmap:'bitmaps/download.png',width:24,height:22});
                    bt.setOnChanged(function() {that.AddTrackToList();})
                    examples.push(bt);

					var sep=Controls.HorizontalSeparator(20);
					examples.push(sep);

					bt = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Expand/Collapse child nodes from selected parent node', bitmap:'bitmaps/expand_branch.png',width:24,height:22});
                    bt.setOnChanged(function() 
					{
						var dbobject=Application.GetActiveDbObject();
						if (dbobject==null) return;
						dbobject.databaselist.ExpandParent();
					})
                    examples.push(bt);
					
					bt = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Expand/Collapse all nodes at selected level', bitmap:'bitmaps/expand_all.png',width:24,height:22});
                    bt.setOnChanged(function() 
					{
						var dbobject=Application.GetActiveDbObject();
						if (dbobject==null) return;
						dbobject.databaselist.ExpandAll();
					})

                    examples.push(bt);

					return Controls.CompoundHor(examples)
 //                       .setLegend('An sample instance of each control')    // Defines a titled box around the controls
                        .setAutoFillX(false);                               // Instruct this compound control to use only the X space required by the members, rather than the full size
				}
				that.CreateProjectListButtons = function() 
				{
                    var examples = [];
                    
					bt = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description: 'Open Calculation', bitmap:'bitmaps/open.png',width:24,height:22});
                    bt.setOnChanged(function() {AddProjectToTrackList();});
                    examples.push(bt);					

					return Controls.CompoundHor(examples)
 //                       .setLegend('An sample instance of each control')    // Defines a titled box around the controls
                        .setAutoFillX(false);                               // Instruct this compound control to use only the X space required by the members, rather than the full size
				}
				that.UpdateLayers=function()
				{
					var dbobject=Application.GetActiveDbObject();
					if (dbobject==null) return;
					var layers=[];
					layers=dbobject.GetLayers();
					var comboStates=[];
					for (var i=0;i<layers.length;i++)
					{
						var obj={id:layers[i].name,name:layers[i].name};
						comboStates.push(obj);
					}
					if (layers.length>0) that.LayerSelect.setItems(comboStates,layers[0].name);
				}
				that.NewUUID=function()
				{
					var d = new Date().getTime();
					var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) 
					{
						var r = (d + Math.random()*16)%16 | 0;
						d = Math.floor(d/16);
						return (c=='x' ? r : (r&0x7|0x8)).toString(16);
					});
					return uuid;
				}

				that.createViewStackHtml=function(track,substack)
				{
 					var clientdiv=that.viewStack.getClientDivID();
					var tabset = $('#' + that.viewStack.getClientDivID());
					$(tabset).children('.DQXTabs').children('.DQXTab').removeClass('DQXTabActive');
					$(tabset).children('.DQXTabs').children('.DQXTab').addClass('DQXTabInactive');
					
 					var divid=substack.myID;
					var html=substack._createElements(1).toString();
					track.tabnumber=1;
					$('#'+clientdiv+'_tabbody').append("<div id=C"+clientdiv+"_tab_"+track.GetCode()+" class=\"DQXTabContent\" style=\"display: inline\">"+html+'</div>');
					$('#'+clientdiv+'_tabheader').append("<div id="+clientdiv+"_tab_"+track.GetCode()+" class=\"DQXTab DQXTabActive\" style</div>");
					substack._postCreateHTML();//om de mousedown & mousemove events te initieren
				}
				that.OpenIntro=function(track)
				{
				}
				that.OpenDataBase=function(track)
				{
//					var track=Application.DbTracks.FindTrack(track.GetCode());
//					if (track!=null) {track.execute();return;}
					var dbobj=Application.GetDbObject(track.GetCode());//object is reeds open
					if (dbobj!=null) {that.switchTab(track.GetCode());Application.SetActiveDbObject(track.GetCode());return;}
					if (track.tabnumber!=-1) {that.switchTab(track.GetCode());Application.SetActiveDbObject(track.GetCode());return;}
 					var dbobject=Application.RegisterDbObject(track.GetCode());
					Application.SetActiveDbObject(track.GetCode());
					Application.ClientConnection.send(JSON.stringify(track.action));
					var substack=that.viewStack.addMemberFrame(Framework.FrameGroupVert(track.GetCode(), 1)).setSeparatorSize(0).setDisplayTitle(track.GetDefiner());
					var buttonsFrame=substack.addMemberFrame(Framework.FrameFinal('Buttons', 0.3));
					buttonsFrame.setFixedSize(Framework.dimY,38);
						
					var dataBaseFrame=substack.addMemberFrame(Framework.FrameFinal('Database', 0.5)).setAllowScrollBars(false,false);
					
					that.createViewStackHtml(track,substack);

					var panelButtons = Framework.Form(buttonsFrame); // Define a Form type panel, attached to the frame this.frameForm
                    var buttons_components = []; // This will accumulate all the components of the form
                    buttons_components.push(that.CreateButtons());
                    panelButtons.addControl(Controls.CompoundHor(buttons_components)); // Put all the components on the form, in a vertical layout

					var databaselist=MarcTable.Init(dataBaseFrame,dbobject);
					databaselist.AddColumn(0,"_index","_Index",200);
					dbobject.databaselist=databaselist;
					databaselist.UpdateDbList();
					
					databaselist.ImportDbInfoFields();
					panelButtons.render();
					that.switchTab(track.GetCode());
				}
                that.OpenDataBaseProjects=function(track)
				{
					if (track.tabnumber!=-1) {Application.SetActivePjObject(track.GetCode());that.switchTab(track.GetCode());return;}
					var pjobject=Application.RegisterPjObject(track.GetCode());
					Application.SetActivePjObject(track.GetCode());
					Application.ClientConnection.send(JSON.stringify(track.action));
					var substack=that.viewStack.addMemberFrame(Framework.FrameGroupVert(track.GetCode(), 1)).setSeparatorSize(0).setDisplayTitle(track.GetDefiner());
					var buttonsFrame=substack.addMemberFrame(Framework.FrameFinal('', 0.3));
					buttonsFrame.setFixedSize(Framework.dimY,32);
						
					var fieldFrame=substack.addMemberFrame(Framework.FrameFinal('', 0.3));
					fieldFrame.setFixedSize(Framework.dimY,20);
	 
					var dataBaseFrame=substack.addMemberFrame(Framework.FrameFinal('', 0.5));

					that.createViewStackHtml(track,substack);

					var panelButtons = Framework.Form(buttonsFrame); // Define a Form type panel, attached to the frame this.frameForm
                    var buttons_components = []; // This will accumulate all the components of the form
                    buttons_components.push(that.CreateProjectListButtons());
                    panelButtons.addControl(Controls.CompoundHor(buttons_components)); // Put all the components on the form, in a vertical layout


					var projectlist=ProjectList.Init(dataBaseFrame,pjobject);
					projectlist.AddColumn(0,"index","Index",50);
					projectlist.AddColumn(0,"","Name",400);
					projectlist.AddColumn(0,"","Data Item",400);
					projectlist.AddColumn(0,"","Code",200);
					projectlist.AddColumn(0,"","File",200);
					projectlist.AddColumn(0,"","Organism",200);
					projectlist.Update();
					Application.ProjectList=projectlist;

					panelButtons.render();
					that.switchTab(track.GetCode());
				}
				that.GetDbScopeBox=function()
				{
					var dbobject=Application.GetActiveDbObject();
					dbobject.ClearScopeBox();
					var sels=dbobject.databaselist.GetRowColSels();
					for (var i=0;i<sels.length;i++)	dbobject.AddDataScopeBox(sels[i].rowindex,sels[i].field.fieldcode);
					return dbobject.ScopeBox;
				}
				that.CaptureSelectionInProject=function(project)
				{
					var dbobject=Application.GetActiveDbObject();
					if (dbobject==null) return;
					var scopebox=that.GetDbScopeBox();//steek geselecteerde items in de scopebox
					if (scopebox==null) return;
					for (var i=0;i<project.classobjects.length;i++) dbobject.AddClassObjectScopeBox(project.classobjects[i]);//als er reeds items in project zijn, steek ze in de scopebox
					var elements=[];
					var organismClusters=[];
					for (var i=0;i<scopebox.length;i++) 
					{
						var classobj=scopebox[i];
						var orgclassobj=classobj.GetOrganismParent();
						if (orgclassobj!=null)
						{
							var fnd=-1;
							for (var j=0,fnd=-1;j<organismClusters.length;j++) if (organismClusters[j]==orgclassobj) fnd=j;
							if (fnd==-1) 
							{
								organismClusters.push(orgclassobj);
								fnd=organismClusters.length-1;
							}
							var path=Application.DecodePath(classobj.pathcode);
							path=path+"/"+classobj.filename;
							var element={'dbobjectcode':dbobject.GetCode(),'classobjectcode':classobj.uniquecode,'clusternumber':fnd,'file':path,'seqlength':100000};
							elements.push(element);
						}
					}
					var command=project.GetServerCommand();
//					var command=JSON.stringify({'type':'ServerUpdateDataBaseProject','projectpath':project.projectPath,'filelocation':project.fileLocation,'projectdescription':project.displayName,'projectcode':project.uniqueCode,'elements':elements});
					command.type='ServerUpdateDataBaseProject';
					command.elements=elements;
					Application.ClientConnection.send(JSON.stringify(command));
				}
				that.CreateDataBaseProject=function()
				{
					function create()
					{
						var dbobject=Application.GetActiveDbObject();
						dbobject.ClearScopeBox();
						var sels=dbobject.databaselist.GetRowColSels();
						for (var i=0;i<sels.length;i++)	dbobject.AddDataScopeBox(sels[i].rowindex,sels[i].field.fieldcode);
						var scopebox=that.GetDbScopeBox();

						var elements=[];
						var organismClusters=[];
						for (var i=0;i<scopebox.length;i++) 
						{
							var classobj=scopebox[i];
							var orgclassobj=classobj.GetOrganismParent();
							if (orgclassobj!=null)
							{
								var fnd=-1;
								for (var j=0,fnd=-1;j<organismClusters.length;j++) if (organismClusters[j]==orgclassobj) fnd=j;
								if (fnd==-1) 
								{
									organismClusters.push(orgclassobj);
									fnd=organismClusters.length-1;
								}
								var path=Application.DecodePath(classobj.pathcode);
								path=path+"/"+classobj.filename;
								var element={'dbobjectcode':dbobject.GetCode(),'classobjectcode':classobj.uniquecode,'clusternumber':fnd,'file':path,'seqlength':0};
								elements.push(element);
							}
						}
						

						var pjobject=Application.GetActivePjObject();
						if (pjobject==null) return;
						var project=new MPjObject.Project(Application,pjobject);
						project.fileLocation=dataPath+'projects/'+obj.projectFileName+'/setup.txt';
//						console.log("project file ----------------->"+project.fileLocation);
						project.uniqueCode=code;
	//					project.dataItems=json.data;
						project.displayName=obj.projectDisplayName;
						project.projectPath=dataPath+'projects/'+obj.projectFileName;
						pjobject.RegisterProject(project);
						Application.ProjectList.Update();
						var database=Application.DataBaseModule;
						var track=database.AddProjectToTrackList(project);
						
						var pjobject=Application.GetActivePjObject();
						if (pjobject!=null) pjobject.ClearProjects();//wissen, alle files worden heringelezen
						var command=JSON.stringify({'type':'ServerCreateDataBaseProject','projectpath':project.projectPath,'filelocation':project.fileLocation,'displayname':project.displayName,'projectcode':project.uniqueCode,'elements':elements});
						Application.ClientConnection.send(command);
					}
					var code=Application.DataBaseModule.NewUUID();
					var d=new Date();
					var nstr=d.toISOString();
					var date=new Date();
					var components=[date.getYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes(),date.getSeconds(),date.getMilliseconds()];
					var id=components.join("");		
					var obj={projectFileName:id,projectDisplayName:nstr,'projectType':''}
					var wizard=NewProjectWizard.init(obj);
					wizard.execute(function()
					{
						create();
					});
				}
/*				that.CreateDataBaseProject=function()
				{
					var code=Application.DataBaseModule.NewUUID();
					var d=new Date();
					var nstr=d.toISOString();
					var date = new Date();
					var components = [
						date.getYear(),
						date.getMonth(),
						date.getDate(),
						date.getHours(),
						date.getMinutes(),
						date.getSeconds(),
						date.getMilliseconds()
					];

					var id = components.join("");		
					var obj={projectFileName:id,projectDisplayName:nstr,'projectType':''}
					var wizard=NewProjectWizard.init(obj);
					wizard.execute(function()
					{
						var track=Application.GetLastTrack();
						if (track==null || track.GetClassObjectCount()==0)
						{
							//melding dat geen data geselecteerd is
						}
						var organismClusters=[];
						var elements=[];
						for (var i=0;i<track.classobjects.length;i++)
						{
							var classobj=Application.FindClassObject(track.classobjects[i].GetUniqueCode());
							var orgclassobj=classobj.GetOrganismParent();
							if (orgclassobj!=null)
							{
								var fnd=-1;
								for (var j=0,fnd=-1;j<organismClusters.length;j++) if (organismClusters[j]==orgclassobj) fnd=j;
								if (fnd==-1) 
								{
									organismClusters.push(orgclassobj);
									fnd=organismClusters.length-1;
								}
								var path=Application.DecodePath(track.classobjects[i].pathcode);
								path=path+"/"+track.classobjects[i].filename;
								var element={'classobjectcode':track.classobjects[i].GetUniqueCode(),'clusternumber':fnd,'file':path,'seqlength':0};
								elements.push(element);
							}
						}
						var pjobject=Application.GetActivePjObject();
						if (pjobject!=null) pjobject.ClearProjects();
						var command=JSON.stringify({'type':'CreateDataBaseProject','dbpath':dataPath,'projectname':obj.projectFileName,'projectdescription':obj.projectDisplayName,'projectcode':that.NewUUID(),'elements':elements});
						Application.ClientConnection.send(command);
					});
				}*/
				that.OpenSequenceData=function(track) 
				{
//					that.OpenIntro(track);	return;
					
					if (track.tabnumber!=-1) {that.switchTab(track.GetCode());return;}
 					var dbobject=track.dbobject;
					if (dbobject==null) return;
					var _id=that.NewUUID();
					
					var SeqBox=new MSequenceBox.Init();
					SeqBox.SetIdentifier(_id);
					Application.RegisterSeqBox(SeqBox);

					var substack=that.viewStack.addMemberFrame(Framework.FrameGeneric(track.GetCode(), 1)).setDisplayTitle(track.GetDefiner());
					var theFrame=SequenceViewer.init(SeqBox);
					theFrame.SetIdentifier(_id);
					theFrame.createFrames(substack);

					that.createViewStackHtml(track,substack);

					theFrame.createPanels();
					Application.RegisterWindow(theFrame);
					that.switchTab(track.GetCode());
					theFrame.UpdateFacts();
					for (var i=0;i<track.classobjects.length;i++) SeqBox.AddClassObjectCode(track.classobjects[i].GetUniqueCode());
					SeqBox.OpenSequences(theFrame.LoadStatus,theFrame.UpdateFacts);
				}
                that.OpenProjectData=function(track) 
				{
					if (track.tabnumber!=-1) {that.switchTab(track.GetCode());return;}
					var _id=that.NewUUID();
					
					var pjobject=Application.GetActivePjObject();
					if (pjobject==null) return;
					var project=pjobject.GetProject(track.GetCode());
					project.CreateDotPlot();

					var SeqBox=new MSequenceBox.Init();
					SeqBox.SetIdentifier(_id);
					Application.RegisterSeqBox(SeqBox);
					
					var substack=that.viewStack.addMemberFrame(Framework.FrameGeneric(track.GetCode(), 1)).setDisplayTitle(track.GetDefiner());
					var theFrame=DotPlotViewer.init(project,SeqBox);
					theFrame.SetIdentifier(_id);
					theFrame.createFrames(substack);

					that.createViewStackHtml(track,substack);

					theFrame.createPanels();
					Application.RegisterWindow(theFrame);
					theFrame.LoadDotPlot();
					that.switchTab(track.GetCode());
					theFrame.UpdateFacts();
				}
                that.ShowPDFDoc=function(track) 
				{
//					var PDFDocument=require('pdfkit');
//					var blobStream=require('blob-stream');
					var doc=new PDFDocument();
//					doc.font('fonts/PalatinoBold.ttf').fontSize(25).text('Some text with an embedded font!', 100, 100);
					doc.fontSize(25).text('Here is some vector graphics...', 100, 100);
					doc.save().moveTo(100, 150).lineTo(100, 250).lineTo(200, 250).fill("#FF3300");
//					doc.scale(0.6).translate(470, -380).path('M 250,75 L 323,301 131,161 369,161 177,301 z').fill('red', 'even-odd').restore();
//					doc.addPage().fillColor("blue").text('Here is a link!', 100, 100).link(100, 100, 160, 27, 'http://google.com/');
					doc.end();
					
					var stream=doc.pipe(blobStream());

					stream.on('finish', function() 
					{
						if (track.tabnumber!=-1) {that.switchTab(track.GetCode());return;}
						var substack=that.viewStack.addMemberFrame(Framework.FrameFinal(track.GetCode(), 1));
						that.createViewStackHtml(track,substack);
//						var url=stream.toBlobURL('application/pdf');
						var url="d:\\temp\\out.pdf";
						substack.setContentHtml('<iframe id=\"myiframe\" src=\"'+url+'\" />');
						substack.hasiFrame=true;
						that.switchTab(track.GetCode());
					});
				}
				
                that.FetchEmblData=function(track) 
				{
					if (track.tabnumber!=-1) {that.switchTab(track.GetCode());return;}
					var substack=that.viewStack.addMemberFrame(Framework.FrameFinal(track.GetCode(), 1));
					that.createViewStackHtml(track,substack);
					var url="http://www.ncbi.nlm.nih.gov/blast/Blast.cgi?CMD=Put&DATABASE=nr&PROGRAM=blastn&FORMAT_TYPE=Text&QUERY="+track.action.sequence;
					substack.setContentHtml('<iframe id=\"myiframe\" src=\"'+url+'\" />');
					substack.hasiFrame=true;
					that.switchTab(track.GetCode());
				}

				that.switchTab=function(trackcode) 
				{
					var clientdiv=that.viewStack.getClientDivID();
					that.viewStack._reactClickTab('',clientdiv+"_tab_"+trackcode);
					that.viewStack._performSwitchTab(clientdiv+"_tab_"+trackcode);
//					that.viewStack._setSubFramesPosition(true);//doet een update resize
					that.root._uuuupdate();


				}
				that.AddTrackToList=function()
				{
					var dbobject=Application.GetActiveDbObject();
					if (dbobject==null) return;
					var _id=that.NewUUID();
					var track=Application.DbTracks.AddTrack();
					track.SetCode(_id);
					track.SetDefiner("Data selection");
					track.SetView(true);
					track.dbobject=dbobject;
					var sels=dbobject.databaselist.GetRowColSels();
					for (var i=0;i<sels.length;i++)	dbobject.AddDataTrack(sels[i].rowindex,sels[i].field.fieldcode,track);
					var code="";
					var dataIds=[];
					for (var i=0;i<track.classobjects.length;i++) 
					{
						if (i>0) code=code+"::"
						var obj={'fielddata':"",'fieldcode':track.classobjects[i].GetFieldCode()};
						track.classobjects[i].GetText(obj);
						code=code+obj.fielddata;
						var dataid=dbobject.FindFieldCodeDataId(track.classobjects[i].GetFieldCode());
						if (dataIds.indexOf(dataid)<0) dataIds.push(dataid);
					}
					track.ViewFunction=that.GetFunctionFromFieldCodes(dataIds);
					track.action=JSON.stringify({type: that.GetJSONCommandFromFieldCodes(dataIds)});
					track.SetDefiner(code);
					that.finaliseTrackForm();
					return track;
				}
				that.AddProjectToTrackList=function(project)
				{
					var track=Application.DbTracks.FindTrack(project.uniqueCode);
					if (track!=null) {track.execute();return;}
					if (project!=null)
					{
						var track=Application.DbTracks.AddTrack();
						track.SetCode(project.uniqueCode);
						if (project.displayName!="") track.SetDefiner(project.displayName);else track.SetDefiner(project.projectPath);
						track.SetView(true);
						track.ViewFunction=that.OpenProjectData;
						that.finaliseTrackForm();
						return track;
					}
					return null;
				}
				that.createTrack=function(view,command,execute_function)
				{
//					var _id=that.NewUUID();
					var _id=command.action.code;
					var track=Application.DbTracks.FindTrack(_id);
					if (track!=null) return track;
					var track=Application.DbTracks.AddTrack();
					track.SetCode(_id);
					var str="";
//					if (command.parent!=null) str=command.parent.GetDefiner()+'|';
//					str+=command.GetDefiner();
					str+=command.action.note;
					track.SetDefiner(str);
					track.SetView(view);
					track.ViewFunction=execute_function;
					track.action=command.action;
					track.action.code=_id;
					that.finaliseTrackForm();
					return track;
				}
				that.evaluateTrack=function(command,view)
				{
					if (command.action.type=="OpenDataBase") return that.createTrack(view,command,that.OpenDataBase);
					if (command.action.type=="OpenDataBaseProjects") return that.createTrack(view,command,that.OpenDataBaseProjects);
					if (command.action.type=="CreateDataBaseProject") that.CreateDataBaseProject();
					if (command.action.type=="FetchEmblData") return that.createTrack(view,command,that.FetchEmblData);
//					if (json.type=="OpenSequenceData") that.createTrack(view,command,that.OpenSequenceData);
				}
				
				that.PostCreateFunctions=function(action,code)//komt van client
				{
					if (action=="DataBaseLoaded")
					{
						var dbobject=Application.GetActiveDbObject();
						if (dbobject==null) return;
						if (dbobject.databaselist!=null) dbobject.databaselist.UpdateDbList();
						that.UpdateLayers();
					}
				}
				that.GetFunctionFromFieldCodes=function(dataIds)
				{
					if (dataIds.length==1 && dataIds[0]==100) return that.OpenSequenceData;
					if (dataIds.length==1 && dataIds[0]==102) return that.OpenExcelData;
				}
				that.GetJSONCommandFromFieldCodes=function(dataIds)
				{
					if (dataIds.length==1 && dataIds[0]==100) return 'OpenSequenceData';
					if (dataIds.length==1 && dataIds[0]==102) return 'OpenExcelData';
				}
				that.GetColourFromFieldCode=function(dataId)
				{
					if (dataId==100) return 'rgba(150,0,150,255)';
					if (dataId==102) return 'rgba(0,150,150,255)';
					if (dataId==103) return 'rgba(150,150,0,255)';
				}
				Application.DataBaseModule=that;
				return that;
            }

        };

        return DataBaseModule;
    });