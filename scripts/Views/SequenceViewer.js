define(["require", "DQX/Application", "DQX/Framework", "DQX/HistoryManager", "DQX/Controls", "DQX/Msg", "DQX/DocEl", "DQX/Utils","DQX/FrameTree", "DQX/PopupFrame", "DQX/Popup","MRC/MFieldBar","MRC/SeqPlot","MRC/DataStreamView","MRC/MFtsSearch","MRC/MFtsSearchWizard","MRC/MSeqSearchWizard","MRC/MSequenceBox"],
    function (require, Application, Framework, HistoryManager, Controls, Msg, DocEl, DQX, FrameTree, PopupFrame, Popup, MFieldBar, SeqPlot,DataStreamView, MPlotChannel, MFtsSearchWizard, MSeqSearchWizard, MSequenceBox) 
	{

        var SequenceModule=
		{
			init: function (SequenceBox) 
			{
					// Instantiate the view object
				var that=Application.View
				(
                    SequenceBox.GetIdentifier(),     // View ID
                    SequenceBox.GetIdentifier()     // View title
				)
				that.Identifier="";
				that.SeqBox=SequenceBox;
			
				//This function is called during the initialisation. Create the frame structure of the view here
				that.createFrames = function(_root) 
				{
					var frameRoot=_root;
					frameRoot.makeGroupTab(); // Define the top-level frame to be a tabbed environment
/*				var websocketConnection = new WebSocket("ws://localhost:8000/echo");
				websocketConnection.onopen = function(ev) {
					console.log('Connected to the echo service')
				};
				websocketConnection.onmessage = function(event) {
				  var payload = event.data;
				  displayEcho(payload);
				};
				websocketConnection.send("Hello Echo Server");*/
					this.seqtab=frameRoot.addMemberFrame(Framework.FrameGroupHor('1', 1.0)).setDisplayTitle('Sequence Panel');;
					{
						this.frame1=this.seqtab.addMemberFrame(Framework.FrameFinal('', 0.2)).setDisplayTitle('Data Selection Display');

						var rightpanels=this.seqtab.addMemberFrame(Framework.FrameGroupVert('', 0.8)).setDisplayTitle('Sequence');

						that.treeFrame=rightpanels.addMemberFrame(Framework.FrameFinal('Tree', 1.0)).setAllowScrollBars(false,true);;
						that.treeForm=FrameTree.Tree(that.treeFrame);
		 
						{
							that.sequencePlaceHolder=Controls.PlaceHolder('SequencePlaceHolder',0,400);
							var _branch=that.treeForm.root.addItem(FrameTree.Branch("SequencePlaceHolderBranch", DocEl.StyledText("Graphical Sequence Plot", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);


							var bt1 = Controls.Button(null, { buttonClass: 'DQXToolButtonBar', bitmap:'bitmaps/open.png', width:15, height:18 });
							bt1.setOnChanged(function() {that.SequencePanel.FullZoom();})
							var bt2 = Controls.Button(null, { buttonClass: 'DQXToolButtonBar', bitmap:'bitmaps/test.png', width:15, height:18 });
							var bt3 = Controls.Button(null, { buttonClass: 'DQXToolButtonBar', bitmap:'bitmaps/delete.png', width:15, height:18 });
							var comp=Controls.CompoundHor([bt1,bt2,bt3]);
							_branch.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem4');
							
							_branch.addItem(FrameTree.Control(that.sequencePlaceHolder)).setCanSelect(true).setCssClass('DQXTreeSubWindow');;
							that.SequencePanelGhost=that.treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.sequencePlaceHolder.getFullID(''), 0.0));
						}
						{
							that.streamPlaceHolder=Controls.PlaceHolder('StreamPlaceHolder',0,300);
							var _branch=that.treeForm.root.addItem(FrameTree.Branch("StreamPlaceHolderBranch", DocEl.StyledText("Map Information", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
							_branch.addItem(FrameTree.Control(that.streamPlaceHolder)).setCanSelect(true).setCssClass('DQXTreeSubWindow');;
							that.StreamPanelGhost=that.treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.streamPlaceHolder.getFullID(''), 0.0));
						}
						var check=Controls.Hyperlink(null,{content:"see you later"});
						that.treeForm.root.addItem(FrameTree.Control(check)).setCanSelect(true).setCssClass('DQXTreeItem');;
					}
					var tab2 = frameRoot.addMemberFrame(Framework.FrameGroupHor('2', 0.2)).setDisplayTitle('A split panel');
					{
						tab2.addMemberFrame(Framework.FrameFinal('', 0.2)).setDisplayTitle('Component 1');
						tab2.addMemberFrame(Framework.FrameFinal('', 0.8)).setDisplayTitle('Component 2');
					}
					dbobject=Application.DbObject;
				}

				//This function is called during the initialisation. Create the panels that will populate the frames here
				that.createPanels = function() 
				{
					that.createPanelForm();
					that.createDataStreamView();
					that.createSequencePanel();
					this.finaliseChannelControlPanel();
					that.SequencePanel.SetChannelCallBack(that.DataStreamView.ChannelCallBack);
					that.treeForm.render();
			   }

				that.createPanelForm = function() 
				{
					var st = Controls.Static('This is some static text in a form');

					that.ChannelOptions=FrameTree.Tree(this.frame1);
					this.branch=that.ChannelOptions.root;

					this.branchDisplay=this.branch.addItem(FrameTree.Branch('Display', DocEl.StyledText('Display', 'DQXLarge'))).setCanSelect(true).setCollapsed(true);
						var CheckCircular=Controls.Check(null, {label:'Circular',value:false})
						CheckCircular.setOnChanged(function(controlID,theControl) {that.SequencePanel.SwitchCircular();})
						this.branchDisplay.addItem(FrameTree.Control(CheckCircular));

						var CheckMultiline=Controls.Check(null, {label:'Multiline',value:true})
						CheckMultiline.setOnChanged(function(controlID,theControl) {that.SequencePanel.SwitchMultiLine();})
						this.branchDisplay.addItem(FrameTree.Control(CheckMultiline));
						
						var CheckDisplayLabel=Controls.Check(null, {label:'Feature labels',value:true})
						CheckDisplayLabel.setOnChanged(function(controlID,theControl) {that.SequencePanel.SwitchDisplayLabel();})
						this.branchDisplay.addItem(FrameTree.Control(CheckDisplayLabel));

						var DisplaySize=Controls.Edit(null, {label:'Display Size (px)',value:300})
						DisplaySize.setOnChanged(function(controlID,theControl) 
						{
							var val=parseInt(theControl.value);
							if (isNaN(val)) val=0;
							if (val>5000) val=5000;
							that.changeSequencePanelSize(val);
						})
						this.branchDisplay.addItem(FrameTree.Control(DisplaySize));

					this.branchChannels=this.branch.addItem(FrameTree.Branch('Maps', DocEl.StyledText('Maps', 'DQXLarge'))).setCanSelect(true).setCollapsed(false);
	/*					that.alleleFreqDispType = Controls.RadioGroup('', { states:[
									{id:'none', name:'Do not show variant frequencies'},
									{ id:'all', name:'Show frequencies for all variants'},
									{ id:'query', name:'Show frequencies for variants in the current Variant Catalogue query set'}
								], value:'none' } );
						that.alleleFreqDispType.setOnChanged(function() {})
						this.branchChannels.addItem(FrameTree.Control(that.alleleFreqDispType));*/

					this.SequenceChannelBranch=this.branchChannels.addItem(FrameTree.Branch('SequenceChannel', DocEl.StyledText('Sequence', 'DQXSemiLarge'))).setCanSelect(false).setCollapsed(false);
						var check=Controls.Check(null, {label:"Sequence data",value:true, colorbox:true, color:"rgb(255,0,0)"})
						check.setOnChanged(function(controlID,theControl) {that.SwitchChannel(theControl.getValue(),"Sequence data");})
						this.SequenceChannelBranch.addItem(FrameTree.Control(check));
						check=Controls.Check(null, {label:"Sequence numbering",value:false, colorbox:true, color:"rgb(180,180,180)"})
						check.setOnChanged(function(controlID,theControl) {that.SwitchChannel(theControl.getValue(),"Sequence numbering");})
						this.SequenceChannelBranch.addItem(FrameTree.Control(check));

					that.FeatureChannelBranch=this.branchChannels.addItem(FrameTree.Branch('FeatureChannel', DocEl.StyledText('Features', 'DQXSemiLarge'))).setCanSelect(false).setCollapsed(false);
					{
						var bt = Controls.Button(null, { buttonClass: 'DQXToolButtonBar', content: "New search", width:80, height:18 });
						bt.setOnChanged(function() {that.CallFtsSearch()})
						that.FeatureChannelBranch.addItem(FrameTree.Control(bt));
					}
					
					this.SqSearchChannelBranch=this.branchChannels.addItem(FrameTree.Branch('SqSearchChannel', DocEl.StyledText('Sequence search', 'DQXSemiLarge'))).setCanSelect(false).setCollapsed(false);
					{
						var bt = Controls.Button(null, { buttonClass: 'DQXToolButtonBar', content: "New search", width:80, height:18 });
						bt.setOnChanged(function() {that.CallSeqSearch()})
						this.SqSearchChannelBranch.addItem(FrameTree.Control(bt));
					}
					
					this.ReSearchChannelBranch=this.branchChannels.addItem(FrameTree.Branch('ReSearchChannel', DocEl.StyledText('Restriction enzyme search', 'DQXSemiLarge'))).setCanSelect(false).setCollapsed(false);
					{
						var bt=Controls.Button(null, { buttonClass: 'DQXToolButtonBar', bitmap:'bitmaps/AddSearch.png', width:34, height:17 });
						bt.setOnChanged(function() {that.CallReSearch()})
						this.ReSearchChannelBranch.addItem(FrameTree.Control(bt));
					}
					this.FrameSearchChannelBranch=this.branchChannels.addItem(FrameTree.Branch('FrameSearchChannel', DocEl.StyledText('Frame mapping', 'DQXSemiLarge'))).setCanSelect(false).setCollapsed(false);
					{
						var bt=Controls.Button(null, { buttonClass: 'DQXToolButtonBar', content: "New search", width:80, height:18 });
						bt.setOnChanged(function() {that.CallFrameMapping()})
						this.FrameSearchChannelBranch.addItem(FrameTree.Control(bt));
					}
				}
				that.finaliseChannelControlPanel=function()
				{
					that.ChannelOptions.render();
				};
				that.createSequencePanel=function() 
				{
					that.SequencePanel=SeqPlot.Panel(that.sequencePlaceHolder.getFullID(''),that.SeqBox);
					that.SequencePanelGhost.SetClientObject(that.SequencePanel);
					that.sequencePlaceHolder.modifyValue(that.SequencePanel.canvasStr);
				}
				that.changeSequencePanelSize=function(size)
				{
					var frameel=$('#' +that.sequencePlaceHolder.getFullID(''));
					frameel.css('height',size+'px');
					that.seqtab._uuuupdate();
				}
				that.CreateSequencePanelButtons = function() 
				{
					var examples = [];
					   
					var bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar',description:'Jump to previous channel item',bitmap:'bitmaps/previous_item.png',width:22,height:22});
					bt.setOnChanged(function() {that.SequencePanel.FullZoom();})
					examples.push(bt);

					bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar',description:'Jump to next channel item',bitmap:'bitmaps/next_item.png',width:22,height:22});
					bt.setOnChanged(function() {that.SequencePanel.NextChannelItem();})
					examples.push(bt);

					bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar',description:'Zoom sequence details', bitmap:'bitmaps/fullzoom.png',width:22,height:22});
					bt.setOnChanged(function() {that.SequencePanel.FullZoom();})
					examples.push(bt);

					return Controls.CompoundHor(examples).setAutoFillX(false);
				}

				that.createDataStreamView=function() 
				{
					that.DataStreamView=DataStreamView.Panel(that.streamPlaceHolder.getFullID(''),this,that.dataStreamViewCallBack);
					that.StreamPanelGhost.SetClientObject(that.DataStreamView);
					that.streamPlaceHolder.modifyValue(that.DataStreamView.canvasStr);
					
					that.DataStreamView.AddColumn(0,"index","Index",200);
					that.DataStreamView.AddColumn(0,"","Data Item",200);
				}
				that.SetIdentifier=function(id)
				{
					that.Identifier=id;
				};
				that.GetClientSequence=function(nr)
				{
					return that.SeqBox.GetSequence(nr);
				}
				that.GetClientSequenceCount=function()
				{
					return that.SeqBox.GetSequenceCount();
				}
				that.LoadStatus=function(status)
				{
					that.SequencePanel.SetProcessing(status);
				}
				that.UpdateFacts=function()
				{
					that.SeqBox.UpdateSequenceBitPlots();
					that.SequencePanel.handleResize(false);
					that.DataStreamView.handleResize(false);
					that.SequencePanel.UpdateAndScrollToFocus();
				}
				that.SetFrameData=function(data)
				{
					that.SequencePanel.SetFrameData(data);
				}
				that.SendRequest=function(data)
				{
					Application.ClientConnection.send(data);
				}
				that.SetProcessing=function(label)
				{
					that.SequencePanel.SetProcessing(label);
				}
				that.SwitchChannel=function(state,accession)
				{
					that.SeqBox.SwitchChannel(state,accession);
					that.SequencePanel.UpdateAndScrollToFocus();
				}
				that.CallFtsSearch=function()
				{
					that.SeqBox.CallFtsSearchWizard(that.CallBackFtsSearch,that.FtsSearchFinished);
				}
				that.CallBackFtsSearch=function(ftssearch)
				{
					var check=Controls.Check(null, {label:ftssearch.label,value:true, colorbox:true, color:ftssearch.color})
					check.setOnChanged(function(controlID,theControl) {that.SwitchChannel(theControl.getValue(),ftssearch.accession);})
					that.FeatureChannelBranch.addItem(FrameTree.Control(check));
					that.ChannelOptions.render();
					that.SequencePanel.UpdateAndScrollToFocus();
					that.SequencePanel.SetProcessing("Fetching features...");
				}
				that.FtsSearchFinished=function(ftssearch)
				{
					that.SequencePanel.SetProcessing("");
					ftssearch.LinkAllSequences();
					that.UpdateFacts();
				}
				that.CallSeqSearch=function()
				{
					//verzamel alle accessies van de sequenties
					var obj=that.SeqBox.FetchAllAccessions();
					//start de wizard
					wizard=MSeqSearchWizard.init(obj);
					wizard.execute(function(){that.SendSeqSearch(obj)});
				}
				that.SendSeqSearch=function(data)
				{
					var code=Application.DataBaseModule.NewUUID();
					var command=JSON.stringify( { 'type': 'SequenceSearchRequest', 'sequence_box_code': that.SeqBox.GetIdentifier(), 'sequence_search_accession':code, 'sequence_accessions': data.accessions, 'search': data.searchtext});
					Application.ClientConnection.send(command);
					var seqsearch=new MPlotChannel.SeqSearch(that.SeqBox);
					seqsearch.SetAccession(code);
					seqsearch.SetLabel(JSON.stringify(data.searchtext));
					var check=Controls.Check(null, {label:seqsearch.label,value:true, colorbox:true, color:seqsearch.color})
					check.setOnChanged(function(controlID,theControl) {that.SwitchChannel(theControl.getValue(),seqsearch.accession);})
					this.SqSearchChannelBranch.addItem(FrameTree.Control(check));
					that.ChannelOptions.render();
					that.SequencePanel.UpdateAndScrollToFocus();
				}
				that.CallReSearch=function()
				{
					that.SequencePanel.fullscreen();
				}
				that.CallFrameMapping=function()
				{
					that.SeqBox.CallFrameMappingWizard(that.CallBackFrameMapping,that.FrameMappingFinished);
				}
				that.CallBackFrameMapping=function(framemapping)
				{
					var check=Controls.Check(null, {label:framemapping.label,value:true, colorbox:true, color:framemapping.color})
					check.setOnChanged(function(controlID,theControl) {that.SwitchChannel(theControl.getValue(),framemapping.accession);})
					that.FrameSearchChannelBranch.addItem(FrameTree.Control(check));
					framemapping.LinkAllSequences();
					that.SequencePanel.UpdateFacts();
					that.ChannelOptions.render();
					that.SequencePanel.UpdateAndScrollToFocus();
				}
				that.FrameMappingFinished=function(framemapping)
				{
					framemapping.LinkAllSequences();
					that.SetFrameData();
				}

				that.FindChannel=function(accession)
				{
					return that.SeqBox.FindChannel(accession);
				}
				that.dataStreamViewCallBack=function(channel,channelitemnr)
				{
					var p1=channel.GetFullItemStart(channelitemnr);
					var p2=channel.GetFullItemStop(channelitemnr);
					that.SequencePanel.SetSel(p1,p2,p2);
				}
				return that;
			}
        };

        return SequenceModule;
    });