define(["require", "DQX/Application", "DQX/Framework", "DQX/HistoryManager", "DQX/Controls", "DQX/Msg", "DQX/DocEl", "DQX/Utils","DQX/FrameTree", "DQX/PopupFrame", "DQX/Popup","MRC/MFieldBar","MRC/DotPlotOverview","MRC/DotPlotCell","MRC/SeqPlot","MRC/MSequenceBox","MRC/MFtsSearch"],
    function (require, Application, Framework, HistoryManager, Controls, Msg, DocEl, DQX, FrameTree, PopupFrame, Popup, MFieldBar, DotPlotOverview, DotPlotCell, SeqPlot, MSequenceBox, MPlotChannel) 
	{

        var DotPlotModule=
		{
			init : function(Project,SequenceBox) 
			{
				var that=Application.View
				(
                    SequenceBox.GetIdentifier(),     // View ID
                    SequenceBox.GetIdentifier()     // View title
				)
				that.Project=Project;
				that.SeqBox=SequenceBox;

				that.seedcompsettings=$.extend(true,{},that.Project.getDotPlotSeedCompSettings());
				that.filtersettings=$.extend(true,{},that.Project.getDotPlotFilterSettings());
				that.alignsettings=$.extend(true,{},that.Project.getDotPlotAlignSettings());
				that.multialignsettings=$.extend(true,{},that.Project.getDotPlotMultiAlignSettings());

				var _id=Application.DataBaseModule.NewUUID();
				that.AlignedSeqBox=new MSequenceBox.Init();
				that.AlignedSeqBox.SetIdentifier(_id);
				Application.RegisterSeqBox(that.AlignedSeqBox);

				var _id=Application.DataBaseModule.NewUUID();
				that.CSeqBox=new MSequenceBox.Init();
				that.CSeqBox.SetIdentifier(_id);
				that.CSeqBox.concatenate=true;
				Application.RegisterSeqBox(that.CSeqBox);

				that.Identifier="";
				that.Aligner=null;
				that.SetIdentifier=function(id)
				{
					that.Identifier=id;
				};
				that.GetIdentifier=function()
				{
					return that.Identifier;
				}
				that.SetData=function(data)
				{
					that.CellPlotDetails.SetData(data);
				}
			   //This function is called during the initialisation. Create the frame structure of the view here
				that.createFrames = function(_root) 
				{
					that.frameRoot=_root;
					var frameRoot=_root;
					frameRoot.makeGroupHor().setSeparatorSize(8);
					this.dotplottabs=frameRoot.addMemberFrame(Framework.FrameGroupTab('',1.0));
					
					var settingspanel=this.dotplottabs.addMemberFrame(Framework.FrameGroupVert('settingstab', 0.7)).setDisplayTitle('Tasks');
					{
						settingspanel.setSeparatorSize(2);
						this.dotplotcontrolspanel=settingspanel.addMemberFrame(Framework.FrameFinal('', 0.3)).setAllowScrollBars(false,true);;
					}
					var overviewpanels=this.dotplottabs.addMemberFrame(Framework.FrameGroupVert('overviewtab', 1.0)).setDisplayTitle('Matrix Homologies');
					{
						var treeFrame=overviewpanels.addMemberFrame(Framework.FrameFinal('Tree', 0.9)).setAllowScrollBars(false,true);
						that.overviewTreeForm=FrameTree.Tree(treeFrame);

						{
							that.overviewPlaceHolder=Controls.PlaceHolder('OverviewPlaceHolder',0,800);
							var branch=that.overviewTreeForm.root.addItem(FrameTree.Branch("OverviewPlaceHolderBranch", DocEl.StyledText("Dot plot matrix", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
							var _branch2=branch.addItem(FrameTree.Branch("OverviewPlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var check=Controls.Check(null, {label:"Circular Maps",value:false});
								check.setOnChanged(function(controlID,theControl) {if (check.getValue()==true) that.DotPlotOverview.setCircularMaps(true);else that.DotPlotOverview.setCircularMaps(false)});
								var comp=Controls.CompoundHor([check]);
								_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
							}
							branch.addItem(FrameTree.Control(that.overviewPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
							that.OverviewPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.overviewPlaceHolder.getFullID(''), 0.0));
						}
						{
							that.SyntenyMapPlaceHolder=Controls.PlaceHolder('SyntenyMapPlaceHolder',0,800);
							var branch=that.overviewTreeForm.root.addItem(FrameTree.Branch("SyntenyMapPlaceHolderBranch", DocEl.StyledText("Synteny map", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
/*							var _branch2=branch.addItem(FrameTree.Branch("OverviewPlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var check=Controls.Check(null, {label:"Circular Maps",value:false});
								check.setOnChanged(function(controlID,theControl) {if (check.getValue()==true) that.DotPlotOverview.setCircularMaps(true);else that.DotPlotOverview.setCircularMaps(false)});
								var comp=Controls.CompoundHor([check]);
								_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
							}*/
							branch.addItem(FrameTree.Control(that.SyntenyMapPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
							that.SyntenyMapPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.SyntenyMapPlaceHolder.getFullID(''), 0.0));
						}
					}
					this.cellplottab=this.dotplottabs.addMemberFrame(Framework.FrameGroupVert('cellplottab', 0.5)).setDisplayTitle('Pairwise Homologies');
					{
						var treeFrame=this.cellplottab.addMemberFrame(Framework.FrameFinal('Tree', 1.0)).setAllowScrollBars(false,true);
						that.cellplotTreeForm=FrameTree.Tree(treeFrame);
		 
						{
							that.sequencePlaceHolder=Controls.PlaceHolder('SequencePlaceHolder',0,300);
							var branch=that.cellplotTreeForm.root.addItem(FrameTree.Branch("SequencePlaceHolderBranch", DocEl.StyledText("Dot plot matrix", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(false);
							var _branch2=branch.addItem(FrameTree.Branch("SequencePlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var check=Controls.Check(null, {label:"Filter stretches",value:true});
								check.setOnChanged(function(controlID,theControl) 
								{
									var dotplot=that.Project.dotplot;
									if (dotplot!=null && dotplot.current_cell!=null)
									{
										if (theControl.getValue()==true) dotplot.StretchNoFilter=false;else dotplot.StretchNoFilter=true;
										that.overlap(1);
//										that.reLoadCell();
									}
								});
//								_branch2.addItem(FrameTree.Control(check));
								var bt1 = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', content: 'Larger', width:44,height:22});
								bt1.setOnChanged(function(controlID,theControl) {that.changesize(+1);});
//								_branch2.addItem(FrameTree.Control(bt));
								var bt2 = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', content: 'Smaller', width:44,height:22});
								bt2.setOnChanged(function(controlID,theControl) {that.changesize(-1);});
//								_branch2.addItem(FrameTree.Control(bt));
								var comp=Controls.CompoundHor([check,bt1,bt2]);
								_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');

								var bt3 = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', content: 'Select overlap', width:44,height:22});
								bt3.setOnChanged(function(controlID,theControl) {that.overlap(0);});
								var bt4 = Controls.Button(null, {buttonClass: 'DQXToolButtonBar', content: 'All', width:44,height:22});
								bt4.setOnChanged(function(controlID,theControl) {that.overlap(1);});
								var comp=Controls.CompoundHor([bt3,bt4]);
								_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
//								_branch2.addItem(FrameTree.Control(bt));
							}
							branch.addItem(FrameTree.Control(that.sequencePlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
							that.SequencePanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.sequencePlaceHolder.getFullID(''), 0.0));
						}
						
						{
							var grid = Controls.CompoundGrid();                     // Instruct this compound control to use only the X space required by the members, rather than the full size
							grid.setAutoFillX(true).setCssClass('DQXLightFrame');;                               // Defines a titled box around the controls
//							grid.setLegend('Some controls arranged on a grid');
							grid.setLegend('nihil');
							{
								that.StretchPlaceHolder=Controls.PlaceHolder('StretchPlaceHolder',0,200);
								that.StretchPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.StretchPlaceHolder.getFullID(''), 0.0));
							}
							{
								that.StretchDetailsPlaceHolder=Controls.PlaceHolder('StretchDetailsPlaceHolder',0,200);
								that.StretchDetailsPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.StretchDetailsPlaceHolder.getFullID(''), 0.0));
							}
							grid.setItem(0,0,that.StretchPlaceHolder);
							grid.setItem(0,1,that.StretchDetailsPlaceHolder);
							var _branch=that.cellplotTreeForm.root.addItem(FrameTree.Branch("GridPlaceHolderBranch", DocEl.StyledText("Stretch tables", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(false);
							var _branch2=_branch.addItem(FrameTree.Branch("GridPlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var StretchState=Controls.RadioGroup('', { states:[{id:'synteny', name:'Synteny stretches'},
								{ id:'uniqueX', name:'Unique stretches sequence X axis'},
								{ id:'uniqueY', name:'Unique stretches sequence Y axis'}], value:'none' , display:'flex'} );
								StretchState.setOnChanged(function(controlID, theControl) 
								{
									var dotplot=that.Project.dotplot;
									if (dotplot!=null && dotplot.current_cell!=null)
									{
										dotplot.StretchDataType=theControl._selectedState;
										that.reLoadCell();
									}
								});
								_branch2.addItem(FrameTree.Control(StretchState)).setCssClass('DQXTreeItem4');
							}
							_branch.addItem(FrameTree.Control(grid)).setCanSelect(false).setCssClass('DQXTreeSubWindow');
							var _branch3=_branch.addItem(FrameTree.Branch("GridPlaceHolderBranchSub2", DocEl.StyledText("Tasks", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var bt1=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', content: 'Blast selected stretch', width:120,height:22});
								bt1.setOnChanged(function(controlID,theControl) {that.blastStretch();});
								var comp=Controls.CompoundHor([bt1]);
								_branch3.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
							}
						}
						{
							var _branch=that.cellplotTreeForm.root.addItem(FrameTree.Branch("DetailPlaceHolderBranch", DocEl.StyledText("Stretch pairwise sequence mapping", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(false);
							var _branch3=_branch.addItem(FrameTree.Branch("detailssettingsBranch", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var check=Controls.Check(null, {label:"Use synteny heat map colours",value:false});
								check.setOnChanged(function(controlID,theControl) {if (theControl.getValue()==true) that.setHeatMapColors(true);else that.setHeatMapColors(false)});
								var comp=Controls.CompoundHor([check]);
								_branch3.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
							}
							that.detailPlaceHolder=Controls.PlaceHolder('DetailPlaceHolder',0,250);
							_branch.addItem(FrameTree.Control(that.detailPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
							that.DetailPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.detailPlaceHolder.getFullID(''), 0.0));
						}
						{
							that.CDetailPlaceHolder=Controls.PlaceHolder('CDetailPlaceHolder',0,800);
							var _branch=that.cellplotTreeForm.root.addItem(FrameTree.Branch("CDetailPlaceHolderBranch", DocEl.StyledText("Stretch pairwise sequence mapping", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							_branch.addItem(FrameTree.Control(that.CDetailPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
							that.CDetailPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.CDetailPlaceHolder.getFullID(''), 0.0));
						}
						{
							var _branch=that.cellplotTreeForm.root.addItem(FrameTree.Branch("AlignmentPlaceHolderBranch", DocEl.StyledText("Pairwise sequence alignment", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							var _branch3=_branch.addItem(FrameTree.Branch("alignmentsettingsBranch", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
							{
								var check=Controls.Check(null, {label:"Circular view",value:false});
								check.setOnChanged(function(controlID,theControl) {if (theControl.getValue()==true) that.setAlignmentCircularView(true);else that.setAlignmentCircularView(false)});
								var comp=Controls.CompoundHor([check]);
								_branch3.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
							}
							that.alignmentPlaceHolder=Controls.PlaceHolder('AlignmentPlaceHolder',0,350);
							_branch.addItem(FrameTree.Control(that.alignmentPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
							that.AlignmentPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.alignmentPlaceHolder.getFullID(''), 0.0));
						}
					}
/*					this.circleoverviewtab=this.dotplottabs.addMemberFrame(Framework.FrameGroupVert('aligntab', 0.5)).setDisplayTitle('Circluar Overview');
					{
						this.circleoverviewpanel=this.circleoverviewtab.addMemberFrame(Framework.FrameFinal('', 0.7));
					}*/
				}

				that.changesize=function(dir)
				{
					frameel = $('#' +that.sequencePlaceHolder.getFullID(''));
					if (dir>0) frameel.css('height', frameel.height()+100 + 'px');else frameel.css('height', Math.max(100,frameel.height()-100) + 'px')
//					that.frameRoot._updateSize();
					that.frameRoot._uuuupdate();
				}
				
				//This function is called during the initialisation. Create the panels that will populate the frames here
				that.createPanels = function() 
				{
					that.createPanelForm();
					that.DotPlotOverview=DotPlotOverview.Panel(that.overviewPlaceHolder.getFullID(''),this);
					that.OverviewPanelGhost.SetClientObject(that.DotPlotOverview);
					that.overviewPlaceHolder.modifyValue(that.DotPlotOverview.canvasStr);

					that.CircleOverview=DotPlotOverview.Circle(that.SyntenyMapPlaceHolder.getFullID(''),this);
					that.SyntenyMapPanelGhost.SetClientObject(that.CircleOverview);
					that.SyntenyMapPlaceHolder.modifyValue(that.CircleOverview.canvasStr);

					that.CellPlotGraph=DotPlotCell.Filtered(that.sequencePlaceHolder.getFullID(''),this);
					that.SequencePanelGhost.SetClientObject(that.CellPlotGraph);
					that.sequencePlaceHolder.modifyValue(that.CellPlotGraph.canvasStr);

					that.CellPlotStretches=DotPlotCell.StretchList(that.StretchPlaceHolder.getFullID(''),this);
					that.StretchPanelGhost.SetClientObject(that.CellPlotStretches);
					that.StretchPlaceHolder.modifyValue(that.CellPlotStretches.canvasStr);
					
					that.CellPlotStretchDetails=DotPlotCell.StretchDetails(that.StretchDetailsPlaceHolder.getFullID(''),this);
					that.StretchDetailsPanelGhost.SetClientObject(that.CellPlotStretchDetails);
					that.StretchDetailsPlaceHolder.modifyValue(that.CellPlotStretchDetails.canvasStr);

					that.CellPlotDetails=DotPlotCell.SeqDetails(that.detailPlaceHolder.getFullID(''),this,that.SeqBox);
					that.DetailPanelGhost.SetClientObject(that.CellPlotDetails);
					that.detailPlaceHolder.modifyValue(that.CellPlotDetails.canvasStr);

					that.CellPlotCDetails=DotPlotCell.SeqCDetails(that.CDetailPlaceHolder.getFullID(''),this,that.CSeqBox);
					that.CDetailPlaceHolder.clientObject=that.CellPlotCDetails;
					that.CDetailPanelGhost.SetClientObject(that.CellPlotCDetails);
					that.CDetailPlaceHolder.modifyValue(that.CellPlotCDetails.canvasStr);
					
					that.CellAlignment=DotPlotCell.Alignment(that.alignmentPlaceHolder.getFullID(''),this,that.AlignedSeqBox);
					that.AlignmentPanelGhost.SetClientObject(that.CellAlignment);
					that.alignmentPlaceHolder.modifyValue(that.CellAlignment.canvasStr);

					that.CellPlotStretches.AddColumn(0,"_index","Index",60);
					that.CellPlotStretches.AddColumn(0,"_x_position","x-Position",80);
					that.CellPlotStretches.AddColumn(0,"_y_position","y-Position",80);
					that.CellPlotStretches.AddColumn(0,"_length","Length",60);
					that.CellPlotStretches.AddColumn(0,"_direction","Direction",60);
					that.CellPlotStretches.AddColumn(0,"_score","Score",50);
					that.CellPlotStretches.AddColumn(0,"_include","Cluster",50);

					that.overviewTreeForm.render();
					that.cellplotTreeForm.render();
					that.UpdateControlInfo();
					that.DotPlotControls.render();
					
					Msg.listen('',{ type: 'ChangeTab'}, function(context,id) {
						if (id=="cellplottab") that.cellplottabSelected();
						if (id=="settingstab") that.settingstabSelected();
					});
					Msg.listen('',{ type: 'TreeNodeClicked'}, function(msg) {
						if (msg.id=='AlignmentPlaceHolderBranch') that.cellaligntabSelected();
					});
 
					that.Project.dotplot.dotplotoverview_callback=that.UpdateDotPlotCallback;
					that.DotPlotOverview.SetProject(that.Project);
					that.CircleOverview.SetProject(that.Project);
					that.CellPlotGraph.SetProject(that.Project);
				}

				that.CreateCellPlotButtons = function()
				{
					var examples = [];
					   
					var bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Align',content:'Align',width:150,height:22});
					bt.setOnChanged(function() {that.RunAlignment();})
					examples.push(bt);


					var bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Variations',content:'Search variations',width:150,height:22});
					bt.setOnChanged(function() {that.FindVariations();})
					examples.push(bt);

					bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'UniqueSeq',content:'Search unique sequences',width:150,height:22});
					bt.setOnChanged(function() {that.FindUniqueSequences();})
					examples.push(bt);

					bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'BestHits',content:'Search best hits',width:150,height:22});
					bt.setOnChanged(function() {that.FindBestHits();})
					examples.push(bt);

					return Controls.CompoundVert(examples).setAutoFillX(false);
				}
				that.CreateCellPlotDetailButtons = function() 
				{
					var examples = [];
					bt=Controls.Button(null, {buttonClass: 'DQXToolButtonBar',description:'Adapt Y filter zone',bitmap:'bitmaps/previous_item.png',width:22,height:22});
					bt.setOnChanged(function() {that.AdaptYZone();})
					examples.push(bt);
					return Controls.CompoundHor(examples).setAutoFillX(false);
				}
				that.createPanelForm = function() 
				{
					// Create a form-type panel and attach it to the frame this.frame1
					this.panelForm = Framework.Form(this.dotplotcontrolspanel);
					this.panelForm.setPadding(10);

	 
					this.DotPlotControls=FrameTree.Tree(this.dotplotcontrolspanel);
					var subbranch1=this.DotPlotControls.root;
					var subbranch2=subbranch1.addItem(FrameTree.Branch('HomolStretch', DocEl.StyledText('Homology stretches', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
					var branch=subbranch2.addItem(FrameTree.Branch('Settings', DocEl.StyledText('Settings', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
					{
						this.branchCalculation=branch.addItem(FrameTree.Branch('Calculation', DocEl.StyledText('Calculation', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
							this._reflexCalc=Controls.Check(null, {label:"Reflexive calculation",value:that.seedcompsettings.Reflexive_calculation})
							this._reflexCalc.setOnChanged(function(controlID,theControl) {that.seedcompsettings.Reflexive_calculation=theControl.getValue();that.NeedDotPlotUpdate();})

							this._seedPattern=Controls.Edit(null, {label:'Seed pattern',value:that.seedcompsettings.Seed_pattern})
							this._seedPattern.setOnChanged(function(controlID,theControl) {that.seedcompsettings.Seed_pattern=theControl.value;that.NeedDotPlotUpdate();})

							this._seed_hit_frequency_cutoff=Controls.Edit(null, {label:'Seed hit frequency cutoff',value:that.seedcompsettings.Seed_hit_frequency_cutoff})
							this._seed_hit_frequency_cutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=1;that.seedcompsettings.Seed_hit_frequency_cutoff=val;that.NeedDotPlotUpdate();})

							this._seed_extension_allowed_mismatches=Controls.Edit(null, {label:'Seed extension allowed mismatches',value:that.seedcompsettings.Seed_extension_allowed_mismatches})
							this._seed_extension_allowed_mismatches.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=10;that.seedcompsettings.Seed_extension_allowed_mismatches=val;that.NeedDotPlotUpdate();})

							this._seed_extension_window_size=Controls.Edit(null, {label:'Seed extension window size',value:that.seedcompsettings.Seed_extension_window_size})
							this._seed_extension_window_size.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=25;that.seedcompsettings.Seed_extension_window_size=val;that.NeedDotPlotUpdate();})

							this._scan_increment=Controls.Edit(null, {label:'Scan increment',value:that.seedcompsettings.Scan_increment})
							this._scan_increment.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=10;that.seedcompsettings.Scan_increment=val;that.NeedDotPlotUpdate();})

							this._loop_increment=Controls.Edit(null, {label:'Loop increment',value:that.seedcompsettings.Loop_increment})
							this._loop_increment.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=10;that.seedcompsettings.Loop_increment=val;that.NeedDotPlotUpdate();})

							this._stretch_identity_cutoff=Controls.Edit(null, {label:'Stretch identity cutoff',value:that.seedcompsettings.Stretch_identity_cutoff})
							this._stretch_identity_cutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=70;that.seedcompsettings.Stretch_identity_cutoff=val;that.NeedDotPlotUpdate();})

							this._stretch_length_cutoff=Controls.Edit(null, {label:'Stretch length cutoff',value:that.seedcompsettings.Stretch_length_cutoff})
							this._stretch_length_cutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=70;that.seedcompsettings.Stretch_length_cutoff=val;that.NeedDotPlotUpdate();})

							this._stretch_extension_window_size=Controls.Edit(null, {label:'Stretch extension window size',value:that.seedcompsettings.Stretch_extension_window_size})
							this._stretch_extension_window_size.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=20;that.seedcompsettings.Stretch_extension_window_size=val;that.NeedDotPlotUpdate();})

							this._applyFilter=Controls.Check(null, {label:"Apply repeat filter",value:that.seedcompsettings.Apply_filter})
							this._applyFilter.setOnChanged(function(controlID,theControl) {that.seedcompsettings.Apply_filter=theControl.getValue();that.NeedDotPlotUpdate();})

							this._filter_frequency_cutoff=Controls.Edit(null, {label:'Filter repeat frequency cutoff',value:that.seedcompsettings.Filter_frequency_cutoff})
							this._filter_frequency_cutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=1;that.seedcompsettings.Filter_frequency_cutoff=val;that.NeedDotPlotUpdate();})

							var grid=Controls.CompoundGrid();                     // Instruct this compound control to use only the X space required by the members, rather than the full size
							grid.setAutoFillX(false).setCssClass('DQXGridFrame');// Defines a titled box around the controls
							grid.sepH=40;grid.sepV=0;
							grid.setLegend('nihil');
							grid.setItem(0,0,this._reflexCalc);
							grid.setItem(1,0,this._seedPattern);
							grid.setItem(2,0,this._seed_hit_frequency_cutoff);
							grid.setItem(3,0,this._seed_extension_allowed_mismatches);
							grid.setItem(4,0,this._seed_extension_window_size);
							grid.setItem(5,0,this._scan_increment);
							grid.setItem(6,0,this._loop_increment);
							grid.setItem(0,1,this._stretch_identity_cutoff);
							grid.setItem(1,1,this._stretch_length_cutoff);
							grid.setItem(2,1,this._stretch_extension_window_size);
							grid.setItem(3,1,this._applyFilter);
							grid.setItem(4,1,this._filter_frequency_cutoff);
							this.branchCalculation.addItem(FrameTree.Control(grid));

						this.branchFilter=branch.addItem(FrameTree.Branch('Filtering', DocEl.StyledText('Filtering', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);


							this._FilterX=Controls.Check(null, {label:'Apply on horizontal sequence',value:that.filtersettings.filterX})
							this._FilterX.setOnChanged(function(controlID,theControl) {that.filtersettings.filterX=theControl.getValue();that.NeedDotPlotUpdate();})

							this._FilterY=Controls.Check(null, {label:'Apply on vertical sequence',value:that.filtersettings.filterY})
							this._FilterY.setOnChanged(function(controlID,theControl) {that.filtersettings.filterY=theControl.getValue();that.NeedDotPlotUpdate();})
							
							this._RepeatMin=Controls.Edit(null, {label:'Minimal repeat count cutoff',value:0})
							this._RepeatMin.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;if (val>100) val=100;that.filtersettings.repeatMin=val;that.NeedDotPlotUpdate();})

							this._RepeatMax=Controls.Edit(null, {label:'Maximal repeat count cutoff',value:0})
							this._RepeatMax.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;if (val>100) val=100;that.filtersettings.repeatMax=val;that.NeedDotPlotUpdate();})

							this._LengthMin=Controls.Edit(null, {label:'Minimal stretch length',value:0})
							this._LengthMin.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;that.filtersettings.lengthMin=val;that.NeedDotPlotUpdate();})

							this._LengthMax=Controls.Edit(null, {label:'Maximal stretch length',value:0})
							this._LengthMax.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;that.filtersettings.lengthMax=val;that.NeedDotPlotUpdate();})

							var grid=Controls.CompoundGrid();                     // Instruct this compound control to use only the X space required by the members, rather than the full size
							grid.setAutoFillX(false).setCssClass('DQXGridFrame');// Defines a titled box around the controls
							grid.sepH=40;
							grid.sepV=0;
							grid.setLegend('nihil');
							grid.setItem(0,0,this._FilterX);
							grid.setItem(1,0,this._FilterY);
							grid.setItem(2,0,this._RepeatMin);
							grid.setItem(3,0,this._RepeatMax);
							grid.setItem(0,1,this._LengthMin);
							grid.setItem(1,1,this._LengthMax);
							this.branchFilter.addItem(FrameTree.Control(grid));
							
						this.branchAlignCell=branch.addItem(FrameTree.Branch('Alignment', DocEl.StyledText('Alignment', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
							this.AlignGapCost=Controls.Edit(null, {label:'Gap cost',value:that.alignsettings.gapcost})
							this.AlignGapCost.setOnChanged(function(controlID,theControl) {var val=parseFloat(theControl.value);if (isNaN(val)) val=0;that.alignsettings.gapcost=val;that.NeedDotPlotUpdate();})

							this.AlignUnitCost=Controls.Edit(null, {label:'Unit cost',value:that.alignsettings.unitcost})
							this.AlignUnitCost.setOnChanged(function(controlID,theControl) {var val=parseFloat(theControl.value);if (isNaN(val)) val=0;that.alignsettings.unitcost=val;that.NeedDotPlotUpdate();})

							this.AlignHeadToTailCutoff=Controls.Edit(null, {label:'Stretch head-to-tail distance cutoff (bp)',value:that.alignsettings.head_to_tail_cutoff})
							this.AlignHeadToTailCutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;that.alignsettings.head_to_tail_cutoff=val;that.NeedDotPlotUpdate();})

							this.AlignDiagonalCutoff=Controls.Edit(null, {label:'Stretch diagonal distance cutoff (bp)',value:that.alignsettings.diagonal_cutoff})
							this.AlignDiagonalCutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;that.alignsettings.diagonal_cutoff=val;that.NeedDotPlotUpdate();})

							this.AlignStretchClusterCutoff=Controls.Edit(null, {label:'Minimal stretch count clustering',value:that.alignsettings.stretch_cluster_cutoff})
							this.AlignStretchClusterCutoff.setOnChanged(function(controlID,theControl) {var val=parseInt(theControl.value);if (isNaN(val)) val=0;that.alignsettings.stretch_cluster_cutoff=val;that.NeedDotPlotUpdate();})

							this.AlignStretchScoreCutoff=Controls.Edit(null, {label:'Distance score cutoff',value:that.alignsettings.score_cutoff})
							this.AlignStretchScoreCutoff.setOnChanged(function(controlID,theControl) {var val=parseFloat(theControl.value);if (isNaN(val)) val=0;that.alignsettings.score_cutoff=val;that.NeedDotPlotUpdate();})

							this.AlignStretchFillGaps=Controls.Check(null, {label:'Fill gaps',value:that.alignsettings.fill_gaps})
							this.AlignStretchFillGaps.setOnChanged(function(controlID,theControl) {that.alignsettings.fill_gaps=theControl.getValue();that.NeedDotPlotUpdate();})
							
							var grid=Controls.CompoundGrid();                     // Instruct this compound control to use only the X space required by the members, rather than the full size
							grid.setAutoFillX(false).setCssClass('DQXGridFrame');// Defines a titled box around the controls
							grid.sepH=40;
							grid.sepV=0;
							grid.setLegend('nihil');
							grid.setItem(0,0,this.AlignGapCost);
							grid.setItem(1,0,this.AlignUnitCost);
							grid.setItem(2,0,this.AlignHeadToTailCutoff);
							grid.setItem(3,0,this.AlignDiagonalCutoff);
							grid.setItem(0,1,this.AlignStretchClusterCutoff);
							grid.setItem(1,1,this.AlignStretchScoreCutoff);
							grid.setItem(2,1,this.AlignStretchFillGaps);
							this.branchAlignCell.addItem(FrameTree.Control(grid));
						
//						this.branchCalculate=branch.addItem(FrameTree.Branch('Calculations', DocEl.StyledText('Tasks', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
						var branchSettStatus=branch;
							var bt0=Controls.Static('Status: ');
							that.defaultsettbutton=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Default settings',content:'Defaults',width:80,height:18});
							that.defaultsettbutton.setOnChanged(function() {that.AdaptStandardSettings();})
							that.undosettbutton=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Undo changes, adapt last settings',content:'Undo changes',width:100,height:18});
							that.undosettbutton.setOnChanged(function() {that.UndoChangeSettings();})
							var sep1=Controls.HorizontalSeparator(30)
							that.adaptsettbutton=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Adapt changed settings',content:'Adapt changes',width:100,height:18});
							that.adaptsettbutton.setOnChanged(function() {that.AdaptChangeSettings();})
							var sep2=Controls.HorizontalSeparator(10)
							that.notestatic=Controls.Static('').makeComment();;
							that.adaptsettbutton.enable(false);
							that.undosettbutton.enable(false);
							var comp=Controls.CompoundHor([bt0,that.defaultsettbutton,that.undosettbutton,sep1,that.adaptsettbutton,sep2,that.notestatic]);
							branchSettStatus.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem4');
							
					var branchCalcStatus=subbranch2.addItem(FrameTree.Branch('CalcStat', DocEl.StyledText('Calculation status', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
							that.calcStatic=Controls.Static('Project calculation is up to date');
							that.calcStatusStatic=Controls.Static('');
							var comp=Controls.CompoundHor([that.calcStatic,that.calcStatusStatic]);
							branchCalcStatus.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem4');

							that.calcButton=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Run',content:'Update calculations',width:120,height:18});
							that.calcButton.setOnChanged(function() {that.UpdateRun();})
							var comp=Controls.CompoundHor([that.calcButton]);
							branchCalcStatus.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem4');

				//			var bt0=Controls.Static('');var bt0=Controls.PlaceHolder('SpacePlaceHolder',300,1);branchCalcStatus.addItem(FrameTree.Control(bt0)).setCanSelect(true).setCssClass('DQXTreeItem4');// een separator
						}
							
					var subbranch3=subbranch1.addItem(FrameTree.Branch('HomolStat', DocEl.StyledText('Homology statistics', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
							var comp=that.CreateCellPlotButtons();
							subbranch3.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem4');


					var subbranch4=subbranch1.addItem(FrameTree.Branch('MultAlign', DocEl.StyledText('Multiple Alignment', 'DQXLarge'))).setCanSelect(false).setCollapsed(false);
							var items=[];
							var comboStates=[];
							var OrgTemplateSelect=Controls.Combo(null,{label:'Template Organism',states: comboStates,value:'state1',hint:'Template Organism'});
							OrgTemplateSelect.setOnChanged(function(controlID, theControl) 
							{
								var templOrg=theControl._selectedState;
								var orgCluster=that.Project.GetOrganismCluster(templOrg);
								that.multialignsettings.Templates.length=0;
								for (var i=0;i<orgCluster.GetClassObjectCnt();i++) that.multialignsettings.Templates.push(orgCluster.GetClassObject(i).GetUniqueCode());
							})
							var comp=Controls.CompoundHor([OrgTemplateSelect]);
							subbranch4.addItem(FrameTree.Control(comp)).setCanSelect(true).setCssClass('DQXTreeItem4');
							
							for (var i=0;i<that.Project.organismClusters.length;i++)
							{
								var orgcluster=that.Project.organismClusters[i];
								var obj={id:orgcluster.definition,name:orgcluster.definition};
								comboStates.push(obj);
							}
							if (comboStates.length>0) OrgTemplateSelect.setItems(comboStates,comboStates[0].name);
							var multiAlignButton=Controls.Button(null, {buttonClass: 'DQXToolButtonBar', description:'Multiple Align',content:'Multiple Align',width:120,height:18});
							multiAlignButton.setOnChanged(function() {that.RunMultipleAlign();})
							subbranch4.addItem(FrameTree.Control(multiAlignButton));
				}
				that.UpdateFacts=function()
				{
					this.dotplottabs.switchTab('overviewtab');
				}
				that.CreateDotPlotInputFile=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					Application.DataBaseModule.CaptureSelectionInProject(that.Project);
					return;
					dotplot.CreateDotPlotInputFile();
				}
				that.CreateSeedCompSettingsFile=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.CreateSeedCompSettingsFile();
				}
				that.NeedDotPlotUpdate=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.needUpdate=true;
					that.UpdateControlInfo();
					that.setDotPlotUpdateButtonStatus();
				}
				that.AdaptStandardSettings=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.StandardSettings(that.seedcompsettings,that.filtersettings,that.alignsettings,that.multialignsettings);
					that.NeedDotPlotUpdate();
				}
				that.UndoChangeSettings=function()
				{
					that.seedcompsettings=$.extend(true,{},that.Project.getDotPlotSeedCompSettings());
					that.filtersettings=$.extend(true,{},that.Project.getDotPlotFilterSettings());
					that.alignsettings=$.extend(true,{},that.Project.getDotPlotAlignSettings());
					that.multialignsettings=$.extend(true,{},that.Project.getDotPlotMultiAlignSettings());
					that.NeedDotPlotUpdate();
				}
				that.AdaptChangeSettings=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					var update_matrix=false;
					if (dotplot.seedcompsettings.Reflexive_calculation!=that.seedcompsettings.Reflexive_calculation) update_matrix=true;
					dotplot.ImportSettings(that.seedcompsettings,that.filtersettings,that.alignsettings,that.multialignsettings);
					if (update_matrix==true) that.Project.ReFormatDotPlot();
					that.setDotPlotUpdateButtonStatus();
				}
				
				that.UpdateDotPlotCallback=function(json)
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
	//				that.DotPlotOverview.UpdateFacts(true);
	//				that.setDotPlotUpdateButtonStatus(); 
					if (json.action=="UpdateStatus")
					{
						that.DotPlotOverview.invalidate();
						that.CircleOverview.invalidate();
						return;
					}
					if (json.action=="StretchCalculationStatus")
					{
						that.calcStatic.modifyValue("Homology calculation: Running...");
						that.calcStatusStatic.modifyValue(json.status+"%");
					}
					if (json.action=="StretchAlignStatus")
					{
						that.calcStatic.modifyValue("Alignment calculation: Running...");
						that.calcStatusStatic.modifyValue(json.status+"%");
					}
					if (json.action=="StretchAlignmentFinished" || json.action=="StretchCalculationFinished")
					{
						that.calcStatic.modifyValue("Project is up to date");
						that.calcStatusStatic.modifyValue("");
					}
					if (json.action=='ImportAlignBlocks')
					{
						that.UpdateCellAlign(json.blocks);
					}
					if (json.action=='ImportVariations')
					{
						that.UpdateCellVariations();
					}
					if (json.action=='UpdateCell')
					{
						that.UpdateCellCallback(json);
					}
					if (json.action=="FinalUpdate")
					{
						that.seedcompsettings=$.extend(true,{},that.Project.getDotPlotSeedCompSettings());
						that.filtersettings=$.extend(true,{},that.Project.getDotPlotFilterSettings());
						that.alignsettings=$.extend(true,{},that.Project.getDotPlotAlignSettings());
						that.multialignsettings=$.extend(true,{},that.Project.getDotPlotMultiAlignSettings());
						that.UpdateControlInfo();
						that.DotPlotOverview.UpdateFacts(true);
						that.CircleOverview.UpdateFacts(true);
						that.CellPlotGraph.UpdateFacts(true);
					}
					if (json.action=="ImportBestHits")
					{
						var substack=that.dotplottabs.addMemberFrame(Framework.FrameGroupVert('hulu',1)).setDisplayTitle('Best Hits');
						var substack1=substack.addMemberFrame(Framework.FrameGroupVert('',0.5));
					
						var treeFrame=substack1.addMemberFrame(Framework.FrameFinal('Tree', 0.9)).setAllowScrollBars(false,true);
						that.BestHitsTreeForm=FrameTree.Tree(treeFrame);

						that.BestHitsPlaceHolder=Controls.PlaceHolder('BestHitsPlaceHolder',0,400);
						var branch=that.BestHitsTreeForm.root.addItem(FrameTree.Branch("BestHitsPlaceHolderBranch", DocEl.StyledText("Best Hits", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
						branch.addItem(FrameTree.Control(that.BestHitsPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
						that.BestHitsPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.BestHitsPlaceHolder.getFullID(''), 0.0));
						var _branch2=branch.addItem(FrameTree.Branch("OverviewPlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
						{
							var check=Controls.Check(null, {label:"Circular Maps",value:false});
							check.setOnChanged(function(controlID,theControl) {if (theControl.getValue()==true) that.DotPlotOverview.setCircularMaps(true);else that.DotPlotOverview.setCircularMaps(false)});
							var comp=Controls.CompoundHor([check]);
							_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
						}

						that.BestHitsPDFPlaceHolder=Controls.PlaceHolder('BestHitsPDFPlaceHolder',0,800);
						var branch=that.BestHitsTreeForm.root.addItem(FrameTree.Branch("BestHitsPDFPlaceHolderBranch", DocEl.StyledText("Best Hits PDF", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
						branch.addItem(FrameTree.Control(that.BestHitsPDFPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
						
						var clientdiv=that.dotplottabs.getClientDivID();
						var tabset=$('#'+that.dotplottabs.getClientDivID());
						$(tabset).children('.DQXTabs').children('.DQXTab').removeClass('DQXTabActive');
						$(tabset).children('.DQXTabs').children('.DQXTab').addClass('DQXTabInactive');
						
						var divid=substack.myID;
						var html=substack._createElements(1).toString();
						$('#'+clientdiv+'_tabbody').append("<div id=C"+clientdiv+"_tab_"+"hulu"+" class=\"DQXTabContent\" style=\"display: inline\">"+html+'</div>');
						$('#'+clientdiv+'_tabheader').append("<div id="+clientdiv+"_tab_"+"hulu"+" class=\"DQXTab DQXTabActive\" style</div>");
						substack._postCreateHTML();//om de mousedown & mousemove events te initieren
						substack1.hasiFrame=true;

						that.dotplottabs._reactClickTab('',clientdiv+"_tab_"+"hulu");
						that.dotplottabs._performSwitchTab(clientdiv+"_tab_"+"hulu");
						that.frameRoot._uuuupdate();
						that.BestHitsTreeForm.render();
						
						that.BestHits=DotPlotOverview.BestHits(that.BestHitsPlaceHolder.getFullID(''),this);
						that.BestHitsPanelGhost.SetClientObject(that.BestHits);
						that.BestHitsPlaceHolder.modifyValue(that.BestHits.canvasStr);
						that.BestHits.setData(that.Project.dotplot.besthits);
						that.BestHits.AddColumn(0,"_index","Index",60);
						that.BestHits.AddColumn(0,"_code1","Sequence 1",150);
						that.BestHits.AddColumn(0,"_code2","Sequence 2",150);
						that.BestHits.AddColumn(0,"_xpos","Position 1",100);
						that.BestHits.AddColumn(0,"_ypos","Position 2",100);
						that.BestHits.AddColumn(0,"_len","Length",80);
						that.BestHits.AddColumn(0,"_ori","Orientation",80);
						
						var url=that.Project.projectPath+"\\sequence_data\\dotplot\\best_hits.pdf";
						that.BestHitsPDFPlaceHolder.modifyValue('<iframe id=\"myiframe\" src=\"'+url+'\" style=\"width:100%;height:100%\"/>');
					}
					if (json.action=="ImportNoHits")
					{
						var substack=that.dotplottabs.addMemberFrame(Framework.FrameGroupVert('hili',1)).setDisplayTitle('Unique Sequences');
						var substack1=substack.addMemberFrame(Framework.FrameGroupVert('',0.5));
					
						var treeFrame=substack1.addMemberFrame(Framework.FrameFinal('Tree', 0.9)).setAllowScrollBars(false,true);
						that.NoHitsTreeForm=FrameTree.Tree(treeFrame);

						that.NoHitsPlaceHolder=Controls.PlaceHolder('NoHitsPlaceHolder',0,400);
						var branch=that.NoHitsTreeForm.root.addItem(FrameTree.Branch("NoHitsPlaceHolderBranch", DocEl.StyledText("Unique Sequences", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
						branch.addItem(FrameTree.Control(that.NoHitsPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
						that.NoHitsPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.NoHitsPlaceHolder.getFullID(''), 0.0));
						var _branch2=branch.addItem(FrameTree.Branch("OverviewPlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
						{
							var check=Controls.Check(null, {label:"Circular Maps",value:false});
							check.setOnChanged(function(controlID,theControl) {if (theControl.getValue()==true) that.DotPlotOverview.setCircularMaps(true);else that.DotPlotOverview.setCircularMaps(false)});
							var comp=Controls.CompoundHor([check]);
							_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
						}

						that.NoHitsPDFPlaceHolder=Controls.PlaceHolder('NoHitsPDFPlaceHolder',0,800);
						var branch=that.NoHitsTreeForm.root.addItem(FrameTree.Branch("NoHitsPDFPlaceHolderBranch", DocEl.StyledText("Unique Sequences PDF", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
						branch.addItem(FrameTree.Control(that.NoHitsPDFPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
						
						var clientdiv=that.dotplottabs.getClientDivID();
						var tabset=$('#'+that.dotplottabs.getClientDivID());
						$(tabset).children('.DQXTabs').children('.DQXTab').removeClass('DQXTabActive');
						$(tabset).children('.DQXTabs').children('.DQXTab').addClass('DQXTabInactive');
						
						var divid=substack.myID;
						var html=substack._createElements(1).toString();
						$('#'+clientdiv+'_tabbody').append("<div id=C"+clientdiv+"_tab_"+"hili"+" class=\"DQXTabContent\" style=\"display: inline\">"+html+'</div>');
						$('#'+clientdiv+'_tabheader').append("<div id="+clientdiv+"_tab_"+"hili"+" class=\"DQXTab DQXTabActive\" style</div>");
						substack._postCreateHTML();//om de mousedown & mousemove events te initieren
						substack1.hasiFrame=true;

						that.dotplottabs._reactClickTab('',clientdiv+"_tab_"+"hili");
						that.dotplottabs._performSwitchTab(clientdiv+"_tab_"+"hili");
						that.frameRoot._uuuupdate();
						that.NoHitsTreeForm.render();
						
						that.NoHits=DotPlotOverview.NoHits(that.NoHitsPlaceHolder.getFullID(''),this);
						that.NoHitsPanelGhost.SetClientObject(that.NoHits);
						that.NoHitsPlaceHolder.modifyValue(that.NoHits.canvasStr);
						that.NoHits.setData(that.Project.dotplot.nohits);
						that.NoHits.AddColumn(0,"_index","Index",60);
						that.NoHits.AddColumn(0,"_code","Sequence",150);
						that.NoHits.AddColumn(0,"_start","Position",150);
						that.NoHits.AddColumn(0,"_len","Length",100);
						
						var url=that.Project.projectPath+"\\sequence_data\\dotplot\\no_hits.pdf";
						that.NoHitsPDFPlaceHolder.modifyValue('<iframe id=\"myiframe\" src=\"'+url+'\" style=\"width:100%;height:100%\"/>');
					}
					if (json.action=="ImportMultiAlign")
					{
						that.MultiAlignOverview.SetProcessing("");
						that.MultiAlignOverview.UpdateFacts(true);
					}
//					that.DotPlotOverview.UpdateFacts(true);
//					that.setDotPlotUpdateButtonStatus(); 
				}
				that.NeedCellPlotUpdate=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.cellStatus="needUpdate";
				}
				that.UpdateCellCallback=function(json)
				{
					if (json.what_is_updated=='invalidate_all') {that.UpdateViews();return;}
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.cellStatus="Updated";
					var cell=that.DotPlotOverview.getSelectedCell();
					if (cell==null) return;
					if (json.what_is_updated=='stretchlist') {that.CellPlotStretches.setCell(cell);return;}

					that.CellPlotGraph.setCell(cell);
					that.CellPlotGraph.UpdateCellCallback();
					that.CellPlotStretches.setCell(cell);
					that.CellPlotStretchDetails.setCell(cell);
					
					that.setDotPlotUpdateButtonStatus();
					that.UpdateControlInfo();

					that.SeqBox.Clear();
					that.SeqBox.AddClassObjectCode(cell.classobjCode1);
					that.SeqBox.AddClassObjectCode(cell.classobjCode2);
					that.SeqBox.OpenSequences(that.LoadStatus,that.OnSequencesOpen);
				}
				that.LoadStatus=function(status)
				{
					that.CellPlotDetails.SetProcessing(status);
				}
				that.setHeatMapColors=function(how)
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.heapMapColors=how;
					that.CellPlotDetails.invalidate();
				}
				that.setAlignmentCircularView=function(how)
				{
					that.CellAlignment.SwitchCircular();
				}
				that.UpdateViews=function()
				{
					var cell=that.DotPlotOverview.getSelectedCell();
					if (cell!=null) that.CellPlotStretches.setCell(cell);					
					that.UpdateControlInfo();
					that.DotPlotOverview.invalidate();
					that.CircleOverview.invalidate();
					that.CellPlotGraph.UpdateFacts(true);
//					that.CellPlotStretches.invalidate();
					that.CellPlotStretchDetails.invalidate();
					that.CellPlotDetails.invalidate();
					that.CellPlotCDetails.invalidate();
					that.CellAlignment.invalidate();
				}
				that.OnSequencesOpen=function()
				{
					that.CellPlotDetails.RequestCallBack();
					
					that.CSeqBox.concatenateGap=Math.floor(that.SeqBox.GetMaxSeqLen()/50);
					that.CSeqBox.copyFromSequenceBox(that.SeqBox);
					that.CellPlotCDetails.RequestCallBack();

					that.AlignedSeqBox.copyFromSequenceBox(that.SeqBox);
				}
				that.UpdateCellAlign=function(blocks)
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					var cell=dotplot.current_cell;
					if (cell==null) return;
					var sequence=that.AlignedSeqBox.GetSequenceFromCode(cell.classobjCode2);
					var entryclient=that.AlignedSeqBox.GetEntryFromCode(cell.classobjCode2);
					if (sequence==null) {console.log("function UpdateCellAlign contains invalid sequence pointer");return;}
					if (entryclient==null) {console.log("function UpdateCellAlign contains invalid entryclient pointer");return;}
					sequence.ResetBlocks();
					for (var i=0;i<blocks.oris.length;i++) 
					{
						var invert=false;
						if (blocks.oris[i]==-1) invert=true;
						sequence.AddBlock(entryclient,blocks.startbasegaps[i],blocks.startbases[i],blocks.endbases[i],invert,0,0);
					}
					that.AlignedSeqBox.UpdateSequenceBitPlots();
					that.CellAlignment.RequestCallBack();
					that.frameRoot._uuuupdate();
					that.CellAlignment.UpdateFacts();
				}
				that.UpdateCellVariations=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					var cell=dotplot.current_cell;
					if (cell==null) return;
					that.CellAlignment.CallBackVarSearch();
				}
				that.setDotPlotUpdateButtonStatus=function() 
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					if (dotplot.StretchesCalculated==0) that.calcStatic.modifyValue("Project should be re-calculated");
					
					var changed1=dotplot.CheckSeedCompSettings(that.seedcompsettings);
					var changed2=2;
					if (dotplot.CheckFilterSettings(that.filtersettings)==0) changed2=0;
					if (dotplot.CheckAlignSettings(that.alignsettings)==0) changed2=0;
					if (changed1==0) changed2=0;
					if (changed1==0 || changed2==0)
					{
						that.adaptsettbutton.enable(true);
						that.undosettbutton.enable(true);
						that.notestatic.modifyValue('Note: adapting changes may imply the project to be recalculated');
					}
					else 
					{
						that.adaptsettbutton.enable(false);
						that.undosettbutton.enable(false);
						that.notestatic.modifyValue('');
					}
					if (dotplot.StretchesCalculated==2 && dotplot.StretchesAligned==2) // alles berekend
					{
						that.calcButton.enable(false);
					}
					if (dotplot.StretchesCalculated==0 || dotplot.StretchesAligned==0) // ��n van de berekeningen moet uitgevoerd worden
					{
						that.calcButton.enable(true);
					}
					if (changed1==0 || changed2==0) that.calcButton.enable(false);//settings zijn veranderd
					that.defaultsettbutton.enable(true);
					if (dotplot.StretchesCalculated==1 || dotplot.StretchesAligned==1)//berekening loopt
					{
						that.defaultsettbutton.enable(false);
						that.adaptsettbutton.enable(false);
						that.undosettbutton.enable(false);
						that.calcButton.enable(false);
					}
				}

				that.UpdateControlInfo=function()
				{
					var seedcompsettings=that.seedcompsettings;
					this._reflexCalc.modifyValue(seedcompsettings.Reflexive_calculation,true);
					this._seedPattern.modifyValue(seedcompsettings.Seed_pattern,true);
					this._seed_hit_frequency_cutoff.modifyValue(seedcompsettings.Seed_hit_frequency_cutoff,true);
					this._seed_extension_allowed_mismatches.modifyValue(seedcompsettings.Seed_extension_allowed_mismatches,true);
					this._seed_extension_window_size.modifyValue(seedcompsettings.Seed_extension_window_size,true);
					this._scan_increment.modifyValue(seedcompsettings.Scan_increment,true);
					this._loop_increment.modifyValue(seedcompsettings.Loop_increment,true);
					this._stretch_identity_cutoff.modifyValue(seedcompsettings.Stretch_identity_cutoff,true);
					this._stretch_length_cutoff.modifyValue(seedcompsettings.Stretch_length_cutoff,true);
					this._stretch_extension_window_size.modifyValue(seedcompsettings.Stretch_extension_window_size,true);
					this._applyFilter.modifyValue(seedcompsettings.Apply_filter,true);
					this._filter_frequency_cutoff.modifyValue(seedcompsettings.Filter_frequency_cutoff,true);

					var filtersettings=that.filtersettings;
					this._FilterX.modifyValue(filtersettings.filterX,true);
					this._FilterY.modifyValue(filtersettings.filterY,true);
					this._RepeatMin.modifyValue(filtersettings.repeatMin,true);
					this._RepeatMax.modifyValue(filtersettings.repeatMax,true);
					this._LengthMin.modifyValue(filtersettings.lengthMin,true);

					var alignsettings=that.alignsettings;
					this.AlignHeadToTailCutoff.modifyValue(alignsettings.head_to_tail_cutoff,true);
					this.AlignStretchClusterCutoff.modifyValue(alignsettings.stretch_cluster_cutoff,true);
					this.AlignGapCost.modifyValue(alignsettings.gapcost,true);
					this.AlignUnitCost.modifyValue(alignsettings.unitcost,true);
					this.AlignHeadToTailCutoff.modifyValue(alignsettings.head_to_tail_cutoff,true);
					this.AlignDiagonalCutoff.modifyValue(alignsettings.diagonal_cutoff,true);
					this.AlignStretchClusterCutoff.modifyValue(alignsettings.stretch_cluster_cutoff,true);
					this.AlignStretchScoreCutoff.modifyValue(alignsettings.score_cutoff,true);
					this.AlignStretchFillGaps.modifyValue(alignsettings.fill_gaps,true);

					var txt="";
					if (filtersettings.lengthMax==Number.MAX_VALUE) txt="Inf";else txt=filtersettings.lengthMax;
					this._LengthMax.modifyValue(txt,true);
				}
				that.settingstabSelected=function()
				{
					return;
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.needUpdate=true;
				}
				that.cellplottabSelected=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					var cell=that.DotPlotOverview.getSelectedCell();
					if (cell==null || cell==dotplot.current_cell || cell.skip==true) return;
					cell.SendServerRequestCellStretchData();
					that.CellPlotGraph.SetProcessing("Fetching stretch data...");
				}
				that.cellaligntabSelected=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					var cell=that.DotPlotOverview.getSelectedCell();
					if (cell==null) return;
					//check of alignment al opgeladen is, indien nieuwe cell, alignment heropladen
					cell.SendServerRequestCellAlign();
				}

				that.LoadDotPlot=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					if (dotplot.needUpdate==false) return;
					dotplot.loadStatus=0.001;
					that.Project.OpenDotPlot(true);
				}
				that.reLoadCell=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					if (dotplot.current_cell!=null) dotplot.current_cell.SendServerRequestCellStretchData();
				}
				that.CellSelectStretch=function(index,xcurpos,update_cellplot,update_cellseqs,update_lists,update_alignment)
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					var cell=dotplot.current_cell;
					if (cell==null) return;
					var stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
					dotplot.GetStretch(index,stretch);
					var xx1=Number.MAX_VALUE;
					var xx2=0;
					var yy1=Number.MAX_VALUE;
					var yy2=0;
					var ycurpos=-1;
					
					if (xcurpos<stretch.x) xcurpos=stretch.x;
					if (xcurpos>stretch.x+stretch.len) xcurpos=stretch.x+stretch.len;
					xx1=Math.min(xx1,stretch.x);
					xx2=Math.max(xx2,stretch.x+stretch.len);
					var y1=stretch.y;
					var y2=stretch.y+stretch.len;
					if (stretch.ori<0)	
					{
						y1=(cell.ll2-1)-(stretch.y+stretch.len);//2
						y2=(cell.ll2-1)-stretch.y;//2
					}
					yy1=Math.min(yy1,y1);
					yy2=Math.max(yy2,y2);
					if (stretch.ori>0 && xcurpos>=stretch.x && xcurpos<=stretch.x+stretch.len) ycurpos=y1+xcurpos-stretch.x;
					if (stretch.ori<0 && xcurpos>=stretch.x && xcurpos<=stretch.x+stretch.len) ycurpos=y2-(xcurpos-stretch.x);
					that.CellPlotGraph.SetSelection(xcurpos,xx1,xx2,ycurpos,yy1,yy2,update_cellplot);
					if (update_lists) that.CellPlotStretches.changeposit(index,0,true);
					if (update_cellseqs) that.CellPlotDetails.SetSelection(xcurpos,ycurpos,stretch);
					if (update_alignment) that.CellAlignment.SetSelection(xcurpos,ycurpos,stretch);
				}
				that.AdaptXZone=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.filtersettings.xZoneFilter=true;
					dotplot.filtersettings.xZoneStart=that.hscale.sel1;
					dotplot.filtersettings.xZoneStop=that.hscale.sel2;
					that.reLoadCell();
				}
				that.AdaptYZone=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.filtersettings.yZoneFilter=true;
					dotplot.filtersettings.yZoneStart=that.vscale.sel1;
					dotplot.filtersettings.yZoneStop=that.vscale.sel2;
					that.reLoadCell();
				}
				that.ReleaseZoneFilter=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.filtersettings.yZoneFilter=false;
					dotplot.filtersettings.xZoneFilter=false;
					dotplot.filtersettings.yZoneStart=0;
					dotplot.filtersettings.yZoneStop=0;
					dotplot.filtersettings.xZoneStart=0;
					dotplot.filtersettings.xZoneStop=0;
					that.reLoadCell();
				}
				that.UpdateRun=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.UpdateRun();
				}
				that.RunCalcCell=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.CreateSeedCompSettingsFile();
					dotplot.DeleteCurrentCell();
				}
				that.RunAlignment=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.RequestStretchAlignment();
				}
				that.FindVariations=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.FindVariations();
				}
				that.FindUniqueSequences=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.RequestFindUniqueSeq();
				}
				that.FindBestHits=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					dotplot.RequestFindBestHits();
				}
				that.LoadSyntenyStretches=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot!=null && dotplot.current_cell!=null) dotplot.StretchDataType="synteny";
					that.reLoadCell();
				}
				that.LoadUniqueStretchesX=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot!=null && dotplot.current_cell!=null) dotplot.StretchDataType="uniqueX";
					that.reLoadCell();
				}
				that.LoadUniqueStretchesY=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot!=null && dotplot.current_cell!=null) dotplot.StretchDataType="uniqueY";
					that.reLoadCell();
				}
				that.overlap=function(all)
				{
					var dotplot=that.Project.dotplot;
					if (dotplot!=null && dotplot.current_cell!=null) 
					{
						if (all==0) dotplot.SelectOverlapStretches(that.CellPlotDetails.selectedStretch);
						if (all==1) dotplot.SetAllStretches();
					}
				}
				that.blastStretch=function()
				{
					var stretch=that.CellPlotDetails.selectedStretch;
					that.CellPlotDetails.GetDirectSequence(stretch.y,stretch.y+stretch.len,stretch.ori);
				}
				that.RunMultipleAlign=function()
				{
					var dotplot=that.Project.dotplot;
					if (dotplot==null) return;
					that.multialign();
					dotplot.RequestMultiAlignment();
				}
				that.multialign=function()
				{
					var substack=that.dotplottabs.addMemberFrame(Framework.FrameGroupVert('multialign',1)).setDisplayTitle('Multiple Alignment');
					var substack1=substack.addMemberFrame(Framework.FrameGroupVert('',0.5));
					
					var treeFrame=substack1.addMemberFrame(Framework.FrameFinal('Tree', 0.9)).setAllowScrollBars(false,true);
					that.MultiAlignTreeForm=FrameTree.Tree(treeFrame);

					that.MultiAlignPlaceHolder=Controls.PlaceHolder('MultiAlignPlaceHolder',0,800);
					var branch=that.MultiAlignTreeForm.root.addItem(FrameTree.Branch("MultiAlignPlaceHolderBranch", DocEl.StyledText("Unique Sequences", 'DQXTreeItem'))).setCanSelect(true).setCollapsed(false);
					branch.addItem(FrameTree.Control(that.MultiAlignPlaceHolder)).setCanSelect(false).setCssClass('DQXTreeSubWindow');;
					that.MultiAlignPanelGhost=treeFrame.addMemberFrameGhost(Framework.FrameGhost(that.MultiAlignPlaceHolder.getFullID(''), 0.0));
					var _branch2=branch.addItem(FrameTree.Branch("OverviewPlaceHolderBranchSub1", DocEl.StyledText("Settings", 'DQXTreeItem'))).setCanSelect(false).setCollapsed(true);
					{
						var check=Controls.Check(null, {label:"Circular Maps",value:false});
						check.setOnChanged(function(controlID,theControl) {if (theControl.getValue()==true) that.DotPlotOverview.setCircularMaps(true);else that.DotPlotOverview.setCircularMaps(false)});
						var comp=Controls.CompoundHor([check]);
						_branch2.addItem(FrameTree.Control(comp)).setCanSelect(false).setCssClass('DQXTreeItem4');
					}

					var clientdiv=that.dotplottabs.getClientDivID();
					var tabset=$('#'+that.dotplottabs.getClientDivID());
					$(tabset).children('.DQXTabs').children('.DQXTab').removeClass('DQXTabActive');
					$(tabset).children('.DQXTabs').children('.DQXTab').addClass('DQXTabInactive');
						
					var divid=substack.myID;
					var html=substack._createElements(1).toString();
					$('#'+clientdiv+'_tabbody').append("<div id=C"+clientdiv+"_tab_"+"multialign"+" class=\"DQXTabContent\" style=\"display: inline\">"+html+'</div>');
					$('#'+clientdiv+'_tabheader').append("<div id="+clientdiv+"_tab_"+"multialign"+" class=\"DQXTab DQXTabActive\" style</div>");
					substack._postCreateHTML();//om de mousedown & mousemove events te initieren
					substack1.hasiFrame=true;

					that.dotplottabs._reactClickTab('',clientdiv+"_tab_"+"multialign");
					that.dotplottabs._performSwitchTab(clientdiv+"_tab_"+"multialign");
					that.frameRoot._uuuupdate();
					that.MultiAlignTreeForm.render();

					that.MultiAlignOverview=DotPlotOverview.MultiAlign(that.MultiAlignPlaceHolder.getFullID(''),this);
					that.MultiAlignPanelGhost.SetClientObject(that.MultiAlignOverview);
					that.MultiAlignPlaceHolder.modifyValue(that.MultiAlignOverview.canvasStr);
					that.MultiAlignOverview.SetProject(that.Project);
					that.MultiAlignOverview.UpdateFacts(true);
				}
				return that;
			}
		}
		return DotPlotModule;
	});