	define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScroller","MRC/MFieldBar","MRC/MScrollList"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScroller, MFieldBar, MScrollList) 
	{
		var ScopeBoxList = {};
		ScopeBoxList.Init = function (iParentRef)
		{
			var that = MScrollList.Init(iParentRef);
			that.Update=function()
			{
				dbobject=Application.DbObject;
//				that.SetDataCount(dbobject.scope.GetItemCnt());
				that.SetDataCount(Application.GetTrackCount());
				that.render();
			}
			that.Clear=function()
			{
				dbobject=Application.DbObject;
				dbobject.scope.Clear();
				that.SetDataCount(0);
				that.render();
			}
			that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				var track=Application.GetTrack(Application.GetTrackCount()-1-rowindex);
				if (track==null) return;
				var txt="?";
				if (field.fieldcode==='index') txt=track.description;
				ctx.fillText(txt,px+4,py+2);
				return;
				dbobject=Application.DbObject;
				var scopeItem=dbobject.scope.GetItem(rowindex);
//					ctx.fillRect(px,py,sizex,sizey);
				if (scopeItem!=null)
				{
					var classobject=scopeItem.classobject;
					var d=5;
					if (field.fieldcode==='index') 
					{
						ctx.fillStyle=scopeItem.color;
						ctx.fillRect(px,py,px+sizey,sizey);
						d=sizey+5;
					}
					ctx.fillStyle='rgba(0,0,0,1.0)';
					var txt="";
					if (field.fieldcode==='index') txt=scopeItem.organism;
					if (field.fieldtext==="Data Item") txt=classobject.GetFieldData();
					if (field.fieldtext==="Code") txt=classobject.GetUniqueCode();
					if (field.fieldtext==="File") txt=classobject.GetDataFile();
					if (field.fieldtext==="Organism") txt=classobject.GetFullDefiner(txt);
					ctx.fillText(txt,px+d,py+2);
				}
			}
			that.ListMouseDown=function(posx,posy)
			{
			}
			return that;
		}
		return ScopeBoxList;
   });
