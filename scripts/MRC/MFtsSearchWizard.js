define(["require", "DQX/Application","DQX/Framework", "DQX/Controls", "DQX/Msg", "DQX/SQL", "DQX/DocEl", "DQX/Popup", "DQX/Wizard"],
    function (require, Application, Framework, Controls, Msg, SQL, DocEl, Popup, Wizard) 
	{
        var MFtsSearchWizard = {};
        MFtsSearchWizard.init=function(data)
		{
            var that={};
			that.wizard=Wizard.Create("WizardFindGene");
			that.wizard.setTitle("Find features");
			that.wizard._sizeY=350;
			that.data=data;
			var buttonList=[];
			
			var comment=Controls.Static('Select feature type(s):').makeComment();
			buttonList.push(comment);

            that.FeatureTypeSelect=Controls.List(null,{width:230,height : 160,checkList:true});
            that.FeatureTypeSelect.setOnChanged(function(controlID, theControl) {that.GetCheckedItems();})
			buttonList.push(that.FeatureTypeSelect);

			listitems=[];
			var emblgnbkfeatures=Application.EmblGnbk.features;
			for (var i=0;i<emblgnbkfeatures.length;i++)
			{
				var value=0;
	//			if (emblgnbkfeatures[i].key==="CDS") value=1;
				var obj={id:i,content:emblgnbkfeatures[i].key,checked:value};
				listitems.push(obj);
			}
			that.FeatureTypeSelect.setItems(listitems);
//			that.data.features.push("CDS");
			
			comment=Controls.Static('Enter specific text to be searched for within qualifiers:').makeComment();
			buttonList.push(comment);
			
			that.searchText=Controls.Edit('FeatureSearchText', { size: 50 }).setHasDefaultFocus();
			that.searchText.setOnChanged(function(controlID, theControl) {that.data.searchtext=theControl.getValue();});
			buttonList.push(that.searchText);

			that.wizard.addPage({id: 'init',helpUrl: 'Doc/WizardFindGene/Help.htm',form: Controls.CompoundVert([Controls.CompoundVert(buttonList), Controls.CompoundHor([])]),hideNext: true	});

			that.execute=function(sendfunction)
			{
				that.wizard.run(sendfunction);
			}
			that.GetCheckedItems=function()
			{
				that.data.features.length=0;
//                items=that.FeatureTypeSelect.getCheckedItems();
				for (var i=0;i<that.FeatureTypeSelect._items.length;i++) 
				{
                    if (that.FeatureTypeSelect._items[i].checked) that.data.features.push(that.FeatureTypeSelect._items[i].content);
				}
			}
			that.FindMatchingGenes=function()
			{
			}
			return that;
		}
        return MFtsSearchWizard;
    });
