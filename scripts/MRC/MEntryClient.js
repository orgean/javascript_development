
MEntryClient=function()
{
	var that={};
	that.identifier="";
	that.format=-1;
	that.changed=false;
	
	that.sequence=null;
	that.header=null;;
	that.featuretable=null;

	that.color=('#'+Math.floor(Math.random()*16777215).toString(16));

	that.GetColor=function()
	{
		return that.color;
	}
	that.SetHeader=function(_header)
	{
		that.header=_header;
	}
	that.SetFeatureTable=function(_featuretable)
	{
		that.featuretable=_featuretable;
	}
	that.SetSequence=function(_sequence)
	{
		that.sequence=_sequence;
	}
	that.GetLength=function()
	{
		if (that.sequence!=null) return that.sequence.seqlen;
		return 0;
	}
	that.GetFtsCnt=function()
	{
		if (that.featuretable!=null) return that.featuretable.ftrs.length;
		return 0;
	}
	that.GetFts=function(index)
	{
		if (that.CheckFeatureTableIndex(index)==false) return null;
		return that.featuretable.ftrs[index];
	}
	that.GetFtsStart=function(index)
	{
		if (that.CheckFeatureTableIndex(index)==false) return -1;
		return that.featuretable.ftrs[index].start;
	}
	that.GetFtsStop=function(index)
	{
		if (that.CheckFeatureTableIndex(index)==false) return -1;
		return that.featuretable.ftrs[index].stop;
	}
	that.GetFtsKey=function(index)
	{
		if (that.CheckFeatureTableIndex(index)==false) return -1;
		return that.featuretable.ftrs[index].key;
	}
	that.CheckFeatureTableIndex=function(index)
	{
		if (that.featuretable==null) return false;
		if (index<0) return false;
		if (index>=that.featuretable.ftrs.length) return false;
		return true;
	}
	that.GetBase=function(pos)
	{
		if (that.sequence==null) return ' ';
		var k=parseInt(pos/that.sequence.seqblocksize);
		if (k>=0 && k<that.sequence.seqblocks.length)
		{
			var seqblk=that.sequence.seqblocks[k];
			ps=pos-seqblk.offset;
			if (ps>=0 && ps<seqblk.seqlen) return seqblk.seq[ps]; 
		}
		for (var i=0;i<that.sequence.seqblocks.length;i++)
		{
			var seqblk=that.sequence.seqblocks[i];
			ps=pos-seqblk.offset;
			if (ps>=0 && ps<seqblk.seqlen) return seqblk.seq[ps]; 
		}
		return ' ';
	}
	that.GetBaseFast=function(obj)//obj.seqblk,obj.pos,obj.base
	{
		if (obj.seqblk!=null)
		{
			var ps=obj.pos-obj.seqblk.offset;
			if (ps>=0 && ps<obj.seqblk.seqlen) {obj.base=obj.seqblk.seq[ps];return;}//gevonden
		}
		for (var i=0;i<that.sequence.seqblocks.length;i++)//zoek nieuw block
		{
			var seqblk=that.sequence.seqblocks[i];
			if (seqblk.offset+seqblk.seqlen<obj.pos) continue;
			var ps=obj.pos-seqblk.offset;
			if (ps>=0 && ps<seqblk.seqlen) 
			{
				obj.base=seqblk.seq[ps];
				obj.seqblk=seqblk;
				return;
			}
		}
		obj.seqblk=null;
		obj.base=' ';
	}
	that.GetCodonFast=function(obj)//obj.seqblk,obj.pos,obj.codon
	{
		if (obj.seqblk!=null)
		{
			var ps=obj.pos-obj.seqblk.offset;
			if (ps>=0 && ps<obj.seqblk.seqlen-2) 
			{
				obj.codon[0]=obj.seqblk.seq[ps];
				obj.codon[1]=obj.seqblk.seq[ps+1];
				obj.codon[2]=obj.seqblk.seq[ps+2];
				return;
			}//gevonden
		}
		for (var i=0;i<that.sequence.seqblocks.length;i++)//zoek nieuw block
		{
			var seqblk=that.sequence.seqblocks[i];
			if (seqblk.offset+seqblk.seqlen<obj.pos) continue;
			var ps=obj.pos-seqblk.offset;
			if (ps>=0 && ps<seqblk.seqlen-2) 
			{
				obj.codon[0]=seqblk.seq[ps];
				obj.codon[1]=seqblk.seq[ps+1];
				obj.codon[2]=seqblk.seq[ps+2];
				obj.seqblk=that.sequence.seqblocks[i];
				return;
			}
		}
		obj.seqblk=null;
		obj.codon[0]=' ';
		obj.codon[1]=' ';
		obj.codon[2]=' ';
	}
	
	that.GetAccession=function()
	{
		return that.header.identifier;
	}
	that.GetFtsCodon=function(fts,base)
	{
//		var ftrs=that.featuretable.ftrs;
//		if (fnr<0 || fnr>=ftrs.length) return -1;
//		var fts=ftrs[fnr];
		if (fts.key!="CDS") return -1;
		var codon_start=0;
		var qnr=-1;
		for (var i=0;i<fts.qlfs.length;i++)
		{
			var qlf=fts.qlfs[i];
			if (qlf.key==="/codon_start=") qnr=i;
		}
		if (qnr!=-1) codon_start=parseInt(fts.qlfs[qnr].item)-1;
		var exon=-1;var intron=-1;var len=0;
		for (var i=0;i<fts.exons.length;i++)
		{
			if (base>=fts.exons[i].start && base<=fts.exons[i].stop) {exon=i;break;}
			len+=fts.exons[i].stop-fts.exons[i].start+1;
			if (i>0 && base>fts.exons[i-1].stop && base<fts.exons[i].start) {intron=i;break;}
		}
		if (intron==-1 && exon==-1) return -1;
		if (intron>-1) return 0;
		len+=base-fts.exons[exon].start-codon_start;
		if (fts.dir==-1) 
		{
			var ftslen=0;
			for (var i=0;i<fts.exons.length;i++) ftslen+=fts.exons[i].stop-fts.exons[i].start+1;
			len=ftslen-len;
		}
		qnr=-1;
		for (var i=0;i<fts.qlfs.length;i++)
		{
			var qlf=fts.qlfs[i];
			if (qlf.key==="/translation=") qnr=i;
		}
		if (qnr!=-1) transl=fts.qlfs[qnr].item;else return -1;
		var rem=len%3;
		len=~~(len/3);
		if (fts.dir>=0)
		{
			if (rem==1) 
			{
				if (len==transl.length) return '*';
				if (len>=0 && len<transl.length) return transl[len];
				else return ' ';
			}
			if (rem==0) return 1;
			if (rem==2) return 3;
		}
		if (fts.dir==-1)
		{
			if (rem==2) 
			{
				if (len==transl.length) return '*';
				if (len>=0 && len<transl.length) return transl[len];
				else return ' ';
			}
			if (rem==0) return 3;
			if (rem==1) return 1;
		}
		return -1;
	}
	return that;
}
