/************************************************************************************************************************************
 *************************************************************************************************************************************

 A FramePanel that implements a html Canvas drawing area

 *************************************************************************************************************************************
 *************************************************************************************************************************************/
		

define(["jquery", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScroller","MRC/MFieldBar","server/ImageDataObject"],
    function ($, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScroller, MFieldBar) 
	{
        var MScrollWindow = {};
        MScrollWindow.Init = function (iParentRef,_zoom) 
		{
            var that = FramePanel(iParentRef);
			that._panelfirstRendered = false;
            that._toolTipInfo = { ID: null };
            that._directRedraw = true;
			that.processing="";

            that.selectionHorOnly = false;//Set to true to have the user (mouse driven) selection restricted to horizontal areas

            that._canvasLayerIds = ['main','vscroller','hscroller','zscroller','fieldbar','kiwiimage'];
            that.getCanvasID = function(layerid)
			{
				for (var i=0;i<that._canvasLayerIds.length;i++) if (that._canvasLayerIds[i]===layerid)  return that.canvasBaseId+layerid;
				return DQX.reportError('Invalid canvas id: '+layerid);
            };

            that.getMyCanvasElement = function (layerid) 
			{
				var h=$("#" + that.getCanvasID(layerid));
                return h[0];
            }
			
			that.vscrollpos=0;
			that.hscrollpos=0;
			that.zoomscroll=false;
			that.zoomsize=250;
			
			that.zoomscroll=_zoom;
			that.scrollcircular=false;
			that.touchMove=true;
			that.lastX=0;
			that.lastY=0;
			that.mousepressed=false;
			that._cnvWidth=0;
			that._cnvHeight=0;
			that._cnvContext=null;
			that.paintcount=0;
			
            that.canvasBaseId = 'CNV_'+that.getDivID()+'_';

			that.imageData=new ImageDataObject.ImageData();
			var _vscroller=new MScroller.VScroll(that);
			var _hscroller=new MScroller.HScroll(that);
			var _zscroller=new MScroller.ZScroll(that);
			var _fieldbar=new MFieldBar.FieldBar(that);

			that.canvasStr = '';
			{
//				that.canvasStr+='<div id='+that.getDivID()+'_div_fieldbar style="display:none;">';
				var cnv=DocEl.Create('canvas', { id: that.getCanvasID('fieldbar') });
				cnv.addStyle('display', 'none');
				that.canvasStr += cnv.toString();
//				that.canvasStr+='</div>';
			}
			{ 
				that.canvasStr+='<div id='+that.getDivID()+'_div_maincanvas style="float:left; display:inline-block;">';
				var cnv = DocEl.Create('canvas', { id: that.getCanvasID('main') });
				cnv.addStyle('float', 'left');
				cnv.addStyle('display', 'inline-block');
				that.canvasStr+=cnv.toString();
//				that.canvasStr+='<img id="kiwiimage", src="http://s.cdpn.io/3/kiwi.svg" style="position: absolute; left: 100px; top:100px; z-index: 2; width: 300px;" />'
				that.canvasStr+='</div>';
			}
			{
//				that.canvasStr+='<div id='+that.getDivID()+'_div_vscroller style="float:right; display:none;">';
				var cnv=DocEl.Create('canvas', { id: that.getCanvasID('vscroller') });
				cnv.addStyle('float', 'right');
				cnv.addStyle('display', 'none');
				that.canvasStr+= cnv.toString();
	//			that.canvasStr+='</div>';
			}
			{
				that.canvasStr+='<div id='+that.getDivID()+'_div_corner style="float:right; display:none;background-color:rgb(240,240,250);box-shadow:inset 0px 0px 0px 1px rgb(200,200,200);">';
				that.canvasStr+='</div>';
			}
			{
//				that.canvasStr+='<div id='+that.getDivID()+'_div_hscroller style="float:right; display:none;">';
				var cnv=DocEl.Create('canvas', { id: that.getCanvasID('hscroller') });
				cnv.addStyle('float', 'right');
				cnv.addStyle('display', 'none');
				that.canvasStr += cnv.toString();
//				that.canvasStr+='</div>';
			}
			{
//				that.canvasStr+='<div id='+that.getDivID()+'_div_zscroller style="float:left; display:none;">';
				var cnv=DocEl.Create('canvas', { id: that.getCanvasID('zscroller') });
				cnv.addStyle('float', 'left');
				cnv.addStyle('display', 'none');
				that.canvasStr += cnv.toString();
//				that.canvasStr+='</div>';
			}
			that.canvasStr+='<p draggable="true" id='+that.getDivID()+'_drag_field style="visibility:hidden;margin:0px;padding:0px;border: 1px solid #aaaaaa"></p>'

		
			$('#' + that.getDivID()).html(that.canvasStr);

//			_vscroller.SetScrollSize(0);
//			_hscroller.SetScrollSize(0);

            that.SetProcessing=function(intext)
			{
				that.processing=intext;
				that.render();
			}
			
			that.FieldBarMoved=function()//overwritten in scrolllist
			{
			}
			that.setDirectDedraw = function(newStatus) 
			{
                that._directRedraw = newStatus;
            }
			that.setTouchMove=function()
			{
				that.touchMove=true;
			}

			that.render = function () 
			{
				that._cnvContext=that.getMyCanvasElement('main').getContext("2d");;
				that.draw();

				if (that.processing!="")
				{
					that._cnvContext.fillStyle="rgba(100,100,100,1.0)";
					that._cnvContext.fillRect(0,0,that.GetXSize(),100);
					that._cnvContext.fillStyle="rgba(200,200,200,1.0)";
					that._cnvContext.fillRect(5,5,that.GetXSize()-10,90);
					that._cnvContext.font="italic bold 24px serif";
					that._cnvContext.fillStyle="rgba(255,0,0,1)";
					that._cnvContext.fillText(that.processing,30,40);
				}
				return;//om de update rate te zien
				that._cnvContext.fillStyle="rgba(255,255,255,1.0)";
				that._cnvContext.fillRect(25,10,60,30);
				that._cnvContext.font="italic bold 18px serif";
				that._cnvContext.textBaseline='top';
				that._cnvContext.fillStyle="rgba(0,0,0,1.0)";
				that._cnvContext.fillText(that.paintcount,30,20);
				that.paintcount++;
			}

            that.invalidate = function() 
			{
				that.render();
  				_vscroller.Invalidate();
  				_hscroller.Invalidate();
				_fieldbar.Invalidate();
				_zscroller.Invalidate();
            }
            that.fullUpdate=function() 
			{
 				that.handleResize(false);
            }

            // Override this function
            that.draw = function()
			{
			};

            // Override this function. Return a object with members ID, px,py, content
            that.getToolTipInfo = function (px, py) 
			{
                return null;
            }

            //Returns the position X coordinate of an event, relative to the center canvas element
            that.getEventPosX = function (ev) {
                var ev1 = ev;
                if (ev.originalEvent)
                    ev1 = ev.originalEvent;
                return ev1.pageX - $(that.getMyCanvasElement('main')).offset().left;
            }

            //Returns the position Y coordinate of an event, relative to the center canvas element
            that.getEventPosY = function (ev) {
                var ev1 = ev;
                if (ev.originalEvent)
                    ev1 = ev.originalEvent;
                return ev1.pageY - $(that.getMyCanvasElement('main')).offset().top;
            }

            that.posXCanvas2Screen = function (px) {
                return px + $(that.getMyCanvasElement('main')).offset().left;
            }

            that.posYCanvas2Screen = function (py) {
                return py + $(that.getMyCanvasElement('main')).offset().top;
            }


			that._handleOnMouseDown=function(ev)
			{
			}
			
			that._onMouseDown=function(ev) 
			{
				that.mousepressed=true;
				var posx=ev.clientX-$(that.getMyCanvasElement('main')).offset().left;
				var posy=ev.clientY-$(that.getMyCanvasElement('main')).offset().top;
				that.lastY=posy;
				that.lastX=posx;
 				var clickLayerId=that.getCanvasID('main');
				if (that.touchMove)
				{
					$('#'+clickLayerId).off('mousemove',that._onMouseMove);
					$('#'+clickLayerId).on('mousemove', $.proxy(that._onMouseMove,that));
					$('#'+clickLayerId).on('mouseout', $.proxy(that._onMouseOut,that));
				}
				that.MouseDown(posx,posy);
			};
			that.MouseDown=function(posx,posy) {} //overwrite
			
			that._onMouseDblClick=function(ev)
			{
				var posx=ev.clientX-$(that.getMyCanvasElement('main')).offset().left;
				var posy=ev.clientY-$(that.getMyCanvasElement('main')).offset().top;
				that.MouseDblClick(posx,posy);
			}
			that.MouseDblClick=function(posx,posy) {} //overwrite
			
			that._onMouseOut=function(ev)
			{
				var clickLayerId=that.getCanvasID('main');
				$('#'+clickLayerId).off('mousemove',that._onMouseMove);
			}

			that._onMouseUp=function(ev)
			{
				that.mousepressed=false;
				var posx=ev.clientX-$(that.getMyCanvasElement('main')).offset().left;
				var posy=ev.clientY-$(that.getMyCanvasElement('main')).offset().top;
				if (that.touchMove)
				{
					var clickLayerId=that.getCanvasID('main');
//					$('#'+clickLayerId).off('mousemove',that._onMouseMove);
					$('#'+clickLayerId).off('mouseout',that._onMouseOut);
				}
				that.MouseUp(posx,posy);
				return;
            };
			that.MouseUp=function(posx,posy) {} //overwrite
	
			that._onMouseMove=function(ev)
			{
				var posx=ev.clientX-$(that.getMyCanvasElement('main')).offset().left;
				var posy=ev.clientY-$(that.getMyCanvasElement('main')).offset().top;
//				var nwx=_hscroller.scrollmax*posx/that._cnvWidth;//zottes scrolleffect reageert op muisbeweging
//				var nwy=_vscroller.scrollmax*posy/that._cnvHeight;
//				that.SetNewHVScrollPos(nwx,nwy);
				if (that.touchMove && that.mousepressed && (that.HasVScroller() || that.HasHScroller()))
				{
					var deltay=that.lastY-posy;
					that.lastY=posy;
					var deltax=that.lastX-posx;
					that.lastX=posx;
					that.SetNewHVScrollPos(that.hscrollpos+deltax,that.vscrollpos+deltay);
				}
				that.MouseMove(posx,posy);
			}
			that.MouseMove=function(posx,posy) {} //overwrite

			that._onKeyDown = function(ev) 
			{
				that.KeyDown(ev);
				return false;
            };
			that.KeyDown=function(ev) {}//overwrite

 			that._onKeyUp = function(ev) 
			{
				that.KeyUp(ev);
				return false;
            };
			that.KeyUp=function(ev) {}//overwrite

			that.SetListeners=function()
			{
				var clickLayerId=that.getCanvasID('fieldbar');
				$('#'+clickLayerId).off('mouseover',_fieldbar.MouseOver);
				$('#'+clickLayerId).off('mouseout',_fieldbar.MouseOut);
				$('#'+clickLayerId).off('mousedown',_fieldbar.MouseDown);
				$('#'+clickLayerId).on('mouseover', $.proxy(_fieldbar.MouseOver,_fieldbar));
				$('#'+clickLayerId).on('mouseout', $.proxy(_fieldbar.MouseOut,_fieldbar));
				$('#'+clickLayerId).on('mousedown', $.proxy(_fieldbar.MouseDown,_fieldbar));

				var clickLayerId=that.getCanvasID('main');
				$('#'+clickLayerId).off('mousemove',that._onMouseMove);
				$('#'+clickLayerId).off('mousedown',that._onMouseDown);
				$('#'+clickLayerId).off('mouseup',that._onMouseUp);
				$('#'+clickLayerId).off('dblclick',that._onMouseDblClick); 
				$('#'+clickLayerId).on('mousemove', $.proxy(that._onMouseMove,that));
				$('#'+clickLayerId).on('mousedown',$.proxy(that._onMouseDown,that));
				$('#'+clickLayerId).on('mouseup',$.proxy(that._onMouseUp,that));
				$('#'+clickLayerId).on('dblclick',$.proxy(that._onMouseDblClick,that)); 

				
				clickLayerId=that.getCanvasID('vscroller');
				$('#'+clickLayerId).off('mouseover',_vscroller.MouseOver);
				$('#'+clickLayerId).off('mouseout',_vscroller.MouseOut);
				$('#'+clickLayerId).off('mousedown',_vscroller.MouseDown);
				$('#'+clickLayerId).on('mouseover', $.proxy(_vscroller.MouseOver,_vscroller));
				$('#'+clickLayerId).on('mouseout', $.proxy(_vscroller.MouseOut,_vscroller));
				$('#'+clickLayerId).on('mousedown', $.proxy(_vscroller.MouseDown,_vscroller));
				
				clickLayerId=that.getCanvasID('hscroller');
				$('#'+clickLayerId).off('mouseover',_hscroller.MouseOver);
				$('#'+clickLayerId).off('mouseout',_hscroller.MouseOut);
				$('#'+clickLayerId).off('mousedown',_hscroller.MouseDown);
				$('#'+clickLayerId).on('mouseover', $.proxy(_hscroller.MouseOver,_hscroller));
				$('#'+clickLayerId).on('mouseout', $.proxy(_hscroller.MouseOut,_hscroller));
				$('#'+clickLayerId).on('mousedown', $.proxy(_hscroller.MouseDown,_hscroller));

				clickLayerId=that.getCanvasID('zscroller');
				$('#'+clickLayerId).off('mouseover',_zscroller.MouseOver);
				$('#'+clickLayerId).off('mouseout',_zscroller.MouseOut);
				$('#'+clickLayerId).off('mousedown',_zscroller.MouseDown);
				$('#'+clickLayerId).on('mouseover', $.proxy(_zscroller.MouseOver,_zscroller));
				$('#'+clickLayerId).on('mouseout', $.proxy(_zscroller.MouseOut,_zscroller));
				$('#'+clickLayerId).on('mousedown', $.proxy(_zscroller.MouseDown,_zscroller));
				
				document.removeEventListener('keydown',that._onKeyDown,false);
				document.removeEventListener('keyup',that._onKeyUp,false);
				document.addEventListener('keydown',that._onKeyDown,false);
				document.addEventListener('keyup',that._onKeyUp,false);
			}

            that.handleResize = function (isDragging) 
			{
//				that.ReCalcScrollers();
                that._cnvWidth=$('#'+that.getDivID()).innerWidth();
                that._cnvHeight=$('#'+that.getDivID()).innerHeight();
				that.BeforeResize();
				that.ReCalcScrollers();
				that.AfterResize();
				that.invalidate();
				that.SetListeners();
			};
			that.GetFieldBarHeight=function()
			{
				if (_fieldbar.IsPresent())	return _fieldbar.GetHeight();
				return 0;
			}
            that.ReCalcScrollers=function () 
			{
                that._cnvWidth=$('#'+that.getDivID()).innerWidth();
                that._cnvHeight=$('#'+that.getDivID()).innerHeight();
				var cnv_dx=that._cnvWidth;
				var cnv_dy=that._cnvHeight;

				_vscroller.handleResize(that._cnvHeight-_hscroller.GetScrollWidth());
				if (_vscroller.IsPresent()==true) cnv_dx=that._cnvWidth-_vscroller.GetScrollWidth()-1;

				if (that.zoomscroll)
				{
					_zscroller.handleResize(that.zoomsize);
					_hscroller.handleResize(that._cnvWidth-_vscroller.GetScrollWidth()-that.zoomsize-1);
				}
				else _hscroller.handleResize(that._cnvWidth-_vscroller.GetScrollWidth());
				
				if (_hscroller.IsPresent()==true) cnv_dy=that._cnvHeight-_hscroller.GetScrollWidth();
                if (that.zoomscroll) cnv_dy=that._cnvHeight-_zscroller.GetScrollWidth();
				if (_fieldbar.IsPresent()==true) cnv_dy=cnv_dy-_fieldbar.GetHeight();
				
				if (_fieldbar.IsPresent())
				{
					that.getMyCanvasElement('fieldbar').width=that._cnvWidth;
					that.getMyCanvasElement('fieldbar').height=_fieldbar.GetHeight();
					$('#'+that.getCanvasID('fieldbar')).css('display','inline-block');
				}
				else $('#'+that.getCanvasID('fieldbar')).css('display','none');
				
//				$('#'+that.getDivID()+'_div_maincanvas').width(cnv_dx);
//				$('#'+that.getDivID()+'_div_maincanvas').height(cnv_dy);
				that.getMyCanvasElement('main').width=cnv_dx;
				that.getMyCanvasElement('main').height=cnv_dy;

				if (that.zoomscroll==true)
				{
					that.getMyCanvasElement('zscroller').width=that.zoomsize;
					that.getMyCanvasElement('zscroller').height=_zscroller.GetScrollWidth();
					$('#'+that.getCanvasID('zscroller')).css('display','inline-block');
				}
				else $('#'+that.getCanvasID('zscroller')).css('display','none');
				
				$('#'+that.getDivID()+'_div_corner').css('display','none');
				if (_vscroller.IsPresent() && _hscroller.IsPresent())
				{
					$('#'+that.getDivID()+'_div_corner').css('display','inline-block');
					$('#'+that.getDivID()+'_div_corner').width(_hscroller.GetScrollWidth());
					$('#'+that.getDivID()+'_div_corner').height(_hscroller.GetScrollWidth());
				}
				if (that.zoomscroll==true && _hscroller.IsPresent()==false)
				{
					$('#'+that.getDivID()+'_div_corner').css('display','inline-block');
					$('#'+that.getDivID()+'_div_corner').width(that._cnvWidth-that.zoomsize-1);
					$('#'+that.getDivID()+'_div_corner').height(_hscroller.GetScrollWidth());
				}
				
				if (_vscroller.IsPresent())
				{
					that.getMyCanvasElement('vscroller').width=_vscroller.GetScrollWidth();
					that.getMyCanvasElement('vscroller').height=cnv_dy;
					$('#'+that.getCanvasID('vscroller')).css('display','inline-block');
				}
				else $('#'+that.getCanvasID('vscroller')).css('display','none');
				
				if (_hscroller.IsPresent())
				{
					var _cnv_dx=0;
					if (that.zoomscroll) _cnv_dx=cnv_dx-that.zoomsize-1;else _cnv_dx=cnv_dx;
					that.getMyCanvasElement('hscroller').width=_cnv_dx;
					that.getMyCanvasElement('hscroller').height=_hscroller.GetScrollWidth();
					$('#'+that.getCanvasID('hscroller')).css('display','inline-block');
				}
				else $('#'+that.getCanvasID('hscroller')).css('display','none');
			}
			that.GetXSize=function()
			{
				if (_vscroller.IsPresent()) return that._cnvWidth-_vscroller.GetScrollWidth();else return that._cnvWidth;
			}
			that.GetYSize=function()
			{
				if (that.zoomscroll) return that._cnvHeight-_zscroller.GetScrollWidth(); 
				return that._cnvHeight-_hscroller.GetScrollWidth();
			}


 //           DQX.ExecPostCreateHtml();
 //           that.myParentFrame.notifyContentChanged();
			that.BeforeResize=function()
			{
			}
			that.AfterResize=function()
			{
			}
			
			that.setvscroll=function(nscrollpos,scrollmax,deltascroll)
			{
				that.vscrollpos=nscrollpos;
				if (that.vscrollposchanged(nscrollpos,scrollmax,deltascroll)==false) that.render();
			}
			that.vscrollposchanged=function(nscrollpos,scrollmax,deltascroll)//overwrite
			{
				return false;
 				// overwrite
			}
			that.sethscroll=function(nscrollpos,scrollmax,deltascroll)
			{
				that.hscrollpos=nscrollpos;
				if (that.hscrollposchanged(nscrollpos,scrollmax,deltascroll)==false) that.render();
			}
			that.hscrollposchanged=function(nscrollpos,scrollmax,deltascroll)//overwrite
			{
				return false;
				//overwrite
			}
			that.hvscrollposchanged=function(nscrollpos,scrollmax,deltascroll)//overwrite
			{
				return false;
 				// overwrite
			}
			that.setzscroll=function(nscrollpos,scrollmax,deltascroll)
			{
				that.zscrollposchanged(nscrollpos,scrollmax,deltascroll);
			}
			that.zscrollposchanged=function(nscrollpos,scrollmax,deltascroll)//overwrite
			{
//				that.render();//if overwritten, render should be done there
			}
			that.GetVScrollPos=function()
			{
				return that.vscrollpos;
			}
			that.GetHScrollPos=function()
			{
				return that.hscrollpos;
			}
			that.SetVScrollSize=function(sz)
			{
				_vscroller.SetScrollSize(sz);
				that.vscrollpos=_vscroller.scrollpos;
			}
			that.GetVScrollSize=function()
			{
				return _vscroller.scrollmax;
			}
			that.SetHScrollSize=function(sz)
			{
				_hscroller.SetScrollSize(sz);
				that.hscrollpos=_hscroller.scrollpos;
			}
			that.GetHScrollSize=function()
			{
				return _hscroller.scrollmax;
			}
			that.SetNewVScrollPos=function(newpos)
			{
//				if (that.vscrollpos==newpos) return;
				if (that.HasVScroller()==false) {that.vscrollpos=0;return;}
				if (that.vscrollpos==0 && newpos<that.vscrollpos) return;
				if (that.vscrollpos==_vscroller.scrollmax && newpos>_vscroller.scrollmax) return;
				that.vscrollpos=newpos;
				if (that.vscrollpos<0) that.vscrollpos=0;
				if (that.vscrollpos>_vscroller.scrollmax) that.vscrollpos=_vscroller.scrollmax;
				if (that.vscrollposchanged(that.vscrollpos,_vscroller.scrollmax,0)==false) that.invalidate();
				_vscroller.scrollpos=that.vscrollpos;
				_vscroller.render();
			}
			that.SetNewHScrollPos=function(newpos)
			{
//				if (that.hscrollpos==newpos) return;
				if (that.HasHScroller()==false) {that.hscrollpos=0;return;}
				if (that.hscrollpos==0 && newpos<that.hscrollpos) return;
				if (that.hscrollpos==_hscroller.scrollmax && newpos>_hscroller.scrollmax) return;
				that.hscrollpos=newpos;
				if (that.hscrollpos<0) that.hscrollpos=0;
				if (that.hscrollpos>_hscroller.scrollmax) that.hscrollpos=_hscroller.scrollmax;
				if (that.hscrollposchanged(that.hscrollpos,_hscroller.scrollmax,0)==false) that.invalidate();
				_hscroller.scrollpos=that.hscrollpos;
				_hscroller.render();
			}
			that.SetNewHVScrollPos=function(hnewpos,vnewpos)
			{
				var skip=false;
				if (that.vscrollpos==0 && vnewpos<that.vscrollpos) skip=true;
				if (that.vscrollpos==_vscroller.scrollmax && vnewpos>_vscroller.scrollmax) skip=true;
				if (that.HasVScroller()==false) 
				{
					that.vscrollpos=0;
					skip=true;
				}
				if (skip==false)
				{
					that.vscrollpos=vnewpos;
					if (that.vscrollpos<0) that.vscrollpos=0;
					if (that.vscrollpos>_vscroller.scrollmax) that.vscrollpos=_vscroller.scrollmax;
					_vscroller.scrollpos=that.vscrollpos;
					_vscroller.render();
				}
				skip=false;
				if (that.hscrollpos==0 && hnewpos<that.hscrollpos) 
				{
					that.hscrollpos=0;
					skip=true;
				}
				if (that.hscrollpos==_hscroller.scrollmax && hnewpos>_hscroller.scrollmax) skip=true;
				if (that.HasHScroller()==false) skip=true;
				if (skip==false)
				{
					that.hscrollpos=hnewpos;
//					if (that.hscrollpos<0) that.hscrollpos=0;
//					if (that.hscrollpos>_hscroller.scrollmax) that.hscrollpos=_hscroller.scrollmax;
					if (that.hscrollpos<0) 
					{
						if (that.scrollcircular) that.hscrollpos=that.hscrollpos+_hscroller.scrollmax;
						else that.hscrollpos=0;
					}
					if (that.hscrollpos>_hscroller.scrollmax) 
					{
						if (that.scrollcircular) that.hscrollpos=that.hscrollpos-_hscroller.scrollmax;
						else that.hscrollpos=_hscroller.scrollmax;
					}
					_hscroller.scrollpos=that.hscrollpos;
					_hscroller.render();
				}
				that.invalidate();
				that.hvscrollposchanged(that.hscrollpos,_hscroller.scrollmax,0);
			}
			that.AddZoom=function()
			{
				that.zoomscroll=true;
//				that.render();
			}
			that.HasVScroller=function()
			{
				return _vscroller.IsPresent();
			}
			that.HasHScroller=function()
			{
				return _hscroller.IsPresent();
			}
			that.GetFieldBar=function()
			{
				return _fieldbar;
			}
			that.SortColumn=function(col,reverse) //overwrite in scrolllist
			{
			}
			that.SetListeners();
 			return that;
        };
        return MScrollWindow;
    });