Array.prototype.sortIndices=function (inverse,func) 
{
    var i=j=this.length,that = this;
    while (i--) this[i] = { k: i, v: this[i] };
    this.sort(function (a, b) 
	{
		if (inverse) return b.v < a.v ? -1 : b.v > a.v ? 1 : 0; 
		return func ? func.call(that, a.v, b.v) : a.v < b.v ? -1 : a.v > b.v ? 1 : 0; 
	});
    while (j--) this[j] = this[j].k;
}

MScrollListRow=function(rowindex)
{
	var that = this;
	that.colsels=[];
	that.rowindex=rowindex;
	that.GetRowIndex=function()
	{
		return that.rowindex;
	}
	that.SetColSel=function(col)
	{
		for (var i=0;i<that.colsels.length;i++) if (that.colsels[i]==col) return;
		that.colsels.push(col);
	}
	that.GetColSel=function(col)
	{
		for (var i=0;i<that.colsels.length;i++) if (that.colsels[i]==col) return true;
		return false;
	}
	that.GetColSels=function()
	{
		return that.colsels;
	}
	return that;
};

define(["jquery", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScrollWindow"],
    function ($, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScrollWindow) {
        var MScrollList = {};
		MScrollList.Init = function (iParentRef) 
		{
            var that=MScrollWindow.Init(iParentRef,false);

			that.dist=20; //hoogte van kolom
			that.rowpos=0;
			that.colpos=0;
			that.control=false;
			that.shift=false;
			that.singlesel=false;
			that.touchMove=false;
			that.rows=[];
			that.indices=[];
			that.count=0;
			that.ColAndRowSelected='rgba(200,200,200,0.5)';
			that.RowSelected='rgba(200,200,200,0.2)';
			that.BackGround='rgb(255,255,255)';
			that.PopupButtons=[];
			that.popup={'left':0,'top':0,'xsize':0,'ysize':0};

			that.SetDataCount=function(cnt)
			{
				that.count=cnt;
				that.indices.length=0;
				for (var i=0;i<cnt;i++) that.indices.push(i);
				that.rowpos=0;
				that.AddRowSelection(that.rowpos,that.colpos);
				that.UpdateScrollSize();
				that.changeposit(that.rowpos,that.colpos,true);
			}
			that.SetDataCountNoScroll=function(cnt)
			{
				that.count=cnt;
				that.indices.length=0;
				for (var i=0;i<cnt;i++) that.indices.push(i);
				that.rowpos=0;
				that.AddRowSelection(that.rowpos,that.colpos);
				that.UpdateScrollSize();
			}
			that.AddRowSelection=function(rowindex,colsel)
			{
				var _row=null;
				for (var i=0;i<that.rows.length;i++) if (that.rows[i].rowindex==rowindex) _row=that.rows[i];
				if (_row==null) {_row=new MScrollListRow(rowindex);that.rows.push(_row);}
				_row.SetColSel(colsel);
			}
			that.ClearSelection=function()
			{	
				that.rows.length=0;
			}
			that.resetFieldBar=function()
			{
				that.GetFieldBar().Reset();
			}
			that.FieldBarMoved=function()
			{
				that.UpdateScrollSize();
//				that.invalidate();
			}
			that.AddColumn=function(idd,code,name,width)
			{
				that.GetFieldBar().AddColumn(idd,code,name,width);
//				that.GetFieldBar().Invalidate();
			}
			that.UpdateScrollSize=function()
			{
				that.SetVScrollSize(that.dist*that.count);
				that.SetHScrollSize(that.GetFieldBar().GetFullWidth());
				that.ReCalcScrollers();
				that.invalidate();
			}
			that.MouseDown = function(posx,posy)
			{
				var oldrowpos=that.rowpos;
				var oldcolpos=that.colpos;
				var rowpos=that.rowpos;
				var colpos=that.colpos;
				if (that.CheckPopupClicked(posx,posy)==false)
				{
					rowpos=that.GetHitRow(posy);
					colpos=that.GetHitCol(posx);
				}
				else
				{
					var xoo=that.GetHScrollPos(); 
					var	yoo=that.GetVScrollPos();
					var left=that.popup.left+2-xoo;
					var top=that.popup.top+3-yoo;
					var xsize=25;
					var ysize=25;
					for (var i=0;i<that.PopupButtons.length;i++)
					{
						if (posx>=left && posx<=left+xsize && posy>=top && posy<=top+ysize)
						{
							that._cnvContext.fillStyle='rgb(0,0,0)';
							that._cnvContext.fillRect(left,top,xsize,ysize);
							that._cnvContext.fillStyle='rgb(100,100,100)';
							that._cnvContext.fillRect(left+1,top+1,xsize-2,ysize-2);
							that._cnvContext.fillStyle='rgb(200,200,200)';
							that._cnvContext.fillRect(left+2,top+2,xsize-3,ysize-3);
							that._cnvContext.drawImage(that.PopupButtons[i].bitmap,left+3,top+3);
						}
						else 
						{
							that._cnvContext.fillStyle='rgb(255,255,255)';
							that._cnvContext.fillRect(left,top,xsize,ysize);
							that._cnvContext.drawImage(that.PopupButtons[i].bitmap,left+2,top+2);
						}
						top=top+25+2;
					}
				}
				that.prechangeposit(rowpos,colpos,posx,posy);
				if (rowpos!=oldrowpos || colpos!=oldcolpos)	that.changeposit(rowpos,colpos);
				$('#'+that.getDivID()+'_drag_field').css('visibility','visible');
				$('#'+that.getDivID()+'_drag_field').css('position','relative');
				that.ListMouseDown(posx,posy);
				return false;
            };
			that.MouseUp=function(posx,posy)
			{
				var xoo=that.GetHScrollPos(); 
				var	yoo=that.GetVScrollPos();
				var left=that.popup.left+2-xoo;
				var top=that.popup.top+3-yoo;
				var xsize=25;
				var ysize=25;
				for (var i=0;i<that.PopupButtons.length;i++)
				{
					if (posx>=left && posx<=left+xsize && posy>=top && posy<=top+ysize)
					{
						that.PopupButtons[i].callback();
					}
					top=top+25+2;
				}
				that.ListMouseUp(posx,posy);
			}
			that.MouseMove=function(posx,posy)
			{
				var xoo=that.GetHScrollPos(); 
				var yoo=that.GetVScrollPos();
				var left=that.popup.left+2-xoo;
				var top=that.popup.top+3-yoo;
				var xsize=25;
				var ysize=25;
				for (var i=0;i<that.PopupButtons.length;i++)
				{
					if (posx>=left && posx<=left+xsize && posy>=top && posy<=top+ysize)
					{
						that._cnvContext.fillStyle='rgb(100,100,100)';
						that._cnvContext.fillRect(left,top,xsize,ysize);
						that._cnvContext.fillStyle='rgb(200,200,200)';
						that._cnvContext.fillRect(left+1,top+1,xsize-2,ysize-2);
						that._cnvContext.drawImage(that.PopupButtons[i].bitmap,left+2,top+2);
					}
					else 
					{
						that._cnvContext.fillStyle='rgb(255,255,255)';
						that._cnvContext.fillRect(left,top,xsize,ysize);
						that._cnvContext.drawImage(that.PopupButtons[i].bitmap,left+2,top+2);
					}
					top=top+25+2;
				}
			}
			that.prechangeposit=function(rowps,colps,posx,posy)//overwrite
			{
			};
			that.ListMouseDown=function(posx,posy)//overwrite
			{
			};
			that.ListMouseUp=function(posx,posy)//overwrite
			{
			};
			
			that.KeyDown = function(ev) 
			{
				that.control=ev.ctrlKey;
				that.shift=ev.shiftKey;
				return false;
            };
			
 			that.KeyUp = function(ev) 
			{
				that.shift=false;
                that.control=false;
				return false;
            };

			that.GetHitRow=function(posy)
			{
				if (that.dist<1) return 0;
//				if (vertical==0) {ps1=vscrollpos;pt=point.y;}
//				else {ps1=that.hscrollpos;pt=point.x;}
				var ps1=that.GetVScrollPos()-that.dist/2;
				ps2=Math.round((posy+ps1)/that.dist);
				if (ps2<0) ps2=0;if (ps2>=that.count) ps2=that.count-1;
				return ps2;
			}

			that.GetHitCol=function(posx)
			{
				if (that.GetFieldBar().GetInfoFieldCnt()==0) return -1;
				var px=-that.GetHScrollPos();
				for (var colnr=0;colnr<that.GetFieldBar().GetFieldCount();colnr++)
				{
					if (posx>=px && posx<px+that.GetFieldBar().GetFieldSize(colnr)) return colnr;
					px+=that.GetFieldBar().GetFieldSize(colnr);
				}
				return -1;
			}
			that.GetRowOffset=function(index)
			{
				return that.dist*index;
			}
			that.GetColumnOffset=function(index)
			{
				return that.GetFieldBar().GetFieldXp(index);
			}
			that.GetRowHeight=function()
			{
				return that.dist;
			}
			that.GetColumnSize=function(index)
			{
				return that.GetFieldBar().GetFieldSize(index);
			}
			that.IsRowSel=function(pos)
			{
				if (pos<0 || pos>=that.count) return false;
				for (var i=0;i<that.rows.length;i++) if (that.rows[i].rowindex==pos) return i;
				return -1;
			}
			that.GetRowSels=function()
			{
				var sels=[];
				for (var i=0;i<that.rows.length;i++) sels.push(that.rows[i].rowindex);
				return sels;
			}
			that.GetRowColSels=function()
			{
				var rowcolsels=[];
				for (var i=0;i<that.rows.length;i++)
				{
					var colsels=that.rows[i].GetColSels();
					for (var j=0;j<colsels.length;j++)
					{
						var obj={rowindex:that.rows[i].rowindex,field:that.GetFieldBar().GetField(colsels[j])};
						rowcolsels.push(obj);
					}
				}
				return rowcolsels;
			}

			that.IsColSel=function(_rowpos,_colpos)
			{
				var i=that.IsRowSel(_rowpos);
				if (i==-1) return false;
				return that.rows[i].GetColSel(_colpos);
			}

			that.changeposit=function(newrowpos,newcolpos,scroll)
			{
				var selchange=0;
				var	oldrowpos=that.rowpos;
				var	oldcolpos=that.colpos;
				if (newrowpos<0 || newrowpos>=that.count || newcolpos<0 || newcolpos>=200) 
				{
					that.ClearSelection();
					that.rowpos=0;
					that.colpos=0;
					that.render();
					return;
				}
				if (that.count==0) return;
				that.rowpos=newrowpos;
				that.colpos=newcolpos;
				if (that.control && !that.singlesel)
				{
					that.AddRowSelection(that.rowpos,that.colpos);
					selchange=1;
					that.render();
				}
				if (that.shift && !that.singlesel)
				{
						r1=oldrowpos;r2=that.rowpos;
						if (r2<r1) {r1=that.rowpos;r2=oldrowpos;}
						c1=oldcolpos;c2=that.colpos;
						if (c2<c1) {c1=that.colpos;c2=oldcolpos;}
						for (var i=r1;i<=r2;i++) for (var j=c1;j<=c2;j++) that.AddRowSelection(i,j);
						that.render();
						selchange=1;
				}
				if ( (!that.control && !that.shift) || that.singlesel )
				{
					that.ClearSelection();
					that.AddRowSelection(that.rowpos,that.colpos);
					that.render();
					selchange=1;
				}
				if (scroll==1)
				{
					that.SetNewVScrollPos(that.dist*that.rowpos);
				}
			}

			that.prePaintList=function()
			{
			}
			
			that.postPaintList=function()
			{
			}
			
			that.draw=function()
			{
//				if (dataloader==null) return;
				that.prePaintList();
				var fieldbar=that.GetFieldBar();
				var xoo=that.GetHScrollPos(); 
				var yoo=that.GetVScrollPos();
				
				var lx=that._cnvWidth;
				var ly=that._cnvHeight;
				
				var zoom=1;
				var xcurpos=0;
				var ycurpos=0;
				
				var ctx=that._cnvContext;
 				ctx.strokeStyle='rgba(0,192,192,0.5)';
				ctx.fillStyle=that.BackGround;
				ctx.fillRect(0, 0, lx,ly);
				ctx.font="12px sans-serif";
				ctx.font="12px microsoft jhenghei";
					
				ps1=Math.round(yoo/that.dist)-1;
				if (ps1<0) ps1=0;
				ps2=Math.round((yoo+ly)/that.dist)+1;
				if (ps2>that.count) ps2=that.count;
				var py=0;
				for (var i=ps1;i<ps2;i++)
				{
					py=Math.floor(i*that.dist-yoo);

					px=Math.floor(-xoo);
					ctx.textBaseline="top"; 
					for (var colnr = 0; colnr<fieldbar.GetFieldCount(); colnr++)
					{
						var fieldsize=fieldbar.GetFieldSize(colnr);
						if (colnr==fieldbar.GetFieldCount()-1) fieldsize=that.GetXSize()-px;

						ctx.fillStyle='rgb(255,255,255)';
						ctx.fillRect(px,py,fieldsize,that.dist);
						
						if (that.IsRowSel(i)>-1) ctx.fillStyle=that.RowSelected;else ctx.fillStyle=that.BackGround;
						if (that.IsColSel(i,colnr)==true) ctx.fillStyle=that.ColAndRowSelected;
						ctx.fillRect(px+2,py+1,fieldsize-4,that.dist);

						ctx.save();
						ctx.beginPath();
						ctx.strokeStyle="rgba(0,0,0,0)";
						ctx.fillStyle="rgb(0,0,0)";
						ctx.lineWidth=0;
						ctx.rect(px+1.5,py+1.5,fieldsize-4,that.dist-3);
						ctx.clip();
						that.SetFieldText(ctx,that.indices[i],colnr,fieldbar.GetField(colnr),"",px,py,fieldsize,that.dist);
						ctx.closePath();
						ctx.restore();

						ctx.beginPath();
						ctx.lineWidth=1;
						ctx.strokeStyle='rgba(200,200,200,0.5)';
						ctx.rect(px+1.5,py+1.5,fieldsize-4,that.dist-3);
						ctx.closePath();
						ctx.stroke();
						
						that.DrawField(ctx,that.indices[i],colnr,fieldbar.GetField(colnr),px,py,fieldsize,that.dist);
						
						px+=fieldsize;
					}
					//laatste cel uittekenen
					ctx.fillStyle='rgba(250,250,250,1.0)';
					ctx.fillRect(px,py,lx-px,that.dist);
					if (that.IsRowSel(i)==true) ctx.fillStyle=that.RowSelected;else ctx.fillStyle=that.BackGround;
					ctx.fillRect(px+2,py+1,lx-px,that.dist-3);
					//einde laatste cel
				}
				if (that.PopupButtons.length>0)
				{
					that._cnvContext.fillStyle='rgb(100,100,100)';
					that._cnvContext.fillRect(that.popup.left-xoo,that.popup.top-yoo,that.popup.xsize,that.popup.ysize);
					that._cnvContext.fillStyle='rgb(255,255,255)';
					that._cnvContext.fillRect(that.popup.left+1-xoo,that.popup.top+1-yoo,that.popup.xsize-2,that.popup.ysize-2);
					var left=that.popup.left+2-xoo;
					var top=that.popup.top+3-yoo;
					var xsize=25;
					var ysize=25;
					for (var i=0;i<that.PopupButtons.length;i++)
					{
						that._cnvContext.fillStyle='rgb(255,255,255)';
						that._cnvContext.fillRect(left,top,xsize,ysize);
						that._cnvContext.drawImage(that.PopupButtons[i].bitmap,left+2,top+2);
						top=top+25+2;
					}
				}
				$('#'+that.getDivID()+'_drag_field').css('left',that.GetColumnOffset(that.colpos)-1);
				$('#'+that.getDivID()+'_drag_field').css('top',that.GetRowOffset(that.rowpos)-that.GetVScrollPos()-1);
				$('#'+that.getDivID()+'_drag_field').css('width',that.GetColumnSize(that.colpos));
				$('#'+that.getDivID()+'_drag_field').css('height',that.GetRowHeight()+1);
				that.postPaintList();
			}
			that.SetFieldText=function(context,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				ctx.fillStyle='rgba(0,0,0,1.0)';
				ctx.fillText(text,px+2,py+2);
			}
			that.DrawField=function(context,rowindex,colnr,field,px,py,sizex,sizey)
			{
			}
/*			that.vscrollposchanged=function(nscrollpos,scrollmax,deltascroll)//overwrite in scrolllist
			{
				that.GetFieldBar().render()
			}*/
			that.hscrollposchanged=function(nscrollpos,scrollmax,deltascroll)//overwrite in scrolllist
			{
				that.render();
				that.GetFieldBar().GetHScrollPos();
				that.GetFieldBar().render();
				return true;
			}
			that.GetRowPos=function()
			{
				return that.rowpos;
			}
			that.GetFieldSel=function()
			{
				return that.GetFieldBar().GetField(that.colpos);
			}
			
			that.GetFieldText=function(rowindex,field)//overwrite in each derived list
			{
				return '?';
			}
			
			that.DoSortColumn=function(field,reverse)
			{
				var oldrow=that.indices[that.rowpos];
				var newrow=0;
				for (var i=0;i<that.count;i++)
				{
					that.indices[i]=that.GetFieldText(i,field);
				}
				that.indices.sortIndices(reverse);
				for (var i=0;i<that.count;i++)
				{
					if (that.indices[i]==oldrow) newrow=i;
				}
				that.changeposit(newrow,that.colpos,true);
//				that.render();
			}
			
			that.SortColumn=function(col,reverse) //overwrite in scrolllist
			{
				var field=that.GetFieldBar().GetField(col);
				if (field==null) return;
				that.DoSortColumn(field,reverse);
			}
			that.ConvertToIndices=function(nr)
			{
				if (nr>=0 && nr<that.indices.length) return that.indices[nr];
				return -1;
			}
			that.AddPopupButton=function(bitmap,callback)
			{
				return;
				var xoo=that.GetHScrollPos(); 
				var yoo=that.GetVScrollPos();
				var button={'bitmap':bitmap,'callback':callback}
				that.PopupButtons.push(button);
				that.popup.top=Math.floor(that.GetRowOffset(that.rowpos))+that.GetRowHeight()-that.PopupButtons.length*30;
				if (that.popup.top-yoo<0) that.popup.top=yoo;
				that.popup.left=that.GetColumnOffset(that.colpos)+that.GetHScrollPos()-32;//GetColumnOffset geeft positie met scroll offset afgetrokken weer, dus erweer bijtellen
				that.popup.xsize=30;
				that.popup.ysize=that.PopupButtons.length*30;
				if (that.popup.top+that.popup.ysize-yoo>that.GetYSize()) that.popup.top=that.GetYSize()+yoo-that.popup.ysize;
			}
			that.ClearPopup=function()
			{
				that.PopupButtons.length=0;
				that.invalidate();
			}
			that.CheckPopupClicked=function(posx,posy)
			{
				if (that.PopupButtons.length==0) return false;
				var xoo=that.GetHScrollPos(); 
				var yoo=that.GetVScrollPos();
				if (posx>=that.popup.left-xoo && posx<=that.popup.left+that.popup.xsize-xoo && posy>=that.popup.top-yoo && posy<=that.popup.top+that.popup.ysize-yoo) return true;
				return false;
			}
			
			that.drag=$('#'+that.getDivID()+'_drag_field');
			that.drag.on("dragstart", function(event) 
			{
				$('#'+that.getDivID()+'_drag_field').css('background-color','rgba(180,180,180,0.1)');
			});
			that.drag.on("dblclick", function(event)
			{
				var posx=event.clientX-$(that.getMyCanvasElement('main')).offset().left;
				var posy=event.clientY-$(that.getMyCanvasElement('main')).offset().top;
				that.MouseDblClick(posx,posy);
			});
			return that;
        };
		return MScrollList;
    });