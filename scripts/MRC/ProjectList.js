	define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScroller","MRC/MFieldBar","MRC/MScrollList","Views/DotPlotViewer","MRC/MSequenceBox"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScroller, MFieldBar, MScrollList, DotPlotViewer, MSequenceBox) 
	{
		var ProjectList = {};
		ProjectList.Init = function (iParentRef,_pjobject)
		{
			var that = MScrollList.Init(iParentRef);
			that.pjobject=_pjobject;
			that.OpenBitmap=new Image();that.OpenBitmap.src='bitmaps/open.png';
			that.Update=function()
			{
				that.SetDataCount(that.pjobject.Projects.length);
				that.render();
			}
			that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				var projectItem=that.pjobject.Projects[rowindex];
				if (projectItem!=null)
				{
					if (field.fieldtext==="Name") ctx.fillStyle='rgba(0,0,0,1.0)';else ctx.fillStyle='rgba(120,120,120,1.0)';
					var txt="";
					if (field.fieldcode==='index') txt=rowindex+1;
					if (field.fieldtext==="Name") txt=projectItem.displayName;
					if (field.fieldtext==="Data Item") txt=projectItem.fileLocation;
					if (field.fieldtext==="Code") txt=projectItem.uniqueCode;
					if (field.fieldtext==="File") txt=projectItem.dataItems.length;
					ctx.fillText(txt,px+2,py+2);
				}
			}
			that.prechangeposit=function(row,col,posx,posy)
			{
				if (that.CheckPopupClicked(posx,posy)) return;
				that.ClearPopup();
				var colfield=that.GetFieldSel();
				if (colfield.fieldtext==='Name')
				{
					that.AddPopupButton(that.OpenBitmap,that.OpenData);
				}
			}
			that.MouseDblClick=function(posx,posy)
			{
				that.OpenData();
			}
			that.OpenData=function()
			{
				var project=that.GetSelectedProject();
				if (project==null) return;
				var database=Application.DataBaseModule;
				var track=database.AddProjectToTrackList(project);
				if (track) track.execute();//voer actie ook direct uit
			}
			that.GetSelectedProject=function()
			{
				var row=that.GetRowPos();
				if (row<0 || row>=that.pjobject.Projects.length) return null;
				return that.pjobject.Projects[row];
			}
			return that;
		}
		return ProjectList;
   });
