MSeqTools=function()
{
	var that=this;
	that.cbasen=[];
	for (var i=0;i<256;i++) that.cbasen[i]=' ';
	that.cbasen[' ']=' ';
	that.cbasen['a']='t';
	that.cbasen['b']='v';
	that.cbasen['c']='g';
	that.cbasen['d']='h';
	that.cbasen['g']='c';
	that.cbasen['h']='d';
	that.cbasen['k']='m';
	that.cbasen['m']='k';
	that.cbasen['n']='n';
	that.cbasen['r']='y';
	that.cbasen['s']='s';
	that.cbasen['t']='a';
	that.cbasen['v']='b';
	that.cbasen['w']='w';
	that.cbasen['y']='r';

	that.cbasen['A']='T';
	that.cbasen['B']='V';
	that.cbasen['C']='G';
	that.cbasen['D']='H';
	that.cbasen['G']='C';
	that.cbasen['H']='D';
	that.cbasen['K']='M';
	that.cbasen['M']='K';
	that.cbasen['N']='N';
	that.cbasen['R']='Y';
	that.cbasen['S']='S';
	that.cbasen['T']='A';
	that.cbasen['V']='B';
	that.cbasen['W']='W';
	that.cbasen['Y']='R';

	that.AA="FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG";
	that.ST="---M---------------M---------------M----------------------------";
	that.B1="ttttttttttttttttccccccccccccccccaaaaaaaaaaaaaaaagggggggggggggggg";
	that.B2="ttttccccaaaaggggttttccccaaaaggggttttccccaaaaggggttttccccaaaagggg";
	that.B3="tcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcag";

	that.azcoder=[];
	for (var i=0;i<255;i++) that.azcoder.push(-100);
	that.azcoder['t']=0;
	that.azcoder['c']=1;
	that.azcoder['a']=2;
	that.azcoder['g']=3;

	that.Complement=function(bs)
	{
		if (bs=='|') return bs;
		if (bs<0 || bs>255) return bs;
		return that.cbasen[bs];
	}

	that.FastAz=function(obj)//obj.b1,obj.b2,obj.b3,obj.az
	{
		obj.az='X';
		var start=0;
		var i=that.azcoder[obj.b1]*16+that.azcoder[obj.b2]*4+that.azcoder[obj.b3];
		if (i>-1 && i<64)	
		{
			if (that.ST[i]=='M') start=1;
			obj.az=that.AA[i];
		}
		if (obj.az!='X') return start;

		if (obj.b1=='|' || obj.b2=='|' || obj.b3=='|') 
		{
			obj.az='*';
			return start; 
		}
		obj.az=' ';
		return 0;
		
/*	//doe nog een poging als het iupac code is.
		int j,k,k1,k2,k3,p,cnt;
		int cnts1[5],cnts2[5],cnts3[5];

		for (i=0;i<5;i++) {cnts1[i]=0;cnts2[i]=0;cnts3[i]=0;}
		addtoconsensus(c1,cnts1[0],cnts1[1],cnts1[2],cnts1[3],cnts1[4]);
		addtoconsensus(c2,cnts2[0],cnts2[1],cnts2[2],cnts2[3],cnts2[4]);
		addtoconsensus(c3,cnts3[0],cnts3[1],cnts3[2],cnts3[3],cnts3[4]);
		az='X';start=0;cnt=0;
		for (i=0;i<4;i++)
		{
			k1=-100;
			if (cnts1[i]>0) k1=i;
			for (j=0;j<4;j++)
			{
				k2=-100;
				if (cnts2[j]>0) k2=j;
				for (k=0;k<4;k++)
				{
					k3=-100;
					if (cnts3[k]>0) k3=k;
					p=k1*16+k2*4+k3;
					if ((p>-1)&&(p<64))	
					{
						if (az!=AA[p]) cnt++;
						az=AA[p];
						if (ST[p]=='M') start=1;else start=0;
					}
				}
			}
		}
		if (cnt==1) return start;
	//als cnt>1 betekent dit dat er niet ��nduidig ��n aminozuur gevonden is
		az='X';
		return 0;*/
	}
	that.GetScale=function(fac)
	{
		var d=150;
		step=1;
		if (fac*10<d) step=1;
		if (fac*20<d) step=2;
		if (fac*50<d) step=5;
		if (fac*100<d) step=10;
		if (fac*200<d) step=20;
		if (fac*500<d) step=50;
		if (fac*1000<d) step=100;
		if (fac*2000<d) step=200;
		if (fac*5000<d) step=500;
		if (fac*10000<d) step=1000;
		if (fac*20000<d) step=2000;
		if (fac*50000<d) step=5000;
		if (fac*100000<d) step=10000;
		if (fac*200000<d) step=20000;
		if (fac*500000<d) step=50000;
		if (fac*1000000<d) step=100000;
		if (fac*2000000<d) step=200000;
		if (fac*5000000<d) step=500000;
		if (fac*10000000<d) step=1000000;
		if (fac*20000000<d) step=2000000;
		if (fac*50000000<d) step=5000000;
		if (fac*100000000<d) step=10000000;
		if (fac*200000000<d) step=20000000;
		if (fac*500000000<d) step=50000000;
		if (fac*1000000000<d) step=100000000;
		if (fac*2000000000<d) step=200000000;
		return ~~step;
	}
	that.NewUUID=function()
	{
		var d = new Date().getTime();
		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) 
		{
			var r = (d + Math.random()*16)%16 | 0;
			d = Math.floor(d/16);
			return (c=='x' ? r : (r&0x7|0x8)).toString(16);
		});
		return uuid;
	}
	return that;
}
	