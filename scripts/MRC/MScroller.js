define(["jquery", "DQX/DocEl", "DQX/Msg"],
    function ($, DocEl, Msg) {
    var MScroller={};
	MScroller.body=function(_iparent)
	{
		var that={};
		this.iparent=_iparent;
		this.scrollmax=0;
		this.scrollwidth=22;
		this.move=0;
		this.scrollpos=0;
		this.focus=false;
		this.press=0;
		this.movefield=20;
		this.slide=0;
		this.deltaclick=0;
		this.screenclick=0;
		this.click=0;
		this.scpos=0;
		this.hidescroller=true;
		this.background='rgba(240,240,250,1.0)';
		this.zoomer=false;
		this.slidetimeout=50;
		this.presstimeout=50;
	}
	MScroller.body.prototype.MouseMove=function(e)//overwrite in hscroll,vscroll
	{
	}
	MScroller.body.prototype.MouseOver=function(e)
	{
		this.focus=true;
		this.render();
	}
	MScroller.body.prototype.MouseOut=function(e)
	{
		if (this.slide==2) return;
		e.preventDefault();
		e.stopPropagation();
		$(document).off('mousemove',this.MouseMove);
		this.focus=false;
		this.press=0;
		this.slide=0;
		this.render();
	}
	MScroller.body.prototype.MouseDown=function(e)//overwrite in hscroll,vscroll
	{
	}
	MScroller.body.prototype.MouseUp=function(e)//overwrite in hscroll,vscroll
	{
		this.press=0;
		this.slide=0;
		$(document).off('mousemove', this.MouseMove);
		$(document).off('mouseup',this.MouseUp);
	}
	MScroller.body.prototype.GetScrollWidth=function()
	{
		return this.scrollwidth;
	}
	MScroller.body.prototype.IsPresent=function()
	{
		if (this.hidescroller==true) return false;else return true;
	}
    MScroller.body.prototype.handleResize = function (size) 
	{
		if (size<=0) return;
		if (this.scrollmax>size) this.hidescroller=false;else this.hidescroller=true;
		this.render();
	}
	MScroller.body.prototype.StartTimer=function()
	{
		var that=this;
		if (this.press>0) 
		{
			that.pressing(false);
			setTimeout(function(){that.pressing(true)},4*this.presstimeout);//eerste keer langer wachten
			return;
		}
		if (this.slide>0) 
		{
			that.sliding(false);
			setTimeout(function(){that.sliding(true)},4*this.slidetimeout);//eerste keer langer wachten
		}
	}
	MScroller.body.prototype.pressing=function(repeat)
	{
		var that=this;
		var deltascroll=0;
		var nscrollpos=0;
		var d=1;
		if (this.press==1 || this.press==3 || this.press==2 )
		{
			if (this.press==1) d=+160;
			if (this.press==3) d=-160;
			nscrollpos=this.scrollpos-d;
			if (nscrollpos<0) nscrollpos=0;
			if (nscrollpos>this.scrollmax) nscrollpos=this.scrollmax;
			deltascroll=this.scrollpos-nscrollpos;
			if (this.zoomer==false) this.scrollpos=this.scrollpos-deltascroll;
			if (deltascroll!=0 || this.zoomer==true) this.getscroll(nscrollpos,this.scrollmax,deltascroll);
			this.render();
			if (repeat) setTimeout(function(){that.pressing(true)},this.presstimeout);
		}
	}
	MScroller.body.prototype.sliding=function(repeat)
	{
		if (this.focus==false) return;
		var that=this;
		var deltascroll=0;
		var nscrollpos=0;
		var d=1;
		var k=0;
		if (this.slide==1) k=this.scpos-this.click;
		if (this.slide==3) k=this.click-(this.scpos+this.movefield);
//		if (this.slide==2 && this.scrollpos!=this.scrollmax/2) {this.getscroll(this.scrollpos,this.scrollmax,0);that.sliding(false);return;}
		if (this.slide==2) {this.getscroll(this.scrollpos,this.scrollmax,0);setTimeout(function(){that.sliding(false)},this.slidetimeout);return;}
		if (k<=0) return;
		if (k<=10) d=4;
		if ((k>10)&&(k<=20)) d=16;
		if ((k>20)&&(k<=30)) d=40;
		if (k>30) d=160;
		k=this.scrollmax/50;
		if (this.slide==1) 
		{
			nscrollpos=this.scrollpos-d;
			if (nscrollpos<0) nscrollpos=0;
			if (nscrollpos>this.scrollmax) nscrollpos=this.scrollmax;
			deltascroll=this.scrollpos-nscrollpos;
			this.scrollpos=nscrollpos;
			this.render();
			if (repeat) setTimeout(function(){that.sliding(true)},this.slidetimeout);
		}
		if (this.slide==3) 
		{
			nscrollpos=this.scrollpos+d;
			if (nscrollpos<0) nscrollpos=0;
			if (nscrollpos>this.scrollmax) nscrollpos=this.scrollmax;
			deltascroll=this.scrollpos-nscrollpos;
			this.scrollpos=nscrollpos;
			this.render();
			if (repeat) setTimeout(function(){that.sliding(true)},this.slidetimeout);
		}
		if (deltascroll!=0) this.getscroll(nscrollpos,this.scrollmax,deltascroll);
	}
	
	MScroller.body.prototype.getscroll=function(nscrollpos,scrollmax,deltascroll)//overwrite in hscroll,vscroll
	{
	}
	MScroller.body.prototype.Invalidate=function()
	{
		this.render();
	}
	MScroller.body.prototype.render=function()//overwrite in hscroll,vscroll
	{
	}

	
	
	
	MScroller.VScroll=function(_iparent)
	{
		var that = new MScroller.body(_iparent);
		
		that.SetScrollSize=function(_size)
		{
			if (this.iparent.getMyCanvasElement('vscroller')==null) return;
			that.scrollmax=_size;
			if (that.scrollpos>_size) that.scrollpos=_size;
			if (that.scrollmax>this.iparent.getMyCanvasElement('vscroller').height) that.hidescroller=false;
			else {that.hidescroller=true;that.scrollpos=0;}
			that.render();
		}
		that.render = function () 
		{
			if (that.IsPresent()==false) return;
            var ctx=this.iparent.getMyCanvasElement('vscroller').getContext("2d");;
			var lx=this.iparent.getMyCanvasElement('vscroller').width;
			var ly=this.iparent.getMyCanvasElement('vscroller').height;
			that.scpos=that.scrollwidth;
			if (that.scrollmax>0) that.scpos=((ly-that.movefield-2*that.scrollwidth)*that.scrollpos/that.scrollmax)+that.scrollwidth;
			var px=0;
			ypos1=that.scrollwidth;
			ypos2=that.scpos;
			ypos3=that.scpos+that.movefield;
			ypos4=ly-that.scrollwidth;

			//->scrollbar background
			var klr=that.background;
			if (that.focus==true) klr='rgba(230,230,200,1.0)';
			ctx.fillStyle=klr;
			ctx.fillRect(0,0,lx,ly);
			
			//->left press button
			ctx.fillStyle=that.background;
			if (that.press==1) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(0,0,lx,ypos1);
			
			//->slider button
			ctx.fillStyle='rgba(192,192,192,1.0)';
			if (that.press==2) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(2,ypos2,lx-3,ypos3-ypos2);

			//->right press button
			ctx.fillStyle=that.background;
			if (that.press==3) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(0,ypos4,lx,lx);

			//->top arrow
			ctx.fillStyle='rgba(100,100,100,1.0)';
			if (that.press==1) ctx.fillStyle='rgba(0,0,0,1.0)';
			var psx=Math.round(that.scrollwidth/2);
			var ypc=Math.round(that.scrollwidth/2);
			var sze=Math.round(that.scrollwidth/4);
			ctx.beginPath();
			ctx.moveTo(psx,ypc-sze);
			ctx.lineTo(psx+sze,ypc+sze);
			ctx.lineTo(psx-sze,ypc+sze);
			ctx.closePath();
			ctx.fill();

			//->bottom arrow
			ctx.fillStyle='rgba(100,100,100,1.0)';
			if (that.press==3) ctx.fillStyle='rgba(0,0,0,1.0)';
			ypc=ly-Math.round(that.scrollwidth/2);
			ctx.beginPath();
			ctx.moveTo(psx-sze,ypc-sze);
			ctx.lineTo(psx+sze,ypc-sze);
			ctx.lineTo(psx,ypc+sze);
			ctx.closePath();
			ctx.fill();

			ctx.fillStyle='rgba(200,200,200,1.0)';
			ctx.fillRect(0,0,lx,1);
//			ctx.fillRect(0,ly-1,lx,1);
			ctx.fillRect(0,0,1,ly);
			ctx.fillRect(lx-1,0,1,ly);
		}
		
		that.MouseMove=function(_e)
		{
			var e=window.event || _e;
			var ly=this.iparent.getMyCanvasElement('vscroller').height;
			e.preventDefault();
			e.stopPropagation();
			var posy=that.click+e.screenY-that.screenclick;
			if (that.slide==2)
			{
				that.scpos=posy+that.deltaclick;
				if (that.scpos<that.scrollwidth) that.scpos=that.scrollwidth;
				if (that.scpos>ly-that.scrollwidth-that.movefield) that.scpos=ly-that.scrollwidth-that.movefield; 
				fc=1.0*(that.scpos-that.scrollwidth)/Math.max((ly-2*that.scrollwidth-that.movefield),1);
				nscrollpos=Math.floor(that.scrollmax*fc);
				if (nscrollpos<0) nscrollpos=0;
				if (nscrollpos>that.scrollmax) nscrollpos=that.scrollmax;
				that.deltascroll=that.scrollpos-nscrollpos;
				if (that.scrollpos!=nscrollpos)
				{
					that.scrollpos=nscrollpos;//kijken of dat hier zo blijft
					that.iparent.setvscroll(nscrollpos,that.scrollmax,that.deltascroll);
					that.render();
				}
				return;
			}
			if (that.focus==false) {that.focus=true;that.render;}
		}
		that.MouseDown=function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			$(document).on('mousemove',$.proxy(that.MouseMove,that));
			$(document).on('mouseup',$.proxy(that.MouseUp,that));
//			var posx=e.offsetX;
//			var posy=e.offsetY;
			var posx=e.offsetX==undefined?e.originalEvent.layerX:e.offsetX;
			var posy=e.offsetY==undefined?e.originalEvent.layerY:e.offsetY;
			var ly=this.iparent.getMyCanvasElement('vscroller').height;
			ypos1=that.scrollwidth;
			ypos2=that.scpos;
			ypos3=that.scpos+that.movefield;
			ypos4=ly-that.scrollwidth;
			that.press=0;
			if (posy<ypos1) that.press=1;
			if (posy>=ypos2 && posy<=ypos3) that.press=2;
			if (posy>=ypos4) that.press=3;
			if (that.press==1 || that.press==3)	that.StartTimer();
			if (posy>that.scpos-that.movefield && posy<that.scpos+that.movefield)	that.slide=2;
			if (posy>that.scrollwidth && posy<that.scpos-that.movefield) that.slide=1;
			if (posy>that.scpos+that.movefield && posy<ly-that.scrollwidth) that.slide=3;
			if (that.slide>0)
			{
				that.deltaclick=that.scpos-posy;
				that.click=posy;
				that.screenclick=e.screenY;
				if (that.slide==1 || that.slide==3) that.StartTimer();
			}
			that.render();
            var now = new Date();
            that.ticks = now.getTime();
		}
		that.MouseUp=function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			that.press=0;
			that.slide=0;
			$(document).off('mousemove', that.MouseMove);
			$(document).off('mouseup',that.MouseUp);
			that.render();
		}
		that.getscroll=function(nscrollpos,scrollmax,deltascroll)
		{
			that.iparent.setvscroll(nscrollpos,scrollmax,deltascroll);
		}
		return that;
	}


	MScroller.HScroll=function(_iparent)
	{
		var that = new MScroller.body(_iparent);
		
		that.SetScrollSize=function(_size)
		{
			if (this.iparent.getMyCanvasElement('hscroller')==null) return;
			that.scrollmax=_size;
			if (that.scrollpos>_size) that.scrollpos=_size;
			if (that.scrollmax>this.iparent.getMyCanvasElement('hscroller').width) that.hidescroller=false;
			else {that.hidescroller=true;that.scrollpos=0;}
			that.render();
		}
		that.render = function () 
		{
			if (that.IsPresent()==false) return;
			var ctx=this.iparent.getMyCanvasElement('hscroller').getContext("2d");
			var lx=this.iparent.getMyCanvasElement('hscroller').width;
			var ly=this.iparent.getMyCanvasElement('hscroller').height;
			that.scpos=that.scrollwidth;
			if (that.scrollmax>0) that.scpos=((lx-that.movefield-2*that.scrollwidth)*that.scrollpos/that.scrollmax)+that.scrollwidth;
			var px=0;
			xpos1=that.scrollwidth;
			xpos2=that.scpos;
			xpos3=that.scpos+that.movefield;
			xpos4=lx-that.scrollwidth;
			
			//->scrollbar background
			var klr=that.background;
			if (that.focus==true) klr='rgba(230,230,200,1.0)';
			ctx.fillStyle=klr;
			ctx.fillRect(0,0,lx,ly);
			
			//->left press button
			ctx.fillStyle=that.background;
			if (that.press==1) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(0,0,xpos1,ly);
			
			//->slider button
			ctx.fillStyle='rgba(192,192,192,1.0)';
			if (that.press==2) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(xpos2,2,xpos3-xpos2,ly-3);
			
			//->right press button
			ctx.fillStyle=that.background;
			if (that.press==3) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(xpos4,0,ly,ly);

			//->left arrow
			ctx.fillStyle='rgba(100,100,100,1.0)';
			if (that.press==1) ctx.fillStyle='rgba(0,0,0,1.0)';
			var psx=Math.round(that.scrollwidth/2);
			var ypc=Math.round(that.scrollwidth/2);
			var sze=Math.round(that.scrollwidth/4);
			ctx.beginPath();
			ctx.moveTo(psx-sze,ypc);
			ctx.lineTo(psx+sze,ypc-sze);
			ctx.lineTo(psx+sze,ypc+sze);
			ctx.closePath();
			ctx.fill();

			//->right arrow
			ctx.fillStyle='rgba(100,100,100,1.0)';
			if (that.press==3) ctx.fillStyle='rgba(0,0,0,1.0)';
			psx=lx-Math.round(that.scrollwidth/2);
			ctx.beginPath();
			ctx.moveTo(psx-sze,ypc-sze);
			ctx.lineTo(psx+sze,ypc);
			ctx.lineTo(psx-sze,ypc+sze);
			ctx.closePath();
			ctx.fill();

			ctx.fillStyle='rgba(200,200,200,1.0)';
			ctx.fillRect(0,0,lx,1);
			ctx.fillRect(0,ly-1,lx,1);
			ctx.fillRect(0,0,1,ly);
//			ctx.fillRect(lx-1,0,1,ly);
		}
		
		that.MouseMove=function(_e)
		{
//			var e=window.event || _e;
			var e=window.event || _e;
			var lx=this.iparent.getMyCanvasElement('hscroller').width;
			e.preventDefault();
			e.stopPropagation();
			var posx=that.click+e.screenX-that.screenclick;
			if (that.slide==2)
			{
				that.scpos=posx+that.deltaclick;
				if (that.scpos<that.scrollwidth) that.scpos=that.scrollwidth;
				if (that.scpos>lx-that.scrollwidth-that.movefield) that.scpos=lx-that.scrollwidth-that.movefield; 
				fc=1.0*(that.scpos-that.scrollwidth)/Math.max((lx-2*that.scrollwidth-that.movefield),1);
				nscrollpos=Math.floor(that.scrollmax*fc);
				if (nscrollpos<0) nscrollpos=0;
				if (nscrollpos>that.scrollmax) nscrollpos=that.scrollmax;
				that.deltascroll=that.scrollpos-nscrollpos;
				if (that.scrollpos!=nscrollpos)
				{
					that.scrollpos=nscrollpos;//kijken of dat hier zo blijft
					that.iparent.sethscroll(nscrollpos,that.scrollmax,that.deltascroll);
					that.render();
				}
				return;
			}
			if (that.focus==false) {that.focus=true;that.render;}
		}
		that.MouseDown=function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			$(document).on('mousemove',$.proxy(that.MouseMove,that));
			$(document).on('mouseup',$.proxy(that.MouseUp,that));
//			var posx=e.offsetX;
//			var posy=e.offsetY;
			var posx=e.offsetX==undefined?e.originalEvent.layerX:e.offsetX;
			var posy=e.offsetY==undefined?e.originalEvent.layerY:e.offsetY;
			var lx=this.iparent.getMyCanvasElement('hscroller').width;
			xpos1=that.scrollwidth;
			xpos2=that.scpos;
			xpos3=that.scpos+that.movefield;
			xpos4=lx-that.scrollwidth;
			that.press=0;
			if (posx<xpos1) that.press=1;
			if (posx>=xpos2 && posx<=xpos3) that.press=2;
			if (posx>=xpos4) that.press=3;
			if (that.press==1 || that.press==3)	that.StartTimer();
			if (posx>that.scpos-that.movefield && posx<that.scpos+that.movefield)	that.slide=2;
			if (posx>that.scrollwidth && posx<that.scpos-that.movefield) that.slide=1;
			if (posx>that.scpos+that.movefield && posx<lx-that.scrollwidth) that.slide=3;
			if (that.slide>0)
			{
				that.deltaclick=that.scpos-posx;
				that.click=posx;
				that.screenclick=e.screenX;
				if (that.slide==1 || that.slide==3) that.StartTimer();
			}
			that.render();
		}
		that.MouseUp=function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			that.press=0;
			that.slide=0;
			$(document).off('mousemove', that.MouseMove);
			$(document).off('mouseup',that.MouseUp);
			that.render();
		}
		that.getscroll=function(nscrollpos,scrollmax,deltascroll)
		{
			that.iparent.sethscroll(nscrollpos,scrollmax,deltascroll);
		}
		return that;
	}

	MScroller.ZScroll=function(_iparent)
	{
		var that=new MScroller.body(_iparent);
		that.hidescroller=true;
		that.scrollmax=1000;
		that.scrollpos=500;
		that.zoomer=true;
		that.render = function () 
		{
			if (that.IsPresent()==false) return;
			var ctx=this.iparent.getMyCanvasElement('zscroller').getContext("2d");
			var lx=this.iparent.getMyCanvasElement('zscroller').width;
			var ly=this.iparent.getMyCanvasElement('zscroller').height;
			that.scpos=that.scrollwidth;
			if (that.scrollmax>0) that.scpos=((lx-that.movefield-2*that.scrollwidth)*that.scrollpos/that.scrollmax)+that.scrollwidth;
			var px=0;
			xpos1=that.scrollwidth;
			xpos2=that.scpos;
			xpos3=that.scpos+that.movefield;
			xpos4=lx-that.scrollwidth;
			
			//->scrollbar background
			var klr=that.background;
			if (that.focus==true) klr='rgba(230,230,200,1.0)';
			ctx.fillStyle=klr;
			ctx.fillRect(0,0,lx,ly);
			
			//->left press button
			ctx.fillStyle=that.background;
			if (that.press==1) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(0,0,xpos1,ly);
			
			//->slider button
			ctx.fillStyle='rgba(192,192,192,1.0)';
			if (that.press==2) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(xpos2,2,xpos3-xpos2,ly-3);
			
			//->right press button
			ctx.fillStyle=that.background;
			if (that.press==3) ctx.fillStyle='rgba(150,150,150,1.0)';
			ctx.fillRect(xpos4,0,ly,ly);

			//->left arrow
			ctx.beginPath();
			ctx.strokeStyle='rgba(100,100,100,1.0)';
			if (that.press==1) ctx.strokeStyle='rgba(0,0,0,1.0)';
			var psx=Math.round(that.scrollwidth/2);
			var ypc=Math.round(that.scrollwidth/2);
			var sze=Math.round(that.scrollwidth/4);
			ctx.lineWidth=3;
			ctx.moveTo(psx-sze,ypc);
			ctx.lineTo(psx+sze,ypc);
			ctx.closePath();
			ctx.stroke();

			//->right arrow
			ctx.beginPath();
			ctx.strokeStyle='rgba(100,100,100,1.0)';
			if (that.press==3) ctx.strokeStyle='rgba(0,0,0,1.0)';
			psx=lx-Math.round(that.scrollwidth/2);
			ctx.lineWidth=3;
			ctx.moveTo(psx-sze,ypc);
			ctx.lineTo(psx+sze,ypc);
			ctx.moveTo(psx,ypc-sze);
			ctx.lineTo(psx,ypc+sze);
			ctx.closePath();
			ctx.stroke();

			ctx.fillStyle='rgba(200,200,200,1.0)';
			ctx.fillRect(0,0,lx,1);
			ctx.fillRect(0,ly-1,lx,1);
			ctx.fillRect(0,0,1,ly);
			ctx.fillRect(lx-1,0,1,ly);
		}
		
		that.MouseMove=function(_e)
		{
			var e=window.event || _e;
			var lx=this.iparent.getMyCanvasElement('zscroller').width;
//			e.preventDefault();
//			e.stopPropagation();
			var posx=that.click+e.screenX-that.screenclick;
			if (that.slide==2)
			{
				that.scpos=posx+that.deltaclick;
				if (that.scpos<that.scrollwidth) that.scpos=that.scrollwidth;
				if (that.scpos>lx-that.scrollwidth-that.movefield) that.scpos=lx-that.scrollwidth-that.movefield; 
				fc=1.0*(that.scpos-that.scrollwidth)/Math.max((lx-2*that.scrollwidth-that.movefield),1);
				nscrollpos=Math.floor(that.scrollmax*fc);
				if (nscrollpos<0) nscrollpos=0;
				if (nscrollpos>that.scrollmax) nscrollpos=that.scrollmax;
				that.deltascroll=that.scrollpos-nscrollpos;
				if (that.scrollpos!=nscrollpos)
				{
					that.scrollpos=nscrollpos;//kijken of dat hier zo blijft
//					that.iparent.setzscroll(nscrollpos,that.scrollmax,that.deltascroll);
					that.render();
				}
				return;
			}
			if (that.focus==false) {that.focus=true;that.render;}
		}
		that.MouseDown=function(e)
		{
//			e.preventDefault();
//			e.stopPropagation();
			$(document).on('mousemove', $.proxy(that.MouseMove,that));
			$(document).on('mouseup',$.proxy(that.MouseUp,that));
//			var posx=e.offsetX;
//			var posy=e.offsetY;
			var posx=e.offsetX==undefined?e.originalEvent.layerX:e.offsetX;
			var posy=e.offsetY==undefined?e.originalEvent.layerY:e.offsetY;
			var lx=this.iparent.getMyCanvasElement('zscroller').width;
			xpos1=that.scrollwidth;
			xpos2=that.scpos;
			xpos3=that.scpos+that.movefield;
			xpos4=lx-that.scrollwidth;
			that.press=0;
			if (posx<xpos1) that.press=1;
			if (posx>=xpos2 && posx<=xpos3) that.press=2;
			if (posx>=xpos4) that.press=3;
			if (that.press==1 || that.press==3)	that.StartTimer();
			if (posx>that.scpos-that.movefield && posx<that.scpos+that.movefield)	that.slide=2;
			if (posx>that.scrollwidth && posx<that.scpos-that.movefield) that.slide=1;
			if (posx>that.scpos+that.movefield && posx<lx-that.scrollwidth) that.slide=3;
			if (that.slide>0)
			{
				that.deltaclick=that.scpos-posx;
				that.click=posx;
				that.screenclick=e.screenX;
				that.StartTimer();
			}
		}
		that.MouseUp=function(e)
		{
//			e.preventDefault();
//			e.stopPropagation();
			that.press=0;
			that.slide=0;
			$(document).off('mousemove', that.MouseMove);
			$(document).off('mouseup',that.MouseUp);
			that.scrollpos=that.scrollmax/2;
			that.render();
		}
		that.getscroll=function(nscrollpos,scrollmax,deltascroll)
		{
			that.iparent.setzscroll(nscrollpos,scrollmax,deltascroll);
		}
		return that;
	}
	return MScroller;
})

