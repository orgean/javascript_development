(function(exports)
{

exports.ClusterStretch=function(_project)
{
	var that={};
	that.score=0;
	that.ori=1;
	that.include=0;
	that.Items=[];
	that.AddStretch=function(nr)
	{
		for (var i=0;i<that.Items.length;i++) if (that.Items[i]==nr) return 0;
		that.Items.push(nr);
		return 1;
	}

	that.Reset=function()
	{
		that.Items.length=0;
		that.score=0;
		that.ori=1;
		that.include=0;
	}

	that.GetItemCnt=function()
	{
		return that.Items.length;
	}

	that.GetItem=function(nr)
	{
		if (nr<0 || nr>=that.GetItemCnt()) return -1;
		return that.Items[nr];
	}

	that.AddScore=function(val)
	{
		that.score+=val;
	}
	return that;
}//****************************** Txymatrix *************************************
exports.Txymatrix=function(_project)
{
	var that={};
	that.project=_project;
	that.classobjCode1="";
	that.classobjCode2="";
	that.xlen=0;
	that.ylen=0;
	that.block=10000;
	that.ll1=0;
	that.ll2=0;
	that.clusterNumber1=0;
	that.clusterNumber2=0;
	that.clusterColor1='rgb(0,0,0)';
	that.clusterColor2='rgb(0,0,0)';
	that.opened=false;
	that.fielddata1="";
	that.fielddata2="";
	that.direct_dots=null;
	that.reverse_dots=null;
	that.dataReceived=false;
	that.skip=false;
	that.canvas=null;
	that.ctx=null;
	that.unfiltered_stretches=0;
	that.filtered_stretches=0;
	that.longest_stretch=0;
	that.imageData=new ImageDataObject.ImageData();
	that.cPos1=0;
	that.cPos2=0;
	that.lines=null;
	

	that.GetServerCommand=function()
	{
		var command=that.project.GetServerCommand();
		command.classobjcode1=that.classobjCode1;
		command.classobjcode2=that.classobjCode2;
		command.Txymatrix=that.index;
		command.arrayblock=that.block;
		return command;
	}
	that.Initiate=function(_index,code1,code2,_xlen,_ylen,_block,num1,num2,klr1,klr2)
	{
		that.index=_index;
		that.classobjCode1=code1;
		that.classobjCode2=code2;
		that.ll1=_xlen;
		that.ll2=_ylen;
		that.block=_block;
		that.xlen=Math.floor(_xlen/that.block)+1;
		that.ylen=Math.floor(_ylen/that.block)+1;
		var seedcompsettings=that.project.getDotPlotSeedCompSettings();
		if (seedcompsettings.Reflexive_calculation==false && num1==num2) that.skip=true;
		that.clustnum1=num1;
		that.clustnum2=num2;
		that.clustcolor1=klr1;
		that.clustcolor2=klr2;
		that.fielddata1="";
		that.fielddata2="";
		that.canvas=null;
	}

	that.SendServerRequestCellCanvasData=function()
	{
		var command=that.GetServerCommand();
		command.type='RequestDPCellCanvasData';
		that.project.sendRequest(command);
	}
	that.SendServerRequestCellStretchData=function()
	{
		var command=that.GetServerCommand();
		command.type='RequestDPCellStretchData'
		command.settings=that.project.dotplot.filtersettings;
//		command.fullread=that.project.dotplot.StretchNoFilter;
		command.fullread=true;
		command.stretchdatatype=that.project.dotplot.StretchDataType;
		that.project.sendRequest(command);
	}
	that.SendServerRequestCellAlign=function()
	{
		var command=that.GetServerCommand();
		command.type='RequestDPCellAlign';
		that.project.sendRequest(command);
	}
	that.SendServerRequestCellVariations=function(entry_accession,seqboxcode)
	{
		var command=that.GetServerCommand();
		command.type='RequestDPCellVariations';
		command.entryaccession=entry_accession;
		command.seqboxcode=seqboxcode;
		that.project.sendRequest(command);
	}

	that.SetData=function()
	{
		that.xlen=Math.floor(that.ll1/that.block)+1;
		that.ylen=Math.floor(that.ll2/that.block)+1;
		if (that.project.dotplot.circles==true)
		{
			that.xlen=200;
			that.ylen=200;
		}
		that.dataReceived=true;
		if (that.canvas==null) that.canvas=document.createElement('canvas');
		that.canvas.width=that.xlen;
		that.canvas.height=that.ylen;
		that.ctx=that.canvas.getContext('2d');
	}
	
	that.ClearCanvas=function()
	{
/*		if (that.canvas==null) return;
		context.fillStyle="rgba(0,0,0,0)";
		context.fillRect(0,0,that.canvas.width,that.canvas.height);*/
	}

	that.drawPixelCanvas=function()
	{
		var lines=that.lines;
		if (that.project.dotplot.circles==false)
		{
			if (lines!=null)
			{
				var x=0,y=0,_x=0,_y=0;
				var obj=null;
				for (x=0;x<lines.length;x++)
				{
					obj=lines[x];
					if (obj.x==_x && obj.y==_y) continue;
					if (obj.val==1) that.ctx.fillStyle='rgb(255,255,255)';
					if (obj.val==2) that.ctx.fillStyle='rgb(0,0,0)';
					if (obj.val==3) that.ctx.fillStyle='rgb(255,0,255)';
					if (obj.val==4) that.ctx.fillStyle='rgb(200,0,200)';
					that.ctx.fillRect(obj.x,obj.y,1,2);
					_x=obj.x;
					_y=obj.y;
				}
			}
		}
		else
		{
			var context=that.canvas.getContext('2d');
			var PI=Math.PI;
			var cx=that.canvas.width/2;
			var cy=that.canvas.height/2;
			var ll=(that.ll1+that.ll2)/that.block;
			var rad=9*Math.min(cx,cy)/10;

			var floor=Math.floor;
			var sin=Math.sin;
			var cos=Math.cos;
			var pt1x=0,pt1y=0,pt2x=0,pt2y=0;
			if (lines!=null)
			{
				var obj=null;
				for (var x=0;x<lines.length;x++)
				{
					obj=lines[x];
					var h1=2*PI-2*PI*obj.x/ll;
					pt1x=floor(cx-sin(h1)*rad);
					pt1y=floor(cy-cos(h1)*rad);
					if (obj.val<=2) continue;
					var y1=(that.ll1+that.ll2)/that.block-obj.y;
					var h2=2*PI-2*PI*y1/ll;
					pt2x=floor(cx-sin(h2)*rad);
					pt2y=floor(cy-cos(h2)*rad);

					context.beginPath();
					if (obj.val==4) context.strokeStyle='rgba(255,100,100,0.2)';else context.strokeStyle='rgba(100,100,255,0.2)';
					context.moveTo(pt1x,pt1y);
					context.quadraticCurveTo(cx,cy,pt2x,pt2y);
					context.stroke();
					context.closePath();
				}
			}
			var h1=2*PI*(that.ll1/that.block)/ll-PI/2;
			var h2=2*PI*((that.ll1+that.ll2)/that.block)/ll-PI/2;
			context.beginPath();
//			context.arc(cx,cy,rad,0,2*Math.PI,false);
			context.arc(cx,cy,rad,-PI/2,h1,false);
			context.lineWidth=3;
			context.strokeStyle='rgba(128,255,255,1.0)';
			context.stroke();
			context.closePath();

			context.beginPath();
			context.arc(cx,cy,rad,h1,h2,false);
			context.lineWidth=3;
			context.strokeStyle='rgba(255,255,0,1.0)';
			context.stroke();
			context.closePath();
		}
	}
	
	that.HasDataReceived=function()
	{
		return that.dataReceived;
	}
	that.getClassObjectCode1=function()
	{
		return that.classobjCode1;
	}
	that.getClassObjectCode2=function()
	{
		return that.classobjCode2;
	}
	that.getClassObject1=function()
	{
		return that.project.FindClassObject(that.classobjCode1);
	}
	that.getClassObject2=function()
	{
		return that.project.FindClassObject(that.classobjCode2);
	}
	that.getClassObjectOrganism1=function()
	{
		var classobject=that.project.FindClassObject(that.classobjCode1);
		if (classobject!=null) return classobject.GetOrganismParent();
		return null;
	}
	that.getClassObjectOrganism2=function()
	{
		var classobject=that.project.FindClassObject(that.classobjCode2);
		if (classobject!=null) return classobject.GetOrganismParent();
		return null;
	}
	that.isMatrixFromSameOrganism=function()
	{
		var class1=that.getClassObjectOrganism1();
		var class2=that.getClassObjectOrganism2();
		if (class1==class2) return true;
		return false;
	}
	that.GetCellAlignFilePath=function()
	{
		var path=that.project.projectPath+'/sequence_data/dotplot/'+that.classobjCode1+"_"+that.classobjCode2+".aln";
		return path;
	}
	that.GetSeq1=function()
	{
		var seq={};
		seq.organism="";seq.sequence="";seq.length="0bp";
		var classobject=that.project.FindClassObject(that.classobjCode1);
		if (classobject!=null)
		{
			var p_class=classobject.GetOrganismParent();
			if (p_class!=null)
			{
				seq.organism=p_class.GetDefiner()+" ("+classobject.GetUniqueCode()+")";
				seq.sequence=classobject.GetFieldData();
				seq.length=that.ll1+"bp";
			}
		}
		return seq;
	}
	that.GetSeq2=function()
	{
		var seq={};
		seq.organism="";seq.sequence="";seq.length="0bp";
		var classobject=that.project.FindClassObject(that.classobjCode2);
		if (classobject!=null)
		{
			var p_class=classobject.GetOrganismParent();
			if (p_class!=null)
			{
				seq.organism=p_class.GetDefiner()+" ("+classobject.GetUniqueCode()+")";
				seq.sequence=classobject.GetFieldData();
				seq.length=that.ll2+"bp";
			}
		}
		return seq;
	}
	return that;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
exports.DotPlotUnit=function(project)
{
	var that={};
	that.project=project;
	that.sequenceLengths=null;
	that.orderswitch=[];
	that.arrays=[];
	that.jobs=[];
	that.matrixsize=0;
	that.arrayblock=10000;
	that.maxLength=0;
	
	that.dotplotoverview_callback=null;
	
	that.stretches=null;
	that.stretchindex=[];
	that.variations=null;
	that.uniquestretches=[];
	that.superstretches=null;
	that.multialigns=[];
	
	that.filtersettings={}
	that.alignsettings={}
	that.seedcompsettings={};
	that.multialignsettings={};
	
	that.current_cell=null;
	that.calculated_cells=0;
	that.loaded_cells=0;
	that.loadStatus=0;
	that.needUpdate=true;
	that.cellStatus="needUpdate";
	that.cancelled=false;
	that.circles=false;
	that.calculated=false;
	that.StretchesCalculated=2;//0:niet berekend, 1:loopt, 2: berekend
	that.StretchesAligned=2;
	that.canvas=document.createElement('canvas');
	
	that.display_diagonal_cutoff=2000000;//tonen van stretches die te ver liggen onderbinden
	that.StretchDataType="synteny";
	that.StretchNoFilter=false;
	that.heapMapColors=false;

	that.StandardSettings=function(seedcompsettings,filtersettings,alignsettings,multialignsettings)
	{
		filtersettings.repeatMin=0;
		filtersettings.repeatMax=1;
		filtersettings.filterX=true;
		filtersettings.filterY=true;
		filtersettings.lengthMin=0;
		filtersettings.lengthMax=Number.MAX_VALUE;
		filtersettings.yZoneFilter=false;
		filtersettings.yZoneStart=0;
		filtersettings.yZoneStop=0;
		
		alignsettings.diagonal_cutoff=100000;
		alignsettings.gapcost=0.5;
		alignsettings.unitcost=1.0;
		alignsettings.score_cutoff=0.01;
		alignsettings.head_to_tail_cutoff=100000;
		alignsettings.stretch_cluster_cutoff=6;
		alignsettings.fill_gaps=true;
		
		seedcompsettings.Reflexive_calculation=true;
		seedcompsettings.Seed_pattern="111111";
		seedcompsettings.Seed_hit_frequency_cutoff=100;
		seedcompsettings.Seed_extension_allowed_mismatches=15;
		seedcompsettings.Seed_extension_window_size=30;
		seedcompsettings.Loop_increment=10;
		seedcompsettings.Scan_increment=1;
		seedcompsettings.Stretch_identity_cutoff=70;
		seedcompsettings.Stretch_length_cutoff=70;
		seedcompsettings.Stretch_extension_window_size=20;
		seedcompsettings.Apply_filter=true;
		seedcompsettings.Filter_frequency_cutoff=1;
		
		multialignsettings.Templates=[];
	}
	that.SetStandardSettings=function(seedcompsettings,filtersettings,alignsettings,multialignsettings)
	{
		that.StandardSettings(seedcompsettings,filtersettings,alignsettings,multialignsettings);	
	}	

	that.SetStandardSettings(that.seedcompsettings,that.filtersettings,that.alignsettings,that.multialignsettings);	
	
	that.ReadInput=function()
	{
		var command=that.project.GetServerCommand();
		command.type='ReadDotPlotInput';
		that.project.sendRequest(command);
	}
	that.ReadSettings=function()
	{
		var command=that.project.GetServerCommand();
		command.type='ReadDotPlotSettings';
		that.project.sendRequest(command);
	}
	
/*	that.CreateDotPlotInputFile=function()
	{
		var path=that.project.projectPath+"/sequence_data/dotplot/dp_input.txt"
		var elements=that.project.elements;
		var buffer="";
		for (var i=0;i<elements.length;i++)
		{
			var classobj=elements[i].classobject;
			var clusternr=elements[i].clusternumber;
			var len=that.GetSequenceLength(classobj.GetUniqueCode());
			var path=that.project.Application.DecodePath(classobj.pathcode);
			path=path+"/"+classobj.filename;
			if (len>-1)//otherwice not succeeded
			{
//				buffer+="DataCluster\t"+clusternr+"\t"+classobj.GetUniqueCode()+"\t"+classobj.GetDataFile()+"\t"+len+"\n";
				buffer+="DataCluster\t"+clusternr+"\t"+classobj.GetUniqueCode()+"\t"+path+"\t"+len+"\n";
			}
		}
		var command={ 'type':'RequestDPInputFile', 'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath, 'stream':buffer};
		that.project.sendRequest(command);
	}*/
	
	that.SetSequenceLengths=function(json)
	{
		that.sequenceLengths=json.data;
		that.maxlength=0;
		for (var i=0;i<that.sequenceLengths.length;i++) 
		{
			that.maxLength=Math.max(that.sequenceLengths[i].sequenceLength,that.maxLength);
		}
		that.arrayblock=~~(that.maxLength/500);
	}
	that.GetSequenceLength=function(code)
	{
		if (that.sequenceLengths==null) return 0;
		for (var i=0;i<that.sequenceLengths.length;i++) 
		{
			if (that.sequenceLengths[i].classObjectCode===code) return that.sequenceLengths[i].sequenceLength;
		}
		return 0;
	}
	that.GetTotalSequenceLength=function()
	{
		var ll=0;
		for (var i=0;i<that.sequenceLengths.length;i++) 
		{
			ll+=that.sequenceLengths[i].sequenceLength;
		}
		return ll;
	}

	that.readdata=function()
	{
		that.cancelled=false;
		that.matrixsize=that.project.elements.length;
		that.orderswitch.length=0;
		that.arrays.length=0;
		for (var i=0;i<that.matrixsize;i++) that.orderswitch.push(i);
		for (var i=0;i<that.matrixsize;i++) 
		{
			for (var j=i;j<that.matrixsize;j++) 
			{
				var matrix=new exports.Txymatrix(that.project);
				that.arrays.push(matrix);
			}
		}
		that.jobs.length=0;
		for (var i=0;i<that.matrixsize;i++) that.jobs.push(new Array(that.matrixsize));
		var nr=0;
		that.calculated_cells=0;
		for (var i=0;i<that.matrixsize;i++)
		{
			var num1=that.project.elements[i].clusternumber;
			var klr1=that.project.elements[i].color;
			var code1=that.project.elements[i].classobject.GetUniqueCode();
			var ll1=that.GetSequenceLength(code1);
			var fielddata1=that.project.elements[i].classobject.GetFieldData();
			for (j=i;j<that.matrixsize;j++)
			{
				var num2=that.project.elements[j].clusternumber;
				var klr2=that.project.elements[j].color;
				var code2=that.project.elements[j].classobject.GetUniqueCode();
				var ll2=that.GetSequenceLength(code2);
				var fielddata2=that.project.elements[j].classobject.GetFieldData();
				that.arrays[nr].Initiate(nr,code1,code2,ll1,ll2,that.arrayblock,num1,num2,klr1,klr2);
				that.jobs[i][j]=nr;
				that.jobs[j][i]=nr;
//				if (that.reflexive_calculation==false && that.arrays[nr].isMatrixFromSameOrganism()) that.calculated_cells++;
				if (that.arrays[nr].skip==false) that.calculated_cells++;
				that.arrays[nr].ClearCanvas();
				nr++;
			}
		}
		//stuur eerste aanvraag
		that.loaded_cells=0;
		that.canvas.width=800;
		that.canvas.height=800;
		that.calcysizes();
		that.drawCMap();
		if (that.arrays.length>0) that.arrays[0].SendServerRequestCellCanvasData();
	}

	that.UpdateRun=function(json)//wordt na elke actie weer aangeroepen en volgende actie wordt uitgevoerd
	{
		if (json!=null)
		{
			if (json.action=="StretchCalculationFinished") that.StretchesCalculated=2;
			if (json.action=="StretchAlignmentFinished" && that.StretchesCalculated==2 && that.StretchesAligned==1) 
			{
				that.StretchesAligned=2;
				that.ReadInput();//wordt naar server gezonden, client krijgt resultaten en roept "SetSequenceLengths" aan. Daar wordt "readdata" aangeroepen, wat de verschillende cellen inleest
			}
		}
		if (that.StretchesCalculated==0)
		{
			that.RequestSeedCompCalculation();
			return;
		}
		if (that.StretchesCalculated==2 && that.StretchesAligned==0) 
		{
			that.RequestStretchAlignment();
			return;
		}
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(json);
	}
	that.ReLoad=function(json)
	{
		that.loaded_cells=0;
		that.drawCMap();
		if (that.arrays.length>0) that.arrays[0].SendServerRequestCellCanvasData();
	}
	that.SetDotPlotSettings=function(json)
	{
		if (that.cancelled) return;
		for (var i=0;i<json.seedcompsettings.length;i++) that.seedcompsettings[json.seedcompsettings[i].key]=json.seedcompsettings[i].value;
		for (var i=0;i<json.filtersettings.length;i++) that.filtersettings[json.filtersettings[i].key]=json.filtersettings[i].value;
		for (var i=0;i<json.alignsettings.length;i++) that.alignsettings[json.alignsettings[i].key]=json.alignsettings[i].value;
		that.ReadInput();
	}
	that.CheckSeedCompSettings=function(insett)
	{
		var update=2;
		var cursett=that.seedcompsettings;
		for (var key in cursett) if (cursett[key]!=insett[key]) update=0;
		return update;
	}
	that.CheckFilterSettings=function(insett)
	{
		var update=2;
		var cursett=that.filtersettings;
		for (var key in cursett) if (cursett[key]!=insett[key]) update=0;
		return update;
	}
	that.CheckAlignSettings=function(insett)
	{
		var update=2;
		var cursett=that.alignsettings;
		for (var key in cursett) if (cursett[key]!=insett[key]) update=0;
		return update;
	}
	that.ImportSettings=function(_seedcompsett,_filtersett,_alignsett,_multialignsett)
	{
		that.StretchesCalculated=that.CheckSeedCompSettings(_seedcompsett);
		that.StretchesAligned=2;
		if (that.CheckFilterSettings(_filtersett)==false || that.CheckAlignSettings(_alignsett)==false) that.StretchesAligned=0;
		if (that.StretchesCalculated==0) that.StretchesAligned=0;
		
		that.seedcompsettings=$.extend(true,{},_seedcompsett);
		that.filtersettings=$.extend(true,{},_filtersett);
		that.alignsettings=$.extend(true,{},_alignsett);
		that.multialignsettings=$.extend(true,{},_multialignsett);
		that.CreateSeedCompSettingsFile();
	}

	that.SetDotPlotCellData=function(json)
	{
		if (that.cancelled) return;
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		if (json.status=="ok")	that.arrays[json.Txymatrix].lines=json.lines;
		var nr=json.Txymatrix;
		if (nr<that.arrays.length-1)
		{
			nr++;
			while (nr<that.arrays.length-1 && that.arrays[nr].skip) nr++;
			that.loaded_cells++;
			that.loadStatus=100*that.loaded_cells/Math.max(that.calculated_cells,1);
			if (that.arrays[nr].skip==false) that.arrays[nr].SendServerRequestCellCanvasData();
		}
		if (nr==that.arrays.length-1) 
		{
			that.loadStatus=100.0;
			that.needUpdate=false;
			var k=0;
			for (var i=0;i<that.arrays.length;i++) 
			{
				if (that.arrays[i].skip==true) continue;
				if (that.arrays[i].dataReceived==false) k++;
				that.arrays[i].SetData();
			}
			if (k==0) that.calculated=true;
			that.generatePictures();
		}
		var action="UpdateStatus";
		if (nr==that.arrays.length-1) action="FinalUpdate";
		var command={'action':action,'status':0};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.generatePictures=function()
	{
//		var blob=new Blob(that.arrays);
//		var worker=new Worker(window.URL.createObjectURL(blob));
		for (var i=0;i<that.arrays.length;i++) 
		{
			if (that.arrays[i].skip==true) continue;
			that.arrays[i].SetData();
			that.drawCircleMap(that.arrays[i],1);
			that.drawCircleMap(that.arrays[i],2);
			that.arrays[i].drawPixelCanvas();
//			that.arrays[i].canvas.transferControlToOffscreen();
		}
//var worker = new Worker("offscreencanvas.js"); 
//worker.postMessage({canvas: offscreen}, [offscreen]);12345		}
	}
	
	that.getswitch=function(nr)
	{
		return nr;
	}
	that.calcysizes=function()
	{
		var ll=0;
		var cpos1array=[];
		var cpos2array=[];
		var ll=0;//eerst globale lengte berekenen om spatie te berekenen
		var elements=that.project.elements;
		for (var i=0;i<elements.length;i++)
		{
			elements[i].length=that.GetSequenceLength(elements[i].classobject.GetUniqueCode());
			ll=ll+elements[i].length/that.arrayblock;
			elements[i].color=('#'+Math.floor(Math.random()*16777215).toString(16));
		}
		var dd=ll/100;//spatie tussen chroms
		ll=0;//globale lengte met spatie
		for (var i=0;i<elements.length;i++)	
		{
			elements[i].cpos=ll;
			ll=ll+elements[i].length/that.arrayblock+dd;
			if (elements[i].joincanvas==null) {elements[i].joincanvas=document.createElement('canvas');elements[i].joincanvas.width=800;elements[i].joincanvas.height=800;}
			elements[i].r=Math.floor(Math.random()*200);
			elements[i].g=Math.floor(Math.random()*200);
			elements[i].b=Math.floor(Math.random()*200);
		}
		that.cLength=ll;
	}

	that.C_GetPoint=function(centrumX,centrumY,point,h,rad)
	{
		point.x=Math.floor(centrumX-Math.sin(Math.PI/2-h+Math.PI)*rad);
		point.y=Math.floor(centrumY-Math.cos(Math.PI/2-h+Math.PI)*rad);
	}
	that.setContextAlign=function(hk,context)
	{
		var PI=Math.PI;
		if (hk>2*PI) hk=hk-2*PI;
		if (hk<0) hk=hk+2*PI;
		if (hk>=0 && hk<PI/2) {context.textBaseline='bottom';context.textAlign="left";};
		if (hk>=PI/2 && hk<PI) {context.textBaseline='top';context.textAlign="left";};
		if (hk>=PI && hk<3*PI/2) {context.textBaseline='top';context.textAlign="right";};
		if (hk>=3*PI/2 && hk<2*PI) {context.textBaseline='bottom';context.textAlign="right";};
	}

	that.drawCMap=function()
	{
		var context=that.canvas.getContext('2d');
		context.fillStyle="rgba(250,250,250,1)";
		context.fillRect(0,0,that.canvas.width,that.canvas.height);
		var PI=Math.PI;
		var floor=Math.floor;
		var sin=Math.sin;
		var cos=Math.cos;
		var pt1x=0,pt1y=0,pt2x=0,pt2y=0;
		var _pt1x=0,_pt1y=0,_pt2x=0,_pt2y=0;

		var centrumX=that.canvas.width/2;
		var centrumY=that.canvas.height/2;
		var rad=that.canvas.height/2-100;
		var point1={x:0,y:0};
		var point2={x:0,y:0};

		var elements=that.project.elements;
/*		context.fillStyle="rgba(0,0,0,1)";
		context.fillText("Mouse over:",10,10)
		var d=100;
		for (var i=0;i<elements.length;i++)
		{
			context.fillStyle=elements[i].color;
			context.fillRect(d,0,10,20);
			d+=4;
		}*/
		context.fillStyle="rgba(0,0,0,1)";
		for (var i=0;i<elements.length;i++)
		{
			var h1=2*PI*elements[i].cpos/that.cLength-PI/2;
			var h2=2*PI*(elements[i].cpos+elements[i].length/that.arrayblock)/that.cLength-PI/2;

			context.beginPath();
			that.C_GetPoint(centrumX,centrumY,point1,h1,rad-6);that.C_GetPoint(centrumX,centrumY,point2,h1,rad+6);
			context.moveTo(point1.x,point1.y);context.lineTo(point2.x,point2.y);
			that.C_GetPoint(centrumX,centrumY,point1,h2,rad-6);that.C_GetPoint(centrumX,centrumY,point2,h2,rad+6);
			context.moveTo(point1.x,point1.y);context.lineTo(point2.x,point2.y);
			context.lineWidth=1;
			context.strokeStyle='rgb(0,0,0)';
			context.stroke();
			context.closePath();
						
			context.beginPath();
			context.arc(centrumX,centrumY,rad,h1,h2,false);
			context.lineWidth=3;
			context.strokeStyle=elements[i].color;
			context.stroke();
			context.closePath();

			h2=2*PI*(elements[i].cpos+(elements[i].length/2)/that.arrayblock)/that.cLength-PI/2;
			var hk=h2+PI/2;
			that.setContextAlign(hk,context);
			that.C_GetPoint(centrumX,centrumY,point1,h2,rad+12);
			context.save();
			context.translate(point1.x,point1.y);
			if (h2<PI/2) context.rotate(h2);else context.rotate(h2+PI);
			var classobject=elements[i].classobject;
			if (classobject!=null)
			{
				var p_class=classobject.GetOrganismParent();
				if (p_class!=null) context.fillText(classobject.GetFieldData(),0,0);
			}
			context.restore();
		}
	}
/*	that.drawCircleMap=function(lines,matrix,repeat)
	{
		if (matrix==null || matrix.skip || lines==null) return;
		var code="";
		if (repeat==1) code=matrix.classobjCode1;
		if (repeat==2) code=matrix.classobjCode2;
		var _element=that.project.FindElement(code);
		if (_element==null) return;
		var cnv=_element.joincanvas;//var cnv=that.canvas;//slechts ��n globale canvas
		var rad=cnv.height/2-100;
		var context=cnv.getContext('2d');
		var PI=Math.PI;
		var floor=Math.floor;
		var sin=Math.sin;
		var cos=Math.cos;
		var pt1x=0,pt1y=0,pt2x=0,pt2y=0;

		var centrumX=cnv.width/2;
		var centrumY=cnv.height/2;

		var ll=that.cLength;
		var k2=0;
		for (var x=0;x<lines.length;x++)
		{
			var line=lines[x];
			for (var y=0;y<line.length;y++)	if (line[y]>2) k2++;
		}
		var h=10/k2;if (h>0.1) h=0.1;if (h<0.005) h=0.005;//inschatten hoe hoog de homologie is, meer homologie -> hogere alpha waarde in kleur
		h=0.03;
		context.strokeStyle='rgba('+_element.r+','+_element.g+','+_element.b+','+h+')';
		var element1=that.project.FindElement(matrix.classobjCode1);
		var element2=that.project.FindElement(matrix.classobjCode2);
		for (var x=0;x<lines.length;x++)
		{
			var x1=element1.cpos+x;
			var line=lines[x];
			for (var y=0;y<line.length;y++)
			{
				if (line[y]<=2) continue;
				var y1=element2.cpos+matrix.ll2/matrix.block-y;

				var h1=2*PI-2*PI*x1/ll;
				var h2=2*PI-2*PI*y1/ll;
									
				pt1x=floor(centrumX-sin(h1)*rad);
				pt1y=floor(centrumY-cos(h1)*rad);
									
				pt2x=floor(centrumX-sin(h2)*rad);
				pt2y=floor(centrumY-cos(h2)*rad);

				context.beginPath();
				context.moveTo(pt1x,pt1y);
				context.quadraticCurveTo(centrumX,centrumY,pt2x,pt2y);
				context.stroke();
				context.closePath();
			}
		}
	}*/
	
	that.drawCircleMap=function(matrix,repeat)
	{
		if (matrix==null || matrix.skip) return;
		var lines=matrix.lines;
		var code="";
		if (repeat==1) code=matrix.classobjCode1;
		if (repeat==2) code=matrix.classobjCode2;
		var _element=that.project.FindElement(code);
		if (_element==null) return;
		var cnv=_element.joincanvas;//var cnv=that.canvas;//slechts ��n globale canvas
		var rad=cnv.height/2-100;
		var context=cnv.getContext('2d');
		var PI=Math.PI;
		var floor=Math.floor;
		var sin=Math.sin;
		var cos=Math.cos;
		var pt1x=0,pt1y=0,pt2x=0,pt2y=0,_pt1x=0,_pt1y=0,_pt2x=0,_pt2y=0;
		var y1=0,h1=0,h2=0;

		var centrumX=cnv.width/2;
		var centrumY=cnv.height/2;

		var ll=that.cLength;
		context.strokeStyle='rgba('+_element.r+','+_element.g+','+_element.b+',0.03)';
		var element1=that.project.FindElement(matrix.classobjCode1);
		var element2=that.project.FindElement(matrix.classobjCode2);
		var obj=null;
		var x1=0;
		var r=0;
		if (lines!=null)
		for (var x=0;x<lines.length;x++)
		{
			obj=lines[x];
			x1=element1.cpos+obj.x;
			h1=2*PI-2*PI*x1/ll;
			pt1x=floor(centrumX-sin(h1)*rad);
			pt1y=floor(centrumY-cos(h1)*rad);
			if (obj.val<=2) continue;
			y1=element2.cpos+matrix.ll2/matrix.block-obj.y;
			h2=2*PI-2*PI*y1/ll;
			pt2x=floor(centrumX-sin(h2)*rad);
			pt2y=floor(centrumY-cos(h2)*rad);
			if (r>0 && (_pt2x!=pt2x || _pt2y!=pt2y || _pt1x!=pt1x || _pt1y!=pt1y)) 
			{
				context.strokeStyle='rgba('+_element.r+','+_element.g+','+_element.b+','+Math.min(r*0.03,0.2)+')';
				context.beginPath();
				context.moveTo(_pt1x,_pt1y);
				context.quadraticCurveTo(centrumX,centrumY,_pt2x,_pt2y);
				context.stroke();
				context.closePath();
				r=0;
			}
			r++;
			_pt1x=pt1x;
			_pt1y=pt1y;
			_pt2x=pt2x;
			_pt2y=pt2y;
		}
		if (r>0)
		{
			context.strokeStyle='rgba('+_element.r+','+_element.g+','+_element.b+','+Math.max(r*0.03,0.2)+')';
			context.beginPath();
			context.moveTo(_pt1x,_pt1y);
			context.quadraticCurveTo(centrumX,centrumY,_pt2x,_pt2y);
			context.stroke();
			context.closePath();
		}
	}

	that.SetDotPlotCellStretchData=function(json)
	{
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		that.current_cell=that.arrays[json.Txymatrix];
		that.stretches=json.stretches;
		that.stretchindex.length=0;
		var ll=that.stretches.xpossen.length;
		if (that.StretchNoFilter==true)	for (var j=0;j<ll;j++) that.stretchindex.push(j);
		else for (var j=0;j<ll;j++) if (that.stretches.includes[j]>0) that.stretchindex.push(j);
		var command={'action':'UpdateCell','what_is_updated':''};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.SetDotPlotCellStretchIncludeData=function(json)
	{
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		that.current_cell=that.arrays[json.Txymatrix];
	}
	that.SetDotPlotBestHits=function(json)
	{
		that.besthits=json.data;
		var command={'action':'ImportBestHits','what_is_updated':''};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.SetDotPlotNoHits=function(json)
	{
		that.nohits=json.data;
		var command={'action':'ImportNoHits','what_is_updated':''};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.SetDotPlotCellUniqueStretchData=function(json)
	{
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		that.current_cell=that.arrays[json.Txymatrix];
		var lens=json.data.lens;
		var scores=json.data.copyscores;
		var ll=lens.length;
		var k1=0;k2=0;
		that.uniquestretches.length=0;
		for (var i=0;i<ll;i++)
		{
			k2=k1+lens[i];
			if (scores[i]==0)
			{
				var obj={start:k1,end:k2,copy:scores[i]}
				that.uniquestretches.push(obj);
			}
			k1=k2+1;
		}
		var command={'action':'UpdateCell','what_is_updated':'stretchlist'};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.SetCellAlignData=function(json)
	{
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		that.current_cell=that.arrays[json.Txymatrix];
		that.superstretches=json.superstretches;
		json.action='ImportAlignBlocks';
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(json);
	}
	that.SetCellMultiAlignData=function(json)
	{
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		that.current_cell=that.arrays[json.Txymatrix];
		that.superstretches=json.superstretches;
		json.action='ImportAlignBlocks';
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(json);
	}
	that.SetCellVariationsData=function(json)
	{
		if (json.Txymatrix<0 || json.Txymtrix>=that.arrays.length) return;
		that.current_cell=that.arrays[json.Txymatrix];
		that.variations=json.variations;
		json.action='ImportVariations';
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(json);
	}
	that.SetMultiAlignData=function(json)
	{
	}
	that.GetCloseVariation=function(ps)
	{
		var p1=0;
		var p2=that.variations.length-1;
		var cnt=0;
		while (cnt<that.variations.length)
		{
			var p=Math.floor((p1+p2)/2);
			if (that.variations[p]>ps) p2=p;else p1=p;
			if (p1==p2 || p1==p2-1)	return p1;
			cnt++;
		}
		return -1;
	}
	that.getMatrixCount=function() {return that.arrays.length;}
	that.getMatrix=function(index)
	{
		if (index<0 || index>=that.arrays.length) return null;
		return that.arrays[index];
	}
	that.GetStretchFromPos=function(x,y)
	{
		if (that.current_cell==null) return -1;
		var fnd=-1;var fac=5;var score=Number.MAX_VALUE;var sc=0;
		var x1=0;var x2=0;var y1=0;var y2=0;
		var i=that.GetStretchCount();
		var abs=Math.abs;
		var stretches=that.stretches;
		var len=0,index=-1;
		while (i--)
		{
			index=that.stretchindex[i];
			len=stretches.lens[index];
			x1=stretches.xpossen[index];
			x2=x1+len;
			if (stretches.oris[index]>0)
			{
				y1=stretches.ypossen[index];
				y2=y1+len;
			}
			else
			{
				y2=that.current_cell.ll2-stretches.ypossen[index];
				y1=y2-len;
			}
			sc=abs((x1-y1)-(x-y));
			if (x<x1) sc+=fac*abs(x1-x);
			if (x>x2) sc+=fac*abs(x2-x);
			if (y<y1) sc+=fac*abs(y1-y);
			if (y>y2) sc+=fac*abs(y2-y);
			if (sc<score) {score=sc;fnd=i;}
		}
		return fnd;
	}	
	that.SelectOverlapStretches=function(_stretch)
	{
		that.stretchindex.length=0;
		var ll=that.stretches.xpossen.length;
		var add=0;
		for (var j=0;j<ll;j++) 
		{
			add=0;
			if (that.stretches.xpossen[j]>_stretch.x && that.stretches.xpossen[j]<=_stretch.x+_stretch.len) add=1;
			if (that.stretches.ypossen[j]>_stretch.y && that.stretches.ypossen[j]<=_stretch.y+_stretch.len) add=1;
			if (that.stretches.xpossen[j]+that.stretches.lens[j]>_stretch.x && that.stretches.xpossen[j]+that.stretches.lens[j]<=_stretch.x+_stretch.len) add=1;
			if (that.stretches.ypossen[j]+that.stretches.lens[j]>_stretch.y && that.stretches.ypossen[j]+that.stretches.lens[j]<=_stretch.y+_stretch.len) add=1;
			if (that.stretches.xpossen[j]<_stretch.x && that.stretches.xpossen[j]+that.stretches.lens[j]>_stretch.x+_stretch.len) add=1;
			if (that.stretches.ypossen[j]<_stretch.y && that.stretches.ypossen[j]+that.stretches.lens[j]>_stretch.y+_stretch.len) add=1;
			if (add==1) that.stretchindex.push(j);
		}
		var command={'action':'UpdateCell','what_is_updated':'invalidate_all'};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.SetAllStretches=function()
	{
		that.stretchindex.length=0;
		var ll=that.stretches.xpossen.length;
		if (that.StretchNoFilter==true)	for (var j=0;j<ll;j++) that.stretchindex.push(j);
		else for (var j=0;j<ll;j++) if (that.stretches.includes[j]>0) that.stretchindex.push(j);
		var command={'action':'UpdateCell','what_is_updated':'invalidate_all'};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	that.GetStretchCount=function()
	{
		return that.stretchindex.length;
	}
	that.GetStretch=function(index,stretch)
	{
		if (index>=0 && index<that.stretchindex.length) 
		{
			var i=that.stretchindex[index];
			stretch.x=that.stretches.xpossen[i];
			stretch.y=that.stretches.ypossen[i];
			stretch.len=that.stretches.lens[i];
			stretch.score=that.stretches.scores[i];
			stretch.include=that.stretches.includes[i];
			stretch.ori=that.stretches.oris[i];
			return stretch;
		}
		stretch.x=0;
		stretch.y=0;
		stretch.len=0;
		stretch.score=0;
		stretch.include=0;
		stretch.ori=0;
		return stretch;
	}
	that.getStretches=function()
	{
		return that.stretches;
	}

	that.DeleteCurrentCell=function()
	{
		if (that.current_cell==null) return;
		var classobject1=that.current_cell.getClassObject1();
		var classobject2=that.current_cell.getClassObject2();
		var command={ 'type':'RequestDPDeleteCurrentCell',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath, 'classobjcode1':classobject1.GetUniqueCode(), 'classobjcode2':classobject2.GetUniqueCode() };
		that.project.sendRequest(command);
	}
	that.CreateSeedCompSettingsFile=function()
	{
		var command={ 'type':'RequestSeedCompSettingsFile',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath, 'seedcompsettings':that.seedcompsettings,'alignsettings':that.alignsettings,'filtersettings':that.filtersettings,'multialignsettings':that.multialignsettings};
		that.project.sendRequest(command);
	}
	that.RequestSeedCompCalculation=function()
	{
		that.StretchesCalculated=1;
		var command={ 'type':'ServerRequestSeedCompCalculation',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath};
		that.project.sendRequest(command);
	}
	that.RequestFindUniqueSeq=function()
	{
		var command={ 'type':'ServerRequestStretchFindNoHits',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath};
		that.project.sendRequest(command);
	}
	that.RequestFindBestHits=function()
	{
		var command={ 'type':'ServerRequestStretchFindBestHits',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath};
		that.project.sendRequest(command);
	}
	that.RequestStretchAlignment=function()
	{
		that.StretchesAligned=1;
		var command={ 'type':'ServerRequestStretchAlignment',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath};
		that.project.sendRequest(command);
	}
	that.FindVariations=function(perform_align)
	{
		var command={ 'type':'RequestFindVariations',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath};
		that.project.sendRequest(command);
	}
	that.RequestMultiAlignScore=function()
	{
		var organismClusters=that.project.organismClusters;
		if (organismClusters.length==0) return;
		that.multialigns=[];
		var nn=0;
		var cc=organismClusters.length-1;
		if (organismClusters.length==1) cc=1;
		for (var i=0;i<cc;i++)
		{
			var template=organismClusters[nn];//tegenover cluster die als alignment template gedefinieerd werd
			var aligned=null;
			if (organismClusters.length==1) aligned=organismClusters[0];
			else 
			{
				aligned=organismClusters[i];
				if (i>=nn) aligned=organismClusters[i+1];
			}
			var alignment={templates:[]};
			var alignment={templ:template,align:aligned,templates:[]};
			for (var j=0;j<template.GetClassObjectCnt();j++)
			{
				var classobj1=template.GetClassObject(j);
				var templ_object={'code':classobj1.GetUniqueCode(),'aligns':[]}
				for (var k=0;k<aligned.GetClassObjectCnt();k++)
				{
					var classobj2=aligned.GetClassObject(k);
					var align_object={'code':classobj2.GetUniqueCode(),'score':0}
					templ_object.aligns.push(align_object);
//					console.log("->>>>>>>>> " +templ_object.code + "<>" + align_object.code);
				}
				alignment.templates.push(templ_object);
			}
			that.multialigns.push(alignment);
		}
		var command={ 'type':'ServerRequestMultiAlignScore',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath,'index':-1};
		that.SetDotPlotMultiAlignScore(command);
	}
	that.SetDotPlotMultiAlignScore=function(json)
	{
		var nr=0;
		for (var i=0;i<that.multialigns.length;i++)
		{
			var alignment=that.multialigns[i];
			for (var j=0;j<alignment.templates.length;j++)
			{
				var template=alignment.templates[j];
				for (var k=0;k<template.aligns.length;k++)
				{
					var align=template.aligns[k];
					if (json.index==nr)	{align.score=json.score;console.log("align score ->>>>>>>>> " +template.code + "<>" + align.code + "   "+json.score);}
					nr++;
				}
			}
		}
		nr=0;
		for (var i=0;i<that.multialigns.length;i++)//volgende aanvraag versturen
		{
			var alignment=that.multialigns[i];
			for (var j=0;j<alignment.templates.length;j++)
			{
				var template=alignment.templates[j];
				for (var k=0;k<template.aligns.length;k++)
				{
					var align=template.aligns[k];
					if (json.index+1==nr)
					{
						var command={ 'type':'ServerRequestMultiAlignScore',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath,'code1':template.code,'code2':align.code,'index':json.index+1};
						that.project.sendRequest(command);
						return;
					}
					nr++;
				}
			}
		}
		for (var i=0;i<that.multialigns.length;i++)
		{
			var alignment=that.multialigns[i];
			for (var j=0;j<alignment.templates.length;j++)
			{
				var template=alignment.templates[j];
				template.aligns.sort(function(a,b) { return a.score>b.score} );
			}
		}
//		that.RequestMultiAlignment();
	}
	that.RequestMultiAlignment=function()
	{
		var organismClusters=that.project.organismClusters;
		if (organismClusters.length==0) return;
		that.multialigns=[];
		that.longest_plot=1;
		var cc=organismClusters.length-1;
		if (organismClusters.length==1) cc=1;
		for (var i=0;i<cc;i++)
		{
			var template=organismClusters[i];
			var aligned=null;
			if (organismClusters.length==1) aligned=organismClusters[0];else aligned=organismClusters[i+1];
			var alignment={templ:template,align:aligned,templates:[],templ_intervals:[],align_intervals:[],templ_codes:[],align_codes:[]};
			var ll1=0;
			for (var j=0;j<template.GetClassObjectCnt();j++)
			{
				var classobj=template.GetClassObject(j);
				alignment.templ_intervals.push(ll1);
				ll1+=that.GetSequenceLength(classobj.GetUniqueCode());
				alignment.templ_codes.push(classobj.GetUniqueCode());
			}
			alignment.templ_intervals.push(ll1);
			that.longest_plot=Math.max(that.longest_plot,ll1);
			var ll2=0;
			for (var j=0;j<aligned.GetClassObjectCnt();j++)
			{
				var classobj=aligned.GetClassObject(j);
				alignment.align_intervals.push(ll2);
				ll2+=that.GetSequenceLength(classobj.GetUniqueCode());
				alignment.align_codes.push(classobj.GetUniqueCode());
			}
			alignment.align_intervals.push(ll2);
			that.longest_plot=Math.max(that.longest_plot,ll2);

			for (var j=0;j<template.GetClassObjectCnt();j++)
			{
				var classobj1=template.GetClassObject(j);
				var cl='rgba(' + Math.floor(Math.random() * 200) + ',' + Math.floor(Math.random() * 200) + ',' + Math.floor(Math.random() * 200) + ',0.3)';
				var templ_object={'code':classobj1.GetUniqueCode(),'aligns':[],'color':cl}
				for (var k=0;k<aligned.GetClassObjectCnt();k++)
				{
					var classobj2=aligned.GetClassObject(k);
					var align_object={'code':classobj2.GetUniqueCode(),'blocks':null}
					templ_object.aligns.push(align_object);
				}
				alignment.templates.push(templ_object);
			}
			that.multialigns.push(alignment);
		}
		var command={ 'type':'ServerRequestMultiAlignment',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath,'index':-1,'blocks':null};
		that.SetDotPlotMultiAlignData(command);
	}
	that.SetDotPlotMultiAlignData=function(json)
	{
		var nr=0;
		for (var i=0;i<that.multialigns.length;i++)
		{
			var alignment=that.multialigns[i];
			for (var j=0;j<alignment.templates.length;j++)
			{
				var template=alignment.templates[j];
				for (var k=0;k<template.aligns.length;k++)
				{
					var align=template.aligns[k];
					if (json.index==nr)	align.blocks=json.blocks;
					nr++;
				}
			}
		}
		nr=0;
		for (var i=0;i<that.multialigns.length;i++)//volgende aanvraag versturen
		{
			var alignment=that.multialigns[i];
			for (var j=0;j<alignment.templates.length;j++)
			{
				var template=alignment.templates[j];
				for (var k=0;k<template.aligns.length;k++)
				{
					var align=template.aligns[k];
					if (json.index+1==nr)
					{
						var command={ 'type':'ServerRequestMultiAlignment',  'projectcode': that.project.uniqueCode, 'projectpath': that.project.projectPath,'code1':template.code,'code2':align.code,'index':json.index+1};
						that.project.sendRequest(command);
						return;
					}
					nr++;
				}
			}
		}
		var command={'action':'ImportMultiAlign','what_is_updated':'invalidate_all'};
		if (that.dotplotoverview_callback!=null) that.dotplotoverview_callback(command);
	}
	return that;
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
exports.Project=function(_appl,pjobj)
{
	var that={};
	that.Application=_appl;
	that.PjObject=pjobj;
	that.fileLocation="";
	that.uniqueCode="";
	that.dataItems=[];
	that.classobjects=[];
	that.elements=[];
	that.organismClusters=[];
	that.dotplot=null;
	that.projectPath="";
	that.displayName="";
	
	that.GetServerCommand=function()
	{
		var command={};
		command.projectcode=that.uniqueCode;
		command.projectpath=that.projectPath;
		command.pjobjectcode=that.PjObject.GetCode();//voeg de pjobject toegangscode toe
		command.projectdisplayname=that.displayName;
		command.projectfilelocation=that.fileLocation;
		return command;
	}
	that.AddLogMessage=function(mess)
	{
		var clc=require('cli-color');
		console.log(clc.greenBright(mess+"...."));
//		console.log(mess);
	}
	that.CheckContent=function()
	{
		if (that.classobjects.length==0) 
		{
//			std::stringstream strm;
//			strm << "No data present in project '" << GetName() << "'";
//			AddLogMessage(strm.str().c_str(),RGB(255,128,128));
			return 0;
		}
		return that.classobjects.length;
	}
	that.MakeClassObjectClusters=function()
	{
		if (that.CheckContent()==0) return;
		that.elements.length=0;
		that.organismClusters.length=0;
		for (var i=0;i<that.classobjects.length;i++)
		{
//			COLORREF klr=MakeRandomColor();
			var classobj=that.classobjects[i];
			var orgclassobj=classobj.GetOrganismParent();
			if (orgclassobj!=null)
			{
				var fnd=-1;
				for (var j=0,fnd=-1;j<that.organismClusters.length;j++) if (that.organismClusters[j]==orgclassobj) fnd=j;
				if (fnd==-1) 
				{
					that.organismClusters.push(orgclassobj);
					fnd=that.organismClusters.length-1;
				}
				var element={classobject:classobj,clusternumber:fnd,color:"",joincanvas:null};
				that.elements.push(element);
			}
		}
	}
	that.FindElement=function(code)
	{
		for (var i=0;i<that.elements.length;i++) if (that.elements[i].classobject.GetUniqueCode()==code) return that.elements[i];
		return null;
	}
	that.SetDataItems=function(data)
	{
		that.dataItems=data;
		that.classobjects.length=0;
		for (var i=0;i<that.dataItems.length;i++)
		{
			var classobj=that.Application.FindClassObject(that.dataItems[i].classobjectcode);
			if (classobj!=null) that.classobjects.push(classobj);
		}
		that.MakeClassObjectClusters();
	}
	that.parseProjectPath=function()
	{
		that.projectPath=that.fileLocation;
		var k=that.fileLocation.length;
		for (var i=that.fileLocation.length-1;i>=0;i--) 
		{
			if (that.fileLocation[i]=='/') break;
			k--;
		}
		that.projectPath=that.projectPath.substring(0,k);
	}

	that.CreateDotPlot=function()
	{
		that.classobjects.length=0;
		for (var i=0;i<that.dataItems.length;i++)
		{
			var classobj=that.Application.FindClassObject(that.dataItems[i].classobjectcode);
			if (classobj!=null) that.classobjects.push(classobj);
		}
		that.MakeClassObjectClusters();
		if (that.dotplot==null)	that.dotplot=new exports.DotPlotUnit(this);
		var templates=that.dotplot.multialignsettings.Templates;
		templates.length=0;
		if (that.organismClusters.length>0) 
		{
			orgCluster=that.organismClusters[0];
			for (var i=0;i<orgCluster.GetClassObjectCnt();i++) templates.push(orgCluster.GetClassObject(i).GetUniqueCode());
		}
	}
	that.OpenDotPlot=function(readsettings)
	{
		if (that.dotplot==null) return;
		that.dotplot.loadStatus=0.001;
		if (readsettings) that.dotplot.ReadSettings();//wordt naar server gezonden, client krijgt resultaten en roept "SetSequenceLengths" aan. Daar wordt "readdata" aangeroepen, wat de verschillende cellen inleest
		else that.dotplot.ReadInput();//wordt naar server gezonden, client krijgt resultaten en roept "SetSequenceLengths" aan. Daar wordt "readdata" aangeroepen, wat de verschillende cellen inleest
	}
	that.ReFormatDotPlot=function()
	{
		that.OpenDotPlot(false);
	}
	that.DotPlotUpdateRun=function(json)
	{
		if (that.dotplot==null) return;
		that.dotplot.UpdateRun(json);
	}
	that.SetSequenceLengths=function(json)
	{
		if (that.dotplot==null) return;
		that.dotplot.SetSequenceLengths(json);
		that.dotplot.readdata();
	}
	that.getDotPlotSeedCompSettings=function()
	{
		if (that.dotplot!=null) return that.dotplot.seedcompsettings;
		return null;
	}
	that.getDotPlotFilterSettings=function()
	{
		if (that.dotplot!=null) return that.dotplot.filtersettings;
		return null;
	}
	that.getDotPlotAlignSettings=function()
	{
		if (that.dotplot!=null) return that.dotplot.alignsettings;
		return null;
	}
	that.getDotPlotMultiAlignSettings=function()
	{
		if (that.dotplot!=null) return that.dotplot.multialignsettings;
		return null;
	}
	that.FindClassObject=function(code)
	{
		return that.Application.FindClassObject(code);
	}
	that.sendRequest=function(mess)
	{
		mess.code=that.PjObject.GetCode();//voeg de pjobject toegangscode toe
		var json=JSON.stringify(mess);
		that.Application.ClientConnection.send(json);
	}
	that.GetOrganismCluster=function(templOrg)
	{
		for (var i=0;i<that.organismClusters.length;i++) if (that.organismClusters[i].definition===templOrg) return that.organismClusters[i];
		return null;
	}
	return that;
}

//Project stuff	
exports.PjObject=function(_code)
{
	var that={};
	that.Projects=[];
	that.code=_code;
	that.clientConnection=null;
	that.SetCode=function(_code){that.code=_code;}
	that.GetCode=function() {return that.code;};
	that.ClearProjects=function() {that.Projects.length=0;}
	that.RegisterProject=function(_project)
	{
		that.Projects.push(_project);
	}
	that.GetProject=function(projectcode)
	{
		for (var i=0;i<that.Projects.length;i++) if (that.Projects[i].uniqueCode===projectcode) return that.Projects[i];
		return null;
	}
	that.FindProjectByName=function(name)
	{
		for (var i=0;i<that.Projects.length;i++) if (that.Projects[i].displayName===name) return that.Projects[i];
		return null;
	}
	return that;
}
})(typeof exports === 'undefined'? this['MPjObject']={}: exports);
