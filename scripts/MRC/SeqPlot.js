define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/PopupFrame", "DQX/Popup","DQX/FrameCanvas","MRC/MScrollWindow"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, PopupFrame, Popup, FrameCanvas, MScrollWindow) {
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	

		BitView=function(buf) 
		{
			this.buffer=buf; 
			this.u8=new Uint8Array(buf); 
		}; 

		BitView.prototype.getBit=function(idx) 
		{ 
			var v=this.u8[idx >> 3]; 
			var off=idx & 0x7; 
			return (v >> (7-off)) & 1; 
		}; 

		BitView.prototype.setBit=function(idx,val) 
		{ 
			var off=idx & 0x7; 
			if (val) 
			{ 
				this.u8[idx >> 3] |= (0x80 >> off); 
			}
			else
			{ 
				this.u8[idx >> 3] &= ~(0x80 >> off); 
			} 
		}; 

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
        var SeqPlot = {};
		SeqPlot.Panel = function (iParentRef,_seqbox) 
		{
            var that=MScrollWindow.Init(iParentRef,true);
			that.SeqBox=_seqbox;
			that.zoom=0.00005;
			that.linespace=0;
			that.seqspace=0;
			that.locators=[];
			that.identities=[];
			that.seqblocks=[];
			that.showNumberGrid=true;
			that.hasFrames=false;
			that.ChannelCallBack=null;

			that.pixelCanvas=document.createElement('canvas');
			that.pixelCanvas.width=500;
			that.pixelCanvas.height=400;
			that.pixelCanvasContext=that.pixelCanvas.getContext('2d');

			that.C_offset=0;
			that.xpunten=[];
			that.ypunten=[];
			that.centrumX=0;
			that.centrumY=0;
			that.circular=false;
			that.sequenceUpdateTimer=null;
			that.frameUpdateTimer=null;
			that.cxx1=0;
			that.cxx2=0;
			
//begin tekendata
			that.block=null;
			that.sequence=null;
			that.line=0;
			that.line_b1=0;
			that.line_b2=0;
			that.blockindex=0;
			that.seqindex=0;
			that.selpos1=0;
			that.selpos2=0;
			that.curpos=0;
			that.ycurpos=0;
			that.offsetbase=0;
			that.endbase=0;
			that.diffbase=0;
			that.basesperline=1;
			that.showbasesperline=1;
			that.basespercm=1000.0;
			that.minbasespercm=5.0;
			that.maxbasespercm=100000;
			that.cm2pix=50.0;
			that.multiline=1;
			that.displaylabel=1;
			that.leftmargin=15;
			that.rightmargin=20;
			that.xsize=1;
			that.xscrollpos=0;//xscrollpos is de scroll positie,
			that.yoffset=0;//yoffset is de reeds geconverteerde positie inklusive yscrollpos, dus kan eigenlijk niet groot worden
			that.left=0;
			that.right=0;
			that.bitdirect=new BitView(new ArrayBuffer(100000));
			that.bitinvert=new BitView(new ArrayBuffer(100000));

			that.xconvert=function(x)
			{
				return 1.0*x*that.cm2pix/that.basespercm;
			}
			that.InvertPos=function(_ps1,_ps2)
			{
				var pObj={'ps1':_ps1,'ps2':_ps2};
				if (that.block==null) return pObj;
				var _p1=_ps1;var _p2=_ps2;
				pObj.ps1=that.block.endbase-_p2+that.block.startbase;
				pObj.ps2=that.block.endbase-_p1+that.block.startbase;
				return pObj;
			}
			that.UpdateSettings=function(lx)
			{
				var mul=10*that.GetScale();
				that.basesperline=parseInt(parseInt((1.0*(lx-that.leftmargin-that.rightmargin)*that.basespercm/that.cm2pix)/mul)*mul);
		//		that.basesperline=parseInt((lx-that.leftmargin-that.rightmargin)*that.basespercm/that.cm2pix);
				if (that.basesperline<1) that.basesperline=1;
				that.showbasesperline=parseInt((lx+20)*that.basespercm/that.cm2pix);
				if (that.multiline) that.showbasesperline=that.basesperline;
				that.xsize=parseInt((that.cm2pix/that.basespercm)*that.basesperline); 
		//		if (!that.multiline) {that.basesperline=Number.MAX_VALUE;that.leftmargin=0;that.xsize=lx;}
				if (!that.multiline) {that.basesperline=Number.MAX_VALUE;that.rightmargin=0;that.xsize=lx;}
			}
			that.InScope=function()
			{
				var block=that.block;
				if (block.startbasegaps>that.line_b2) return false;
				if (block.endbasegaps<that.line_b1) return false;
				return true;
			}
			that.SetLeftRightBorders=function()
			{
				var block=that.block;
				if (block.startbasegaps>that.line_b2) return false;
				if (block.endbasegaps<that.line_b1) return false;
				var b1=Math.max(block.startbasegaps,that.line_b1);
				var b2=Math.min(block.endbasegaps,that.line_b2);
				that.left=~~Math.max(that.xconvert(b1-that.offsetbase)+that.xscrollpos,that.leftmargin);
				that.right=~~Math.min(that.xconvert(b2-that.offsetbase)+that.xscrollpos,that.left+that.xsize);
				return true;
			}
			that.GetScale=function()
			{
				var fac=that.cm2pix/that.basespercm;
				var d=150;
				var step=1;
				if (fac*10<d) step=1;
				if (fac*20<d) step=2;
				if (fac*50<d) step=5;
				if (fac*100<d) step=10;
				if (fac*200<d) step=20;
				if (fac*500<d) step=50;
				if (fac*1000<d) step=100;
				if (fac*2000<d) step=200;
				if (fac*5000<d) step=500;
				if (fac*10000<d) step=1000;
				if (fac*20000<d) step=2000;
				if (fac*50000<d) step=5000;
				if (fac*100000<d) step=10000;
				if (fac*200000<d) step=20000;
				if (fac*500000<d) step=50000;
				if (fac*1000000<d) step=100000;
				if (fac*2000000<d) step=200000;
				if (fac*5000000<d) step=500000;
				if (fac*10000000<d) step=1000000;
				if (fac*20000000<d) step=2000000;
				if (fac*50000000<d) step=5000000;
				if (fac*100000000<d) step=10000000;
				if (fac*200000000<d) step=20000000;
				if (fac*500000000<d) step=50000000;
				if (fac*1000000000<d) step=100000000;
				if (fac*2000000000<d) step=200000000;
				return ~~step;
			}
//einde tekendata

			that.GetIdentifier=function()
			{
				return that.SeqBox.GetIdentifier();
			}
			that.SetSequenceData=function(data)
			{
				if (that.basespercm!=that.minbasespercm) return;
				that.locators.length=0;
				that.locators=data;
				that.invalidate();
			}
			that.SetFrameData=function()
			{
				that.invalidate();
			}
			that.DataUpdate=function()
			{
				if (that.basespercm==that.minbasespercm)
				{
					if (that.sequenceUpdateTimer==null) that.sequenceUpdateTimer=setTimeout(function(){that.AskSequenceUpdate();},200);
					else 
					{
						clearTimeout(that.sequenceUpdateTimer);
						that.sequenceUpdateTimer=setTimeout(function(){that.AskSequenceUpdate();},200);
					}
				}
				if (that.hasFrames==true && that.basespercm<10000)
				{
					if (that.frameUpdateTimer==null) that.frameUpdateTimer=setTimeout(function(){that.AskFrameUpdate();},200);
					else
					{
						clearTimeout(that.frameUpdateTimer);
						that.frameUpdateTimer=setTimeout(function(){that.AskFrameUpdate();},200);
					}
				}
			}
			that.AskSequenceUpdate=function()
			{
				that.predrawSequence();
				var command=JSON.stringify( { 'type': 'SequenceDataLoad', 'seqplot_code': that.SeqBox.GetIdentifier(), 'data': that.locators});
				Application.ClientConnection.send(command);
			}
			that.AskFrameUpdate=function()
			{
				that.predrawFrames();
				var command=JSON.stringify( { 'type': 'SequenceFrameLoad', 'sequence_box_code': that.SeqBox.GetIdentifier(), 'data': that.seqblocks});
				Application.ClientConnection.send(command);
			}
			that.InitiateView=function()
			{
				that.UpdateFacts();
			}
			that.BeforeResize=function()
			{
				if (that.circular)
				{
					var rad=200*that.maxbasespercm/(that.basespercm*2*Math.PI);
					rad=Math.max(rad,50);
					that.centrumX=that.GetXSize()/2;
					that.centrumY=rad+50;
				}
				var lx=that.GetXSize();
				if (lx<=0 || that.SeqBox.GetMaxSeqLen()==0) return;
				that.UpdateScroll();
				that.UpdateSettings(lx);
			}
			that.AfterResize=function()
			{
				that.DataUpdate();
			}
			that.UpdateFacts=function(scroll)
			{
				var oldbasesperline=that.basesperline;
				that.linespace=1;
				that.seqspace=20;
				if (that.circular)
				{
					var rad=200*that.maxbasespercm/(that.basespercm*2*Math.PI);
					rad=Math.max(rad,50);
					that.centrumX=that.GetXSize()/2;
					that.centrumY=rad+25;
				}
				var cty=20;
				that.SeqBox.UpdateMaxSeqLen();
				that.maxbasespercm=1.0*that.SeqBox.GetMaxSeqLen()/4;
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var sequence=that.SeqBox.GetSequence(i);
					if (sequence==null) continue;
					sequence.UpdateSeqChannelOffsets();
					cty+=sequence.GetUpperSeqChannelSpace();
					sequence.plotoffset=cty;
					cty+=sequence.GetLowerSeqChannelSpace();
					cty+=5;
					if (sequence.FindChannelType("FrameMapping")) that.hasFrames=true;
				}
				var lx=that.GetXSize();
				if (lx<=0 || that.SeqBox.GetMaxSeqLen()==0) return;
				that.linespace=cty+20;
				that.UpdateSettings(lx);
				that.UpdateScroll();
				that.ReCalcScrollers();
				that.DataUpdate();
				if (scroll) that.scrolltopos(that.curpos);else that.invalidate();
			}
			that.UpdateScroll=function()
			{
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				if (that.circular)
				{
					that.SetVScrollSize(0);
					that.SetHScrollSize(that.centrumY*2*Math.PI);
				}
				else
				{
					if (!that.multiline) 
					{
						var cntx=that.SeqBox.GetMaxSeqLen()*that.cm2pix/that.basespercm;
						if (that.GetHScrollSize()!=cntx) that.SetHScrollSize(cntx);
						that.SetVScrollSize(that.linespace);
					}
					else
					{
						var kk=Math.floor(that.SeqBox.GetMaxSeqLen()/that.basesperline)+1;
						var cnty=that.linespace*kk;
						if (that.GetVScrollSize()!=cnty)
						{
							that.SetVScrollSize(cnty);
							that.SetHScrollSize(0);
						}
					}
				}
			}
			that.predrawFrames=function()
			{
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				that.xscrollpos=xoo+that.leftmargin;
				var line1=Math.floor(-yoo/that.linespace)-1;if (line1<0) line1=0;
				var line2=Math.floor((that.GetYSize()-yoo)/that.linespace)+1;
				if (!that.multiline) {line1=0;line2=0;}
				that.seqblocks.length=0;
				var start_b1=line1*that.basesperline-10000;
				var end_b2=line2*that.basesperline+that.basesperline+10000;
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var _sequence=that.SeqBox.GetSequence(i);
					for (var j=0;j<_sequence.GetSeqChannelCount();j++)
					{
						var seqchannel=_sequence.GetSeqChannel(j);
						if (seqchannel.GetType()!="FrameMapping") continue;
						var xxx1=Math.max(_sequence.GetCloseBlock(start_b1)-2,0);
						var xxx2=Math.min(_sequence.GetCloseBlock(end_b2)+2,_sequence.GetBlockCnt());
						var channel={'accession':seqchannel.GetChannelAccession(), 'seqblocks':[]};
						for (var k=xxx1;k<xxx2;k++) 
						{
							var block=_sequence.GetBlockIndex(k);
							if (block.startbasegaps>end_b2) continue;
							if (block.endbasegaps<start_b1) continue;
							var b1=Math.max(block.startbasegaps,start_b1-10);
							var b2=Math.min(block.endbasegaps,end_b2+10);
							var _b1=block.ContigToEntryPosition(b1);
							var _b2=block.ContigToEntryPosition(b2);
							var seqblock={'accession':block.entry.GetAccession(), 'startbase':_b1, 'endbase':_b2, 'inverted': block.invert};
							channel.seqblocks.push(seqblock);
						}
						that.seqblocks.push(channel);
					}
				}
			}
			that.predrawSequence=function()
			{
				that.locators.length=0;
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				that.xscrollpos=xoo+that.leftmargin;
				var line1=Math.floor(-yoo/that.linespace)-1;if (line1<0) line1=0;
				var line2=Math.floor((that.GetYSize()-yoo)/that.linespace)+1;
				if (!that.multiline) {line1=0;line2=0;}
				that.identities.length=0;
				for (var i=line1;i<=line2;i++)
				{
					that.offsetbase=i*that.basesperline;
					that.endbase=that.SeqBox.GetMaxSeqLen();
					that.line=i;
					that.yoffset=i*that.linespace+yoo;

					that.line_b1=i*that.basesperline;
					that.line_b2=i*that.basesperline+that.basesperline;
					if (!that.multiline)
					{
						that.line_b1=Math.floor((-50-xoo)*that.basespercm/that.cm2pix);
						that.line_b2=Math.floor((that.GetXSize()+50-xoo)*that.basespercm/that.cm2pix);
					}
					that.line_b1=Math.max(that.line_b1,that.offsetbase);
					that.line_b2=Math.min(that.line_b2,that.endbase);
					that.setupLocators();
					if (i*that.basesperline+that.basesperline>that.SeqBox.GetMaxSeqLen()) break;
				}
			}
			that.setupLocators=function()
			{
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var _sequence=that.SeqBox.GetSequence(i);
					xxx1=Math.max(_sequence.GetCloseBlock(that.line_b1)-1,0);
					xxx2=Math.min(_sequence.GetCloseBlock(that.line_b2)+1,_sequence.GetBlockCnt());
					for (var j=xxx1;j<xxx2;j++) 
					{
						var block=_sequence.GetBlockIndex(j);
						if (block.startbasegaps>that.offsetbase+that.basesperline) continue;
						if (block.endbasegaps<that.offsetbase) continue;
						var b1=Math.max(block.startbasegaps,that.line_b1);
						var b2=Math.min(block.endbasegaps,that.line_b2);
//						if (block.invert==true) b1++;
						var _b1=block.ContigToEntryPosition(b1);
						var _b2=block.ContigToEntryPosition(b2);
						if (_b1>_b2) {var k=_b1;_b1=_b2;_b2=k;};
						var locator={'lineindex':that.line, 'seqindex':i, 'blockindex':j,'identindex':that.identities.length,'accession':block.entry.GetAccession(), 'linestart':b1, 'lineend':b2, 'start':_b1, 'end':_b2, 'inverted': block.invert, 'sequence':'',frames:[]};
						that.locators.push(locator);
					}
					var hits=[];
					for (var j=0;j<100;j++) hits.push(' ');
					that.identities.push(hits);
				}
			}
			that.GetSelectedSequence=function(start,stop)//contig posities
			{
				var _sequence=that.SeqBox.GetSequence(0);
				xxx1=Math.max(_sequence.GetCloseBlock(start),0);
				xxx2=Math.min(_sequence.GetCloseBlock(stop),_sequence.GetBlockCnt());
				if (xxx1!=xxx2) alert("sequence spans several sequence blocks");
				var block=_sequence.GetBlockIndex(xxx1);
				var b1=Math.max(block.startbasegaps,start);
				var b2=Math.min(block.endbasegaps,stop);
				var _b1=block.ContigToEntryPosition(b1);
				var _b2=block.ContigToEntryPosition(b2);
				if (_b1>_b2) {var k=_b1;_b1=_b2;_b2=k;};
				var locator={'accession':block.entry.GetAccession(),'start':_b1, 'end':_b2, 'inverted': block.invert, 'sequence':''};
				var command=JSON.stringify( { 'type': 'GetSelectedSequence', 'seqplot_code': that.SeqBox.GetIdentifier(), 'locator': locator});
				Application.ClientConnection.send(command);
			}
			that.GetDirectSequence=function(start,stop,invert)//entry posities
			{
				var _sequence=that.SeqBox.GetSequence(1);
				var accessions=_sequence.GetAccessions();
				if (accessions.length>1) alert("more then one asseccion in sequence");
				var _b1=start;
				var _b2=stop;
				if (_b1>_b2) {var k=_b1;_b1=_b2;_b2=k;};
				var locator={'accession':accessions[0],'start':_b1, 'end':_b2, 'inverted':invert, 'sequence':''};
				var command=JSON.stringify( { 'type': 'GetSelectedSequence', 'seqplot_code': that.SeqBox.GetIdentifier(), 'locator': locator});
				Application.ClientConnection.send(command);
			}
			that.SetSelectedSequence=function(data)
			{
//				alert(data.sequence);return;
				var command={};
				command.action={type: 'FetchEmblData', sequence: data.sequence, note:'Blast'};
				Application.DataBaseModule.evaluateTrack(command,true)
//				alert(data.sequence);
			}
		
			
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgba(255,255,255,1.0)";
				context.fillRect(0,0,that.GetXSize(),that.GetYSize());

				if (that.processing!="")
				{
					context.font = "italic bold 24px serif";
					context.fillStyle="rgba(255,0,0,1)";
					context.fillText(that.processing,30,40);
				}
				if (that.pixelCanvas.width!=that._cnvWidth || that.pixelCanvas.height!=that._cnvHeight)
				{
					that.pixelCanvas.width=that._cnvWidth;
					that.pixelCanvas.height=that._cnvHeight;
					that.imageData.SetImageData(that.pixelCanvasContext,that._cnvWidth,that._cnvHeight);
				}
	//			that.imageData.DrawBackGround(0,0,0,0);

				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();

				if (that.circular)
				{
					var rad=200*that.maxbasespercm/(that.basespercm*2*Math.PI);
					rad=Math.max(rad,50);
					that.centrumX=that.GetXSize()/2;
					that.centrumY=rad+50;
				}
				that.pre_paint();
				if (that.circular)
				{
					that.showCircularMaps();
				}
				else
				{
					that.xscrollpos=xoo+that.leftmargin;
					var line1=Math.floor(-yoo/that.linespace);if (line1<0) line1=0;
					var line2=Math.floor((that.GetYSize()-yoo)/that.linespace);
					if (!that.multiline) {line1=0;line2=0;}
					var yy=0;
					for (var i=line1;i<=line2;i++)
					{
						that.offsetbase=i*that.basesperline;
						that.endbase=that.SeqBox.GetMaxSeqLen();
						that.line=i;
						that.yoffset=i*that.linespace+yoo;
						that.line_b1=i*that.basesperline;
						that.line_b2=i*that.basesperline+that.basesperline;
						if (!that.multiline)
						{
							that.line_b1=Math.floor((-50-xoo)*that.basespercm/that.cm2pix);
							that.line_b2=Math.floor((that.GetXSize()+50-xoo)*that.basespercm/that.cm2pix);
						}
						that.line_b1=Math.max(that.line_b1,that.offsetbase);
						that.line_b2=Math.min(that.line_b2,that.endbase);
						if (i*that.basesperline>that.SeqBox.GetMaxSeqLen()) break;
						that.paintline(true);
						yy=that.yoffset+that.linespace;
						context.fillStyle="rgba(100,100,100,1)";
						context.fillRect(0,yy-6,that._cnvWidth,1);
						context.fillRect(0,yy-2,that._cnvWidth,1);
					}
					if (yy<that._cnvHeight)
					{
						context.fillStyle="rgba(255,255,255,1)";
						context.fillRect(0,yy,that._cnvWidth,that._cnvHeight-yy);
					}
				}
	//			that.imageData.ResolveImageData();
	//			context.drawImage(that.pixelCanvas,0,0);
				//marges links en rechts
				for (var i=line1;i<=line2;i++)
				{
					that.line=i;
					that.yoffset=i*that.linespace+yoo;
					that.paintMarges(true);
				}
				//einde marges
				that.post_paint();
				if (that.processing!="")
				{
					context.font="italic bold 18px serif";
					context.fillStyle="rgba(180,180,180,0.5)";
					context.fillRect(0,0,that._cnvWidth,40);
					context.fillStyle="rgba(255,0,0,0.8)";
					context.fillText(that.processing,30,20);
				}
			}
			that.pre_paint=function()
			{
			}
			that.post_paint=function()
			{
			}
			
			that.pre_paintline=function(update)
			{
			}
			that.post_paintline=function(update)
			{
			}
			
			that.paintline=function(update)
			{
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				var context=that._cnvContext;
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				if (that.line<0) return;
				var offset=that.leftmargin+xoo;
				var left=that.leftmargin;
				var right=left+that.xsize;
				var top=that.yoffset;
				var bottom=top+that.linespace;
				if (bottom<0) return;
				if (top>ly) return;
				
				var col0="rgba(255,255,255,1.0)";
				var col2="rgba(245,245,245,1.0)";
				var bklr=col0;

				var selkleur="rgba(255,255,150,1.0)";
				if (update)
				{
					var sl1=Math.min(that.selpos1,that.selpos2);
					var sl2=Math.max(that.selpos1,that.selpos2);
					if (sl1<that.line_b1) sl1=that.line_b1;if (sl1>that.line_b2) sl1=that.line_b2;
					if (sl2<that.line_b1) sl2=that.line_b1;if (sl2>that.line_b2) sl2=that.line_b2;
					if (sl1<that.line_b1) sl1=that.line_b1;if (sl1>that.line_b2) sl1=that.line_b2;
					if (sl2<that.line_b1) sl2=that.line_b1;if (sl2>that.line_b2) sl2=that.line_b2;
			//lijnen + selectie
					var px1=Math.floor(that.xconvert(sl1-that.offsetbase)+offset);px1=Math.max(left,px1);px1=Math.min(right,px1);
					var px2=Math.floor(that.xconvert(sl2-that.offsetbase)+offset);px2=Math.max(left,px2);px2=Math.min(right,px2);
					context.fillStyle=selkleur;
					context.fillRect(px1,top,px2-px1,bottom-top);
					context.fillStyle=bklr;
					context.fillRect(left,top,px1-left,bottom-top);
					context.fillRect(px2,top,right-px2,bottom-top);
			//einde lijnen
				}
				if (that.line*that.basesperline>that.SeqBox.GetMaxSeqLen()) return;//geen sequentie meer
				that.pre_paintline(update);

				that.shownumbers();

				context.font = "11px serif";
				context.fillStyle="rgb(0,0,0)";

				var b1=0,b2=0;
				var k=0;
				var block=null;
				var xconv=that.xconvert;
				var left=that.leftmargin;
				var right=that.left+that.xsize;
				var mmax=Math.max;
				var mmin=Math.min;
				var offbase=that.offsetbase;
				var scrollp=that.xscrollpos;
				var bb1=that.line_b1;
				var bb2=that.line_b2;
			
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var sequence=that.SeqBox.GetSequence(i);
					that.sequence=sequence;
					that.hasSeqNumbers=sequence.FindChannelType("Sequence numbering");
					that.hasSeqData=sequence.FindChannelType("SequenceData");
					that.seqindex=i;
					that.pre_paintsequenceline(sequence);
					if (that.GetHScrollPos()%2==0) k=1;else k=0;
					var px1=Math.floor(that.xconvert(sequence.GetAlignStart()-that.offsetbase)+offset);px1=Math.max(left,px1);px1=Math.min(right,px1);
					var px2=Math.floor(that.xconvert(sequence.GetAlignEnd()-that.offsetbase)+offset);px2=Math.max(left,px2);px2=Math.min(right,px2);
					px1+=k;
					for (var j=px1;j<=px2;j+=3) context.fillRect(j,that.yoffset+sequence.plotoffset,1,1);
//					xxx1=Math.max(sequence.GetCloseBlock(that.line_b1)-2,0);
//					xxx2=Math.min(sequence.GetCloseBlock(that.line_b2)+2,sequence.GetBlockCnt());
//					for (var j=xxx1;j<xxx2;j++) 
					that.seqoffset=that.yoffset+sequence.plotoffset;
					var blocks=sequence.blocks;
					var j=blocks.length;
					while (j--)
					{
						block=blocks[j];
						if (block.startbasegaps>bb2 || block.endbasegaps<bb1) continue;
						b1=mmax(block.startbasegaps,bb1);
						b2=mmin(block.endbasegaps,bb2);
						that.left=mmax(xconv(b1-offbase)+scrollp,left);
						that.right=mmin(xconv(b2-offbase)+scrollp,right);
						that.diffbase=block.startbasegaps-block.startbase;
						that.blockindex=j;
						that.block=block;
						if (that.right-that.left>5) that.paintblock();
					}
				}
			//cursor
				var cur=that.curpos;
				if (cur>=that.line_b1 && cur<=that.line_b2)
				{
					px1=that.xconvert(cur-that.offsetbase)+offset;
					px1=Math.max(left,px1);px1=Math.min(right,px1);
					context.fillStyle="rgba(255,180,180,0.5)";
					context.fillRect(px1,top,2,bottom-top);
					if (that.sequencesel!=-1 && that.channelsel!=-1)
					{
						var sequence=that.SeqBox.GetSequence(that.sequencesel);
						if (sequence!=null)
						{
							var seqchannel=sequence.GetSeqChannel(that.channelsel);
							if (seqchannel!=null)
							{
								context.fillStyle="rgba(255,0,0,0.5)";
								context.fillRect(px1,top+sequence.plotoffset+seqchannel.GetOffset()-seqchannel.GetSize(),3,2*seqchannel.GetSize());
							}
						}
					}
				}
			//einde cursor	
				that.post_paintline(update);
			}
			
			that.pre_paintsequenceline=function(sequence)
			{
				if (that.loaded==false) return;
				var ppy=that.yoffset+sequence.plotoffset;
				if (ppy<0 || ppy>that.ysize) return;
				var blocks=sequence.blocks;
				var b1=0,b2=0;
				var block=null;
				var left=~~that.leftmargin;
				for (var j=0;j<that.xsize+10;j++) {that.bitdirect.setBit(j,0);that.bitinvert.setBit(j,0);}
				var inv=0;
				var j=blocks.length;
				while (j--)
				{
					block=blocks[j];
					if (block.invert==true) inv=1;else inv=0;
					if (block.startbasegaps>that.line_b2 || block.endbasegaps<that.line_b1) continue;
					b1=block.startbasegaps;if (b1<that.line_b1) b1=that.line_b1;
					b2=block.endbasegaps;if (b2>that.line_b2) b2=that.line_b2;
					b1=~~((b1-that.offsetbase)*that.cm2pix/that.basespercm+that.xscrollpos)-left;if (b1<0) b1=0;
					b2=~~((b2-that.offsetbase)*that.cm2pix/that.basespercm+that.xscrollpos)-left;if (b2>that.xsize) b2=that.xsize;
					if (inv==1) for (var k=b1;k<=b2;k++) that.bitinvert.setBit(k,1);
					else for (var k=b1;k<=b2;k++) that.bitdirect.setBit(k,1);
				}
				var context=that._cnvContext;
				context.fillStyle="rgba(0,0,255,1.0)";
				var k=0;var p=0;
				while (k<that.xsize) 
				{
					if (that.bitdirect.getBit(k)==1) p++;
					else {if (p>0) context.fillRect(left+k-p,ppy,p,2);p=0;}
					k++;
				}
				if (p>0) context.fillRect(left+k-p,ppy,p,2);
				context.fillStyle="rgba(255,0,0,1.0)";
				var k=0;var p=0;
				while (k<that.xsize) 
				{
					if (that.bitinvert.getBit(k)==1) p++;
					else {if (p>0) context.fillRect(left+k-p,ppy,p,2);p=0;}
					k++;
				}
				if (p>0) context.fillRect(left+k-p,ppy,p,2);
			}

			that.paintMarges=function(update)
			{
				if (that.line*that.basesperline>that.SeqBox.GetMaxSeqLen()) return;//geen sequentie meer
				var context=that._cnvContext;
				if (that.line<0) return;
				var offset=that.leftmargin-that.GetHScrollPos();
				var top=that.yoffset;
				if (top+that.linespace<0) return;
				if (top>that.GetYSize()) return;
				
				var col0="rgba(255,255,255,1.0)";
				var col2="rgba(245,245,245,1.0)";
//				if (that.line%2==1) col0=col2;
				var bklr=col0;

				context.fillStyle=bklr;
				context.fillRect(0,top,that.leftmargin,that.linespace);
				context.fillRect(that.leftmargin+that.xsize,top,that.GetXSize()-that.leftmargin-that.xsize,that.linespace);
				context.fillStyle="rgba(220,220,220,1.0)";
				context.fillRect(that.leftmargin-1,top,1,that.linespace);
				if (that.multiline)	context.fillRect(that.leftmargin+that.xsize+1,top,1,that.linespace);
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var sequence=that.SeqBox.GetSequence(i);
					for (var j=0;j<sequence.GetSeqChannelCount();j++)
					{
						var seqchannel=sequence.GetSeqChannel(j);
						context.beginPath();
						context.strokeStyle="rgba(0,0,0,0)";
						context.lineWidth=1;
						if (seqchannel.GetView()==false) continue;
						var py=that.yoffset+sequence.plotoffset+seqchannel.GetOffset()-seqchannel.GetSize();
						context.fillStyle=seqchannel.GetColor();
						context.fillRect(that.leftmargin-9,py,6,2*seqchannel.GetSize());
//						context.arc(that.leftmargin-7,py,3,0,2*Math.PI,false);
						context.closePath();
						context.fill();
						context.stroke();
						if (seqchannel.GetType()=="FrameMapping" && that.basespercm>=10000)
						{
							context.font = "13px serif";
							context.fillStyle="rgb(0,0,0)";
							context.fillText("Zoom in...",that.GetXSize()/2-100,py);
						}
					}
				}
			}

			that.shownumbers=function()
			{
				var context=that._cnvContext;
				var unit=that.xconvert(1)/2;
				context.font = "11px serif";
				context.textBaseline="bottom";
				var step=~~that.GetScale();
				var x1=(Math.floor(that.line_b1/step))*step;
				var x2=(Math.floor(that.line_b2/step))*step;
				if (x1<0) x1=0;if (x2>that.line_b2) x2=that.line_b2;
				var mul=10*step;
				var px1=0;
/*				if (that.showNumberGrid==true)
				{
					var i=x1;
					context.fillStyle="rgb(240,240,240)";
					while (i<x2)
					{
						px1=that.xconvert(i-that.offsetbase)+that.leftmargin-that.GetHScrollPos()-unit;
						px1=Math.floor(px1);
						if (i%mul==0 && i>0) {context.fillRect(px1,that.yoffset,1,that.linespace);i+=10*step;} else i+=step;
					}
				}*/
				context.fillStyle="rgb(0,0,0)";
				for (var i=x1;i<x2;i+=step)
				{
					px1=that.xconvert(i-that.offsetbase)+that.leftmargin-that.GetHScrollPos()-unit;
					px1=Math.floor(px1);
					if (i%step==0 && i>0) context.fillRect(px1,that.yoffset,1,3);
					if (i%mul==0 && i>0)
					{
						context.fillRect(px1,that.yoffset,2,5);
						context.fillText(i,px1+2,that.yoffset+14);
					}
				}
			}

			that.paintblock=function()
			{
				var ctx=that._cnvContext;
				var block=that.block;

				if (that.hasSeqNumbers!=null && that.hasSeqNumbers.GetView()) that.showblocknumbers();
				if (that.hasSeqData!=null && that.hasSeqData.GetView() && that.basespercm==that.minbasespercm) that.showblocksequence();
				var cnt=block.GetSeqBlockChannelCnt();
				for (var j=0;j<cnt;j++)
				{
					var seqblockchannel=block.GetSeqBlockChannel(j);
					if (seqblockchannel==null) continue;
					var seqchannel=seqblockchannel.GetSeqChannel();
					if (seqchannel==null || seqchannel.GetView()==false) continue;
					if (seqchannel.GetType()=="FeatureSearch") that.showfeatures(seqblockchannel);
					if (seqchannel.GetType()=="SequenceSearch") that.showsequencesearch(seqblockchannel);
					if (seqchannel.GetType()=="VariationSearch") that.showsequencesearch(seqblockchannel);
					if (seqchannel.GetType()=="FrameMapping") that.showframemapping(seqblockchannel);
				}
			}
			that.showblocknumbers=function()
			{
				var px1=0;
				var seqchannel=that.hasSeqNumbers;
				var ctx=that._cnvContext;
				var block=that.block;
				ctx.font = "11px serif";
				ctx.fillStyle="rgb(0,0,0)";
				if (block.invert==false) {b1=block.ContigToEntryPosition(that.line_b1);b2=block.ContigToEntryPosition(that.line_b2);}
				else {b1=block.ContigToEntryPosition(that.line_b2);b2=block.ContigToEntryPosition(that.line_b1);}
				var step=Math.floor(that.GetScale());
				var x1=Math.floor(b1/step)*step-step;
				var x2=Math.floor(b2/step)*step+step;
				for (var i=x1;i<x2;i+=step)
				{
					px1=that.xconvert(i+that.diffbase-that.offsetbase-1)+that.xscrollpos;
					if (block.invert) px1=that.xconvert(block.startbase+block.endbase-i+that.diffbase-that.offsetbase+1)+that.xscrollpos;
					px1=Math.floor(px1);
					if (px1<=that.left) continue;if (px1>=that.right) continue;
					if (i%step==0 && i>0) ctx.fillRect(px1,that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset(),1,-3);
					if (i%(10*step)==0 && i>0)
					{
						ctx.fillRect(px1,that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset(),2,-5);
						ctx.fillText(i,px1+2,that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset()-5);
					}
				}
			}
			that.showblocksequence=function()
			{
				var px1=0;
				var seqchannel=that.hasSeqData;
				var ctx=that._cnvContext;
				ctx.font="14px lucida console";
				ctx.textBaseline='top';
				ctx.fillStyle="rgb(0,0,0)";
				for (var i=0;i<that.locators.length;i++)
				{
					if (that.locators[i].lineindex==that.line && that.locators[i].seqindex==that.seqindex && that.locators[i].blockindex==that.blockindex)
					{
						var k=0;var h=' ';var g=' ';
						var len=that.locators[i].sequence.length;
						for (var j=that.locators[i].linestart;j<=that.locators[i].lineend;j++)
						{
							if (k>=0 && k<len)
							{
								var id=that.locators[i].identindex;
								h=that.locators[i].sequence[k];
								if (that.locators[i].inverted) h=Application.SeqTools.Complement(h);
								px1=that.xconvert(j-that.offsetbase)+that.xscrollpos;
								px1=Math.floor(px1);
								ctx.fillText(h,px1,that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset()-7);
								that.identities[id][j-that.offsetbase]=h;
								if (that.locators[i].seqindex>0)
								{
									g=that.identities[id-that.locators[i].seqindex][j-that.offsetbase];
									if (g==h)
										ctx.fillText("+",px1,that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset()-24);
								}
							}
							k++;
						}
					}
				}
				ctx.textBaseline='alphabetic';
			}
			that.showfeatures=function(seqblockchannel)
			{
				var ctx=that._cnvContext;
				var image=that.imageData;
				ctx.fillStyle="rgba(100,100,100,0.5)";
				ctx.font="13px serif";
				ctx.lineWidth=1;
	
				var ll=0;var py=0;var swap=0;
				var pp1=0;var pp2=0;
				var px1=0;var px2=0;
				var x1=0;var x2=0;var x3;
				var label="";

				var seqchannel=seqblockchannel.GetSeqChannel();
				var block=that.block;
				ll=seqblockchannel.ChannelItemIndex.length;
				var ppy=that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset();
				var dd=14;
				var alpha=60;
				for (var k=0;k<ll;k++)
				{
					py=ppy-14;swap=8;
					if (k%2==0) {py=ppy;swap=-15;}
					var fts=seqblockchannel.GetItem(k);
					var arrow=fts.dir;
					if (block.invert==true) if (arrow==1) arrow=-1;else if (arrow==-1) arrow=1;
					var _arrow=arrow;
					pp1=fts.start;pp1=Math.max(pp1,block.startbase);
					pp2=fts.stop;pp2=Math.min(pp2,block.endbase);
					if (pp1>block.endbase) continue;if (pp2<block.startbase) continue;
					if (block.invert==true) {var pObj=that.InvertPos(pp1,pp2);pp1=pObj.ps1;pp2=pObj.ps2;}
					pp2++;
					px1=that.xconvert(pp1+that.diffbase-that.offsetbase)+that.xscrollpos;
					px2=that.xconvert(pp2+that.diffbase-that.offsetbase)+that.xscrollpos;
					px1=Math.floor(px1);
					px2=Math.floor(px2);
					if (px1>that.right) continue;if (px2<that.left) continue;//niets te zoeken hier
					
					//start en stop lijn van volledig feature
					x1=Math.max(that.left,px1);
					x2=Math.min(that.right,px2);
					//feature label uitschrijven
					var llen=x2-x1;
					if (that.displaylabel==1 && 2*llen>20 && that.basespercm!=that.minbasespercm)
					{
						label="";
						for (var i=0;i<fts.qlfs.length;i++)
						{
							var qlf=fts.qlfs[i];
							if (qlf.key==="/product=") label=qlf.item;
//							if (qlf.key==="/gene=") label=qlf.item;
						}
						if (label!="")
						{
							ctx.save();
							ctx.beginPath();
							ctx.textBaseline='top';
							ctx.fillStyle="rgb(0,0,0)";
							ctx.strokeStyle="rgba(0,0,0,0)";
							ctx.rect(x1+2,ppy+swap,x2-x1-4,15);
							ctx.clip();
							ctx.fillText(label,px1+2,ppy+swap);
							ctx.closePath();
							ctx.stroke();
							ctx.restore();
						}
					}	
					//einde feature label uitschrijven
					ctx.beginPath();
					if (fts.dir<0)  ctx.fillStyle="rgba(255,0,0,0.3)";else ctx.fillStyle="rgba(0,0,255,0.3)";
					if (fts.dir<0)  ctx.strokeStyle="rgba(255,0,0,0.5)";else ctx.strokeStyle="rgba(0,0,255,0.5)";
					//exons uittekenen
					if (px2>that.right || llen<4) arrow=0;
					if (fts.stop>fts.start && llen<=10)// einde feature is groter als begin, maar afstand is te klein om exons nog te plotten
					{
						if (arrow==1)
						{
							ctx.moveTo(x2,py);
							ctx.lineTo(x2+4,py+4);
							ctx.lineTo(x2,py+8);
							ctx.lineTo(x1,py+8);
							ctx.lineTo(x1,py);
							ctx.lineTo(x2,py);
						}
						if (arrow==0)
						{
							if (x2!=x1)
							{
								ctx.moveTo(x2,py);
								ctx.lineTo(x2,py+8);
								ctx.lineTo(x1,py+8);
								ctx.lineTo(x1,py);
								ctx.lineTo(x2,py);
							}
							else 
							{
								ctx.moveTo(x2,py);
								ctx.lineTo(x2,py+8);
							}
						}
						if (arrow==-1)
						{
							ctx.moveTo(x2,py);
							ctx.lineTo(x2,py+8);
							ctx.lineTo(x1,py+8);
							ctx.lineTo(x1-4,py+4);
							ctx.lineTo(x1,py);
							ctx.lineTo(x2,py);
						}
					}
					else//teken de exons separaat
					{
						for (var i=0;i<fts.exons.length;i++)
						{
							pp1=fts.exons[i].start;pp1=Math.max(pp1,block.startbase);
							pp2=fts.exons[i].stop;pp2=Math.min(pp2,block.endbase);
							if (block.invert==true) {var pObj=that.InvertPos(pp1,pp2);pp1=pObj.ps1;pp2=pObj.ps2;}//posities inverteren
							pp2++;//feature einde ��n positie verlengen
							px1=that.xconvert(pp1+that.diffbase-that.offsetbase)+that.xscrollpos;
							px2=that.xconvert(pp2+that.diffbase-that.offsetbase)+that.xscrollpos;
							if (px1>that.right) continue;if (px2<that.left) continue;//niets te zoeken hier
							px1=Math.floor(px1);
							px2=Math.floor(px2);
							x1=Math.max(that.left,px1);x1=Math.min(that.right,x1);
							x2=Math.max(that.left,px2);x2=Math.min(that.right,x2);
							var end=0;
							if (arrow==1) if (block.invert==true) end=0;else end=fts.exons.length-1;
							if (arrow==-1) if (block.invert==true) end=fts.exons.length-1;else end=0;
							_arrow=arrow;
							if (i!=end) _arrow=0;
							if (_arrow==1)
							{
								ctx.moveTo(x2,py);
								ctx.lineTo(x2+4,py+4);
								ctx.lineTo(x2,py+8);
								ctx.lineTo(x1,py+8);
								ctx.lineTo(x1,py);
								ctx.lineTo(x2,py);
							}
							if (_arrow==0)
							{
								if (x2!=x1)
								{
									ctx.moveTo(x2,py);
									ctx.lineTo(x2,py+8);
									ctx.lineTo(x1,py+8);
									ctx.lineTo(x1,py);
									ctx.lineTo(x2,py);
								}
								else 
								{
									ctx.moveTo(x2,py);
									ctx.lineTo(x2,py+8);
								}
							}
							if (_arrow==-1)
							{
								ctx.moveTo(x2,py);
								ctx.lineTo(x2,py+8);
								ctx.lineTo(x1,py+8);
								ctx.lineTo(x1-4,py+4);
								ctx.lineTo(x1,py);
								ctx.lineTo(x2,py);
							}
						}
						for (var i=0;i<fts.exons.length-1;i++)
						{
							pp1=fts.exons[i].stop+1;pp1=Math.max(pp1,block.startbase);
							pp2=fts.exons[i+1].start-1;pp2=Math.min(pp2,block.endbase);
							if (block.invert==true) {var pObj=that.InvertPos(pp1,pp2);pp1=pObj.ps1;pp2=pObj.ps2;}//posities inverteren
							pp2++;//feature einde ��n positie verlengen
							px1=that.xconvert(pp1+that.diffbase-that.offsetbase)+that.xscrollpos;
							px2=that.xconvert(pp2+that.diffbase-that.offsetbase)+that.xscrollpos;
							if (px1>that.right) continue;if (px2<that.left) continue;//niets te zoeken hier
							px1=Math.floor(px1);
							px2=Math.floor(px2);
							x1=Math.max(that.left,px1);x1=Math.min(that.right,x1);
							x2=Math.max(that.left,px2);x2=Math.min(that.right,x2);
							if (x2!=x1)
							{
								ctx.moveTo(x2,py+2);
								ctx.lineTo(x2,py+6);
								ctx.lineTo(x1,py+6);
								ctx.lineTo(x1,py+2);
								ctx.lineTo(x2,py+2);
							}
						}
					}
					ctx.closePath();
					ctx.stroke();
					ctx.fill();
					//einde exons uittekenen
					//aminozuur translatie zetten bij volledige zoom
					if (that.basespercm==that.minbasespercm && fts.key=="CDS")
					{
						ctx.fillStyle="rgb(0,0,0)";
						var fts_start_align=0;var fts_stop_align=0;
						if (block.invert==false){fts_start_align=block.EntryToContigPosition(fts.start);fts_stop_align=block.EntryToContigPosition(fts.stop);}
						else {fts_start_align=block.EntryToContigPosition(fts.stop);fts_stop_align=block.EntryToContigPosition(fts.start);}
						ctx.font="12px lucida console";
						fts_start_align=Math.max(that.line_b1,fts_start_align);
						fts_stop_align=Math.min(that.line_b2,fts_stop_align);
						for (var i=fts_start_align;i<=fts_stop_align;i++)
						{
							var p=block.ContigToEntryPosition(i);
							label=block.entry.GetFtsCodon(fts,p);
							px1=that.xconvert(i-that.offsetbase)+that.xscrollpos;
							px1=Math.floor(px1);
							if (label!='1' && label!='3' && label!='0')	ctx.fillText(label,px1,py+10);
						}
						ctx.font="11px serif";
						ctx.fillStyle="rgba(100,100,100,0.5)";
					}
					//einde aminozuur translatie zetten bij volledige zoom
					
				}
			}
			that.showsequencesearch=function(seqblockchannel)
			{
				var ctx=that._cnvContext;

				var ll=0;var py=0;
				var pp1=0;var pp2=0;
				var px1=0;var px2=0;
				var x1=0;var x2=0;

				var seqchannel=seqblockchannel.GetSeqChannel();
				ll=seqblockchannel.ChannelItemIndex.length;
				py=that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset()-seqchannel.GetSize();
				ctx.beginPath();
				ctx.strokeStyle=seqchannel.GetColor();
				ctx.fillStyle=seqchannel.GetColor();
				var lastpos=-1;
				var item=null;
				for (var k=0;k<ll;k++)
				{
					item=seqblockchannel.GetItem(k);
					pp1=item.start;
					pp2=item.stop;
					if (pp1>that.block.endbase) continue;
					if (pp2<that.block.startbase) continue;
					if (that.block.invert==true) 
					{
						var pObj=that.InvertPos(pp1,pp2);
						pp1=pObj.ps1;
						pp2=pObj.ps2;
					}
					pp2++;//feature einde ��n positie verlengen
					px1=that.xconvert(pp1+that.diffbase-that.offsetbase)+that.xscrollpos;
					px2=that.xconvert(pp2+that.diffbase-that.offsetbase)+that.xscrollpos;
					px1=Math.floor(px1);
					px2=Math.floor(px2);
					if (px1>that.right) continue;
					if (px2<that.left) continue;//niets te zoeken hier
					x1=Math.max(that.left,px1);x1=Math.min(that.right,x1);
					x2=Math.max(that.left,px2);x2=Math.min(that.right,x2);
					if (lastpos!=x1)
					{
						if (x1==x2)
						{
							ctx.moveTo(x1,py);
							ctx.lineTo(x1,py+2*seqchannel.GetSize());
						}
						else
						{
							ctx.fillRect(x1,py,x2-x1,2*seqchannel.GetSize());
						}
					}
					lastpos=x1;
				}
				ctx.closePath();
				ctx.stroke();
			}
			that.showframemapping=function(seqblockchannel)
			{
				var ctx=that._cnvContext;
				var image=that.imageData;
				var pp1=0;var pp2=0;var x1=0;var x2=0;
				var b1=Math.max(that.block.startbasegaps,that.line_b1);
				var b2=Math.min(that.block.endbasegaps,that.line_b2);

				var seqchannel=seqblockchannel.GetSeqChannel();
				var ll=seqblockchannel.ChannelItemIndex.length;
				var py=that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset();
				//start en stop van sequenceblock uittekenen
				ctx.beginPath();
				ctx.strokeStyle="rgb(200,200,200)";
				ctx.lineWidth=1;
				var px1=that.xconvert(b1-that.offsetbase)+that.xscrollpos;
				var px2=that.xconvert(b2-that.offsetbase)+that.xscrollpos;

				px1=Math.max(px1,that.left);px1=Math.min(px1,that.right);
				px2=Math.min(px2,that.right);px2=Math.max(px2,that.left);
				var yy=that.yoffset+that.sequence.plotoffset;
				if (px1!=px2) 
				{
					for (var i=0;i<6;i++)
					{
						var yy=that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset()+i*8-24+4;
						ctx.moveTo(px1,yy);
						ctx.lineTo(px2,yy);
					}
				}
				ctx.closePath();
				ctx.stroke();
				//einde
				if (that.basespercm>=10000)	return;
				
				if (that.basespercm==that.minbasespercm)
				{
					var obj={b1:'?',b2:'?',b3:'?',az:'X'}
					ctx.font="12px lucida console";
					ctx.textBaseline='top';
					ctx.fillStyle="rgb(0,0,0)";
					var py=that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset();
	//				ctx.fillStyle="rgba(150,150,150,0.5)";
					for (var i=0;i<that.locators.length;i++)
					{
						if (that.locators[i].lineindex==that.line && that.locators[i].seqindex==that.seqindex && that.locators[i].blockindex==that.blockindex)
						{
							var k=0;var h=' ';
							var len=that.locators[i].sequence.length;
							var start=that.locators[i].linestart;
//								while (start%3!=0) start++;
							for (var j=start;j<that.locators[i].lineend;j++)
							{
								if (k>=0 && k<len-2)
								{
									var p=that.block.ContigToEntryPosition(j);
									px1=that.xconvert(j+1-that.offsetbase)+that.xscrollpos;//+1 omdat aminozuur op tweede plaats van kodon geschreven wordt
									var frame=p%3;
									if (that.locators[i].inverted) frame=p%3-2;
									if (frame<0) frame+=3;
									obj.b1=that.locators[i].sequence[k+0];
									obj.b2=that.locators[i].sequence[k+1];
									obj.b3=that.locators[i].sequence[k+2];
									if (that.locators[i].inverted) 
									{
										obj.b1=that.locators[i].sequence[k+2];
										obj.b2=that.locators[i].sequence[k+1];
										obj.b3=that.locators[i].sequence[k+0];
									}
									Application.SeqTools.FastAz(obj);
									if (obj.az=='*') ctx.fillRect(px1,py+frame*8-24,10,10);
//									if (obj.az=='*') image.DrawBlock(px1,py+frame*8-24,px1+10,py+frame*8-14,150,150,150,255);
									else ctx.fillText(obj.az,px1,that.yoffset+that.sequence.plotoffset+seqchannel.GetOffset()+frame*8-24);
									frame+=3;
									obj.b1=Application.SeqTools.Complement(that.locators[i].sequence[k+2]);
									obj.b2=Application.SeqTools.Complement(that.locators[i].sequence[k+1]);
									obj.b3=Application.SeqTools.Complement(that.locators[i].sequence[k+0]);
									if (that.locators[i].inverted) 
									{
										obj.b1=Application.SeqTools.Complement(that.locators[i].sequence[k+0]);
										obj.b2=Application.SeqTools.Complement(that.locators[i].sequence[k+1]);
										obj.b3=Application.SeqTools.Complement(that.locators[i].sequence[k+2]);
									}
									Application.SeqTools.FastAz(obj);
									if (obj.az=='*') ctx.fillRect(px1,py+frame*8-24,10,10);
//									if (obj.az=='*') image.DrawBlock(px1,py+frame*8-24,px1+10,py+frame*8-14,150,150,150,255);
									else ctx.fillText(obj.az,px1,py+frame*8-24);
								}
								k++;
							}
						}
					}
					ctx.textBaseline='alphabetic';
				}
				ctx.stroke();
				
				ctx.beginPath();
				ctx.fillStyle="rgba(100,100,255,0.5)";
				ctx.strokeStyle="rgba(0,0,100,0.5)";
				for (var k=0;k<ll;k++)
				{
					var orf=seqblockchannel.GetItem(k);
					if (orf.frame>2) continue;
					pp1=orf.start;
					pp2=orf.stop;
					if (pp1>that.block.endbase) continue;
					if (pp2<that.block.startbase) continue;
					if (that.block.invert==true) 
					{
						var pObj=that.InvertPos(pp1,pp2);
						pp1=pObj.ps1;
						pp2=pObj.ps2;
					}
					pp2++;//feature einde ��n positie verlengen
					px1=that.xconvert(pp1+that.diffbase-that.offsetbase)+that.xscrollpos;
					px2=that.xconvert(pp2+that.diffbase-that.offsetbase)+that.xscrollpos;
					if (px1>that.right) continue;
					if (px2<that.left) continue;//niets te zoeken hier
					x1=Math.max(that.left,px1);x1=Math.min(that.right,x1);
					x2=Math.max(that.left,px2);x2=Math.min(that.right,x2);
//					image.DrawLine(x1,py+orf.frame*8-24+4,x2+1,py+orf.frame*8-24+4,100,100,255,255);
//					image.DrawLine(x1,py+orf.frame*8-24+2,x1,py+orf.frame*8-24+8,100,100,255,255);
//					image.DrawLine(x2,py+orf.frame*8-24+2,x2,py+orf.frame*8-24+8,100,100,255,255);
				
					ctx.fillRect(x1,py+orf.frame*8-24+2,x2+1-x1,6);
					ctx.moveTo(x1+0.5,py+orf.frame*8-24+2);
					ctx.lineTo(x1+0.5,py+orf.frame*8-24+2+6);
					ctx.moveTo(x2+0.5,py+orf.frame*8-24+2);
					ctx.lineTo(x2+0.5,py+orf.frame*8-24+2+6);
				}
				ctx.closePath();
				ctx.stroke();

				ctx.beginPath();
				ctx.fillStyle="rgba(255,100,100,0.5)";
				ctx.strokeStyle="rgba(100,0,0,0.5)";
				for (var k=0;k<ll;k++)
				{
					var orf=seqblockchannel.GetItem(k);
					pp1=orf.start;
					pp2=orf.stop;
					if (orf.frame<3) continue;
					if (pp1>that.block.endbase) continue;
					if (pp2<that.block.startbase) continue;
					if (that.block.invert==true) 
					{
						var pObj=that.InvertPos(pp1,pp2);
						pp1=pObj.ps1;
						pp2=pObj.ps2;
					}
					pp2++;//feature einde ��n positie verlengen
					px1=that.xconvert(pp1+that.diffbase-that.offsetbase)+that.xscrollpos;
					px2=that.xconvert(pp2+that.diffbase-that.offsetbase)+that.xscrollpos;
					if (px1>that.right) continue;
					if (px2<that.left) continue;//niets te zoeken hier
					x1=Math.max(that.left,px1);x1=Math.min(that.right,x1);
					x2=Math.max(that.left,px2);x2=Math.min(that.right,x2);
//					image.DrawLine(x1,py+orf.frame*8-24+4,x2+1,py+orf.frame*8-24+4,255,100,100,255);
//					image.DrawLine(x1,py+orf.frame*8-24+2,x1,py+orf.frame*8-24+8,255,100,100,255);
//					image.DrawLine(x2,py+orf.frame*8-24+2,x2,py+orf.frame*8-24+8,255,100,100,255);
					ctx.fillRect(x1,py+orf.frame*8-24+2,x2+1-x1,6);
					ctx.moveTo(x1+0.5,py+orf.frame*8-24+2);
					ctx.lineTo(x1+0.5,py+orf.frame*8-24+2+6);
					ctx.moveTo(x2+0.5,py+orf.frame*8-24+2);
					ctx.lineTo(x2+0.5,py+orf.frame*8-24+2+6);
				}
				ctx.closePath();
				ctx.stroke();
			}
			that.zscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
				var old=that.basespercm;
				if (zscrollpos>=zscrollmax/2)
				{
					var m=zscrollpos-zscrollmax/2;
//					that.basespercm=that.basespercm-0.01*Math.pow(m/zscrollmax,3)*that.maxbasespercm;
					that.basespercm=that.basespercm-5*that.GetScale()*(m/zscrollmax);
					if (that.basespercm<=that.minbasespercm) that.basespercm=that.minbasespercm;
				}
				if (zscrollpos<zscrollmax/2)
				{
					var m=zscrollmax/2-zscrollpos;
//					that.basespercm=that.basespercm+0.3*m/zscrollmax*that.basespercm;
//					that.basespercm=that.basespercm+0.01*Math.pow(m/zscrollmax,3)*that.maxbasespercm;
					that.basespercm=that.basespercm+5*that.GetScale()*(m/zscrollmax);
					if (that.basespercm>=that.maxbasespercm) that.basespercm=that.maxbasespercm;
				}
				if (old!=that.basespercm)
				{
					that.oldzoom=that.basespercm;
					var scroll=false;
					if (that.circular==false) scroll=true;
					that.UpdateFacts(scroll);
					
				}
			}
			that.hscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
				if (that.circular) that.C_offset=zscrollpos*360/zscrollmax;
				that.DataUpdate();
				that.invalidate();
				return true;
			}
			that.vscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
				that.invalidate();
				that.DataUpdate();
				return true;
			}
			that.hvscrollposchanged=function(nscrollpos,scrollmax,deltascroll)
			{
				that.DataUpdate();
				return false;
			}
			that.SwitchCircular=function()
			{
				if (that.circular==false) that.circular=true;else that.circular=false;
				that.UpdateFacts();
				that.scrolltopos(that.curpos);
			}
			that.SwitchMultiLine=function()
			{
				that.multiline=!that.multiline;
				that.UpdateFacts();
				that.scrolltopos(that.curpos);
			}
			that.SwitchDisplayLabel=function()
			{
				that.displaylabel=!that.displaylabel;
				that.UpdateFacts();
			}
			that.UpdateAndScrollToFocus=function()
			{
				that.UpdateFacts(true);
//				that.scrolltopos(that.curpos);
			}

			that.getxpos=function(xpos,ypos)
			{
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				var iy=Math.floor((ypos-yoo)/Math.max(that.linespace,1));
				if (iy<0) iy=0;
				if (!that.multiline) iy=0;
				var ix=(xpos-xoo-that.leftmargin)*that.basespercm/that.cm2pix;
				var ps=Math.floor(ix+iy*that.basesperline);
				if (that.multiline) 
				{
					if (ps<iy*that.basesperline) ps=iy*that.basesperline;
					if (ps>iy*that.basesperline+that.basesperline) ps=iy*that.basesperline+that.basesperline;
				}
				ps=Math.max(0,ps);
				ps=Math.min(that.SeqBox.GetMaxSeqLen(),ps);
				return ps;
			}
			that.getypos=function(xpos,ypos)
			{
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				var iy=(ypos-yoo)/Math.max(that.linespace,1);
				if (iy<0) iy=0;
				if (!that.multiline) iy=0;
				var ix=(xpos-xoo-that.leftmargin)*that.basespercm/that.cm2pix;
				var ps=ix+iy*that.basesperline;
				if (that.multiline) 
				{
					if (ps<iy*that.basesperline) ps=iy*that.basesperline;
					if (ps>iy*that.basesperline+that.basesperline) ps=iy*that.basesperline+that.basesperline;
				}
				ps=Math.max(0,ps);
				ps=Math.min(that.SeqBox.GetMaxSeqLen(),ps);
				var yy=0;
				if (that.multiline) yy=Math.floor(ps/that.basesperline);
				yy=ypos-yy*that.linespace-yoo;
				return yy;
			}
			that.getsequencesel=function(xpos,ypos)
			{
				var yoo=-that.GetVScrollPos();
				var yy=Math.floor((ypos-yoo)/Math.max(that.linespace,1));
				if (yy<0) yy=0;
				if (!that.multiline) yy=0;
				var cty=yy*that.linespace+yoo;
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var sequence=that.SeqBox.GetSequence(i);
					if (ypos>=cty+sequence.plotoffset-sequence.GetUpperSeqChannelSpace() && ypos<cty+sequence.plotoffset+sequence.GetLowerSeqChannelSpace()) return i;
				}
				return -1;
			}
			that.SetChannelCallBack=function(callback)
			{
				that.ChannelCallBack=callback;
			}
			that.getchannelsel=function(xpos,ypos)
			{
				if (that.sequencesel<0 || that.sequencesel>=that.SeqBox.GetSequenceCount()) return -1;
				var sequence=that.SeqBox.GetSequence(that.sequencesel);
				if (sequence==null) return -1;
				var yoo=-that.GetVScrollPos();
				var yy=Math.floor((ypos-yoo)/Math.max(that.linespace,1));
				if (yy<0) yy=0;
				if (!that.multiline) yy=0;
				var cty=yy*that.linespace+yoo;
				for (var i=0;i<sequence.GetSeqChannelCount();i++)
				{
					var seqchannel=sequence.GetSeqChannel(i);
					if (seqchannel.GetView()==false) continue;
					if (ypos>cty+sequence.plotoffset+seqchannel.GetOffset()-seqchannel.GetSize() && ypos<=cty+sequence.plotoffset+seqchannel.GetOffset()+seqchannel.GetSize()) 
					{
						if (that.ChannelCallBack!=null) that.ChannelCallBack(seqchannel,sequence,-1);
						return i;
					}	
				}
				return -1;
			}
			that.CurrentChannelItem=function()
			{
				if (that.sequencesel<0 || that.channelsel<0) return;
				var sequence=that.SeqBox.GetSequence(that.sequencesel);
				if (sequence==null) return;
				var seqchannel=sequence.GetSeqChannel(that.channelsel);
				if (seqchannel==null) return;
				var channel=seqchannel.channel;
				var selection={offset:that.curpos,block:null};
				var nr=sequence.get_channelitem_from_aligned_pos(channel,selection);
				selection.offset=that.curpos+1;
				var nr=sequence.find_current_channelitem(channel,selection);
				if (nr>-1 && selection.block!=null) 
				{
					var p1=channel.GetFullItemStart(nr);
					var p2=channel.GetFullItemStop(nr);
					var diffbase=selection.block.startbasegaps-selection.block.startbase;
					if (selection.block.invert)
					{
						p1=selection.block.endbase-channel.GetFullItemStop(nr)+selection.block.startbase-1;
						p2=selection.block.endbase-channel.GetFullItemStart(nr)+selection.block.startbase-1;
					}
					that.selpos1=p1+diffbase;
					that.selpos2=p2+diffbase+1;
					that.curpos=p1+diffbase;
				}
				if (that.ChannelCallBack!=null) that.ChannelCallBack(seqchannel,sequence,nr);
			}
			that.NextChannelItem=function()
			{
				if (that.sequencesel<0 || that.channelsel<0) return;
				var sequence=that.SeqBox.GetSequence(that.sequencesel);
				if (sequence==null) return;
				var seqchannel=sequence.GetSeqChannel(that.channelsel);
				if (seqchannel==null) return;
				var channel=seqchannel.channel;
				var selection={offset:that.curpos,block:null};
				var nr=sequence.get_channelitem_from_aligned_pos(channel,selection);
				selection.offset=that.curpos+1;
				var nr=sequence.find_next_channelitem(channel,selection);
				if (nr>-1 && selection.block!=null) 
				{
					var p1=channel.GetFullItemStart(nr);
					var p2=channel.GetFullItemStop(nr);
					var diffbase=selection.block.startbasegaps-selection.block.startbase;
					if (selection.block.invert)
					{
						p1=selection.block.endbase-channel.GetFullItemStop(nr)+selection.block.startbase-1;
						p2=selection.block.endbase-channel.GetFullItemStart(nr)+selection.block.startbase-1;
					}
					that.selpos1=p1+diffbase;
					that.selpos2=p2+diffbase+1;
					that.curpos=p1+diffbase;
					that.scrolltopos(that.curpos);
				}
				if (that.ChannelCallBack!=null) that.ChannelCallBack(seqchannel,sequence,nr);
			}
			that.PreviousChannelItem=function()
			{
				if (that.sequencesel<0 || that.channelsel<0) return;
				var sequence=that.SeqBox.GetSequence(that.sequencesel);
				if (sequence==null) return;
				var seqchannel=sequence.GetSeqChannel(that.channelsel);
				if (seqchannel==null) return;
				var channel=seqchannel.channel;
				var selection={offset:that.curpos,block:null};
				var nr=sequence.get_channelitem_from_aligned_pos(channel,selection);
				selection.offset=that.curpos-1;
				var nr=sequence.find_previous_channelitem(channel,selection);
				if (nr>-1 && selection.block!=null) 
				{
					var p1=channel.GetFullItemStart(nr);
					var p2=channel.GetFullItemStop(nr);
					var diffbase=selection.block.startbasegaps-selection.block.startbase;
					if (selection.block.invert)
					{
						p1=selection.block.endbase-channel.GetFullItemStop(nr)+selection.block.startbase-1;
						p2=selection.block.endbase-channel.GetFullItemStart(nr)+selection.block.startbase-1;
					}
					that.selpos1=p1+diffbase;
					that.selpos2=p2+diffbase+1;
					that.curpos=p2+diffbase;
					that.scrolltopos(that.curpos);
				}
				if (that.ChannelCallBack!=null) that.ChannelCallBack(seqchannel,sequence,nr);
			}
			that.scrolltopos=function(pos)
			{
				if (!that.multiline && that.SeqBox.GetMaxSeqLen()>0)
				{
					var ps=parseInt(pos*that.GetHScrollSize()/that.SeqBox.GetMaxSeqLen());
					if (that.HasHScroller()) that.SetNewHScrollPos(Math.max(ps-200,0));else that.invalidate();
				}
				else
				{
					var line=that.linenumber(pos);
					if (that.HasVScroller()) that.SetNewVScrollPos(Math.max(line*that.linespace,0));else that.invalidate();
				}
			}
			that.SetSel=function(ps1,ps2,cursor)
			{
				that.selpos1=ps1;
				that.selpos2=ps2;
				that.curpos=cursor;
				that.scrolltopos(that.curpos);
			}
			that.linenumber=function(pos)
			{
				if (that.basesperline<=0) return -1;
				return Math.floor(pos/that.basesperline);
			}
			that.MouseDown=function(posx,posy)  //overwrite
			{
/*				var point={x:0,y:0};
				point.x=posx;
				point.y=posy;
				that.C_positinv(point);
				return;*/
				that.curpos=that.getxpos(posx,posy);
				that.selpos1=that.curpos;
				that.selpos2=that.curpos;
				that.ycurpos=that.getypos(posx,posy);
				that.sequencesel=that.getsequencesel(posx,posy);
				that.channelsel=that.getchannelsel(posx,posy);
				that.CurrentChannelItem();
				that.render();
			}
			that.FullZoom=function()
			{
				if (that.basespercm==that.minbasespercm)
				{
					that.basespercm=that.oldzoom;
				}
				else
				{
					that.oldzoom=that.basespercm;
					that.basespercm=that.minbasespercm;
				}
				that.DataUpdate();
				that.UpdateAndScrollToFocus();
			}


			that.checkpoints=function(pp1,pp2)
			{
				var passed=0;
				if (that.cxx2<that.cxx1)// springt over het nul-punt
				{
					if (pp1<=that.cxx2)  passed=1;//tussen nul-punt en rechter marge
					if (pp2>=that.cxx1) passed=1;// tussen linker marge en nul-punt
				}
				else//nul-punt is buiten de scope
				{
//					start in scope							stop in scope						start EN stop in scope 					start en stop overspannen scope
					if ((pp1>=that.cxx1 && pp1<=that.cxx2) || (pp2>=that.cxx1 && pp2<=that.cxx2)  || (pp1>=that.cxx1 && pp2<=that.cxx2) ||(pp1<=that.cxx1 && pp2>=that.cxx2)) passed=2;
				}
				return passed;
			}
			that.setpoints=function(_pp1,_pp2,arrow,rad,delta,context)
			{
				var pp1=_pp1;
				var pp2=_pp2;
				if (that.cxx2<that.cxx1)// springt over het nul-punt
				{
					pp1=_pp1;pp2=_pp2;
					if (pp1<=that.cxx2)//tussen nul-punt en rechter marge
					{
						pp2=Math.min(that.cxx2,pp2);
						that.C_polygon(pp1,pp2,arrow,rad,delta);
						context.moveTo(that.xpunten[0],that.ypunten[0]);
						for (var j=1;j<that.xpunten.length;j++) context.lineTo(that.xpunten[j],that.ypunten[j]);
					}
					pp1=_pp1;pp2=_pp2;
					if (pp2>=that.cxx1)// tussen linker marge en nul-punt
					{
						pp1=Math.max(that.cxx1,pp1);
						that.C_polygon(pp1,pp2,arrow,rad,delta);
						context.moveTo(that.xpunten[0],that.ypunten[0]);
						for (var j=1;j<that.xpunten.length;j++) context.lineTo(that.xpunten[j],that.ypunten[j]);
					}
				}
				else//nul-punt is buiten de scope
				{
//					start in scope							stop in scope						start EN stop in scope 					start en stop overspannen scope
					pp1=_pp1;pp2=_pp2;
					if ((pp1>=that.cxx1 && pp1<=that.cxx2) || (pp2>=that.cxx1 && pp2<=that.cxx2)  || (pp1>=that.cxx1 && pp2<=that.cxx2) ||(pp1<=that.cxx1 && pp2>=that.cxx2))
					{
						pp1=Math.max(pp1,that.cxx1);
						pp2=Math.min(pp2,that.cxx2);
						that.C_polygon(pp1,pp2,arrow,rad,delta);
						context.moveTo(that.xpunten[0],that.ypunten[0]);
						for (var j=1;j<that.xpunten.length;j++) context.lineTo(that.xpunten[j],that.ypunten[j]);
					}
				}
			}
			that.showCircularMaps=function()
			{
				var rad=200*that.maxbasespercm/(that.basespercm*2*Math.PI);
				rad=Math.max(rad,50);
				var context=that._cnvContext;
				var points=[];

				var col0="rgba(255,255,255,1.0)";
				var bklr=col0;
				context.fillStyle=bklr;

				var point={x:0,y:0};
				var len=that.SeqBox.GetMaxSeqLen();
				that.cxx1=0;
				that.cxx2=len;
				if (rad>that.GetXSize()/2+25)
				{
					var xxx=that.GetXSize()/2+25;
					point.x=-xxx;
					point.y=-Math.sqrt(rad*rad-xxx*xxx);
					var theta=that.C_hoek(point)+2*Math.PI*that.C_offset/360;
					that.cxx1=Math.floor((1.0*theta/(2.0*Math.PI))*len);
					if (that.cxx1<0) that.cxx1=that.cxx1+len;if (that.cxx1>len) that.cxx1=that.cxx1-len;
					point.x=+xxx;
					point.y=-Math.sqrt(rad*rad-xxx*xxx);
					var theta=that.C_hoek(point)+2*Math.PI*that.C_offset/360;
					that.cxx2=Math.floor((1.0*theta/(2.0*Math.PI))*len);
					if (that.cxx2<0) that.cxx2=that.cxx2+len;if (that.cxx2>len) that.cxx2=that.cxx2-len;
				}
				
				context.beginPath();
				context.arc(that.centrumX,that.centrumY,rad,0,2*Math.PI,false);
				context.lineWidth=1;
				context.strokeStyle='rgba(100,100,100,0.2)';
				context.stroke();
				context.closePath();
				
				var point1={x:0,y:0};
				var point2={x:0,y:0};
				context.strokeStyle='rgba(100,100,100,0.5)';
				that.C_shownumbers();
				var detail=true;
				var k=that.C_boog(0,1000)*rad;
				if (k<1) detail=false;
				that.ff=0.7-Math.min(0.7,2*that.basespercm/that.maxbasespercm);
				for (var s=0;s<that.SeqBox.GetSequenceCount();s++)
				{
//					if (i==0) continue;
					var sequence=that.SeqBox.GetSequence(s);
					that.sequence=sequence;
					that.hasSeqNumbers=sequence.FindChannelType("Sequence numbering");
					that.hasSeqData=sequence.FindChannelType("SequenceData");
//					var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-s*10;
					var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-that.ff*(that.sequence.plotoffset);
					rad=Math.max(rad,50);
					if (detail==false)
					{
						for (var k=0;k<sequence.GetSeqChannelCount();k++)
						{
							var seqchannel=sequence.GetSeqChannel(k);
							var curve=seqchannel.curve;
							var clen=curve.length;
							if (clen==0) continue;
							var _rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-that.ff*(that.sequence.plotoffset+seqchannel.GetOffset()-seqchannel.GetSize());
							_rad=Math.max(_rad,50);
							context.lineWidth=1;
							context.strokeStyle=seqchannel.GetColor();
							context.fillStyle=seqchannel.GetColor();
							h=360-360*0/clen;
							that.C_GetPoint(point1,h,_rad);
							h=360-360*1/clen;
							that.C_GetPoint(point2,h,_rad);
							var showcurve=false;
							if (Math.abs(point1.x-point2.x)>1) showcurve=true;
							if (showcurve)
							{
								for (var c=0;c<clen;c++)
								{
									h=360-360*c/clen+that.C_offset;
									that.C_GetPoint(point1,h,_rad);
									if (point1.x<-10 || point1.x>that.GetXSize()+10) continue;
									if (curve[c]>0) 
									{
										context.beginPath();
										context.moveTo(point1.x,point1.y);
										h=360-360*c/clen+that.C_offset;
										that.C_GetPoint(point2,h,_rad-curve[c]);
										context.lineTo(point2.x,point2.y);
										h=360-360*(c+1)/clen+that.C_offset;
										that.C_GetPoint(point2,h,_rad-curve[c+1]);
										context.lineTo(point2.x,point2.y);
										that.C_GetPoint(point2,h,_rad);
										context.lineTo(point2.x,point2.y);
										context.closePath();
										context.stroke();
										context.fill();
									}
								}
							}
							else
							{
								context.beginPath();
								for (var c=0;c<clen;c++)
								{
									h=360-360*c/clen+that.C_offset;
									that.C_GetPoint(point1,h,_rad);
									if (point1.x<-10 || point1.x>that.GetXSize()+10) continue;
									if (curve[c]>0) 
									{
										context.moveTo(point1.x,point1.y);
										that.C_GetPoint(point2,h,_rad-curve[c]);
										context.lineTo(point2.x,point2.y);
									}
								}
								context.stroke();
								context.closePath();
							}
						}
					}
					if (detail==true)
					{
						var passed=0;
						var j=sequence.GetBlockCnt();
						while (j--)
						{
							that.block=sequence.GetBlockIndex(j);
							passed=that.checkpoints(that.block.startbasegaps,that.block.endbasegaps);
							if (passed==0) continue;
							that.C_paintblock(s,j);
						}
					}
					if (detail==false)
					{
						var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-that.ff*(that.sequence.plotoffset);
						rad=Math.max(rad,50);
						context.lineWidth=1;
						var toplot=0;
						var plot=function(dir)
						{
							if (dir>0) {context.fillStyle='rgba(100,100,255,0.2)';context.strokeStyle='rgba(100,100,255,0.4)';}
							else {context.fillStyle='rgba(255,100,100,0.2)';context.strokeStyle='rgba(255,100,100,0.4)';}
							context.beginPath();
							h=360-360*points[0]/sequence.bitsize+that.C_offset;
							that.C_GetPoint(point1,h,rad);
							context.moveTo(point1.x,point1.y);
							var cnt=points.length;
							for (var j=0;j<cnt;j++)
							{
								h=360-360*points[j]/sequence.bitsize+that.C_offset;
								that.C_GetPoint(point2,h,rad);
								context.lineTo(point2.x,point2.y);
							}
							context.stroke();
							context.closePath();
							points.length=0;
							toplot=0;
						}
						i=0;
						while (i<sequence.bitsize)
						{
							while (sequence.bitinvert.getBit(i)==1) {points.push(i);i++;toplot=1;}
							if (toplot>0) plot(-1);
							i++;
						}
						if (toplot>0) plot(-1);
						i=0;
						while (i<sequence.bitsize)
						{
							while (sequence.bitdirect.getBit(i)==1) {points.push(i);i++;toplot=1;}
							if (toplot>0) plot(+1);
							i++;
						}
						if (toplot>0) plot(+1);
					}
				}
			}
			that.C_paintblock=function(seqnr,blocknr)
			{
				var context=that._cnvContext;
				var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-seqnr*10;
				var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-that.ff*(that.sequence.plotoffset);
				rad=Math.max(rad,50);
				var len=that.SeqBox.GetMaxSeqLen();
				that.diffbase=that.block.startbasegaps-that.block.startbase;

				var pp1=that.block.startbasegaps;
				var pp2=that.block.endbasegaps;
				
				context.lineWidth=1;

				context.beginPath();
				context.fillStyle="rgba(255,100,100,0.5)";
				context.strokeStyle='rgba(100,100,100,0.5)';
				that.setpoints(pp1,pp2,0,rad,0,context);
//				context.fill();
				context.stroke();
				context.closePath();

				context.strokeStyle='rgba(0,0,0,0.5)';
				for (var j=0;j<that.block.GetSeqBlockChannelCnt();j++)
				{
					var seqblockchannel=that.block.GetSeqBlockChannel(j);
					if (seqblockchannel==null) continue;
					var seqchannel=seqblockchannel.GetSeqChannel();
					if (seqchannel==null || seqchannel.GetView()==false) continue;
					if (seqchannel.GetType()=="FeatureSearch") that.C_showfeatures(seqblockchannel);
					if (seqchannel.GetType()=="SequenceSearch") that.C_showsequencesearch(seqblockchannel);
					if (seqchannel.GetType()=="VariationSearch") that.C_showsequencesearch(seqblockchannel);
//					if (seqchannel.GetType()=="FrameMapping") that.C_showframemapping(seqblockchannel);
				}
			}
			that.C_rad=function(ps)
			{
				return 2*Math.PI*ps/that.SeqBox.GetMaxSeqLen()-2*Math.PI*that.C_offset/360;
			}
			that.C_boog=function(ps1,ps2)
			{
				return 2*Math.PI*Math.abs(ps2-ps1)/that.SeqBox.GetMaxSeqLen();
			}
			that.C_hoek=function(point)
			{
				var hk=0;
				if (point.x!=0)
				{
					hk=Math.atan(point.y/point.x);
					if (point.x>0) hk=hk+Math.PI/2;
					if (point.x<0) hk=hk+3*Math.PI/2;
				}
				else 
				{
					if (point.y>0) hk=Math.PI;
					if (point.y<0) hk=0;
				}
				return hk;
			}
			that.C_GetPoint=function(point,h,rad)
			{
				point.x=Math.floor(that.centrumX-Math.sin(h/180*Math.PI)*rad);
				point.y=Math.floor(that.centrumY-Math.cos(h/180*Math.PI)*rad);
			}
			that.C_addPoint=function(h,rad)
			{
				var k=h/180*Math.PI;
				that.xpunten.push(Math.floor(that.centrumX-Math.sin(k)*rad));
				that.ypunten.push(Math.floor(that.centrumY-Math.cos(k)*rad));
			}
			that.C_polygon=function(start,stop,arrow,rad,delta)
			{
				var len=that.SeqBox.GetMaxSeqLen();
				if (stop<start) stop=stop+len;
				
				var rd1=rad+that.ff*delta;
				var rd2=rad-that.ff*delta;

				var h1=360-360.0*start/len+that.C_offset;
				var h2=360-360.0*stop/len+that.C_offset; 
				if (arrow==1) {var h=h1;h1=h2;h2=h;}
				that.xpunten.length=0;
				that.ypunten.length=0;
				var dir=1;
				if (h2<=h1) dir=-1;
	
				var dx=1.0*(rd1-rd2)/2;
				var d=1000.0/rad;
				if (d>(Math.abs(h2-h1))/2) d=(Math.abs(h2-h1))/2;
				if (arrow==0) d=0;
				if (arrow!=0)
				{
					that.C_addPoint(h1+d*dir,rd2-dx);
					that.C_addPoint(h1,(rd1+rd2)/2);
					that.C_addPoint(h1+d*dir,rd1+dx);
				}
				h1=h1+1.0*d*dir;
				var cnt=Math.floor(Math.max(rad/10000,50)*2*Math.PI*Math.abs(stop-start)/that.SeqBox.GetMaxSeqLen())+1;
				for (var i=0;i<=cnt;i++) that.C_addPoint(h1+1.0*i/cnt*(h2-h1),rd1);
				if (delta==0) return;//slechts lijn tekenen
				for (var i=0;i<=cnt;i++) that.C_addPoint(h2+1.0*i/cnt*(h1-h2),rd2);
				if (arrow!=0) that.C_addPoint(h1,rd2-dx);else that.C_addPoint(h1,rd1);
			}
			that.C_showfeatures=function(seqblockchannel)
			{
				var context=that._cnvContext;
				var point1={x:0,y:0};
				var point2={x:0,y:0};
	
				var seqchannel=seqblockchannel.GetSeqChannel();
				var _rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-that.ff*(that.sequence.plotoffset+seqchannel.GetOffset());
				_rad=Math.max(_rad,50);

				ll=seqblockchannel.ChannelItemIndex.length;
				var dd=10;
				var alpha=60;
				var len=that.SeqBox.GetMaxSeqLen();
				for (var k=0;k<ll;k++)
				{
					var fts=seqblockchannel.GetItem(k);
					var arrow=fts.dir;
					var rad=_rad;
					if (fts.dir<0) rad=_rad-10*(1-that.ff);
					if (that.block.invert==true) if (arrow==1) arrow=-1;else if (arrow==-1) arrow=1;
					if (fts.dir<0) {r=255;g=0;b=0;r2=255;g2=170;b2=170;} else {r=0;g=0;b=255;r2=170;g2=170;b2=255;}
					pp1=fts.start;pp1=Math.max(pp1,that.block.startbase);
					pp2=fts.stop;pp2=Math.min(pp2,that.block.endbase);
					if (pp1>that.block.endbase) continue;if (pp2<that.block.startbase) continue;
					if (that.block.invert==true) {var pObj=that.InvertPos(pp1,pp2);pp1=pObj.ps1;pp2=pObj.ps2;}
					pp2++;
					pp1+=that.diffbase;
					pp2+=that.diffbase;
					
					var h1=360-360.0*pp1/len+that.C_offset;
					var kk=h1/180*Math.PI;
					var x1=that.centrumX-Math.sin(kk)*rad;
					var h2=360-360.0*pp2/len+that.C_offset;
					kk=h2/180*Math.PI;
					var x2=that.centrumX-Math.sin(kk)*rad;
					if (x1<0 && x2<0) continue;
					if (x1>that.GetXSize() && x2>that.GetXSize()) continue;
					
					var h=rad*that.C_boog(pp1,pp2);
					if (fts.dir<0) {context.fillStyle='rgba(255,0,0,0.5)';context.strokeStyle='rgba(255,0,0,0.5)';} else {context.fillStyle='rgba(0,0,255,0.5)';context.strokeStyle='rgba(0,0,255,0.5)';}
					context.beginPath();
					if (h<2)
					{
						that.C_GetPoint(point1,h1,rad+that.ff*seqchannel.GetSize()/3);
						that.C_GetPoint(point2,h1,rad-that.ff*seqchannel.GetSize()/3);
						context.moveTo(point1.x,point1.y);
						context.lineTo(point2.x,point2.y);
					}
					else
					{
						for (var i=0;i<fts.exons.length;i++)
						{
							pp1=fts.exons[i].start;pp1=Math.max(pp1,that.block.startbase);
							pp2=fts.exons[i].stop;pp2=Math.min(pp2,that.block.endbase);
							if (that.block.invert==true) {var pObj=that.InvertPos(pp1,pp2);pp1=pObj.ps1;pp2=pObj.ps2;}//posities inverteren
							pp2++;//feature einde ��n positie verlengen
							pp1+=that.diffbase;
							pp2+=that.diffbase;
							var _p=arrow;
							if (arrow==1 && i!=fts.exons.length-1) _p=0;
							if (arrow==-1 && i!=0) _p=0;
							that.setpoints(pp1,pp2,_p,rad,seqchannel.GetSize()/4,context);
						}
					}
					//einde exons uittekenen
					context.fill();
					context.stroke();
					context.closePath();
				}
			}
			that.C_showsequencesearch=function(seqblockchannel)
			{
				var context=that._cnvContext;
	
				var seqchannel=seqblockchannel.GetSeqChannel();
				var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI))-that.ff*(that.sequence.plotoffset+seqchannel.GetOffset());
				rad=Math.max(rad,50);

				ll=seqblockchannel.ChannelItemIndex.length;
				var len=that.SeqBox.GetMaxSeqLen();
				context.beginPath();
				context.fillStyle='rgba(100,100,100,0.5)';
				context.strokeStyle='rgba(100,100,100,0.5)';
				var point={x:0,y:0};
				var point1={x:0,y:0};
				var point2={x:0,y:0};
				var seqhit=null;
				for (var k=0;k<ll;k++)
				{
					seqhit=seqblockchannel.GetItem(k);
					pp1=seqhit.start;
					pp2=seqhit.stop;
					if (pp1>that.block.endbase) continue;
					if (pp2<that.block.startbase) continue;
					if (that.block.invert==true) {var pObj=that.InvertPos(pp1,pp2);pp1=pObj.ps1;pp2=pObj.ps2;}
					pp1+=that.diffbase;
					pp2+=that.diffbase;
					
					var h1=360-360.0*pp1/len+that.C_offset;
					var kk=h1/180*Math.PI;
					var x1=that.centrumX-Math.sin(kk)*rad;
					var h2=360-360.0*pp2/len+that.C_offset;
					kk=h2/180*Math.PI;
					var x2=that.centrumX-Math.sin(kk)*rad;
					
					if (x2<0 || x2>that.GetXSize()) continue;
					
					var h=rad*that.C_boog(pp1,pp2);
					if (h<2)
					{
						that.C_GetPoint(point1,h1,rad-that.ff*seqchannel.GetSize());
						that.C_GetPoint(point2,h1,rad+that.ff*seqchannel.GetSize());
						context.moveTo(point1.x,point1.y);
						context.lineTo(point2.x,point2.y);
					}
					else
					{
						that.setpoints(pp1,pp2,0,rad,seqchannel.GetSize(),context);
					}
				}
				context.fill();
				context.stroke();
				context.closePath();
			}
			
			that.C_shownumbers=function()
			{
				var context=that._cnvContext;
				var point1={x:0,y:0};
				var point2={x:0,y:0};
				var point3={x:0,y:0};
				context.font = "11px serif";
				var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI));
				rad=Math.max(rad,50);
				var len=that.SeqBox.GetMaxSeqLen();
				var step=~~that.GetScale();
				var x1=(Math.floor(0/step))*step;
				var x2=(Math.floor(len/step))*step;
				if (rad>that.GetXSize()/2)
				{
					x1=(Math.floor(that.cxx1/step))*step;
					x2=(Math.floor(that.cxx2/step))*step;
				}
				var mul=10*step;

				i=x1;
				var jump=false
				if (x2<x1) jump=true;
				var k=0;
				context.fillStyle="rgb(255,0,0)";
				context.fillRect(that.GetXSize()/2,20,2,5);
//				context.fillText(0,that.GetXSize()/2,20);
				context.beginPath();
				context.fillStyle="rgb(0,0,0)";
				while(k<1000)
				{
					var h1=360-360.0*i/len+that.C_offset;
					that.C_GetPoint(point1,h1,rad+2);
					that.C_GetPoint(point2,h1,rad+7);
					that.C_GetPoint(point3,h1,rad+12);
					var hk=2*Math.PI*h1/360;
					if (hk>2*Math.PI) hk=hk-2*Math.PI;
					if (hk<0) hk=hk+2*Math.PI;
					if (hk>=0 && hk<Math.PI/2) {context.textBaseline='bottom';context.textAlign="right";};
					if (hk>=Math.PI/2 && hk<Math.PI) {context.textBaseline='top';context.textAlign="right";};
					if (hk>=Math.PI && hk<3*Math.PI/2) {context.textBaseline='top';context.textAlign="left";};
					if (hk>=3*Math.PI/2 && hk<2*Math.PI) {context.textBaseline='bottom';context.textAlign="left";};
					context.moveTo(point1.x,point1.y);
					if (i%step==0 && i>0) context.lineTo(point2.x,point2.y);
					if (i%mul==0 && i>0)
					{
						context.lineTo(point3.x,point3.y);
						var k=parseInt(360-360.0*i/len+that.C_offset);
						if (k>=360) k=k-360;
						if (k<0) k=k+360;
						context.fillText(i,point3.x,point3.y);
					}
					i=i+step;
					if (jump==true && i>len) {i=(Math.floor((i-len)/step))*step;jump=false;}
					if (i>x2 && jump==false) break;
					k++;
				}
				that.C_GetPoint(point1,that.C_offset,rad);
				that.C_GetPoint(point3,that.C_offset,rad+12);
				context.moveTo(point1.x,point1.y);
				context.lineTo(point3.x,point3.y);
				context.fillText(0,point3.x,point3.y);
				context.closePath();
				context.stroke();
			}

			that.C_positinv=function(_point)
			{
				var len=that.SeqBox.GetMaxSeqLen();
				var point={x:0,y:0};
				point.x=_point.x-that.centrumX;
				point.y=_point.y-that.centrumY;
				var theta=that.C_hoek(point)+2*Math.PI*that.C_offset/360;
				var ps=Math.floor((1.0*theta/(2.0*Math.PI))*len);
				if (ps<0) ps=ps+len;if (ps>len) ps=ps-len;
				return ps;
			}
			that.exportPdf=function() 
			{
				  // only jpeg is supported by jsPDF
				  var imgData=that.getMyCanvasElement('main').toDataURL("image/jpeg", 1.0);
				  var pdf=new jsPDF();
				  pdf.addImage(imgData, 'JPEG', 0, 0);
				  var download = document.getElementById('download');
				  pdf.save("download.pdf");
			}
			Application.RegisterSeqPlot(that);
			return that;
        };
		return SeqPlot;
    });