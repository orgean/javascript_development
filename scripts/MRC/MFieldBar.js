TableColInfo = function (iID, iname, isize) {
            var that = {};
            that.ID = iID;
            that.name = iname;
            that.size = isize;
			that.xp=0;
            return that;
}

MFieldBarInfo=function()
{
	var that = this;
	var xp=0;
	var yp=0;
	var cx=0;
	var cy=0;
	var index=-1;
	var fieldcode="";
	var fieldtext="";
	var FieldId=0;
	var DataIdentifier=0;
	var color='rgba(200,200,200,1.0)';
	var next=null;
	var previous=null;
	
	that.SetFieldCode=function(_code) {fieldcode=_code;}
	that.GetFieldCode=function() {return fieldcode;}
	that.SetFieldText=function(_text) {fieldtext=_text;}
	that.GetFieldText=function() {return fieldtext;}
	return that;
};


define(["jquery", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas"],
    function ($, DQX, DocEl, Msg, FramePanel, FrameCanvas) 
	{
    var MFieldBar={};
	MFieldBar.FieldBar=function(_iparent)
	{
		var that={};
		that.iparent=_iparent;
		that.scrolllist=null;
		that.dragSep=-1;
		that.dragOffset=0;
		that.colSel=0;
		that.colSorted=0;
		that.reverse=false;
		that.headerColumns=[];
		that.GetHeight=function()
		{
			return 26;
		}
		that.IsPresent=function()
		{
			if (that.headerColumns.length>0) return true;
			return false;
		}
		that.Reset=function()
		{
			that.headerColumns=[];
		}
		that.AddColumn=function(idd,code,name,width)
		{
			info=new MFieldBarInfo();
			info.cx=width;
			info.fieldtext=name;
			info.fieldcode=code;
			var last=that.headerColumns.length-1;
			if (last>=0) info.xp=that.headerColumns[last].xp+that.headerColumns[last].cx;else info.xp=0;
			that.headerColumns.push(info);
		}
		that.GetHeigth=function()
		{
			return 30;
		}
		that.GetFieldCount=function()
		{
			return that.headerColumns.length;
		}
		that.GetField=function(fieldnr)
		{
			if (fieldnr>=0 && fieldnr<that.headerColumns.length) return that.headerColumns[fieldnr];
			return null;
		}
		that.GetFieldSize=function(fieldnr)
		{
			if (fieldnr>=0 && fieldnr<that.headerColumns.length) 
			{
				return that.headerColumns[fieldnr].cx;
			}
			return 0;
		}
		that.GetFieldXp=function(fieldnr)
		{
			if (fieldnr>=0 && fieldnr<that.headerColumns.length) return that.headerColumns[fieldnr].xp;
			return 0;
		}
		that.GetFullWidth=function()
		{
			var last=that.headerColumns.length-1;
			if (last>=0) return that.headerColumns[last].xp+that.headerColumns[last].cx;
			return 0;
		}
		that.render=function () 
		{
			var ctx=this.iparent.getMyCanvasElement('fieldbar').getContext("2d");;
			var lx=this.iparent.getMyCanvasElement('fieldbar').width;
			var ly=this.iparent.getMyCanvasElement('fieldbar').height;
			ctx.strokeStyle='rgba(170,170,170,1.0)';
			ctx.fillStyle='rgb(255,255,255)';
			ctx.fillRect(0,0,lx,ly);
					
			ctx.lineWidth=1;
			var px=Math.floor(-this.iparent.GetHScrollPos());
			ctx.font="14px corbel";
			for (var colnr=0;colnr<that.headerColumns.length;colnr++) 
			{
				ctx.fillStyle='rgb(255,255,255)';
				ctx.fillRect(px,0,that.headerColumns[colnr].cx,ly);

				ctx.fillStyle='rgb(0,0,0)';
				if (that.headerColumns[colnr].fieldtext!="") ctx.fillText(that.headerColumns[colnr].fieldtext,px+10,that.GetHeigth()/2+2);else ctx.fillText(that.headerColumns[colnr].fieldcode,px+10,that.GetHeigth()/2+2);
				
				ctx.fillStyle='rgb(255,255,255)';
				ctx.fillRect(px+that.headerColumns[colnr].cx-15,0,15,ly);

				ctx.fillStyle='rgb(200,200,200)';
				if (that.colSel==colnr) ctx.fillStyle='rgb(100,100,100)';
				if (that.colSorted==colnr) ctx.fillStyle='rgb(255,0,255)';
				if (that.reverse && that.colSorted==colnr) for (var i=0;i<5;i++) ctx.fillRect(px+that.headerColumns[colnr].cx-15+i,5+5-i,10-2*i,1);
				else for (var i=0;i<5;i++) ctx.fillRect(px+that.headerColumns[colnr].cx-15+i,5+i,10-2*i,1);
				
				ctx.strokeStyle='rgba(170,170,170,1.0)';
				if (that.colSel==colnr) ctx.strokeStyle='rgb(100,100,100)';
				ctx.beginPath();
				ctx.rect(px+2.5,0,that.headerColumns[colnr].cx-6,ly);
				ctx.closePath();
				ctx.stroke();


				that.headerColumns[colnr].xp=px;
				px+=that.headerColumns[colnr].cx;
			}
		}
		
		that.MouseMove=function(e)
		{
			var e=window.event || _e;
			e.preventDefault();
			e.stopPropagation();
//			var posx=e.offsetX;
//			var posy=e.offsetY;
			var posx=e.offsetX==undefined?e.originalEvent.layerX:e.offsetX;
			var posy=e.offsetY==undefined?e.originalEvent.layerY:e.offsetY;
			if (that.dragSep!=-1)
			{
				that.headerColumns[that.dragSep].cx=Math.max(posx-that.headerColumns[that.dragSep].xp,4);
				var px=-this.iparent.GetHScrollPos();
				for (var colnr=0;colnr<that.headerColumns.length;colnr++) 
				{
					that.headerColumns[colnr].xp=px;
					px+=that.headerColumns[colnr].cx;
				}
				that.render();
				that.iparent.FieldBarMoved();
				return;
			}
		}
		that._MouseMove=function(e)
		{
			if (that.dragSep!=-1) return;
			var px=-this.iparent.GetHScrollPos();
//			var posx=e.offsetX;
			var posx=e.offsetX==undefined?e.originalEvent.layerX:e.offsetX;
			var sizecursor=false;
			for (var colnr = 0; colnr < that.headerColumns.length; colnr++) 
			{
				px+=that.headerColumns[colnr].cx;
				if (posx>=px-2 && posx<px+2) 
				{
					e.target.style.cursor = 'col-resize';
					sizecursor=true;
				}
			}
			if (sizecursor==false) e.target.style.cursor = 'default';
		}
		that.MouseDown=function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			$(document).on('mousemove',$.proxy(that.MouseMove,that));
			$(document).on('mouseup',$.proxy(that.MouseUp,that));
//			var posx=e.offsetX;
//			var posy=e.offsetY;
			var posx=e.offsetX==undefined?e.originalEvent.layerX:e.offsetX;
			var posy=e.offsetY==undefined?e.originalEvent.layerY:e.offsetY;
			var px=-this.iparent.GetHScrollPos();
			for (var colnr=0;colnr<that.headerColumns.length;colnr++) 
			{
				if (posx>=px && posx<px+that.headerColumns[colnr].cx) that.colSel=colnr;
				px+=that.headerColumns[colnr].cx;
			}
			var px=-this.iparent.GetHScrollPos();
			for (var colnr = 0; colnr < that.headerColumns.length; colnr++) 
			{
				px+=that.headerColumns[colnr].cx;
				if (posx>=px-2 && posx<px+2)
				{
					e.target.style.cursor = 'col-resize';
					that.dragSep=colnr;
					that.dragOffset=posx-px;
					$(that.iparent.getMyCanvasElement('fieldbar')).on('mousemove', $.proxy(that.MouseMove,that));
				}
				if (posx>=px-17 && posx<px-2)
				{
					if (that.colSorted==colnr)
					{
						that.reverse=!that.reverse;
						this.iparent.SortColumn(that.colSorted,that.reverse);
					}
					else
					{
						that.colSorted=colnr;
						that.reverse=false;
						this.iparent.SortColumn(that.colSorted,that.reverse);
					}
				}
			}
		}
		that.MouseUp=function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			that.press=0;
			that.slide=0;
			that.dragSep=-1;
			$(document).off('mousemove', that.MouseMove);
			$(document).off('mouseup',that.MouseUp);
			that.render();
		}

		that.MouseOver=function(e)
		{
//			this.focus=true;
//			this.render();
			$(that.iparent.getMyCanvasElement('fieldbar')).on('mousemove', $.proxy(that._MouseMove,that));
		}
		that.MouseOut=function(e)
		{
			if (that.dragsep!=-1) return;
			e.target.style.cursor = 'default';
			$(that.iparent.getMyCanvasElement('fieldbar')).off('mousemove',that._MouseMove);
//			$(document).off('mousemove',this.MouseMove);
//			this.focus=false;
		}

		that.Invalidate=function()
		{
			that.render();
		}
		that.GetInfoFieldCnt=function()
		{
			return that.headerColumns.length;
		}
		that.GetHScrollPos=function()
		{
			if (that.scrolllist!=null) return that.scrolllist.GetHScrollPos();
			return 0;
		}
		that.ConnectScrollList=function(scrolllist)
		{
			that.scrolllist=scrolllist;
		}
		return that;
	}
	return MFieldBar;
})

