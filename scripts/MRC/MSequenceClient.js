

MSeqBlockChannel=function(_seqchannel,linkobject)//block is een type AlignBlock
{
	var that=this;
	that.linkobject=linkobject;//features, sequence search,variatiuon list...
	that.ChannelItemIndex=[];
	that.seqchannel=_seqchannel;
	that.GetChannelItemIndexCnt=function()
	{
		return that.ChannelItemIndex.length;
	}
	that.ResetItems=function()
	{
		that.ChannelItemIndex.length=0;
	}
	that.AddChannelItemIndex=function(_nr)
	{
		that.ChannelItemIndex.push(_nr);
	}
	that.GetChannelItemIndex=function(nr)
	{
		if (nr>=0 && nr<that.ChannelItemIndex.length) return that.ChannelItemIndex[nr];
		return -1;
	}
	that.GetItem=function(nr)
	{
		if (nr>=0 && nr<that.ChannelItemIndex.length) return that.linkobject[that.ChannelItemIndex[nr]];
	}
	that.GetSeqChannel=function()
	{
		return that.seqchannel;
	}
	that.HasItem=function(_nr)
	{
		for (var i=0;i<that.ChannelItemIndex.length;i++) if (that.ChannelItemIndex[i]==_nr) return true;
		return false;
	}
	that.GetChannelAccession=function()
	{	
		return that.seqchannel.GetChannelAccession();
	}
	return that;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MSeqChannel=function(_channel)
{
	var that=this;
	that.offset=0;
	that.channel=_channel;
	that.size=10;
	that.location=+1;
	that.color="rgba(240,240,240,0.5)";
	that.curve=[];
	that.GetChannel=function()
	{
		return that.channel;
	}
	that.GetView=function()
	{
		return that.channel.view;
	}
	that.GetType=function()
	{
		return that.channel.GetType();
	}
	that.GetColor=function()
	{
		return that.channel.color;
	}
	that.GetChannelAccession=function()
	{
		return that.channel.accession;
	}
	
	that.SetOffset=function(ps)
	{
		that.offset=ps;
	}
	
	that.GetOffset=function()
	{
		return that.offset;
	}
	that.GetBackColor=function()
	{
		return that.color;
	}
	that.SetBackColor=function(cl)
	{
		that.color=cl;
	}
	that.SetSize=function(sz)
	{
		that.size=sz;
	}
	that.GetSize=function()
	{
		if (that.channel.view) return that.size;else return 0;
	}
	that.SetLocation=function(how)//+1 of -1
	{
		that.location=how;
	}
	that.GetLocation=function()
	{
		return that.location;
	}
	return that;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------

Tcdsblock=function()
{
	var that=this;
	that.cds=[];
	that.add_cds=function(nr)
	{
		for (var i=0;i<that.cds.length;i++) if (that.cds[i]==nr) return;
		that.cds.push(nr);
	}

	that.reset=function()
	{
		that.cds=[];
	}
	return that;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------

Talignblock=function()
{
	var that=this;
	that.startbasegaps=-1;
	that.endbasegaps=-1;
	that.startbase=-1;
	that.endbase=-1;
	that.gap=0;
	that.cluster=-1;
	that.cds=[];
	that.invert=false;
	that.entry=null;
	that.SeqBlockChannels=[];


	that.GetLength=function()
	{
		return that.endbase-that.startbase;
	}

	that.GetEntry=function()
	{
		return that.entry;
	}
	that.GetAccession=function()
	{
		if (that.entry!=null) return that.entry.GetAccession();
		return "";
	}
	that.GetColor=function()
	{
		if (that.entry!=null) return that.entry.color;
		return "rgb(0,0,0)";
	}

	that.LinkSeqChannel=function(seqchannel)
	{
		var channel=seqchannel.channel;
		var entrytable=channel.GetEntryTable(that.entry.GetAccession());
		if (entrytable==null) return null;
/*		var k=0;
		var ll=channel.GetItemCnt(entrytable);
		while (ll--)
		{
			if (channel.GetItemStart(ll,entrytable)>that.endbase) continue;
			if (channel.GetItemStop(ll,entrytable)+1<that.startbase) continue;
			k++;
			break;
		}
		if (k==0) return null;
		var seqblockchannel=that.FindSeqblockChannel(seqchannel);
		return null;
		if (seqblockchannel==null) 
		{
			seqblockchannel=new MSeqBlockChannel(this,seqchannel,entrytable);
			that.SeqBlockChannels.push(seqblockchannel);
		}
		seqblockchannel.ResetItems();*/
		var	seqblockchannel=new MSeqBlockChannel(seqchannel,entrytable);
		var ll=channel.GetItemCnt(entrytable);
		while (ll--)
		{
			if (channel.GetItemStart(ll,entrytable)>that.endbase) continue;
			if (channel.GetItemStop(ll,entrytable)+1<that.startbase) continue;
			seqblockchannel.AddChannelItemIndex(ll);
		}
		if (seqblockchannel.GetChannelItemIndexCnt()>0) that.SeqBlockChannels.push(seqblockchannel);
		return seqblockchannel;
	}
	
	that.FindSeqblockChannel=function(seqchannel)
	{
		for (var i=0;i<that.SeqBlockChannels.length;i++)
		{
			if (that.SeqBlockChannels[i].GetChannelAccession()==seqchannel.GetChannelAccession()) return that.SeqBlockChannels[i];
		}
		return null;
	}
	
	that.HasChannelItem=function(channel,index)
	{
		for (var i=0;i<that.SeqBlockChannels.length;i++)
		{
			if (channel!=that.SeqBlockChannels[i].channel) continue;
			if (that.SeqBlockChannels[i].HasItem(index)) return true;
		}
		return false;
	}

	that.GetSeqBlockChannelCnt=function()
	{
		return that.SeqBlockChannels.length;
	}
	that.GetSeqBlockChannel=function(nr)
	{
		if (nr>=0 && nr<that.SeqBlockChannels.length) return that.SeqBlockChannels[nr];
		return null;
	}

	that.ContigToEntryPosition=function(ps)//positie in contig coordinaten, converteert naar entry positie
	{
		var k=(ps-that.startbasegaps)+that.startbase;
		if (that.invert==true) k=(that.endbase-that.startbase)-(ps-that.startbasegaps)+that.startbase;
		if (k<that.startbase) k=that.startbase;
		if (k>that.endbase) k=that.endbase;
		return k;
	}

	that.EntryToContigPosition=function(ps)//positie in entry coordinaten, converteert naar contig positie
	{
		var k=(ps-that.startbase)+that.startbasegaps;
		if (that.invert==true) k=(that.endbase-that.startbase)-(ps-that.startbase)+that.startbasegaps;
		if (k<that.startbasegaps) k=that.startbasegaps;
		if (k>that.endbasegaps) k=that.endbasegaps;
		return k;
	}
	that.GetFtsFromEntryPos=function(pos,cds,channel)//pos is een entry positie, cds is een array, channel bevat de features
	{
		cds.length=0;
		var k=pos-that.startbase;
		for (var i=0;i<channel.featuretable.ftrs.length;i++)
		{
			var _fts=channel.featuretable.ftrs[i];
			if (_fts.accession!=that.entry.GetAccession()) continue;
			if (pos>=_fts.start && pos<_fts.stop) cds.push(i);
		}
	}
	return that;
}



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MSequenceClient=function()
{
	var that=this;
	that.blocks=[];
	that.SeqChannels=[];
	that.accessions=[];
	that.entries=[];
	that.plotoffset=0;
	that.label="hohohoh";

	that.bitsize=10000;
	that.bitdirect=new BitView(new ArrayBuffer(that.bitsize));
	that.bitinvert=new BitView(new ArrayBuffer(that.bitsize));

	that.ResetBlocks=function()
	{
		that.blocks.length=0;
	}

	that.SortBlocksStartbase=function()
	{
		if (that.blocks.length<2) return;
		for (var gap=that.blocks.length/2;gap>0;gap/=2)
		{
			for (var i=gap;i<that.blocks.length;i++)
			{
				var block1=that.blocks[i];
				var block2=that.blocks[i+gap];
				for (var j=i-gap;j>=0 && block1.startbase>block2.startbase;j-=gap) 
				{
					var temp=block1;
					block1=block2;
					block2=temp;
				}
			}
		}
	}

	that.SortBlocksStartbasegaps=function()
	{
		if (that.blocks.length<2) return;
		for (var gap=that.blocks.length/2;gap>0;gap/=2)
		{
			for (var i=gap;i<that.blocks.length;i++)
			{
				var block1=that.blocks[i];
				var block2=that.blocks[i+gap];
				for (var j=i-gap;j>=0 && block1.startbasegaps>block2.startbasegaps;j-=gap) 
				{
					var temp=block1;
					block1=block2;
					block2=temp;
				}
			}
		}
	}


	that.AddBlockFull=function(_entryclient,invert,concatenateGap)
	{
		var block=new Talignblock();
		block.entry=_entryclient;
		block.startbasegaps=that.GetLength()+concatenateGap;
		block.endbasegaps=block.startbasegaps+_entryclient.GetLength();
		block.startbase=0;
		block.endbase=_entryclient.GetLength();
		block.invert=invert;
		that.blocks.push(block);
		that.UpdateAccessions();
		return that.blocks[that.blocks.length-1];
	}

	that.rainbow=function(numOfSteps, step)
	{
		// This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
		// Adam Cole, 2011-Sept-14
		// HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
		var r, g, b;
		var h = step / numOfSteps;
		var i = ~~(h * 6);
		var f = h * 6 - i;
		var q = 1 - f;
		switch(i % 6){
			case 0: r = 1, g = f, b = 0; break;
			case 1: r = q, g = 1, b = 0; break;
			case 2: r = 0, g = 1, b = f; break;
			case 3: r = 0, g = q, b = 1; break;
			case 4: r = f, g = 0, b = 1; break;
			case 5: r = 1, g = 0, b = q; break;
		}
		var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
		return (c);
	}
	
	that.AddBlock=function(_entryclient,startbasegaps,startbase,endbase,invert,superstretch,check_overlap)
	{
		if (startbase>=endbase) return null;
		if (startbase>_entryclient.GetLength()) return null;
		if (endbase>_entryclient.GetLength()) endbase=_entryclient.GetLength();
		var endbasegaps=0;
		if (check_overlap)
		{
			endbasegaps=startbasegaps+endbase-startbase;
			for (var i=0;i<that.blocks.length;i++)
			{
				var block=that.blocks[i];
				var b1=block.startbasegaps;
				var b2=block.endbasegaps;
				if (startbasegaps<b1 && endbasegaps>b2) return null;
				if (startbasegaps>=b1 && startbasegaps<b2) 
				{
					var k=b2-startbasegaps;
					if (invert==false) {startbasegaps+=k;startbase+=k;}
					else {startbasegaps+=k;endbase-=k;}
				}
				if (endbasegaps>=b1 && endbasegaps<b2)
				{
					var k=endbasegaps-b1;
					if (invert==false) endbase-=k;
					else startbase+=k;
				}
			}
			if (startbase>=endbase) return null;
		}
		endbasegaps=startbasegaps+endbase-startbase;
		var block=new Talignblock();
		block.entry=_entryclient;
		block.startbasegaps=startbasegaps;
		block.endbasegaps=endbasegaps;
		block.startbase=startbase;
		block.endbase=endbase;
		block.invert=invert;
		block.cluster=superstretch;
		that.blocks.push(block);
		return that.blocks[that.blocks.length-1];
	}
	
	that.UpdateBlock=function(block,_startbasegaps,_startbase,_endbase)
	{
		block.startbasegaps=Math.min(block.startbasegaps,_startbasegaps);
		block.startbase=Math.min(block.startbase,_startbase);
		block.endbase=Math.max(block.endbase,_endbase);
		block.endbasegaps=block.startbasegaps+block.endbase-block.startbase;
	}

	that.LinkAllChannels=function()
	{
		for (var j=0;j<that.SeqChannels.length;j++)	that.LinkSeqChannel(that.SeqChannels[j]);
	}
	
	that.ResetBlocks=function()
	{
		that.blocks.length=0;
	}

	that.GetBlockIndex=function(nr)
	{
		return that.blocks[nr];
	}

	that.GetBlockCnt=function()
	{
		return that.blocks.length;
	}

	that.GetLength=function()
	{
		if (that.blocks.length<1) return 0;
		return that.blocks[that.blocks.length-1].endbasegaps;
	}


	//alles met sequentie data verkrijgen
	that.GetCloseBlock=function(ps)
	{
		var len=that.blocks.length;
		var p1=0;
		var p2=len-1;
		var cnt=0;
		var floor=Math.floor;
		while (cnt<len)
		{
			var p=floor((p1+p2)/2);
			var block=that.blocks[p];
			if (block.startbasegaps>ps) p2=p;else p1=p;
			if (p1==p2 || p1==p2-1)
			{
				block=that.blocks[p1];
				if (ps>=block.startbasegaps && ps<block.endbasegaps) return p1;
				block=that.blocks[p2];
				if (ps>=block.startbasegaps && ps<block.endbasegaps) return p2;
				return p2;
			}
			cnt++;
		}
		return -1;
	}

	that.GetBlock=function(ps,block)//geeft sequence block weer die de positie (in alignment coordinaten) overspant
	{
		if (block!=null && ps>=block.startbasegaps && ps<block.endbasegaps) return block;
		var len=that.blocks.length;
		var p1=0;var p2=len-1;var cnt=0;
		var floor=Math.floor;
		while (cnt<len)
		{
			var p=floor((p1+p2)/2);
			var block=that.blocks[p];
			if (block.startbasegaps>ps) p2=p;else p1=p;
			if (p1==p2 || p1==p2-1)
			{
				block=that.blocks[p1];
				if (ps>=block.startbasegaps && ps<block.endbasegaps) return block;
				block=that.blocks[p2];
				if (ps>=block.startbasegaps && ps<block.endbasegaps) return block;
				return null;
			}
			cnt++;
		}
		return null;
	}
	that.find_current_channelitem=function(channel,selection)//channel items zijn locaties met een start en stop positie op de originele sequentie
	{
		selection.block=that.GetBlock(selection.offset,selection.block);
		if (selection.block==null) return;
		return that.get_channelitem_from_aligned_pos(channel,selection);
	}
	that.find_next_channelitem=function(channel,selection)//channel items zijn locaties met een start en stop positie op de originele sequentie
	{
		selection.block=that.GetBlock(selection.offset,selection.block);
		if (selection.block==null) return;
		var olditem=that.get_channelitem_from_aligned_pos(channel,selection);
		var lastitem=-1;
		if (selection.block.invert==false) lastitem=that.mostright_channelitem(selection.block,channel);else lastitem=that.mostleft_channelitem(selection.block,channel);
		var newitem=-1;var round=0;
		if (olditem==lastitem) selection.offset=selection.block.endbasegaps;
//		als positie over de uiterste feature staat, skip naar einde
		var lastpos=-1;
		if (lastitem!=-1) lastpos=channel.GetFullItemStop(lastitem);
		lastpos=selection.block.EntryToContigPosition(lastpos);
		if (lastpos!=-1 && selection.offset>lastpos) selection.offset=selection.block.endbasegaps;
		for (;;)
		{
			selection.block=that.GetBlock(selection.offset,selection.block);
			if (selection.block!=null)
			{
				if (selection.block.invert==false) newitem=that.right_channelitem_from_aligned_pos(selection.block,channel,selection.offset,olditem);
				else newitem=that.left_channelitem_from_aligned_pos(selection.block,channel,selection.offset,olditem);
				if (newitem!=olditem && newitem!=-1) return newitem;
			}
			selection.offset++;
			if (selection.offset>=that.GetLength()) {selection.offset=0;round++;}
			if (selection.offset>=that.GetLength() && round>0) return newitem;
			if (round>1) return newitem;
		}
		return newitem;
	}
	that.find_previous_channelitem=function(channel,selection)//channel items zijn locaties met een start en stop positie op de originele sequentie
	{
		selection.block=that.GetBlock(selection.offset,selection.block);
		if (selection.block==null) return;
		var olditem=that.get_channelitem_from_aligned_pos(channel,selection);
		var lastitem=-1;
		if (selection.block.invert==false) lastitem=that.mostleft_channelitem(selection.block,channel);else lastitem=that.mostright_channelitem(selection.block,channel);
		var newitem=-1;var round=0;
		if (olditem==lastitem) selection.offset=selection.block.startbasegaps;
//		als positie over de uiterste feature staat, skip naar einde
		var lastpos=-1;
		if (lastitem!=-1) lastpos=channel.GetFullItemStop(lastitem);
		lastpos=selection.block.EntryToContigPosition(lastpos);
		if (lastpos!=-1 && selection.offset<lastpos) selection.offset=selection.block.startbasegaps;
		for (;;)
		{
			selection.block=that.GetBlock(selection.offset,selection.block);
			if (selection.block!=null)
			{
				if (selection.block.invert==false) newitem=that.left_channelitem_from_aligned_pos(selection.block,channel,selection.offset,olditem);
				else newitem=that.right_channelitem_from_aligned_pos(selection.block,channel,selection.offset,olditem);
				if (newitem!=olditem && newitem!=-1) return newitem;
			}
			selection.offset--;
			if (selection.offset<0) {selection.offset=that.GetLength()-1;round++;}
			if (selection.offset<0 && round>0) return newitem;
			if (round>1) return newitem;
		}
		return newitem;
	}
	that.get_channelitem_from_aligned_pos=function(channel,selection)
	{
		var ps=selection.offset;
		if (selection.block==null) selection.block=that.GetBlock(null,ps);
		if (selection.block==null) return -1;
		var optimaldist=Number.MAX_VALUE;
		var optimalnr=-1;
		var diffbase=selection.block.startbasegaps-selection.block.startbase;
		ps=ps-diffbase;
		if (selection.block.invert==true) ps=selection.block.endbase-ps+selection.block.startbase+1;
		var p1=0;
		var p2=0;
		var dist=0;
		for (var i=0;i<channel.GetFullItemCnt();i++)
		{
			if (channel.GetFullItemAccession(i)!=selection.block.GetAccession()) continue; 
			p1=Math.max(channel.GetFullItemStart(i),selection.block.startbase);
			p2=Math.min(channel.GetFullItemStop(i)+1,selection.block.endbase);
			if (p1>ps) continue;
			if (p2<ps) continue;
			if (p1<=ps && p2>=ps) 
			{
				dist=Math.abs(p1-ps)+Math.abs(p2-ps);
				if (dist<optimaldist) {optimaldist=dist;optimalnr=i;}
			}
		}
		return optimalnr;
	}	
	that.right_channelitem_from_aligned_pos=function(block,channel,ps,olditem)
	{
		if (block==null) return -1;
		var optimaldist=Number.MAX_VALUE;
		var optimalnr=-1;
		var diffbase=block.startbasegaps-block.startbase;
		ps=ps-diffbase;
		if (block.invert==true) ps=block.endbase-ps+block.startbase+1;
		var p1=0;var p2=0;var dist=0;
		for (var i=0;i<channel.GetFullItemCnt();i++)
		{
			if (channel.GetFullItemAccession(i)!=block.GetAccession()) continue; 
			p1=Math.max(channel.GetFullItemStart(i),block.startbase);
			if (p1<=ps) continue;
			dist=Math.abs(p1-ps);
			if (olditem!=-1) 
			{
				if (dist<optimaldist && olditem!=i) {optimaldist=dist;optimalnr=i;}
			}
			else if (dist<optimaldist) {optimaldist=dist;optimalnr=i;}
			if (dist>5*optimaldist) return optimalnr;
		}
		return optimalnr;
	}	
	that.left_channelitem_from_aligned_pos=function(block,channel,ps,olditem)
	{
		if (block==null) return -1;
		var optimaldist=Number.MAX_VALUE;
		var optimalnr=-1;
		var diffbase=block.startbasegaps-block.startbase;
		ps=ps-diffbase;
		if (block.invert==true) ps=block.endbase-ps+block.startbase+1;
		var p1=0;var p2=0;var dist=0;
		for (var i=0;i<channel.GetFullItemCnt();i++)
		{
			if (channel.GetFullItemAccession(i)!=block.GetAccession()) continue;
			p2=Math.min(channel.GetFullItemStop(i)+1,block.endbase);
			if (p2>=ps) continue;
			dist=Math.abs(p2-ps);
			if (olditem!=-1) 
			{
				if (dist<optimaldist && olditem!=i) {optimaldist=dist;optimalnr=i;}
			}
			else if (dist<optimaldist) {optimaldist=dist;optimalnr=i;}
			if (dist>5*optimaldist) return optimalnr;
		}
		return optimalnr;
	}	
	that.mostleft_channelitem=function(block,channel)
	{
		if (block==null) return -1;
		var p1=0;var p2=Number.MAX_VALUE;var lastnr=-1;
		for (var i=0;i<channel.GetFullItemCnt();i++)
		{
			if (channel.GetFullItemAccession(i)!=block.GetAccession()) continue; 
			p1=Math.max(channel.GetFullItemStart(i),block.startbase);
			if (p1<=p2) {p2=p1;lastnr=i;}
		}
		return lastnr;
	}	
	that.mostright_channelitem=function(block,channel)
	{
		if (block==null) return -1;
		var p1=0;var p2=0;var lastnr=-1;
		for (var i=0;i<channel.GetFullItemCnt();i++)
		{
			if (channel.GetFullItemAccession(i)!=block.GetAccession()) continue; 
			p1=Math.min(channel.GetFullItemStop(i)+1,block.endbase);
			if (p1>=p2) {p2=p1;lastnr=i;}
		}
		return lastnr;
	}
	
	that.GetAlignStart=function()
	{
		if (that.blocks.length>0) return that.blocks[0].startbasegaps;
		return 0;
	}
	that.GetAlignEnd=function()
	{
		if (that.blocks.length>0) return that.blocks[that.blocks.length-1].endbasegaps;
		return 0;
	}
	that.GetBase=function(ps,alignblock)
	{
		if (alignblock==null) alignblock=that.GetBlock(ps,null);
		if (alignblock==null) return ' ';
		if (alignblock.startbasegaps>ps || alignblock.endbasegaps<ps) alignblock=that.GetBlock(ps,null);
		if (alignblock==null) return ' ';
		return alignblock.GetBase(ps);
	}

	that.GetSeqChannelCount=function()
	{
		return that.SeqChannels.length;
	}

	that.GetSeqChannel=function(nr)
	{
		if (nr<0 || nr>=that.SeqChannels.length) return null;
		return that.SeqChannels[nr];
	}

	that.GetUpperSeqChannelSpace=function()
	{
		var size=0;
		for (var i=0;i<that.SeqChannels.length;i++)
		{
			if (that.SeqChannels[i].GetLocation()!=-1) continue;
			size+=2*that.SeqChannels[i].GetSize()
		}
		return size;
	}
	that.GetLowerSeqChannelSpace=function()
	{
		var size=0;
		for (var i=0;i<that.SeqChannels.length;i++)
		{
			if (that.SeqChannels[i].GetLocation()!=+1) continue;
			size+=2*that.SeqChannels[i].GetSize()
		}
		return size;
	}
	that.UpdateSeqChannelOffsets=function()
	{
		var size=0;
		for (var i=0;i<that.SeqChannels.length;i++)
		{
			if (that.SeqChannels[i].GetLocation()!=+1) continue;
			that.SeqChannels[i].offset=size+that.SeqChannels[i].GetSize();
			size=size+2*that.SeqChannels[i].GetSize();
		}
		size=0;
		for (var i=0;i<that.SeqChannels.length;i++)
		{
			if (that.SeqChannels[i].GetLocation()!=-1) continue;
			that.SeqChannels[i].offset=size-that.SeqChannels[i].GetSize();
			size=size-2*that.SeqChannels[i].GetSize();
		}
	}
	that.GetSeqChannelCount=function()
	{
		return that.SeqChannels.length;
	}
	that.GetSeqChannel=function(index)
	{
		if (index<0 || index>=that.SeqChannels.length) return null;
		return that.SeqChannels[index];
	}
	that.AddChannel=function(channel)
	{
		if (channel==null) return null;
		var seqchannel=new MSeqChannel(channel);
		that.SeqChannels.push(seqchannel);
		return seqchannel;
	}

/*	that.LinkChannel=function(channel)
	{
		var seqchannel=that.FindSeqChannel(channel.GetAccession());//bestaat de seqchannel reeds?
		if (seqchannel==null)// indien niet, aanmaken
		{
			seqchannel=new MSeqChannel(channel);
			that.SeqChannels.push(seqchannel);
		}
		for (var i=0;i<that.blocks.length;i++) that.blocks[i].LinkSeqChannel(seqchannel);
		return seqchannel;
	}*/

	that.LinkChannel=function(channel)
	{
		var seqchannel=that.FindSeqChannel(channel.GetAccession());//bestaat de seqchannel reeds?
		if (seqchannel==null)// indien niet, aanmaken
		{
			seqchannel=new MSeqChannel(channel);
			that.SeqChannels.push(seqchannel);
		}
		var found=[];
		var p1=0,p2=0,lastblock=-1;
		for (var i=0;i<that.accessions.length;i++)
		{
			var entrytable=channel.GetEntryTable(that.accessions[i]);
			if (entrytable==null) continue;
			var entry=that.GetEntry(that.accessions[i]);
			if (entry==null) continue;
			var ll=channel.GetItemCnt(entrytable);
			if (ll==0) continue;
			var plot=[];
			var sqll=entry.GetLength();
			var buffer=new ArrayBuffer(4*sqll);
			var int32View=new Int32Array(buffer);
			for (var j=0;j<sqll;j++) int32View[j]=-1;
			for (var j=0;j<that.blocks.length;j++)
			{
				var block=that.blocks[j];
				if (block.entry!=entry) continue;
				for (var k=block.startbase;k<block.endbase;k++) int32View[k]=j;
			}
			while (ll--)
			{
				found.length=0;
				lastblock=-2;
				p1=channel.GetItemStart(ll,entrytable);
				p2=channel.GetItemStop(ll,entrytable);
				for (var j=p1;j<p2;j++) if (int32View[j]!=-1 && lastblock!=int32View[j]) {found.push(int32View[j]);lastblock=int32View[j];}
				if (found.length>0)
				{
					for (var j=0;j<found.length;j++) 
					{
						var block=that.blocks[found[j]];
						var seqblockchannel=block.FindSeqblockChannel(seqchannel);
						if (seqblockchannel==null) 
						{
							seqblockchannel=new MSeqBlockChannel(seqchannel,entrytable);
							block.SeqBlockChannels.push(seqblockchannel);
						}
						seqblockchannel.AddChannelItemIndex(ll);
					}
				}
			}
		}
		return seqchannel;
	}
	that.LinkSeqChannel=function(seqchannel)
	{
		for (var i=0;i<that.blocks.length;i++) that.blocks[i].LinkSeqChannel(seqchannel);
	}

	that.ToggleChannelOnOff=function(accession)
	{
		var seqchannel=that.FindSeqChannel(accession);
		if (seqchannel==null) return;
		if (seqchannel.Show==true) seqchannel.Show=false;else seqchannel.Show=true;
	}

	that.FindSeqChannel=function(accession)
	{
		for (var i=0;i<that.SeqChannels.length;i++)
		{
			if (that.SeqChannels[i].GetChannelAccession()==accession) return that.SeqChannels[i];
		}
		return null;
	}
	that.FindChannelType=function(type)
	{
		for (var i=0;i<that.SeqChannels.length;i++)
		{
			if (that.SeqChannels[i].GetType()==type) return that.SeqChannels[i];
		}
		return null;
	}

	that.GetChannelOffset=function(name)
	{
		var seqchannel=that.FindSeqChannel(name);
		if (seqchannel!=null) return seqchannel.GetOffset();
		return 0;
	}
	that.UpdateAccessions=function()
	{
		for (var i=0;i<that.blocks.length;i++) 
		{
			if (that.accessions.indexOf(that.blocks[i].GetAccession())<0) 
			{
				that.accessions.push(that.blocks[i].GetAccession());
				that.entries.push(that.blocks[i].entry);
			}
		}
		for (var i=0;i<that.accessions.length;i++)
		{
			var color=that.rainbow(that.accessions.length,i);
			for (var j=0;j<that.blocks.length;j++)
			{
				if (that.blocks[j].GetAccession()==that.accessions[i]) that.blocks[j].entry.color=color;
			}
		}
	}
	that.GetAccessions=function()
	{
		return that.accessions;
	}
	that.GetEntry=function(accession)
	{
		for (var i=0;i<that.entries.length;i++) if (that.entries[i].GetAccession()==accession) return that.entries[i];
		return null;
	}
	that.MapBlocks=function(fulllen)
	{
		for (var i=0;i<that.bitsize;i++) {that.bitinvert.setBit(i,0);that.bitdirect.setBit(i,0);}
		var j=that.GetBlockCnt();
		var block=null;
		var pp1=0,pp2=0,k1=0,k2=0,i=0;
		while (j--)
		{
			block=that.GetBlockIndex(j);
			k1=parseInt(that.bitsize*block.startbasegaps/fulllen);
			k2=parseInt(that.bitsize*block.endbasegaps/fulllen);
			if (block.invert==true) for (i=k1;i<=k2;i++) that.bitinvert.setBit(i,1);else for (i=k1;i<=k2;i++) that.bitdirect.setBit(i,1);
		}
	}
	return that;
}
