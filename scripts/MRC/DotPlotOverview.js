define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/PopupFrame", "DQX/Popup","DQX/FrameCanvas","MRC/MScrollWindow","MRC/MScrollList"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, PopupFrame, Popup, FrameCanvas, MScrollWindow, MScrollList) {
        var DotPlotOverview = {};
		DotPlotOverview.Panel = function (iParentRef,parent) 
		{
            var that=MScrollWindow.Init(iParentRef,true);
			that.parent=parent;
			that.zoom=1.0;
			that.project=null;
			that.cellSelect=-1;
			that.circleRad=100;
			that.overallX=0,that.overallY=0,that.overallSize=0;
			that.SetProject=function(_project)
			{
				that.project=_project;
				that.UpdateFacts(true);
				
			}
			that.setCircularMaps=function(type)
			{
				var dotplot=that.project.dotplot;
				if (dotplot==null) return;
				if (dotplot.circles==false) dotplot.circles=true;else dotplot.circles=false;
				dotplot.generatePictures();
				that.UpdateFacts(true);
//				dotplot.ReLoad();
			}
			that.UpdateFacts=function(update)
			{
				that.calcysizes();
				that.UpdateScroll();
				if (update) that.invalidate();
			}
			that.UpdateScroll=function()
			{
				that.SetHScrollSize(that.overallX);
				that.SetVScrollSize(that.overallY);
				that.ReCalcScrollers();
			}
			that.getswitch=function(nr)
			{
				return nr;
			}
			that.xconvert=function(x) 
			{
				return x*that.zoom; 
			} 
			that.calcysizes=function()
			{
				if (that.project==null) return;
				var ysizes=[];
				var xsizes=[];
				var dotplot=that.project.dotplot;
				if (dotplot==null) return;
				var circles=dotplot.circles;
				var d1=0;
				var d2=20;
				var yy=70;
				lastcluster=0;
				for (var i=0;i<dotplot.matrixsize;i++)
				{
					nr=dotplot.jobs[that.getswitch(0)][that.getswitch(i)];
					matrix=dotplot.getMatrix(nr);
					if (!matrix.skip && that.cellSelect<0) that.cellSelect=nr;
					if (!matrix.skip) if (lastcluster!=matrix.clustnum2) yy+=Math.max(d2*that.zoom,2);
					ysizes.push(yy);
					if (!matrix.skip && circles==false) yy+=~~(that.xconvert(matrix.ylen))+d1;
					if (!matrix.skip && circles==true) yy+=~~(that.xconvert(2*that.circleRad))+d1;
					lastcluster=matrix.clustnum2;
				}
				ysizes.push(yy);//voor de scroller size nog laatste cell erbij voegen
				var xx=10;var _xx=10;
				lastcluster=0;
				for (var i=0;i<dotplot.matrixsize;i++)
				{
					nr=dotplot.jobs[that.getswitch(i)][that.getswitch(dotplot.matrixsize-1)];
					matrix=dotplot.getMatrix(nr);
					if (!matrix.skip) if (lastcluster!=matrix.clustnum1) {xx+=Math.max(d2*that.zoom,2);_xx+=d2;}
					xsizes.push(xx);
					if (!matrix.skip && circles==false) {xx+=~~(that.xconvert(matrix.xlen))+d1;_xx+=matrix.xlen+d1;}
					if (!matrix.skip && circles==true) {xx+=~~(that.xconvert(2*that.circleRad))+d1;_xx+=2*that.circleRad+d1;}
					lastcluster=matrix.clustnum1;
				}
				xsizes.push(xx);//voor de scroller size nog laatste cell erbij voegen
				for (var i=0;i<dotplot.matrixsize;i++)
				{
					for (var j=i;j<dotplot.matrixsize;j++)
					{
						nr=dotplot.jobs[that.getswitch(i)][that.getswitch(j)];
						var _matrix=dotplot.getMatrix(nr);
						if (_matrix==null) continue;
						_matrix.yp=ysizes[j];
						_matrix.xp=xsizes[i];
						if (circles)
						{
							_matrix.xlen=2*that.circleRad;
							_matrix.ylen=2*that.circleRad;
						}
					}
				}
				that.overallY=ysizes[ysizes.length-1];
				that.overallX=xsizes[xsizes.length-1];
				that.overallSize=_xx;
			}
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgba(100,100,100,1)";
				context.fillRect(0,0,that.GetXSize(),that.GetYSize());
				if (that.project==null) return;
				var dotplot=that.project.dotplot;
				context.fillStyle="rgb(0,0,0)";
				if (dotplot.loadStatus<100 && dotplot.loadStatus>0)
				{
					var num=dotplot.loadStatus;num.toFixed(1);
					that.processing="Loading..."+num.toFixed(1)+"%";
					return;
				}
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				for (var i=0;i<dotplot.matrixsize;i++)
				{
					for (var j=i;j<dotplot.matrixsize;j++)
					{
						nr=dotplot.jobs[that.getswitch(i)][that.getswitch(i)];
						var matrix=dotplot.getMatrix(nr);
						if (matrix==null) continue;
						var lll=matrix.ll1;
						nr=dotplot.jobs[that.getswitch(i)][that.getswitch(j)];
						matrix=dotplot.getMatrix(nr);
						if (matrix==null) continue;
						var l1=Math.floor(that.xconvert(matrix.xlen));
						var l2=Math.floor(that.xconvert(matrix.ylen));
						var xp=matrix.xp;
						var yp=matrix.yp;
						var skip=0;
						var _switch=0;
						if (lll!=matrix.ll1) _switch=1;
						if (xp+xoo>that.GetXSize()) skip=1;
						if (yp+yoo>that.GetYSize()) skip=1;
						if (xp+xoo+l1<0) skip=1;
						if (yp+yoo+l2<0) skip=1;
						if (matrix.skip) skip=1;
						if (skip==0)
						{
							if (dotplot.circles==false) context.fillStyle="rgb(110,150,190)";else context.fillStyle="rgb(200,200,200)";
							if (that.cellSelect==nr) context.fillStyle="rgb(110,190,110)";
							context.fillRect(xp+xoo,yp+yoo,Math.max(l1,1),Math.max(l2,1));
							{
								context.beginPath();
								context.strokeStyle="rgba(180,180,180,0.5)";
								context.lineWidth=1;
								context.rect(xp+xoo,yp+yoo,l1,l2);
								context.closePath();
								context.stroke();
							}
							if (matrix.canvas!=null) context.drawImage(matrix.canvas,xp+xoo,yp+yoo,that.zoom*matrix.canvas.width,that.zoom*matrix.canvas.height);
						}
					}
				}
				if (dotplot.loadStatus==100)
				{
					that.processing="";
					context.font = "14px serif";
					context.fillStyle="rgb(200,200,200)";
					context.fillRect(0,0,that.GetXSize(),50);
					context.fillStyle="rgb(220,220,220)";
					context.fillRect(0,50,that.GetXSize(),1);
					context.fillStyle="rgb(100,100,100)";
					context.fillRect(0,0,that.GetXSize(),1);
					context.fillRect(0,0,1,50);
					context.fillRect(that.GetXSize()-2,0,1,50);
					context.fillStyle="rgb(0,0,0)";
					var seq1=that.GetSelectedSeq1();
					var seq2=that.GetSelectedSeq2();
					context.fillText("Organism: "+seq1.organism,10,15);
					context.fillText("Sequence:" +seq1.sequence+", "+seq1.length,10,30);
					context.fillText("Organism: "+seq2.organism,400,15);
					context.fillText("Sequence:" +seq2.sequence+", "+seq2.length,400,30);
				}
			}
			that.post_paint=function()
			{
			}
			
			that.zscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
				var old=that.zoom;
				var minzoom=400.0/that.overallSize;
				if (zscrollpos>=zscrollmax/2)
				{
					var m=zscrollpos-zscrollmax/2;
					that.zoom=that.zoom+0.0001*m;
					if (that.zoom>=2) that.zoom=2.0;
				}
				if (zscrollpos<zscrollmax/2)
				{
					var m=zscrollmax/2-zscrollpos;
					that.zoom=that.zoom-0.0001*m;
					if (that.zoom<=minzoom) that.zoom=minzoom;
				}
				if (old!=that.zoom)
				{
					that.oldzoom=that.zoom;
					that.UpdateFacts(true);
				}
			}
			that.MouseDown=function(posx,posy)  //overwrite
			{
				if (that.project==null) return;
				var oldsel=that.cellSelect;
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				var dotplot=that.project.dotplot;
				for (var i=0;i<dotplot.getMatrixCount();i++)
				{
					var matrix=dotplot.getMatrix(i);
					var l1=~~(that.xconvert(matrix.xlen));
					var l2=~~(that.xconvert(matrix.ylen));
					if (posx>=matrix.xp+xoo && posx<matrix.xp+xoo+l1 && posy>=matrix.yp+yoo && posy<=matrix.yp+yoo+l2) that.cellSelect=i;
				}
				if (oldsel!=that.cellSelect) 
				{
					that.render();
					parent.UpdateControlInfo();
				}
			}
			that.getSelectedCell=function()
			{
				if (that.project==null) return;
				var dotplot=that.project.dotplot;
				if (dotplot!=null && that.cellSelect!=-1) return dotplot.getMatrix(that.cellSelect);
				if (dotplot!=null && that.cellSelect==-1) 
				{
					var nr=0;
					while (nr<dotplot.getMatrixCount()) 
					{
						if (dotplot.getMatrix(nr).skip==false) 
						{
							that.cellSelect=nr;
							return dotplot.getMatrix(nr);
						}
						nr++;
					}
				}
				return null;
			}
			that.GetSelectedSeq1=function()
			{
				var cell=that.getSelectedCell();
				if (cell!=null)	return cell.GetSeq1();
			}
			that.GetSelectedSeq2=function()
			{
				var cell=that.getSelectedCell();
				if (cell!=null)	return cell.GetSeq2();
			}
			return that;
        };
		DotPlotOverview.Circle=function(iParentRef,parent,_seqbox) 
		{
			var that=MScrollWindow.Init(iParentRef,true);
			that.project=null;
			that.elemsel=-1;
			that.SetProject=function(_project)
			{
				that.project=_project;
			}
			that.UpdateFacts=function(update)
			{
				if (update) that.invalidate();
			}
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgba(250,250,250,1)";
				context.fillRect(0,0,that.GetXSize(),that.GetYSize());
				if (that.project==null) return;
				var dotplot=that.project.dotplot;
				if (dotplot.loadStatus<100 && dotplot.loadStatus>0)
				{
					var num=dotplot.loadStatus;num.toFixed(1);
					that.processing="Loading..."+num.toFixed(1)+"%";
					return;
				}
				if (dotplot.loadStatus==100) that.processing="";
				context.fillStyle="rgb(0,0,0)";
				context.drawImage(dotplot.canvas,0,0,dotplot.canvas.width,dotplot.canvas.height);
				var elements=that.project.elements;
				var _element=null;
				if (that.elemsel!=-1) _element=elements[that.elemsel];
				for (var j=0;j<elements.length;j++)
				{
					var element=elements[j];
					if (element==null) continue;
					if (_element==null && element.joincanvas!=null) context.drawImage(element.joincanvas,0,0);
					if (_element!=null && _element==element && element.joincanvas!=null) context.drawImage(element.joincanvas,0,0);
				}
				context.fillStyle="rgba(0,0,0,1)";
				context.fillText(that.elemsel,10,30);
			}
			that.zscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
				if (that.project==null) return;
				var dotplot=that.project.dotplot;
				var context=that._cnvContext;
				var tmpCanvas=dotplot.canvas;
				that.hoek=that.hoek+Math.PI/20;if (that.hoek>2*Math.PI) that.hoek=that.hoek-2*Math.PI;
				tmpCtx=context;
				tmpCtx.save();
				tmpCtx.translate(tmpCanvas.width/2,tmpCanvas.height/2);
				tmpCtx.rotate(that.hoek);
				tmpCtx.drawImage(tmpCanvas,-tmpCanvas.width/2,-tmpCanvas.height/2);
				tmpCtx.restore();
				that.invalidate();
			}
			that.MouseMove=function(posx,posy)
			{
				var dotplot=that.project.dotplot;
				var elements=that.project.elements;
				var d=100;
				var old=that.elemsel;
				that.elemsel=-1;
/*				if (posy>20) that.elemsel=-1;
				else
				{
					for (var i=0;i<elements.length;i++)
					{
						if (posx>=d && posx<d+10) that.elemsel=i;
						d+=10+4;
					}
				}
				if (posx>d) that.elemsel=-1;
				if (that.elemsel!=old) that.render();*/
				var rad=dotplot.canvas.height/2-100;
				var PI=Math.PI;
				var point={x:posx-dotplot.canvas.width/2,y:posy-dotplot.canvas.height/2};
				posx-dotplot.canvas.width/2;
				var bb=2*PI;
				if (point.x*point.x+point.y*point.y<rad*rad+10)
				{
					var hk=that.C_hoek(point);
					for (var i=0;i<elements.length;i++)
					{
						var h1=2*PI*elements[i].cpos/dotplot.cLength;
						var h2=2*PI*(elements[i].cpos+elements[i].length/dotplot.arrayblock)/dotplot.cLength;
						var aa=Math.abs(hk-(h1+h2)/2);
						if (aa<bb) {that.elemsel=i;bb=aa;}
					}
				}
				if (that.elemsel!=old) that.render();
			}
			that.C_hoek=function(point)
			{
				var hk=0;
				if (point.x!=0)
				{
					hk=Math.atan(point.y/point.x);
					if (point.x>0) hk=hk+Math.PI/2;
					if (point.x<0) hk=hk+3*Math.PI/2;
				}
				else 
				{
					if (point.y>0) hk=Math.PI;
					if (point.y<0) hk=0;
				}
				return hk;
			}
			return that;
		}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		DotPlotOverview.BestHits=function(iParentRef,parent) 
		{
            var that=MScrollList.Init(iParentRef);
			that.parent=parent;
			that.item1=[];
			that.item2=[];
			that.setData=function(_data)
			{
				that.data=_data;
				for (var i=0;i<_data.length;i++)
				{
					for (var j=0;j<_data[i].besthits.length;j++)
					{
						that.item1.push(i);
						that.item2.push(j);
					}
				}
				that.SetDataCount(that.item1.length);
				that.render();
			}
			that.GetFieldText=function(rowindex,field)
			{
				var pair=that.data[that.item1[rowindex]];
				var hit=pair.besthits[that.item2[rowindex]];
				if (field.fieldcode==='_index') return rowindex+1;
				if (field.fieldcode==='_code1') return pair.code1;
				if (field.fieldcode==='_code2') return pair.code2;
				if (field.fieldcode==='_xpos') return hit.x;
				if (field.fieldcode==='_ypos') return hit.y;
				if (field.fieldcode==='_len') return hit.len;
				if (field.fieldcode==='_ori') if (hit.ori>0) return "direct";else return "invert";
				return 0;
			}
			that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				ctx.fillStyle='rgba(0,0,0,1.0)';
				ctx.fillText(that.GetFieldText(rowindex,field),px+2,py+2);
			}
			that.ListMouseDown=function(posx,posy)
			{
				var row=that.GetHitRow(posy);
				var _row=that.ConvertToIndices(row);
				var col=that.GetHitCol(posx);
				var colfield=that.GetFieldSel();
			}
			return that;
		}
		
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		DotPlotOverview.NoHits=function(iParentRef,parent) 
		{
            var that=MScrollList.Init(iParentRef);
			that.parent=parent;
			that.item1=[];
			that.item2=[];
			that.setData=function(_data)
			{
				that.data=_data;
				for (var i=0;i<_data.length;i++)
				{
					for (var j=0;j<_data[i].nohits.length;j++)
					{
						that.item1.push(i);
						that.item2.push(j);
					}
				}
				that.SetDataCount(that.item1.length);
				that.render();
			}
			that.GetFieldText=function(rowindex,field)
			{
				var pair=that.data[that.item1[rowindex]];
				var hit=pair.nohits[that.item2[rowindex]];
				if (field.fieldcode==='_index') return rowindex+1;
				if (field.fieldcode==='_code') return pair.code;
				if (field.fieldcode==='_start') return hit.pos;
				if (field.fieldcode==='_len') return hit.len;
				return 0;
			}
			that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				ctx.fillStyle='rgba(0,0,0,1.0)';
				ctx.fillText(that.GetFieldText(rowindex,field),px+2,py+2);
			}
			that.ListMouseDown=function(posx,posy)
			{
				var row=that.GetHitRow(posy);
				var _row=that.ConvertToIndices(row);
				var col=that.GetHitCol(posx);
				var colfield=that.GetFieldSel();
			}
			return that;
		}
		
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		DotPlotOverview.MultiAlign=function(iParentRef,parent,_seqbox) 
		{
			var that=MScrollWindow.Init(iParentRef,true);
			that.project=null;
			that.elemsel=-1;
			that.SetProject=function(_project)
			{
				that.project=_project;
				that.SetProcessing("Loading data...");
			}
			that.UpdateFacts=function(update)
			{
				if (update) that.invalidate();
			}
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgba(250,250,250,1)";
				context.fillRect(0,0,that.GetXSize(),that.GetYSize());
				context.font="12px sans-serif";
				if (that.project==null) return;
				var dotplot=that.project.dotplot;
				if (that.GetVScrollSize()!=20000)
				{
					that.SetVScrollSize(20000);
					that.ReCalcScrollers();
				}
				var multialigns=dotplot.multialigns;
				var yy=20;
				var x1=0,x2=0,y1=0,y2=0,lastx=-1,lasty=-1;
				for (var i=0;i<multialigns.length;i++)
				{
					var alignment=multialigns[i];
					var yoffset1=yy-that.GetVScrollPos();yy+=200;
					var yoffset2=yy-that.GetVScrollPos();yy+=30;
					context.fillStyle="rgba(0,0,0,1)";
					context.strokeStyle="rgba(0,0,0,1)";
					var fac=1.0*(that.GetXSize()-40)/dotplot.longest_plot;
					for (var j=0;j<alignment.templ_intervals.length-1;j++)
					{
						var offset1=alignment.templ_intervals[j];
						var offset2=alignment.templ_intervals[j+1];
						context.fillRect(fac*offset1,yoffset1-10,1,10);
						context.fillRect(fac*offset2,yoffset1-10,1,10);
						context.fillRect(fac*offset1,yoffset1,fac*(offset2-offset1),1);
						context.fillText(alignment.templ_codes[j],fac*offset1,yoffset1-j*10);
//						context.fillText(j+1,fac*offset1,yoffset1-10);
						lastx=fac.offset1;
					}
					for (var j=0;j<alignment.align_intervals.length-1;j++)
					{
						var offset1=alignment.align_intervals[j];
						var offset2=alignment.align_intervals[j+1];
						context.fillRect(fac*offset1,yoffset2,1,10);
						context.fillRect(fac*offset2,yoffset2,1,10);
						context.fillRect(fac*offset1,yoffset2,fac*(offset2-offset1),1);
						context.fillText(alignment.align_codes[j],fac*offset1,yoffset2+j*20);
//						context.fillText(j+1,fac*offset1,yoffset2+20);
					}
					for (var j=0;j<alignment.templates.length;j++)
					{
						var template=alignment.templates[j];
						var offset1=alignment.templ_intervals[j];
						context.fillStyle=template.color;
						for (var k=0;k<template.aligns.length;k++)
						{
							var align=template.aligns[k];
							var offset2=alignment.align_intervals[k];
							if (align.blocks!=null)
							for (var l=0;l<align.blocks.length;l++)
							{
								var block=align.blocks[l];
								x1=~~(fac*(offset1+block.xpos1));
								x2=~~(fac*(offset1+block.xpos2));
								y1=~~(fac*(offset2+block.ypos1));
								y2=~~(fac*(offset2+block.ypos2));
//								if (y1>y2) {var _y=y1;y1=y2;y2=_y;}
								if (x1!=x2 || y1!=y2)
								{
									if (block.ori<0) context.fillStyle='rgba(255,0,0,0.3)';else context.fillStyle='rgba(0,0,255,0.3)';
									context.beginPath();
									context.moveTo(x1,yoffset1+20);
									context.lineTo(x1,yoffset1);
									context.lineTo(x2,yoffset1);
									context.lineTo(x2,yoffset1+20);
									context.lineTo(y2,yoffset2-20);
									context.lineTo(y2,yoffset2);
									context.lineTo(y1,yoffset2);
									context.lineTo(y1,yoffset2-20);
									context.lineTo(x1,yoffset1+20);
									context.closePath();
									context.fill();
								}
/*								else 
								if (lastx!=x1 || lasty!=y1)
								{
									if (block.ori<0) context.strokeStyle='rgba(255,0,0,0.05)';else context.strokeStyle='rgba(0,0,255,0.05)';
									context.beginPath();
									context.moveTo(x1,yoffset1);
									context.lineTo(x1,yoffset1+20);
									context.lineTo(y1,yoffset2-20);
									context.lineTo(y1,yoffset2);
									context.closePath();
									context.stroke();
								}
								lastx=x1;
								lasty=y1;*/
							}
						}
					}
				}
			}
			that.zscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
			}
			that.MouseMove=function(posx,posy)
			{
			}
			return that;
		}
		return DotPlotOverview;
    });