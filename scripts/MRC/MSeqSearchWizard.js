define(["require", "DQX/Application","DQX/Framework", "DQX/Controls", "DQX/Msg", "DQX/SQL", "DQX/DocEl", "DQX/Popup", "DQX/Wizard"],
    function (require, Application, Framework, Controls, Msg, SQL, DocEl, Popup, Wizard) 
	{
        var MSeqSearchWizard = {};
        MSeqSearchWizard.init=function(data)
		{
            var that={};
			that.wizard=Wizard.Create("WizardFindSequence");
			that.wizard.setTitle("Find subsequence");
			that.wizard._sizeY=350;
			that.data=data;
			var buttonList=[];
			
			comment=Controls.Static('Enter subsequence to be searched for:').makeComment();
			buttonList.push(comment);
			
			that.searchText=Controls.Edit('SeqSearchText', { size: 50 }).setHasDefaultFocus();
			that.searchText.setOnChanged(function(controlID, theControl) {that.data.searchtext=theControl.getValue();});
			buttonList.push(that.searchText);

            var chk=Controls.Check(null,{label:'Search reverse orientation',value:true});
                chk.setOnChanged(function(controlID,theControl) {theControl.getValue();});
			buttonList.push(chk);

			that.wizard.addPage({id: 'init',helpUrl: 'Doc/WizardFindGene/Help.htm',form: Controls.CompoundVert([Controls.CompoundVert(buttonList), Controls.CompoundHor([])]),hideNext: true	});

			that.execute=function(sendfunction)
			{
				that.wizard.run(sendfunction);
			}
			return that;
		}
        return MSeqSearchWizard;
    });
