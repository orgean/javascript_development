/************************************************************************************************************************************
 *************************************************************************************************************************************

 A FramePanel that implements a html Canvas drawing area

 *************************************************************************************************************************************
 *************************************************************************************************************************************/
	
	define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScroller","MRC/MFieldBar","MRC/MScrollList"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScroller, MFieldBar, MScrollList) {
 			var MarcTable = {};
			MarcTable.Init = function (iParentRef,_dbobject) 
			{
				var that = MScrollList.Init(iParentRef);
				that.dbobject=_dbobject;
				var path = "bitmaps";
				that.TreeNodeClosed=new Image();that.TreeNodeClosed.src=path+'/tree_node_closed.png';
				that.TreeNodeOpen=new Image();that.TreeNodeOpen.src=path+'/tree_node_open.png';
				that.TreeNodeClosedSelected=new Image();that.TreeNodeClosedSelected.src=path+'/tree_node_closed_selected.png';
				that.TreeNodeOpenSelected=new Image();that.TreeNodeOpenSelected.src=path+'/tree_node_open_selected.png';
				that.TreeNodeData=new Image();that.TreeNodeData.src=path+'/tree_node_data.png';
				that.TreeNodeOrg=new Image();that.TreeNodeOrg.src=path+'/tree_node_org.png';
				that.OpenBitmap=new Image();that.OpenBitmap.src='bitmaps/open.png';
				datalinksonly=false;
//				that.touchMove=true;
				var obj={'fielddata':"",'fieldcode':""};
				that.UpdateDbList=function()
				{
					listfields=[];
					listfields=that.dbobject.GetFieldCodes();//=json.fields
					that.resetFieldBar();
					for (var j=0;j<listfields.length;j++)
					{
						field=listfields[j];
						that.AddColumn(0,field.code,field.label,field.size);
					}
					that.SetDataCount(that.dbobject.GetCellCount());
					that.render();
				}
				that.ImportDbInfoFields=function()
				{
					listfields=[];
					listfields=that.dbobject.GetFieldCodes();//=json.fields
					that.resetFieldBar();
					for (var i=0;i<listfields.length;i++)
					{
						field=listfields[i];
						skip=false;
						currentlayer=that.dbobject.GetCurrentLayer();
						if (currentlayer!=null && currentlayer.HasFieldCode(field.code)==false) skip=true;
						if (field.code=="index") skip=false;
						if (skip==false) that.AddColumn(0,field.code,field.label,field.size);
					}
					that.SetDataCount(that.dbobject.GetCellCount());
					that.render();
				}
				that.GetFieldText=function(rowindex,field)
				{
					obj.fielddata="";
					var classobject=that.dbobject.GetClassObject(rowindex);
					if (classobject==null) return;
					obj.fieldcode=field.fieldcode;
					classobject.GetText(obj);
					if (field.fieldcode==='index') return obj.fielddata;//organism boom structuur
					var dataid=that.dbobject.FindFieldCodeDataId(field.fieldcode);
					if (dataid>0) return obj.fielddata;
					return classobject.GetField(field.fieldcode);//vrije velden informatie
				}
				that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
				{
					ctx.fillStyle='rgb(0,0,0)';
					obj.fielddata="";
					var classobject=that.dbobject.GetClassObject(rowindex);
					var selclassobject=that.dbobject.GetClassObject(that.GetRowPos());
					if (classobject==null) return;
					
					obj.fieldcode=field.fieldcode;
					var depth=classobject.GetDepth();
					classobject.GetText(obj);
					var extra=0;
					if (field.fieldcode==='index') 
					{	
						var dx=0;
						if (classobject.IsOrganism()) dx=10;
						ctx.fillText(obj.fielddata,px+(depth+1)*10+8+dx,py+2);//organism boom structuur
					}
					else 
					{
						var dataid=that.dbobject.FindFieldCodeDataId(field.fieldcode);
						var database=Application.DataBaseModule;
						if (dataid>0) // velden die data bevatten (sequenties,excel...)
						{
							ctx.fillStyle=database.GetColourFromFieldCode(dataid);
							ctx.fillText(obj.fielddata,px+4+extra,py+2);
							if (obj.fielddata=="" && that.IsRowSel(rowindex)>-1 && that.IsColSel(rowindex,colindex)==true)//als veld data bevat maar een cummulatief veld is
							{
								var cnt=0;
								cnt=classobject.CountFieldData(cnt,field.fieldcode);
								ctx.fillText('['+cnt+']',px+4,py+2);
							}
						}
					}
					txt=classobject.GetField(field.fieldcode);//vrije velden informatie
					if (txt!="") ctx.fillText(txt,px+4,py+2);
				}
				that.DrawField=function(ctx,rowindex,colindex,field,px,py,sizex,sizey)
				{
					var classobject=that.dbobject.GetClassObject(rowindex);
					var selclassobject=that.dbobject.GetClassObject(that.GetRowPos());
					var classobjectcnt=classobject.GetClassObjectCnt();
					if (classobject==null) return;
					var depth=classobject.GetDepth();
					
					if (field.fieldcode==='index')
					{
						ctx.fillStyle='rgba(220,220,220,1.0)';
						var k=depth;
						var p_parent=classobject.parent;
						while (p_parent!=null)
						{
							if (p_parent.endnode==0 || p_parent.endnode==1) ctx.fillRect(px+k*10,py,1,sizey);
							p_parent=p_parent.parent;
							k--;
						}
						var xx=~~(px+depth*10+6);
						var yy=~~(py+sizey/2-5);
						if (classobject.HasChildren() && datalinksonly==false)
						{
//							if (classobject.endnode==3) niets tekenen: slechts ��n item
							if (classobject.endnode==0) ctx.fillRect(xx+4,py,1,sizey);
							if (classobject.endnode==1) ctx.fillRect(xx+4,py+sizey/2,1,sizey/2);
							if (classobject.endnode==2) ctx.fillRect(xx+4,py,1,sizey/2);
							if (classobject.IsOrganism()) ctx.drawImage(that.TreeNodeOrg,xx+12,yy-2);
							if (classobjectcnt>0)
							{
								if (classobject.IsChildFrom(selclassobject) || that.GetRowPos()==rowindex)
								{
									if (classobject.IsExpand(rowindex)==false) ctx.drawImage(that.TreeNodeOpenSelected,xx,yy);
									if (classobject.IsExpand(rowindex)==true) ctx.drawImage(that.TreeNodeClosedSelected,xx,yy);
								}
								else
								{
									if (classobject.IsExpand(rowindex)==false) ctx.drawImage(that.TreeNodeOpen,xx,yy);
									if (classobject.IsExpand(rowindex)==true) ctx.drawImage(that.TreeNodeClosed,xx,yy);
								}
							}
						}
						yy=py+sizey/2-8;
						if (classobject.HasChildren()==false) ctx.drawImage(that.TreeNodeData,xx,yy);

					}
				}
				
				that.prechangeposit=function(row,col,posx,posy)
				{
					if (that.CheckPopupClicked(posx,posy)) return;
					var colfield=that.GetFieldSel();
					var dataid=that.dbobject.FindFieldCodeDataId(colfield.fieldcode);
					that.ClearPopup();
					if (dataid>0) 
					{
						obj.fielddata="";
						var classobject=that.dbobject.GetClassObject(row);
						if (classobject==null) return;
						obj.fieldcode=colfield.fieldcode;
						classobject.GetText(obj);
						var cnt=0;
						cnt=classobject.CountFieldData(cnt,colfield.fieldcode);
						if (obj.fielddata!="" || cnt>0)//als veld data bevat of een cummulatief veld is
						{
							that.AddPopupButton(that.OpenBitmap,that.OpenData);
							that.AddPopupButton(that.TreeNodeData,that.AddSelection);
						}
					}
				}
				
				that.ListMouseDown=function(posx,posy)
				{
					var row=that.GetHitRow(posy);
					var col=that.GetHitCol(posx);
					var dx=posx-that.GetColumnOffset(col);
					var classobject=that.dbobject.GetClassObject(that.GetRowPos());
					if (classobject==null) return;
					var xx=classobject.GetDepth()*10+2;
					var cx=20;
					var colfield=that.GetFieldSel();
					if (colfield.fieldcode==='index' && dx>xx && dx<xx+cx)
					{
						if (classobject.IsExpand())	classobject.SetExpand(false);
						else classobject.SetExpand(true);
						that.dbobject.BuildTree();
						that.SetDataCountNoScroll(that.dbobject.GetCellCount());
						that.render();
					}
				}
				that.ListMouseUp=function(posx,posy)
				{
				};
				that.MouseDblClick=function(posx,posy) 
				{
					that.OpenData();
				}
				that.OpenData=function()
				{
					that.ClearPopup();
					var database=Application.DataBaseModule;
					var track=database.AddTrackToList();
					track.execute();//voer actie ook direct uit
				}
				that.AddSelection=function()
				{
					that.ClearPopup();
					var database=Application.DataBaseModule;
					var track=database.AddTrackToList();
				}
				
				that.ExpandAll=function()
				{
					var classobject=that.dbobject.GetClassObject(that.GetRowPos());
					if (classobject!=null) 
					{
						depth=classobject.GetDepth();
						expand=classobject.IsExpand();
						that.dbobject.ExpandCells(!expand,depth);
						that.SetDataCount(that.dbobject.GetCellCount());
						that.render();
					}
				}
				that.ExpandParent=function()
				{
					var classobject=that.dbobject.GetClassObject(that.GetRowPos());
					if (classobject!=null) 
					{
						parent=classobject.parent;
						expand=classobject.IsExpand();
						parent.ExpandChildren(!expand);
						that.dbobject.BuildTree();
						that.SetDataCount(that.dbobject.GetCellCount());
						that.render();
					}
				}
				
				var drag=document.getElementById(that.getDivID()+'_drag_field');
/*				drag.onselectstart = function () {
							return false;
						}
				$('#'+that.getDivID()+'_drag_field').on('dragstart',function() {
						$('#'+that.getDivID()+'_drag_field').css('background-color','rgba(180,180,180,0.1)');
					});*/
				drag.addEventListener("dragstart", function(event) 
				{
					event.dataTransfer.setData('text/plain', 'dummy');
					$('#'+that.getDivID()+'_drag_field').css('background-color','rgba(180,180,180,0.1)');
					var dbobject=Application.GetActiveDbObject();
					if (dbobject==null) return;
					var scopebox=Application.DataBaseModule.GetDbScopeBox();
					if (scopebox==null) return;
//					for (var i=0;i<scopebox.length;i++) event.dataTransfer.setData(i,scopebox[i].uniquecode);
				},false);
				
				var drop=document.getElementById("Frame16_client");
				drop.addEventListener("dragover", function(event) 
				{
					event.preventDefault();
					event.target.style.border = "2px dotted red";
				});

				drop.addEventListener("dragleave", function(event) 
				{
					event.target.style.border = "";
				});

				drop.addEventListener("drop", function(event) 
				{
					for (var i=0;i<event.dataTransfer.types.length;i++)
					{
						var data=event.dataTransfer.getData(i);
					}
					event.preventDefault();
					var target=event.target.innerText;
					var id=event.target.id.split('Frame16_client')[1];
					var track=Application.DataBaseModule.FindTrackFromBranchItem(id);
					var pjobject=Application.GetActivePjObject();
					if (pjobject==null) return;
					var project=pjobject.GetProject(track.code);
					if (project==null) {Application.SetMessage("No valid project to capture selection");return;}
					Application.DataBaseModule.CaptureSelectionInProject(project);
//					project.
				});
				return that;
			}
			return MarcTable;
   });
