define(["jquery", "DQX/DocEl", "DQX/Msg"],
    function ($, DocEl, Msg) 
{
    var MPlotChannel={};

	MPlotChannel.body=function()
	{
		var that={};
		this.view=true;
		this.accession="";
		this.size=10;
		this.location=+1;
		this.color=('#'+Math.floor(Math.random()*16777215).toString(16));
		this.label="";
		this.type="unknown";
		this.fullindex1=[];
		this.fullindex2=[];
		this.fields=[];
		var obj={fieldcode:"index",fieldlabel:"Index",fieldsize:60}; this.fields.push(obj);
		var obj={fieldcode:"start",fieldlabel:"Start",fieldsize:60};this.fields.push(obj);
		var obj={fieldcode:"stop",fieldlabel:"Stop",fieldsize:60};this.fields.push(obj);
		var obj={fieldcode:"size",fieldlabel:"Size",fieldsize:60};this.fields.push(obj);
		var obj={fieldcode:"accession",fieldlabel:"Sequence",fieldsize:80};this.fields.push(obj);
		this.curve=[];
	}
	MPlotChannel.body.prototype.GetType=function()
	{
		return this.type;
	}
	MPlotChannel.body.prototype.SetSize=function(sz)
	{
		this.size=sz;
	}
	MPlotChannel.body.prototype.SetLocation=function(loc)
	{
		this.location=loc;
	}
	MPlotChannel.body.prototype.SetLabel=function(lb)
	{
		this.label=lb;
	}
	MPlotChannel.body.prototype.GetLabel=function()
	{
		return this.label;
	}
	MPlotChannel.body.prototype.SetViewState=function(state)
	{
		this.view=state;
	}
	MPlotChannel.body.prototype.GetView=function()
	{
		return this.view;
	}
	MPlotChannel.body.prototype.SetAccession=function(_acc)
	{
		this.accession=_acc;
	}
	MPlotChannel.body.prototype.GetAccession=function()
	{
		return this.accession;
	}
	MPlotChannel.body.prototype.SetColor=function(cl)
	{
		this.color=cl;
	}
	MPlotChannel.body.prototype.GetColor=function()
	{
		return this.color;
	}
	MPlotChannel.body.prototype.GetFullItemCnt=function()
	{
		return 0;
	}
	MPlotChannel.body.prototype.GetFullItemStart=function(nr)
	{
		return -1;
	}
	MPlotChannel.body.prototype.GetFullItemStop=function(nr)
	{
		return -1;
	}
	MPlotChannel.body.prototype.GetFullItemSize=function(nr)
	{
		return 0;
	}
	MPlotChannel.body.prototype.GetFullItemAccession=function(nr)
	{
		return "";
	}
	MPlotChannel.body.prototype.GetFullItem=function(nr)
	{
		return null;
	}
	MPlotChannel.body.prototype.GetItemCnt=function()
	{
		return 0;
	}
	MPlotChannel.body.prototype.GetEntryTable=function(accession)
	{
		return null;
	}
	MPlotChannel.body.prototype.GetItem=function(nr)
	{
		return null;
	}
	MPlotChannel.body.prototype.GetItemStart=function(nr)
	{
		return -1;
	}
	MPlotChannel.body.prototype.GetItemStop=function(nr)
	{
		return -1;
	}
	MPlotChannel.body.prototype.GetItemAccession=function(nr)
	{
		return "";
	}
	MPlotChannel.body.prototype.GetNextItem=function(curpos)
	{
		return null;
	}
	MPlotChannel.body.prototype.GetFieldCnt=function()
	{
		return this.fields.length;
	}	
	MPlotChannel.body.prototype.GetField=function(index)
	{
		if (index>=0 && index<this.fields.length) return this.fields[index];
		return null;
	}	

	MPlotChannel.body.prototype.Reduce=function(seqbox)
	{
		var maxlen=seqbox.GetMaxSeqLen();
		var pp1=0;
		var pp2=0;
		var kk1=0;
		var kk2=0;
		var curvelen=maxlen/10000+1;
		if (curvelen<3000) curvelen=3000;
		this.curve.length=0;
		for (var i=0;i<curvelen;i++) this.curve.push(0);
		for (var i=0;i<seqbox.GetSequenceCount();i++)
		{
			var sequence=seqbox.GetSequence(i);
			var seqchannel=sequence.FindSeqChannel(this.GetAccession());//geef de SeqChannel van MClientSequence
			if (seqchannel==null) continue;// probleem???
			var curve=seqchannel.curve;
//			var curve=this.curve;
			curve.length=0;
			for (var j=0;j<=curvelen;j++) curve.push(0);
			var blocks=sequence.blocks;
			var j=blocks.length;
			while (j--)
			{
				block=blocks[j];
				var diffbase=block.startbasegaps-block.startbase;
				var seqblockchannel=block.FindSeqblockChannel(seqchannel);
				if (seqblockchannel==null) continue;
				ll=seqblockchannel.GetChannelItemIndexCnt();
				for (var k=0;k<ll;k++)
				{
					var seqhit=seqblockchannel.GetItem(k);
					pp1=seqhit.start+diffbase;
					pp2=seqhit.stop+diffbase;
					kk1=parseInt(curvelen*pp1/maxlen);
					kk2=parseInt(curvelen*pp2/maxlen);
					if (kk1>=curvelen) kk1=curvelen-1;
					if (kk2>=curvelen) kk2=curvelen-1;
					for (var l=kk1;l<=kk2;l++) 
					{
						if (curve[l]==0) curve[l]=1;
						curve[l]=curve[l]+1;
					}
				}
			}
		}
	}
//----------------------------------------------------------------------------------------------------------------
	MPlotChannel.FtsSearch=function(seqbox)
	{
		var that=new MPlotChannel.body();
		that.seqbox=seqbox;
		that.type="FeatureSearch";
		that.sequences=seqbox.GetSequences();
		that.featuretable=null;
		var obj={fieldcode:"label",fieldlabel:"Label",fieldsize:200}
		that.fields.push(obj);
		that.SetFeatureTable=function(ftstable)
		{
			that.featuretable=ftstable;
			that.fullindex1.length=0;
			that.fullindex2.length=0;
			for (var i=0;i<that.featuretable.length;i++) 
			{
				var ftrs=that.featuretable[i].ftrs;
				var ll=ftrs.length;
				k=0;
				while (k<ll)
				{
					that.fullindex1.push(k);
					that.fullindex2.push(i);
					k++;
				}
			}
		}
		that.LinkAllSequences=function()
		{
			for (var i=0;i<that.sequences.length;i++) 
			{
				var seqchannel=that.sequences[i].LinkChannel(that);
				seqchannel.location=that.location;
				seqchannel.size=that.size;
			}
			that.Reduce(that.seqbox);
		}
		that.GetFullItemCnt=function()
		{
			return that.fullindex1.length;
		}
		that.GetFullItemStart=function(nr)
		{
			return that.featuretable[that.fullindex2[nr]].ftrs[that.fullindex1[nr]].start;
		}
		that.GetFullItemStop=function(nr)
		{
			return that.featuretable[that.fullindex2[nr]].ftrs[that.fullindex1[nr]].stop;
		}
		that.GetFullItemSize=function(nr)
		{
			return that.featuretable[that.fullindex2[nr]].ftrs[that.fullindex1[nr]].size;
		}
		that.GetFullItemAccession=function(nr)
		{
			return that.featuretable[that.fullindex2[nr]].accession;
		}
		that.GetFullItem=function(nr)
		{
			return that.featuretable[that.fullindex2[nr]].ftrs[that.fullindex1[nr]];
		}

		that.GetItemCnt=function(ftrs)
		{
			return ftrs.length;
		}
		that.GetItem=function(nr,ftrs)
		{
			if (nr>=0 && nr<ftrs.length) return ftrs[nr];
			return null;
		}
		that.GetItemStart=function(nr,ftrs)
		{
			if (nr>=0 && nr<ftrs.length) return ftrs[nr].start;
			return -1;
		}
		that.GetItemStop=function(nr,ftrs)
		{
			if (nr>=0 && nr<ftrs.length) return ftrs[nr].stop;
			return -1;
		}
		that.GetItemAccession=function(nr)
		{
			if (nr>=0 && nr<that.featuretable.ftrs.length) return that.featuretable.ftrs[nr].accession;
			return "";
		}
		that.GetItemSize=function(nr,ftrs)
		{
			if (nr>=0 && nr<ftrs.length) return ftrs[nr].size;
			return "";
		}
		that.GetEntryTable=function(accession)
		{
			for (var i=0;i<that.featuretable.length;i++) if (that.featuretable[i].accession==accession) return that.featuretable[i].ftrs;
			return null;
		}
		seqbox.AddChannel(that);
		return that;
	}
	MPlotChannel.Sequence=function(seqbox)
	{
		var that=new MPlotChannel.body();
		that.sequences=seqbox.GetSequences();
		that.type="SequenceData";
		seqbox.AddChannel(that);
		return that;
	}
	MPlotChannel.Numbering=function(seqbox)
	{
		var that=new MPlotChannel.body();
		that.sequences=seqbox.GetSequences();
		that.type="Sequence numbering";
		seqbox.AddChannel(that);
		return that;
	}
	MPlotChannel.SeqSearch=function(seqbox)
	{
		var that=new MPlotChannel.body();
		that.sequences=seqbox.GetSequences();
		that.seqbox=seqbox;
		that.type="SequenceSearch";
		that.SetFoundHits=function(foundhits)
		{
			that.foundhits=foundhits;
			that.fullindex1.length=0;
			that.fullindex2.length=0;
			for (var i=0;i<that.foundhits.length;i++) 
			{
				var seqfnd=that.foundhits[i].seqfnd;
				var ll=seqfnd.length;
				k=0;
				while (k<ll)
				{
					that.fullindex1.push(k);
					that.fullindex2.push(i);
					k++;
				}
			}
			for (var i=0;i<that.sequences.length;i++) that.sequences[i].LinkChannel(that);
			that.Reduce(that.seqbox);
		}
		that.GetFullItemCnt=function()
		{
			return that.fullindex1.length;
		}
		that.GetFullItemStart=function(nr)
		{
			return that.foundhits[that.fullindex2[nr]].seqfnd[that.fullindex1[nr]].start;
		}
		that.GetFullItemStop=function(nr)
		{
			return that.foundhits[that.fullindex2[nr]].seqfnd[that.fullindex1[nr]].stop;
		}
		that.GetFullItemSize=function(nr)
		{
			return that.foundhits[that.fullindex2[nr]].seqfnd[that.fullindex1[nr]].stop-that.foundhits[that.fullindex2[nr]].seqfnd[that.fullindex1[nr]].start;
		}
		that.GetFullItemAccession=function(nr)
		{
			return that.foundhits[that.fullindex2[nr]].accession;
		}
		that.GetFullItem=function(nr)
		{
			return that.foundhits[that.fullindex2[nr]].seqfnd[that.fullindex1[nr]];
		}

		that.GetItemCnt=function(seqfnd)
		{
			return seqfnd.length;
		}
		that.GetItem=function(nr,seqfnd)
		{
			if (nr>=0 && nr<seqfnd.length) return seqfnd[nr];
			return null;
		}
		that.GetItemStart=function(nr,seqfnd)
		{
			if (nr>=0 && nr<seqfnd.length) return seqfnd[nr].start;
			return -1;
		}
		that.GetItemStop=function(nr,seqfnd)
		{
			if (nr>=0 && nr<seqfnd.length) return seqfnd[nr].stop;
			return -1;
		}
		that.GetItemAccession=function(nr)
		{
			if (nr>=0 && nr<that.foundhits.length) return that.foundhits[nr].accession;
			return "";
		}
		that.GetItemSize=function(nr,seqfnd)
		{
			if (nr>=0 && nr<seqfnd.length) return seqfnd[nr].stop-seqfnd[nr].start;
			return "";
		}
		that.GetEntryTable=function(accession)
		{
			for (var i=0;i<that.foundhits.length;i++) if (that.foundhits[i].accession==accession) return that.foundhits[i].seqfnd;
			return null;
		}
		seqbox.AddChannel(that);
		return that;
	}
	MPlotChannel.VariationSearch=function(seqbox)
	{
		var that=new MPlotChannel.body();
		that.sequences=seqbox.GetSequences();
		that.foundhits=null;
		that.type="VariationSearch";
		that.SetFoundHits=function(foundhits)
		{
			that.foundhits=foundhits;
			for (var i=0;i<that.sequences.length;i++) that.sequences[i].LinkChannel(that);
		}
		that.GetItemCnt=function()
		{
			if (that.foundhits==null) return 0;
			return that.foundhits.length;
		}
		that.GetItem=function(nr)
		{
			if (nr>=0 && nr<that.foundhits.length) return that.foundhits[nr];
			return null;
		}
		that.GetItemStart=function(nr)
		{
			if (nr>=0 && nr<that.foundhits.length) return that.foundhits[nr].start;
			return -1;
		}
		that.GetItemStop=function(nr)
		{
			if (nr>=0 && nr<that.foundhits.length) return that.foundhits[nr].start+1;
			return -1;
		}
		that.GetItemAccession=function(nr)
		{
			if (nr>=0 && nr<that.foundhits.length) return that.foundhits[nr].accession;
			return "";
		}
		that.GetItemSize=function(nr)
		{
			if (nr>=0 && nr<that.foundhits.length) return 1;
			return "";
		}
		seqbox.AddChannel(that);
		return that;
	}
	MPlotChannel.FrameSearch=function(seqbox)
	{
		var that=new MPlotChannel.body();
		that.sequences=seqbox.GetSequences();
		that.entries=[];
		that.orfs=[];
		that.type="FrameMapping";
		that.SetFoundHits=function(foundhits)
		{
//			that.orfs.length=0;
//			for (var i=0;i<foundhits.length;i++) that.orfs.push(foundhits[i]);
			that.orfs=foundhits;
		}
		that.LinkAllSequences=function()
		{
			for (var i=0;i<that.sequences.length;i++) 
			{
				var seqchannel=that.sequences[i].LinkChannel(that);
				seqchannel.location=that.location;
				seqchannel.size=that.size;
			}
		}
		that.SetFrameTable=function(frametable)
		{
//			that.frametable=frametable;
		}
		that.GetItemCnt=function()
		{
			return that.orfs.length;
		}
		that.GetItem=function(nr)
		{
			if (nr>=0 && nr<that.orfs.length) return that.orfs[nr];
			return null;
		}
		that.GetItemStart=function(nr)
		{
			if (nr>=0 && nr<that.orfs.length) return that.orfs[nr].start;
			return -1;
		}
		that.GetItemStop=function(nr)
		{
			if (nr>=0 && nr<that.orfs.length) return that.orfs[nr].stop;
			return -1;
		}
		that.GetItemAccession=function(nr)
		{
			if (nr>=0 && nr<that.orfs.length) return that.orfs[nr].accession;
			return "";
		}
		that.GetItemSize=function(nr)
		{
			if (nr>=0 && nr<that.orfs.length) return that.orfs[nr].stop-that.orfs[nr].start;
			return "";
		}
		seqbox.AddChannel(that);
		return that;
	}
	return MPlotChannel;
})
