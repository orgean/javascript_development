﻿define(["require", "DQX/Application", "MRC/DotPlot", "server/serverobject","server/MDbObject","server/MDbCommands","server/MDbTracks","server/MEntryObject","server/MEmblGnbkObject","MRC/MSeqTools","MRC/MEntryClient","MRC/MFtsSearch","MRC/MSequenceClient","MRC/MPjObject"],
   function (require, Application) 
{
	var Client={};

	datapath = dataPath;
	
	Client.init=function()
	{
		var that={};
		that.dbtracks=new MDbTracks.DbTracks();	var track=that.dbtracks.AddTrack();track.SetDefiner("Track module loaded...");
		that.dbcommands=new MDbCommands.DbCommands();var track=that.dbtracks.AddTrack();track.SetDefiner("Commands module loaded...");
		that.seqtools=new MSeqTools();

		var connection=new WebSocket(webSocketUrl);
		connection.onopen=function (openEvent) 
		{
			console.log("Connected");
			var track=that.dbtracks.AddTrack();
			track.SetDefiner("Connected to server");
			that.LoadEmblGnbkData();
		};

		connection.onerror = function (errorEvent) 
		{
			console.log('There is a problem with the connection or the server is down.');
		};

		connection.onclose = function (closeEvent)
		{
			console.log('Connection closed');
		};
		connection.onmessage=function (messageEvent)
		{
			var json=null;
			try 
			{
				json=JSON.parse(messageEvent.data);
			} 
			catch (e)
			{
				console.log('This does not look like a valid JSON: '+messageEvent.data);
			}
			if (json==null) return;
			if (json.type==='LoadDataBase') that.LoadDataBase(json);
			if (json.type==='DataBaseLoaded') that.DataBaseLoaded(json);
			if (json.type==='LoadDataBaseProjects') that.LoadDataBaseProjects(json);
			if (json.type==='LoadDataBaseFieldCodes') that.LoadDataBaseFieldCodes(json);
			if (json.type==='LoadDataBaseLayers') that.LoadDataBaseLayers(json);
			if (json.type==='LoadDataBasePathCodes') that.LoadDataBasePathCodes(json);
			if (json.type==='FetchEmblGnbkData') Application.EmblGnbk=json.object;
			if (json.type==='TransferEntry') that.TransferEntry(json);
			if (json.type==='SequenceDataLoad') that.SequenceDataLoad(json);
			if (json.type==='GetSelectedSequence') that.GetSelectedSequence(json);
			if (json.type==='FeatureSearchRequest') that.FeatureSearchRequest(json);
			if (json.type==='SequenceSearchRequest') that.SequenceSearchRequest(json);
			if (json.type==='SequenceFrameLoad') that.SequenceFrameLoad(json);
			if (json.type==='ClientLoadDataBaseProject') that.ClientLoadDataBaseProject(json);
			if (json.type==='LoadDotPlotInput') that.LoadDotPlotInput(json);
			if (json.type==='LoadDotPlotSettings') that.LoadDotPlotSettings(json);
			if (json.type==='LoadDotPlotCellData') that.LoadDotPlotCellData(json);
			if (json.type==='ClientLoadDotPlotCellStretchData') that.ClientLoadDotPlotCellStretchData(json);
			if (json.type==='ClientLoadDotPlotBestHits') that.ClientLoadDotPlotBestHits(json);
			if (json.type==='ClientLoadDotPlotNoHits') that.ClientLoadDotPlotNoHits(json);
			if (json.type==='ClientLoadMultiAlignData') that.ClientLoadMultiAlignData(json);
			if (json.type==='ClientCheckMultiAlignData') that.ClientCheckMultiAlignData(json);
			if (json.type==='LoadDotPlotCellStretchIncludeData') that.LoadDotPlotCellStretchIncludeData(json);
			if (json.type==='LoadCellAlignData') that.LoadCellAlignData(json);
			if (json.type==='LoadCellVariationsData') that.LoadCellVariationsData(json);
			if (json.type==='DotPlotUpdateRun') that.DotPlotUpdateRun(json);
			if (json.type==='message') console.log(json.data);
			if (json.type==='ClientCreateDataBaseProject') that.ClientCreateDataBaseProject(json);
			if (json.type==='ClientUpdateDataBaseProject') that.ClientUpdateDataBaseProject(json);
		}	
		
		
		that.OpenFile=function()
		{
			var command=JSON.stringify( {type: 'OpenFile', data: "get the stuff"});
			connection.send(command);
		}
		that.OpenDataBase=function()
		{
			var command=JSON.stringify( {type: 'OpenDataBase', dbpath: datapath + "orgean_database"});connection.send(command);
			command=JSON.stringify( {type: 'OpenDataBase', dbpath: datapath + "orgean_all_bacteria"});connection.send(command);
//			command=JSON.stringify( {type: 'OpenDataBase', dbpath: datapath +  "orgean_bigdata"});connection.send(command);
		}
		that.LoadEmblGnbkData=function()
		{
			var command=JSON.stringify( {type: 'RequestEmblGnbkData'});
			connection.send(command);
		}
		that.TransferEntry=function(json)
		{
			var seqbox=Application.GetRegisteredSeqBox(json.sequence_box_code);
			if (seqbox!=null) seqbox.ServerResponse(json);
		}
		that.SequenceDataLoad=function(json)
		{
			var seqplot=Application.GetRegisteredSeqPlot(json.seqplot_code);
			if (seqplot!=null) seqplot.SetSequenceData(json.data);
		} 
		that.GetSelectedSequence=function(json)
		{
			var seqplot=Application.GetRegisteredSeqPlot(json.seqplot_code);
			if (seqplot!=null) seqplot.SetSelectedSequence(json.data);
		} 
		that.SequenceFrameLoad=function(json)
		{
			var seqbox=Application.GetRegisteredSeqBox(json.sequence_box_code);
			if (seqbox!=null) seqbox.ServerResponseFrameMapping(json);
//			if (seqbox!=null) seqbox.SequenceFrameMappingRequest(json.data);
//			var window=Application.GetRegisteredWindow(json.sequence_viewer_accession);
//			if (window!=null) window.SetFrameData(json.data);
		} 
		that.FeatureSearchRequest=function(json)
		{
			var seqbox=Application.GetRegisteredSeqBox(json.sequence_box_code);
			if (seqbox!=null) seqbox.ServerResponseFtsSearch(json);
		}
		that.SequenceSearchRequest=function(json)
		{
			var seqbox=Application.GetRegisteredSeqBox(json.sequence_box_code);
			if (seqbox!=null) seqbox.ServerResponseSeqSearch(json);
			var seqviewer=Application.GetRegisteredWindow(json.sequence_box_code);
			if (seqviewer!=null) 
			{
				var sequencesearch=seqviewer.FindChannel(json.sequence_search_accession);
				if (sequencesearch!=null) sequencesearch.SetFoundHits(json.foundhits);
				seqviewer.UpdateFacts();
			}
		}
		that.LoadDataBasePathCodes=function(json)
		{ 
			var dbobject=Application.GetDbObject(json.code);
			if (dbobject==null) return;
			for (var i=0; i<json.data.length; i++) 
			{
				var foo={'code':'','path':''};
				foo=json.data[i];
				dbobject.AddDbPathCode(foo);
			}
		}
		that.LoadDataBaseLayers=function(json)
		{ 
			var dbobject=Application.GetDbObject(json.code);
			if (dbobject==null) return;
			for (var i=0; i<json.data.length; i++) 
			{
				var foo={'name':'','fieldcodes':[]};
				foo=json.data[i];
				dbobject.AddDbDataLayer(foo);
//				Application.DataBaseModule.UpdateLayers();
			}
			Application.DataBaseModule.PostCreateFunctions("DataBaseLoaded",json.code);
		}
		that.LoadDataBaseFieldCodes=function(json)
		{ 
			var dbobject=Application.GetDbObject(json.code);
			if (dbobject==null) return;
			for (var i=0; i<json.data.length; i++) 
			{
				var foo={'code':'','label':'','dataId':0,'color':0};
				foo.code=json.data[i].code;
				foo.label=json.data[i].label;
				foo.dataId=json.data[i].dataId;
				foo.color=json.data[i].color;
				var index=dbobject.FindFieldCode(json.data[i].code);
				var fieldcode=dbobject.GetFieldCode(index);
				if (fieldcode!=null)
				{
					fieldcode.label=json.data[i].label;
					fieldcode.dataId=json.data[i].dataId;
					fieldcode.color=json.data[i].color;
				}
			}
			Application.DataBaseModule.PostCreateFunctions("DataBaseLoaded",json.code);
		} 
		that.ClientGetProject=function(json)//haalt project uit project database met pjobjectcode en projectcode
		{
			var pjobject=Application.GetPjObject(json.pjobjectcode);
			if (pjobject==null) return null;
			return pjobject.GetProject(json.projectcode);
		}
		that.ClientLoadDataBaseProject=function(json)
		{
			var pjobject=Application.GetPjObject(json.pjobjectcode);
//			var pjobject=Application.GetActivePjObject();
			if (pjobject==null) return;
			var project=pjobject.GetProject(json.projectcode);
			if (project==null)
			{
				project=new MPjObject.Project(Application,pjobject);
				project.fileLocation=json.projectfilelocation;
//				console.log("project file ----------------->"+project.fileLocation);
				project.uniqueCode=json.projectcode;
				project.displayName=json.displayname;
				project.parseProjectPath();
				pjobject.RegisterProject(project);
				Application.ProjectList.Update();
			}
			project.SetDataItems(json.data);
			if (json.projectcode==project.uniqueCode)
			{
				project.OpenDotPlot(false);
				var database=Application.DataBaseModule;
//				var track=database.AddProjectToTrackList(project);
//				track.execute();//voer actie ook direct uit
			}
		}
		that.LoadDotPlotInput=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project==null) return;//probleem, geen zo een project aanwezig
			project.MakeClassObjectClusters();
			project.SetSequenceLengths(json);
		}
		that.LoadDotPlotSettings=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotSettings(json);
		}
		that.LoadDotPlotCellData=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotCellData(json);
		}
		that.DotPlotUpdateRun=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.DotPlotUpdateRun(json);
		}
		that.ClientLoadDotPlotCellStretchData=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotCellStretchData(json);
		}
		that.ClientLoadDotPlotBestHits=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotBestHits(json);
		}
		that.ClientLoadDotPlotNoHits=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotNoHits(json);
		}
		that.ClientLoadMultiAlignData=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotMultiAlignData(json)
		}
		that.ClientCheckMultiAlignData=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotMultiAlignScore(json)
		}
		that.LoadDotPlotCellStretchIncludeData=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotCellStretchIncludeData(json);
		}
		that.ClientLoadDotPlotCellUniqueStretches=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetDotPlotCellUniqueStretchData(json);
		}
		that.LoadCellAlignData=function(json)
		{
			var project=that.ClientGetProject(json);
			if (project!=null) project.dotplot.SetCellAlignData(json);
		}
		that.LoadCellVariationsData=function(json)
		{
			var project=that.ClientGetProject(json);
			console.log("getting variation data");
			if (project!=null) project.dotplot.SetCellVariationsData(json);

return;
			var seqbox=Application.GetRegisteredSeqBox(json.seqboxcode);
			if (seqbox!=null) 
			{
				var varchannel=seqbox.FindChannel("variations");
				varchannel.SetFoundHits(json.variations);
			}
		}
		that.LoadDataBase=function(json)
		{
			var dbobject=Application.GetDbObject(json.code);
			if (dbobject==null) return;
			if (json.status=== 'linefeed')
			{
				for (var i=0; i<json.data.length; i++) 
				{
					dbobject.ParseDataBaseLine(json.data[i].linedata,json.dbpath+"/database.txt");
				}
			} 
		}			
		that.DataBaseLoaded=function(json)
		{
			var dbobject=Application.GetDbObject(json.code);
			if (dbobject==null) return;
			if (json.status==='finished') 
			{
				dbobject.BuildTree();
				Application.DataBaseModule.PostCreateFunctions("DataBaseLoaded",json.code);
			}
		}			
		that.LoadDataBaseProjects=function(json)
		{
			if (json.status==='finished') 
			{
				Application.ProjectList.Update();
			}
		}
		that.ClientCreateDataBaseProject=function(json)
		{
			var pjobject=Application.GetActivePjObject();
			if (pjobject==null) return;
			var command={};
			command.type='ReScanDataBaseProjects';
			command.dbpath=datapath;
			command.code=pjobject.GetCode();//code is hier eigenlijk de track.action.code
			Application.ClientConnection.send(JSON.stringify(command));
		}
		that.ClientUpdateDataBaseProject=function(json)
		{
			var pjobject=Application.GetActivePjObject();
			if (pjobject==null) return;
			var project=pjobject.GetProject(json.projectcode);
			if (project==null) return;
			var command=project.GetServerCommand();
			command.type='ServerReadProject';
			command.dbpath=dataPath;
			Application.ClientConnection.send(JSON.stringify(command));
		}
		
		Application.ClientConnection=connection;
		Application.SeqTools=that.seqtools;
		Application.DbCommands=that.dbcommands;
		Application.DbTracks=that.dbtracks;
		
		that.dbcommands.InsertCommand("Root|DataBases|orgean_database",action={type: 'OpenDataBase', dbpath: datapath, note:'Database "orgean"', code:that.seqtools.NewUUID()});
		that.dbcommands.InsertCommand("Root|DataBases|All bacteria",action={type: 'OpenDataBase', dbpath: "d:/orgean_all_bacteria/", note:'Database "All bacteria"',code:that.seqtools.NewUUID()});
		that.dbcommands.InsertCommand("Root|DataImport|Sequence types|Fetch external databases",action={type: 'FetchEmblData', dbpath: "D:\\orgean_database",code:that.seqtools.NewUUID()});
		//that.dbcommands.InsertCommand("Root|DataImport|Range types|Import",null);
		//that.dbcommands.InsertCommand("Root|DataImport|State types|Import",null);
		that.dbcommands.InsertCommand("Root|Projects|Load",action={type: 'OpenDataBaseProjects', dbpath: datapath, note:'Projects',code:that.seqtools.NewUUID()});
		that.dbcommands.InsertCommand("Root|Projects|Create",action={type: 'CreateDataBaseProject', dbpath: datapath, note:'Create new project',code:""});

		return that;
	}
	return Client;
});

