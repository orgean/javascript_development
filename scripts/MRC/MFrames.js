	MFrames=function()
	{
		var that=this;
		that.frames=[];
		that.frame=1;
		that.display=2;
		that.translation_table=1;
		that.orfminsize=150;
		that.pcsminsize=150;
		that.pcsonly=1;
		that.usefilter=1;
		that.filter_overlap_check=1;
		that.filter_true_overlap=50;
		that.filter_proportional_size=50;

		that.cbasen=[];
		for (var i=0;i<256;i++) that.cbasen[i]=' ';
		that.cbasen[' ']=' ';
		that.cbasen['a']='t';
		that.cbasen['b']='v';
		that.cbasen['c']='g';
		that.cbasen['d']='h';
		that.cbasen['g']='c';
		that.cbasen['h']='d';
		that.cbasen['k']='m';
		that.cbasen['m']='k';
		that.cbasen['n']='n';
		that.cbasen['r']='y';
		that.cbasen['s']='s';
		that.cbasen['t']='a';
		that.cbasen['v']='b';
		that.cbasen['w']='w';
		that.cbasen['y']='r';

		that.cbasen['A']='T';
		that.cbasen['B']='V';
		that.cbasen['C']='G';
		that.cbasen['D']='H';
		that.cbasen['G']='C';
		that.cbasen['H']='D';
		that.cbasen['K']='M';
		that.cbasen['M']='K';
		that.cbasen['N']='N';
		that.cbasen['R']='Y';
		that.cbasen['S']='S';
		that.cbasen['T']='A';
		that.cbasen['V']='B';
		that.cbasen['W']='W';
		that.cbasen['Y']='R';

		that.AA="FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG";
		that.ST="---M---------------M---------------M----------------------------";
		that.B1="ttttttttttttttttccccccccccccccccaaaaaaaaaaaaaaaagggggggggggggggg";
		that.B2="ttttccccaaaaggggttttccccaaaaggggttttccccaaaaggggttttccccaaaagggg";
		that.B3="tcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcagtcag";

		that.azcoder=[];
		for (var i=0;i<255;i++) that.azcoder.push(-100);
		that.azcoder['t']=0;
		that.azcoder['c']=1;
		that.azcoder['a']=2;
		that.azcoder['g']=3;

		that.Complement=function(bs)
		{
			if (bs=='|') return bs;
			if (bs<0 || bs>255) return bs;
			return that.cbasen[bs];
		}

		that.FastAz=function(obj)//obj.b1,obj.b2,obj.b3,obj.az
		{
			obj.az='X';
			var start=0;
			var i=that.azcoder[obj.codon[0]]*16+that.azcoder[obj.codon[1]]*4+that.azcoder[obj.codon[2]];
			if (i>-1 && i<64)	
			{
				if (that.ST[i]=='M') start=1;
				obj.az=that.AA[i];
			}
			if (obj.az!='X') return start;

			if (obj.codon[0]=='|' || obj.codon[1]=='|' || obj.codon[2]=='|') 
			{
				obj.az='*';
				return start; 
			}
			obj.az=' ';
			return 0;
			
		}
		that.create_exons=function(sequence,_blockstart,_blockend,_contig_start,_contig_end)
		{
			that.frames.length=0;
			var minsize=0;
			if (that.usefilter && !that.pcsonly) minsize=that.orfminsize;
			if (that.usefilter && that.pcsonly) minsize=that.pcsminsize;
			var start=0;var stop=0;
			var obj={seqblk:null,pos:0,codon:[3],az:'X'}
			var ff=0;
			var init=0;
			var skip=false;
			var laststop_for=[3]
			var laststop_rev=[3];
			var inits_for=[3]
			var inits_rev=[3];
			var firstinit_for=[3];
			var firstinit_rev=[3];
			if (_blockstart<0) _blockstart=0;
			if (_blockstart>=sequence.blocks.length) _blockstart=sequence.blocks.length-1;
			if (_blockend<0) _blockend=0;
			if (_blockend>=sequence.blocks.length) _blockend=sequence.blocks.length-1;
			for (var m=_blockstart;m<=_blockend;m++)
			{
				var block=sequence.blocks[m];
				start=block.ContigToEntryPosition(_contig_start);
				stop=block.ContigToEntryPosition(_contig_end);
//				start=block.startbase;
//				stop=block.endbase;
				obj.seqblk=null;
				for (var i=0;i<3;i++)
				{
					laststop_for[i]=start;
					inits_for[i]=0;
					firstinit_for[i]=-1;
					laststop_rev[i]=start;
					inits_rev[i]=0;
					firstinit_rev[i]=-1;
				}
				var len=3*(Math.floor(stop-start)/3);
				for (var i=start;i<stop;i++)
				{
					ff=i%3;
					skip=false;
					obj.pos=i;
					block.entry.GetCodonFast(obj);
					init=that.FastAz(obj);
					if (init && inits_for[ff]==0) firstinit_for[ff]=i;
					if (init) inits_for[ff]++;
					if (obj.az=='*' || i-start+ff>=len-2)
					{
						if (that.pcsonly==true && inits_for[ff]==0) skip=true; 
						if (i-laststop_for[ff]<minsize) skip=true;
						if (skip==false) 
						{
							var exon={start:laststop_for[ff],stop:i,init:firstinit_for[ff],accession:block.entry.GetAccession(),frame:ff}
							that.frames.push(exon);
						}
						laststop_for[ff]=i;inits_for[ff]=0;firstinit_for[ff]=-1;
					}
					skip=false;
					var b1=obj.codon[0];
					obj.codon[0]=that.Complement(obj.codon[2]);
					obj.codon[1]=that.Complement(obj.codon[1]);
					obj.codon[2]=that.Complement(b1);
					init=that.FastAz(obj);

					if (init) {firstinit_rev[ff]=i;inits_rev[ff]++;}
					if (obj.az=='*' || i-start+ff>=len-2)
					{
						if (that.pcsonly==true && inits_rev[ff]==0) skip=true; 
						if (i-laststop_rev[ff]<minsize) skip=true;
						if (skip==false) 
						{
//							var exon={start:p1-start+block.startbasegaps,stop:p2-start+block.startbasegaps,init:firstinit,accession:block.entry.GetAccession(),frame:frm}
							var exon={start:laststop_rev[ff],stop:i,init:firstinit_rev[ff],accession:block.entry.GetAccession(),frame:ff+3}
							that.frames.push(exon);			
						}
						laststop_rev[ff]=i;inits_rev[ff]=0;firstinit_rev[ff]=-1;
					}
				}
			}
		}
		that.create_exons_entry=function(entry)
		{
	//		getseqtools()->selecttransltable(settings->translation_table);
			that.frames.length=0;
			var minsize=0;
			if (that.usefilter && !that.pcsonly) minsize=that.orfminsize;
			if (that.usefilter && that.pcsonly) minsize=that.pcsminsize;
			var start=0;var stop=0;
			var obj={seqblk:null,pos:0,base:'?'}
			var obj2={b1:'?',b2:'?',b3:'?',az:'X'}
			var p=0;var p2=0;var p1=0;var skip=0;
			for (var frm=0;frm<6;frm++)
			{
				var frameset=[];
//				for (var m=0;m<entry.GetLength();m++)
				{
					start=0;
					var ff=frm;if (frm>2) ff=frm-3;
					if (frm<3) {while (start%3!=ff) start++;}
					else {while ((entry.GetLength()-start)%3!=ff) start++;}
					stop=entry.GetLength();
					var laststop=start;var inits=0;var firstinit=-1;
					obj.seqblk=null;
					if (frm<3)
					{
						for (var i=start;i<stop;i+=3)
						{
							p=i;p2=start;p1=start;skip=0;
							obj.pos=i;
							entry.GetBaseFast(obj);obj2.b1=obj.base;obj.pos++;
							entry.GetBaseFast(obj);obj2.b2=obj.base;obj.pos++;
							entry.GetBaseFast(obj);obj2.b3=obj.base;
							var initdir=that.FastAz(obj2);
							var ch=obj2.az;
							if (initdir && inits==0) firstinit=p;
							if (initdir) inits++;
							if (ch!='*') continue;
							if (that.pcsonly && inits==0) skip=1; 
							if (!that.pcsonly) {p1=laststop;p2=p+3;}; 
							if (that.pcsonly) {p1=firstinit;p2=p+3;}; 
							if (p2-p1<minsize  || p1==p2) skip=1;
							if (!skip) 
							{
								var exon={start:p1,stop:p2,init:firstinit}
								frameset.push(exon);
							}
							laststop=p+3;inits=0;firstinit=-1;
						}
						if (laststop<stop)
						{
							skip=0;
							p=entry.GetLength();
							if (that.pcsonly && inits==0) skip=1; 
							if (!that.pcsonly) {p1=laststop;p2=stop;}; 
							if (that.pcsonly) {p1=firstinit;p2=stop;}; 
							if (p2-p1<minsize || p1<0 || p2<0) skip=1;
							if (!skip) 
							{
								var exon={start:p1,stop:p2,init:firstinit}
								frameset.push(exon);
							}
						}
					}
					if (frm>2)
					{
						for (var i=start;i<stop;i+=3)
						{
							p=i;p2=-1;p1=-1;skip=0;
							obj.pos=i;
							entry.GetBaseFast(obj);obj2.b1=obj.base;obj.pos++;
							entry.GetBaseFast(obj);obj2.b2=obj.base;obj.pos++;
							entry.GetBaseFast(obj);obj2.b3=obj.base;
	//						var initdir=Application.SeqTools.FastAz(obj2);
							var b1=obj2.b1;
							obj2.b1=that.Complement(obj2.b3);
							obj2.b2=that.Complement(obj2.b2);
							obj2.b3=that.Complement(b1);
							var initrev=that.FastAz(obj2);
							var ch=obj2.az;
							if (initrev) {firstinit=i;inits++;}
							if (ch!='*' && p!=stop) continue;
							if (that.pcsonly && inits==0) skip=1; 
							if (!that.pcsonly) {p1=laststop;p2=p;}; 
							if (that.pcsonly) {p1=laststop;p2=firstinit+3;}; 
							if (p2-p1<minsize || p1<0 || p2<0) skip=1;
							if (!skip) 
							{
								var exon={start:p1,stop:p2,init:firstinit}
								frameset.push(exon);			
							}
							if (ch=='*') {laststop=p;inits=0;firstinit=-1;}
						}
						if (laststop<stop)
						{
							skip=0;
							p=entry.GetLength();
							if (that.pcsonly && inits==0) skip=1; 
							if (!that.pcsonly) {p1=laststop;p2=p;}; 
							if (that.pcsonly) {p1=laststop;p2=firstinit+3;}; 
							if (p2-p1<minsize || p1<0 || p2<0) skip=1;
							if (!skip) 
							{
								var exon={start:p1,stop:p2,init:firstinit}
								frameset.push(exon);			
							}
						}
					}
				}
				that.frames.push(frameset);
			}
		}
		return that;
	}
