define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg","MRC/MSequenceClient","MRC/MEntryClient",],
function ($, Application, DQX, DocEl, Msg) 
{
	var MGenomeAlign={};
	MGenomeAlign.Init=function()
	{
		var that=this;
		that.gapcost=1;
		that.unitcost=1;
		that.diagonal_cutoff=150000;
		that.head_to_tail_cutoff=150000;
		that.StretchScoreSorter=[];
		that.Stretches=null;
		that.ClusterStretches=[];
		that.inclusion=[];
		that.StretchCount=0;
		that.ImportStretches=function(stretches)
		{
			that.Stretches=stretches;
		}
		that.Run=function(perform_align)
		{
			for (var i=0;i<that.StretchCount;i++) that.Stretches[i].include=0;
			that.ArrangeStretchesPosition();
			that.CreateStretchClusters(perform_align);
			for (var i=0;i<that.StretchCount;i++) that.Stretches[i].include=0;
//			that.MapClusterStretches(1);
		}
		that.ArrangeStretchesPosition=function()
		{
			that.Stretches.sort(function(a,b) { return b.x-a.x} );
		}
		that.ArrangeStretchesScore=function()
		{
			var cnt=that.Stretches.length;
			that.StretchScoreSorter.length=0;
			for (var i=0;i<cnt;i++) that.StretchScoreSorter.push(i);
			for (var gap=cnt/2;gap>0;gap/=2)
			{
				for (var i=gap;i<cnt;i++)
				{
					for (var j=i-gap;j>=0 && that.Stretches[j].score>that.Stretches[j+gap].score;j-=gap) 
					{
						var temp=that.StretchScoreSorter[j];
						that.StretchScoreSorter[j]=that.StretchScoreSorter[j+gap];
						that.StretchScoreSorter[j+gap]=temp;
					}
				}
			}
		}
		that.CreateStretchClusters=function(perform_align)
		{
			that.ArrangeStretchesScore();
//			for (var i=0;i<that.StretchScoreSorter.length;i++) that.ExtendClusterStretch(that.StretchScoreSorter[i],-1);
			if (!perform_align) return;
		}
		that.ExtendClusterStretch=function(stretch,clusterstretch)
		{
			if (that.Stretches[stretch].include) return;
			ori=that.Stretches[stretch].ori;
			that.inclusion.length=0;
			that.AddInclude(stretch);
			var k=that.FindBestUpScore(stretch);
			while (k!=-1) 
			{
				that.AddInclude(k);
				k=that.FindBestUpScore(k);
			}

			k=that.FindBestDownScore(stretch);
			while (k!=-1) 
			{
				that.AddInclude(k);
				k=that.FindBestDownScore(k);
			}
			if (clusterstretch==-1) clusterstretch=that.AddClusterStretch(ori);
			for (var i=0;i<that.inclusion.length;i++) that.ClusterStretchExtend(clusterstretch,that.inclusion[i]);
			that.ClusterStretchFillGaps(clusterstretch);
		}

		that.AddInclude=function(nr)
		{
			for (var i=0;i<inclusion.length;i++) if (that.inclusion[i]==nr) return;
			that.inclusion.push(nr);
		}

		that.FindBestUpScore=function(ss)
		{
			if (ss<0 || ss>=that.StretchCount) return -1;
			if (that.Stretches[ss].include) return -1;
			var x=that.Stretches[ss].x;
			var y=that.Stretches[ss].y;
			var line=x-y;
			var len=that.Stretches[ss].len;
			var stretch=-1;
			var oldscore=0;
			for (var i=ss-1;i>=0;i--)
			{
				if (that.Stretches[i].include || that.Stretches[i].ori!=that.Stretches[ss].ori) continue;
				var x1=that.Stretches[i].x;
				var y1=that.Stretches[i].y;
				var x2=x1+that.Stretches[i].len;
				var y2=y1+that.Stretches[i].len;
//				if (Math.abs(line-(x1-y1))>that.diagonal_cutoff) continue;
				if (Math.abs(line-(x1-y1))>that.diagonal_cutoff) break;
				if (x1>=x || y1>=y) continue;//stretch komt niet verder als vorige stretch
				var gap1=Math.abs((x1-y1)-(x-y));
				var gap2=Math.min(Math.abs(x2-x),Math.abs(y2-y));
				var maxd=that.gapcost*gap1+that.unitcost*gap2;
				var newscore=that.Stretches[i].score/Math.max(maxd,1);
				if (newscore>oldscore && newscore>0.001) {oldscore=newscore;stretch=i;}
				if (x1<x-that.head_to_tail_cutoff) return stretch;//als de stretches te ver in de x-richting komen, kan gestopt worden. Er wordt tegen de hoogste stretch-score vergeleken
			}
			return stretch;
		}

		that.FindBestDownScore=function(ss)
		{
			if (ss<0 || ss>=that.StretchCount) return -1;
			if (that.Stretches[ss].include) return -1;
			var x=that.Stretches[ss].x;
			var y=that.Stretches[ss].y;
			var line=x-y;
			var len=that.Stretches[ss].len;
			var stretch=-1;
			var oldscore=0;
			for (var i=ss+1;i<that.StretchCount;i++)
			{
				if (that.Stretches[i].include || that.Stretches[i].ori!=that.Stretches[ss].ori) continue;
				var x1=that.Stretches[i].x;
				var y1=that.Stretches[i].y;
				var x2=x1+that.Stretches[i].len;
				var y2=y1+that.Stretches[i].len;
//				if (Math.abs(line-(x1-y1))>that.diagonal_cutoff) continue;
				if (Math.abs(line-(x1-y1))>that.diagonal_cutoff) break;
				if (x2<=x+len || y2<=y+len) continue;//stretch komt niet verder als vorige stretch
				var gap1=Math.abs((x1-y1)-(x-y));
				var gap2=Math.min(Math.abs(x+len-x1),Math.abs(y+len-y1));
				var maxd=that.gapcost*gap1+that.unitcost*gap2;
				var newscore=that.Stretches[i].score/Math.max(maxd,1);
				if (newscore>oldscore && newscore>0.001) {oldscore=newscore;stretch=i;}
				if (x1>x+len+that.head_to_tail_cutoff) return stretch;//als de stretches te ver in de x-richting komen, kan gestopt worden. Er wordt tegen de hoogste stretch-score vergeleken
			}
			return stretch;
		}

		that.AddClusterStretch=function(ori)
		{
			clusterstretch=new MClusterStretch();
			clusterstretch.ori=ori;
			that.ClusterStretches.push(clusterstretch);
			return that.ClusterStretches.length-1;
		}
		that.ClusterStretchExtend=function(clusterstretch,stretch)
		{
			if (clusterstretch<0 || clusterstretch>=that.ClusterStretches.length) return 0;
			if (stretch<0 || stretch>=that.StretchCount) return 0;
			var _clusterstretch=that.ClusterStretches[clusterstretch];
			if (that.Stretches[stretch].ori!=_clusterstretch.ori) return 0;
			var in_stretch=that.Stretches[stretch];
			var x1=in_stretch.x;
			var x2=x1+in_stretch.len;
			for (var i=0;i<_clusterstretch.Items.length;i++)
			{
				var nr=_clusterstretch.Items[i];
				if (nr<0) continue;
				var cur_stretch=that.Stretches[nr];
				if (x1>cur_stretch.x && x2<cur_stretch.x+cur_stretch.len) return 0;
				if (stretch==nr) return 0;
			}
			if (_clusterstretch.AddStretch(stretch))
			{
				_clusterstretch.AddScore(that.Stretches[stretch].score);
				that.Stretches[stretch].include=1;
				return 1;
			}
			return 0;
		}
		return that;
	}
	return MGenomeAlign;
}
