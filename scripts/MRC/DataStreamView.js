define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/PopupFrame", "DQX/Popup","DQX/FrameCanvas","MRC/MScrollWindow","MRC/MScrollList"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, PopupFrame, Popup, FrameCanvas, MScrollWindow, MScrollList) {
 		var DataStreamView = {};
		DataStreamView.Panel = function (iParentRef,parent,changecallback) 
		{
            var that=MScrollList.Init(iParentRef);
			that.parent=parent;
			that.changeCallBack=changecallback;
			that.seqchannel=null;
			that.sequence=null;
			that.items=[];
			that.SetChannel=function(seqchannel,sequence,nr)
			{
				if (nr!=-1)
				{
					that.changeposit(nr,0,true);
				}
				if (that.seqchannel==seqchannel && that.sequence==sequence) return;

				that.seqchannel=seqchannel;
				that.sequence=sequence;
				that.resetFieldBar();
				var channel=that.seqchannel.GetChannel();
				for (var i=0;i<channel.fields.length;i++) 
				{
					var field=channel.fields[i];
					that.AddColumn(0,field.fieldcode,field.fieldlabel,field.fieldsize);
				}
				that.items.length=0;
				var accessions=sequence.GetAccessions();
				for (var i=0;i<channel.GetFullItemCnt();i++)
				{
					if (accessions.indexOf(channel.GetFullItemAccession(i))!=-1) that.items.push(i);;
				}
				that.SetDataCount(that.items.length);
				that.invalidate();
			}
			that.ChannelCallBack=function(channel,sequence,nr)
			{
				if (that.channel!=channel || sequence!=that.sequence) that.SetChannel(channel,sequence,nr);
			}
			that.GetFieldText=function(rowindex,field)
			{
				if (that.seqchannel==null) return 0;
				var channel=that.seqchannel.GetChannel();
				var index=that.items[rowindex];
				var item=channel.GetFullItem(index);
				if (item==null) return 0;
				
				if (field.fieldcode==='index') return rowindex+1;
				if (field.fieldcode==='start') return channel.GetFullItemStart(index);
				if (field.fieldcode==='stop') return channel.GetFullItemStop(index);
				if (field.fieldcode==='size') return channel.GetFullItemSize(index);
				if (field.fieldcode==='accession') return channel.GetFullItemAccession(index);
				if (channel.GetType()==="FeatureSearch")
				{
					if (field.fieldcode==='label') 
					{
						var label="";
						for (var i=0;i<item.qlfs.length;i++)
						{
							var qlf=item.qlfs[i];
							if (qlf.key==="/product=") label=qlf.item;
						}
						return label;
					}
				}
				return 0;
			}
			that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				ctx.fillStyle='rgba(0,0,0,1.0)';
				ctx.fillText(that.GetFieldText(rowindex,field),px+2,py+2);
			}
			that.ListMouseDown=function(posx,posy)
			{
				if (that.seqchannel==null) return;
				var channel=that.seqchannel.GetChannel();
				var row=that.GetHitRow(posy);
				var irow=that.ConvertToIndices(row);
				if (irow<0) return;
				var col=that.GetHitCol(posx);
				var colfield=that.GetFieldSel();
//				if (colfield.fieldcode==='Start')
				{
					var index=that.items[irow];
					var item=channel.GetFullItem(index);
					if (item!=null) that.changeCallBack(channel,index);
				}
			}
			return that;
		}
		return DataStreamView;
    });