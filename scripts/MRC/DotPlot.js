/************************************************************************************************************************************
 *************************************************************************************************************************************

 A FramePanel that implements a html Canvas drawing area

 *************************************************************************************************************************************
 *************************************************************************************************************************************/
		
function MatchStretch()
{
	this.x=0;
	this.y=0;
	this.len=0;
	this.score=0.0;
}

define(["jquery", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScrollWindow"],
    function ($, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScrollWindow) {
        var DotPlot = {};
		DotPlot.Panel = function (iParentRef) 
		{
            var that = MScrollWindow.Init(iParentRef);
			that.direct_stretches=[];
			that.invert_stretches=[];
			that.seqlen1=0;
			that.seqlen2=0;
			that.zoom=0.00005;
			that.AddDirectStretch=function(x,y,len,score)
			{
				var stretch=new MatchStretch;
				stretch.x=parseInt(x);
				stretch.y=parseInt(y);
				stretch.len=parseInt(len);
				that.direct_stretches.push(stretch);
			}
			that.AddInvertStretch=function(x,y,len,score)
			{
				var stretch=new MatchStretch;
				stretch.x=parseInt(x);
				stretch.y=parseInt(y);
				stretch.len=parseInt(len);
				that.invert_stretches.push(stretch);
			}
			that.SetSeqlen1=function(len)
			{
				that.seqlen1=len;
				that.SetHScrollSize(len*that.zoom);
			}
			that.SetSeqlen2=function(len)
			{
				that.seqlen2=len;
				that.SetVScrollSize(len*that.zoom);
			}
			that.draw=function()
			{
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				
				var right=that._cnvWidth;
				var bottom=that._cnvHeight;
				
				var zoom=that.zoom;
				var xcurpos=0;
				var ycurpos=0;
				
				var context=that._cnvContext;
				context.fillStyle="rgba(100,100,100,0.2)";
			//	context.fillRect(xoo,yoo,seqlen1*zoom,seqlen2*zoom);

				context.strokeStyle="rgba(0,0,0,1)";
				context.lineWidth=1;
				context.beginPath();
				x1=xcurpos*zoom+xoo;
				context.moveTo(x1,0+yoo);
				context.lineTo(x1,that.seqlen2*zoom+yoo);
				context.stroke();
				context.beginPath();
				y1=ycurpos*zoom+yoo;
				context.moveTo(0+xoo,y1);
				context.lineTo(that.seqlen1*zoom+xoo,y1);
				context.stroke();
				
				context.beginPath();
				context.strokeStyle="rgba(0,0,0,1)";
				for (var i=0;i<that.direct_stretches.length;i++)
				{
					x1=~~(that.direct_stretches[i].x*zoom+xoo);
					y1=~~(that.direct_stretches[i].y*zoom+yoo);
					x2=~~((that.direct_stretches[i].x+that.direct_stretches[i].len)*zoom+xoo);
					y2=~~((that.direct_stretches[i].y+that.direct_stretches[i].len)*zoom+yoo);
					if (x1==x2) x2++;
					if (y1==y2) y2++;
					if (x1<0 || x1>right) continue;
					if (y1<0 || y1>bottom) continue;
			//		context.beginPath();
					context.moveTo(x1,y1);
					context.lineTo(x2,y2);
			//		context.stroke();*/
			//		if (stretches[i].ori>0) context.fillStyle="rgba(0,0,0,1)";else context.fillStyle="rgba(255,0,0,1)"
			//		context.fillRect(x1,y1,1,1);
				}
				context.stroke();
				context.strokeStyle="rgba(255,0,0,1)"
				context.beginPath();
				for (var i=0;i<that.invert_stretches.length;i++)
				{
					x1=~~(that.invert_stretches[i].x*zoom+xoo);
					y1=~~((that.seqlen2-that.invert_stretches[i].y)*zoom+yoo);
					x2=~~((that.invert_stretches[i].x+that.invert_stretches[i].len)*zoom+xoo);
					y2=~~((that.seqlen2-that.invert_stretches[i].y-that.invert_stretches [i].len)*zoom+yoo);
					if (x1==x2) x2++;
					if (y1==y2) y2++;
					if (x1<0 || x1>right) continue;
					if (y1<0 || y1>bottom) continue;
					context.moveTo(x1,y1);
					context.lineTo(x2,y2);
			//		if (stretches[i].ori>0) context.fillStyle="rgba(0,0,0,1)";else context.fillStyle="rgba(255,0,0,1)"
			//		context.fillRect(x1,y1,1,1);
				}
				context.stroke();
			}
			that.ZoomIn=function()
			{
				that.zoom=that.zoom+that.zoom/10;
				that.invalidate();
			}
			that.ZoomOut=function()
			{
				that.zoom=that.zoom-that.zoom/10;
				that.invalidate();
			}
			return that;
        };
		return DotPlot;
    });