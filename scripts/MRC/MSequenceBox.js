define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/FrameCanvas","MRC/MScroller","MRC/MFtsSearch","MRC/MFtsSearchWizard","MRC/MSequenceClient","MRC/MEntryClient","MRC/MFrames",],
function ($, Application, DQX, DocEl, Msg, FramePanel, FrameCanvas, MScroller, MPlotChannel, MFtsSearchWizard) 
{
	var MSequenceBox={};
	MSequenceBox.Init=function()
	{
		var that=this;
		that.Identifier="";
		that.Links=[];
		that.Sequences=[];
		that.Channels=[];
		that.maxseqlen=0;
		
		that.FtsSearchFinished=null;
		that.FrameMappingFinished=null;
		
		that.ClassObjectCodes=[];
		that.requestNumber=0;
		
		that.concatenate=false;
		that.concatenateGap=0;

		that.statuscallbackfunction=null;
		that.callbackfuncion=null;
		
		that.SetIdentifier=function(id)
		{
			that.Identifier=id;
		};
		that.GetIdentifier=function()
		{
			return that.Identifier;
		};
		that.AddChannel=function(channel)
		{
			for (var i=0;i<that.Channels.length;i++) if (that.Channels[i]==channel) return;
			that.Channels.push(channel);
		}
		that.FindChannel=function(accession)
		{
			for (var i=0;i<that.Channels.length;i++) if (that.Channels[i].accession==accession) return that.Channels[i];
			return null;
		}
		that.FindChannelByLabel=function(label)
		{
			for (var i=0;i<that.Channels.length;i++) if (that.Channels[i].label==label) return that.Channels[i];
			return null;
		}
		that.SwitchChannel=function(state,accession)
		{
			channel=that.FindChannel(accession);
			if (channel!=null) channel.SetViewState(state);
		}
		that.Clear=function()
		{
			that.Sequences.length=0;
			that.Links.length=0;
			that.ClassObjectCodes.length=0;
			that.requestNumber=0;
		}
		that.AddClassObjectCode=function(classobjcode)
		{
			that.ClassObjectCodes.push(classobjcode);
		}
		that.OpenSequences=function(statuscallback,callbackfunc)
		{
			that.statuscallbackfunction=statuscallback;
			that.callbackfunction=callbackfunc;
			that.requestNumber=0;
			that.OpenNextSequence();
		}
		that.OpenNextSequence=function()
		{
			if (that.requestNumber==that.ClassObjectCodes.length)
			{
//				DQX.stopAllProcessing();
				if (that.statuscallbackfunction!=null) that.statuscallbackfunction("");
				if (that.callbackfunction!=null) that.callbackfunction();
				return;
			}
			if (that.requestNumber<0 || that.requestNumber>=that.ClassObjectCodes.length) return;
			var classobjectcode=that.ClassObjectCodes[that.requestNumber];
			var classobject=Application.FindClassObject(classobjectcode);
			if (classobject==null) return;
			var path=Application.DecodePath(classobject.pathcode);
			path=path+"/"+classobject.filename;
			if (that.statuscallbackfunction!=null) that.statuscallbackfunction("Opening sequence "+path);
			var command=JSON.stringify( {'type':'OpenSequenceFile', 'filelocation':path,'sequence_box_code':that.GetIdentifier(),'classobjectcode':classobjectcode});
			Application.ClientConnection.send(command);
		}

		that.ServerResponse=function(json)
		{
			if (json.processing=='finished')
			{
				var sequence=null;
				if (that.concatenate) sequence=that.GetSequence(0);//concatenate sequences in one clientsequence
				if (sequence==null)	
				{
					sequence=new MSequenceClient();
					that.Sequences.push(sequence);
					var channel=that.FindChannel("Sequence data");
					if (channel==null)
					{
						channel=new MPlotChannel.Sequence(this);
						channel.SetAccession("Sequence data");
						channel.SetLabel("Sequence data");
						channel.SetColor("rgb(255,0,0)");
					}
					sequence.AddChannel(channel);
					
					channel=that.FindChannel("Sequence numbering");
					if (channel==null)
					{
						channel=new MPlotChannel.Numbering(this);
						channel.SetAccession("Sequence numbering");
						channel.SetLabel("Sequence numbering");
						channel.SetColor("rgb(180,180,180)");
						channel.SetViewState(false);
					}
					var seqchannel=sequence.AddChannel(channel);
					seqchannel.SetLocation(-1);
				}
				var cliententry=new MEntryClient();
				cliententry.SetHeader(json.header);
				cliententry.SetFeatureTable(json.featuretable);
				cliententry.SetSequence(json.sequence);
				sequence.label=cliententry.GetAccession();
	//			block=sequence.AddBlockFull(cliententry,true,100000);
				block=sequence.AddBlockFull(cliententry,false,0);
	//			block=sequence.AddBlock(cliententry,10,3,14,true,0,false);
	//			block=sequence.AddBlock(cliententry,60,1,50,true,0,false);
//				for (var i=0,k=0;i<10;i++) 
//				{
//					sequence.AddBlock(cliententry,k,i*10000,i*10000+10000,false,0,false);
//					k+=11000;
//				}
				sequence.UpdateAccessions();
				if (sequence.GetLength()>that.maxseqlen) that.maxseqlen=sequence.GetLength();
				var obj={'classobjectcode':json.classobjectcode,'entry':cliententry,'sequence':sequence}
				that.Links.push(obj);
			}
			else
			{
				Application.CreateTrack(JSON.stringify(json.processing));
			}
			that.requestNumber++;
			that.OpenNextSequence();
		}
		
		that.copyFromSequenceBox=function(_seqbox)
		{
			that.Clear();
			for (var i=0;i<_seqbox.ClassObjectCodes.length;i++) that.ClassObjectCodes.push(_seqbox.ClassObjectCodes[i]);
			for (var i=0;i<_seqbox.Links.length;i++)
			{
				var link=_seqbox.Links[i];
				var cliententry=link.entry;
				var classobjectcode=that.ClassObjectCodes[i];
				var sequence=null;
				if (that.concatenate) sequence=that.GetSequence(0);//concatenate sequences in one clientsequence
				if (sequence==null)	
				{
					sequence=new MSequenceClient();
					that.Sequences.push(sequence);
					var channel=that.FindChannel("Sequence data");
					if (channel==null)
					{
						channel=new MPlotChannel.Sequence(this);
						channel.SetAccession("Sequence data");
						channel.SetLabel("Sequence data");
						channel.SetColor("rgb(255,0,0)");
					}
					sequence.AddChannel(channel);
					
					channel=that.FindChannel("Sequence numbering");
					if (channel==null)
					{
						channel=new MPlotChannel.Numbering(this);
						channel.SetAccession("Sequence numbering");
						channel.SetLabel("Sequence numbering");
						channel.SetColor("rgb(180,180,180)");
						channel.SetViewState(true);
					}
					var seqchannel=sequence.AddChannel(channel);
					seqchannel.SetLocation(-1);
				}
				sequence.label=cliententry.GetAccession();
				block=sequence.AddBlockFull(cliententry,false,that.concatenateGap);
				sequence.UpdateAccessions();
				if (sequence.GetLength()>that.maxseqlen) that.maxseqlen=sequence.GetLength();
				var obj={'classobjectcode':classobjectcode,'entry':cliententry,'sequence':sequence}
				that.Links.push(obj);
			}
		}
		
		that.UpdateMaxSeqLen=function()
		{
			that.maxseqlen=0;
			for (var i=0;i<that.Sequences.length;i++) 
			{
				var sequence=that.Sequences[i];
				if (that.maxseqlen<sequence.GetLength()) that.maxseqlen=sequence.GetLength();
			}
			return that.maxseqlen;
		}
		that.GetMaxSeqLen=function()
		{
			return that.maxseqlen;
		}
		that.GetSequence=function(nr)
		{
			if (nr<0 || nr>=that.Sequences.length) return null;
			return that.Sequences[nr];
		}
		that.GetSequenceCount=function()
		{
			return that.Sequences.length;
		}
		that.GetEntryFromCode=function(classobjectcode)
		{
			for (var i=0;i<that.Links.length;i++) if (that.Links[i].classobjectcode===classobjectcode) return that.Links[i].entry;
			return null;
		}
		that.GetSequenceFromCode=function(classobjectcode)
		{
			for (var i=0;i<that.Links.length;i++) if (that.Links[i].classobjectcode===classobjectcode) return that.Links[i].sequence;
			return null;
		}
		that.FetchAllAccessions=function()
		{
			//verzamel alle accessies van de sequenties
			var obj={accessions:[],features:[],searchtext:""}
			for (var i=0;i<that.Sequences.length;i++) 
			{
				var _accessions=that.Sequences[i].GetAccessions();
				for (var j=0;j<obj.accessions.length;j++)
				{
					for (var k=0;k<_accessions.length;k++)	if (obj.accessions[j]===_accessions[k]) _accessions.splice(k,1);//kijk of de accessie al in de lijst zit
				}
				obj.accessions=obj.accessions.concat(_accessions);
			}
			return obj;
		}
		that.GetSequences=function()
		{
			return that.Sequences;
		}

// feature search module
		that.PerformFtsSearch=function(ftskey,callback,label)//functie zoekt met opgave van feature keys
		{
			//verzamel alle accessies van de sequenties
			var obj=that.FetchAllAccessions();
			obj.features.push(ftskey);
			var code=Application.DataBaseModule.NewUUID();
			var ftssearch=new MPlotChannel.FtsSearch(that);
			ftssearch.SetAccession(code);
			ftssearch.SetLabel(label);
			ftssearch.SetSize(16);
			obj.code=code;
			that.SendFtsSearch(obj);
			that.FtsSearchFinished=callback;
			return ftssearch;
		}
		that.CallFtsSearchWizard=function(callback,finished)// functie zoekt over wizard opvrage
		{
			//verzamel alle accessies van de sequenties
			var obj=that.FetchAllAccessions();
			var code=Application.DataBaseModule.NewUUID();
			var wizard=MFtsSearchWizard.init(obj);
			wizard.execute(function()
			{
				var ftssearch=new MPlotChannel.FtsSearch(that);
				ftssearch.SetAccession(code);
				ftssearch.SetLabel(JSON.stringify(obj.features));
				ftssearch.SetSize(16);
				obj.code=code;
				that.SendFtsSearch(obj);
				callback(ftssearch);
				that.FtsSearchFinished=finished;
			});
		}
		that.SendFtsSearch=function(data)
		{
			var command=JSON.stringify( {'type':'FeatureSearchRequest','sequence_box_code':that.GetIdentifier(),'feature_search_accession':data.code,'sequence_accessions':data.accessions,'features':data.features,'search': data.searchtext});
			Application.ClientConnection.send(command);
		}
		
		that.ServerResponseFtsSearch=function(json)
		{
			var featuresearch=that.FindChannel(json.feature_search_accession);
			if (featuresearch!=null) featuresearch.SetFeatureTable(json.featuretable);
			if (that.FtsSearchFinished!=null) that.FtsSearchFinished(featuresearch);
		}
//sequence search
		that.PerformSeqSearch=function(seqtofind,callback,label)
		{
			//verzamel alle accessies van de sequenties
			var obj=that.FetchAllAccessions();
			var code=Application.DataBaseModule.NewUUID();
			var seqsearch=new MPlotChannel.SeqSearch(that);
			seqsearch.SetAccession(code);
			seqsearch.SetLabel(label);
			obj.code=code;
			that.SendSeqSearch(obj);
			that.SeqSearchFinished=callback;
			return seqsearch;
		}
		that.SendSeqSearch=function(data)
		{
			var command=JSON.stringify( {'type':'SequenceSearchRequest','sequence_box_code':that.GetIdentifier(),'sequence_search_accession':data.code,'sequence_accessions':data.accessions,'search': data.searchtext});
			Application.ClientConnection.send(command);
		}
		that.ServerResponseSeqSearch=function(json)
		{
			var seqsearch=that.FindChannel(json.sequence_search_accession);
			if (seqsearch!=null) seqsearch.SetFoundHits(json.foundhits);
			if (that.SeqSearchFinished!=null) that.SeqSearchFinished(seqsearch);
		}
// frame mapping module
		that.CallFrameMappingWizard=function(callback,finished)// functie zoekt over wizard opvrage
		{
			//verzamel alle accessies van de sequenties
			var obj=that.FetchAllAccessions();
			var code=Application.DataBaseModule.NewUUID();
			var wizard=MFtsSearchWizard.init(obj);
			wizard.execute(function()
			{
				var framemapping=new MPlotChannel.FrameSearch(that);
				framemapping.SetAccession(code);
				framemapping.SetLabel(JSON.stringify(obj.features));
				framemapping.SetSize(30);
				obj.code=code;
//				that.SendFrameMapping(obj);
				callback(framemapping);
				that.FrameMappingFinished=finished;
				if (that.FrameMappingFinished!=null) that.FrameMappingFinished(framemapping);
			});
		}
		that.SequenceFrameMappingRequest=function(json)
		{ 
			var data=json;
//			console.log(json);
			var frameobject=new MFrames();
			for (var i=0;i<that.Channels.length;i++)
			{
				var channel=that.Channels[i];
				if (that.Channels[i].GetType()!="FrameMapping") continue;
				for (var j=0;j<data.length;j++) 
				{
					frameobject.create_exons(that.Sequences[data[j].seqindex],data[j].firstblock,data[j].lastblock,data[j].startbase,data[j].endbase);
					var frames=frameobject.frames;
					channel.SetFoundHits(frames);
				}
				channel.LinkAllSequences();
			}
//			if (that.FrameMappingFinished!=null) that.FrameMappingFinished(framemapping);
		}
		that.SendFrameMapping=function(data)
		{
			var command=JSON.stringify( {'type':'SequenceFrameMappingRequest','sequence_box_code':that.GetIdentifier(),'feature_search_accession':data.code,'sequence_accessions':data.accessions,'features':data.features,'search': data.searchtext});
			Application.ClientConnection.send(command);
		}
		that.ServerResponseFrameMapping=function(json)
		{
			var framemapping=that.FindChannel(json.channel);
			if (framemapping==null) return;
			framemapping.SetFoundHits(json.foundhits);
			framemapping.LinkAllSequences();
			if (that.FrameMappingFinished!=null) that.FrameMappingFinished(framemapping);
		}
		that.UpdateSequenceBitPlots=function()
		{
			that.UpdateMaxSeqLen();
			for (var i=0;i<that.Sequences.length;i++) that.Sequences[i].MapBlocks(that.maxseqlen);
		}
		return that;
	}
	return MSequenceBox;
});