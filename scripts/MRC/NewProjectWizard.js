define(["require", "DQX/Application","DQX/Framework", "DQX/Controls", "DQX/Msg", "DQX/SQL", "DQX/DocEl", "DQX/Popup", "DQX/Wizard"],
    function (require, Application, Framework, Controls, Msg, SQL, DocEl, Popup, Wizard) 
	{
        var NewProjectWizard = {};
        NewProjectWizard.init=function(data)
		{
            var that={};
			that.data=data;
			that.wizard=Wizard.Create("WizardFindGene");
			that.wizard.setTitle("Create new Project");
			that.wizard._sizeY=450;
			var buttonList=[];
			
			comment=Controls.Static('Project file name:').makeComment();
			buttonList.push(comment);
			
			that.projectFileName=Controls.Edit('ProjectFileName', { value:that.data.projectFileName, size: 75 }).setHasDefaultFocus();
			that.projectFileName.setOnChanged(function(controlID, theControl) {that.data.projectFileName=theControl.getValue();});
			buttonList.push(that.projectFileName);

			comment=Controls.Static('Project display name:').makeComment();
			buttonList.push(comment);
			
			that.projectDisplayName=Controls.Edit('ProjectDisplayName', { value:that.data.projectDisplayName,size: 75 }).setHasDefaultFocus();
			that.projectDisplayName.setOnChanged(function(controlID, theControl) {that.data.projectDisplayName=theControl.getValue();});
			buttonList.push(that.projectDisplayName);

			
			var comment=Controls.Static('Select project type:').makeComment();
			buttonList.push(comment);

            that.ProjectTypeSelect=Controls.List(null,{width:230,height : 160,checkList:true});
            that.ProjectTypeSelect.setOnChanged(function(controlID, theControl) {that.GetCheckedItems();})
			buttonList.push(that.ProjectTypeSelect);

			listitems=[];
			var obj={id:0,content:"Sequence Comparison",checked:true};
			listitems.push(obj);
			that.ProjectTypeSelect.setItems(listitems);
//			that.data.features.push("CDS");
			

			that.wizard.addPage({id: 'init',helpUrl: 'Doc/WizardFindGene/Help.htm',form: Controls.CompoundVert([Controls.CompoundVert(buttonList), Controls.CompoundHor([])]),hideNext: true	});

			that.execute=function(sendfunction)
			{
				that.wizard.run(sendfunction);
			}
			that.GetCheckedItems=function()
			{
				for (var i=0;i<that.ProjectTypeSelect._items.length;i++) 
				{
                    if (that.ProjectTypeSelect._items[i].checked) that.data.projectType=that.ProjectTypeSelect._items[i].content;
				}
			}
			return that;
		}
        return NewProjectWizard;
    });
