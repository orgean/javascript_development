define(["jquery", "DQX/Application", "DQX/Utils", "DQX/DocEl", "DQX/Msg", "DQX/FramePanel", "DQX/PopupFrame", "DQX/Popup","DQX/FrameCanvas","MRC/MScrollWindow","MRC/MScrollList","MRC/SeqPlot","MRC/MFtsSearch"],
    function ($, Application, DQX, DocEl, Msg, FramePanel, PopupFrame, Popup, FrameCanvas, MScrollWindow, MScrollList, SeqPlot, MPlotChannel) {
        var DotPlotCell={};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	

		DotPlotCell.HScale=function (iParentRef,parent,plot) 
		{
            var that=MScrollWindow.Init(iParentRef,false);
			that.parent=parent;
			that.plot=plot;
			that.sel1=0;
			that.sel2=0;
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgb(80,80,80)";
				context.fillRect(0,0,that._cnvWidth,that._cnvHeight);
				var xoo=-that.plot.GetHScrollPos();

				if (that.plot.cell==null) return;
				var step=Application.SeqTools.GetScale(that.plot.zoom);
				context.font = "11px serif";
				context.textAlign="center";

				context.fillStyle="rgb(255,125,0)";
				x1=(Math.round(that.plot.getXStart()/step))*step;
				x2=(Math.round(that.plot.getXStop()/step))*step;
				if (x1<0) x1=0;//if (x2>that.line_b2) x2=that.line_b2;
				if (x1<0) x1=0;if (x2>that.plot.cell.ll1) x2=that.plot.cell.ll1;
				var mul=10*step;
				for (var i=x1;i<x2;i+=step) 
				{
					var x=that.plot.convert(i);
					if (i%step==0 && i>0) context.fillRect(x+xoo,that._cnvHeight-5,1,5);
					if (i%mul==0 && i>0)
					{
						context.fillRect(x+xoo,that._cnvHeight-10,1,10);
						context.fillText(i,x+xoo,that._cnvHeight-12);
					}
				}
			}
			that.MouseDown=function(posx,posy)  //overwrite
			{
				var xoo=-that.plot.GetHScrollPos();
				var xcurpos=parseInt((posx-xoo)/that.plot.zoom);
				if (that.sel1!=that.sel2)
				{
					that.sel1=xcurpos;
					that.sel2=xcurpos;
				}
				else
				{
					that.sel2=xcurpos;
				}
				that.render();
			}
			return that;
		}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	

		DotPlotCell.VScale=function (iParentRef,parent,plot) 
		{
            var that=MScrollWindow.Init(iParentRef,false);
			that.parent=parent;
			that.plot=plot;
			that.sel1=0;
			that.sel2=0;
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgb(80,80,80)";
				context.fillRect(0,0,that._cnvWidth,that._cnvHeight);
				var yoo=-that.plot.GetVScrollPos();

				if (that.plot.cell==null) return;
				context.font = "11px serif";
				var step=Application.SeqTools.GetScale(that.plot.zoom);
				
				context.fillStyle="rgb(255,125,0)";
				y1=(Math.round(that.plot.getYStart()/step))*step;
				y2=(Math.round(that.plot.getYStop()/step))*step;
				if (y1<0) y1=0;if (y2>that.plot.cell.ll2) y2=that.plot.cell.ll2;
				var mul=10*step;
				for (var i=y1;i<y2;i+=step) 
				{
					var y=that.plot.convert(i)+that.GetXSize();
					if (i%step==0 && i>0) context.fillRect(that._cnvWidth-5,y+yoo,5,1);
					if (i%mul==0 && i>0)
					{
						context.fillRect(that._cnvWidth-10,y+yoo,10,1);
						context.save();
						context.translate(that._cnvWidth-12,y+yoo);
						context.rotate(-Math.PI/2);
						context.textAlign="center";
						context.fillText(i,0,0);
						context.restore();						
					}
				}
				context.fillStyle="rgb(80,80,80)";
				context.fillRect(0,0,that._cnvWidth,that._cnvWidth);
			}
			that.MouseDown=function(posx,posy)  //overwrite
			{
				var yoo=-that.plot.GetVScrollPos();
				var ycurpos=parseInt((posy-yoo-that.GetXSize())/that.plot.zoom);
				if (that.sel1!=that.sel2)
				{
					that.sel1=ycurpos;
					that.sel2=ycurpos;
				}
				else
				{
					that.sel2=ycurpos;
				}
				that.render();
			}
			return that;
		}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	


		DotPlotCell.Filtered=function (iParentRef,parent) 
		{
            var that=MScrollWindow.Init(iParentRef,true);
			that.parent=parent;
			that.zoom=0.0001;
			that.project=null;
			that.cell=null;
			that.xcurpos=0;
			that.xsel1=0;
			that.xsel2=0;
			that.ycurpos=0;
			that.ysel1=0;
			that.ysel2=0;
			that.init=false;

			that.pixelCanvas=document.createElement('canvas');
			that.pixelCanvas.width=500;
			that.pixelCanvas.height=400;
			that.pixelCanvasContext=that.pixelCanvas.getContext('2d');
			that.paintcnt=0;
			
			that.r_colors=[];
			that.g_colors=[];
			that.b_colors=[];
			for (var k=0;k<1000;k++)
			{
				that.r_colors.push(100+Math.floor(Math.random()*150));
				that.g_colors.push(100+Math.floor(Math.random()*150));
				that.b_colors.push(100+Math.floor(Math.random()*150));
			}

			that.SetProject=function(_project)
			{
				that.project=_project;
				that.dotplot=_project.dotplot;
			}
			that.setCell=function(_cell)
			{
				if (that.cell==_cell) return;
				that.cell=_cell;
				that.UpdateFacts(true);
			}
			that.UpdateCellCallback=function()
			{
//				if (that.cell!=null) that.zoom=1200/Math.max(Math.max(that.cell.ll1,that.cell.ll2),1);
				if (that.init==false && that.cell!=null)
				{
					that.zoom=1200/Math.max(Math.max(that.cell.ll1,that.cell.ll2),1);
					that.init=true;
				}
				that.UpdateFacts(true);
				that.SetProcessing("");
//				that.invalidate();
			}
			that.UpdateFacts=function(update)
			{
				if (that.init==false && that.cell!=null)
				{
					that.zoom=1200/Math.max(Math.max(that.cell.ll1,that.cell.ll2),1);
					that.init=true;
				}
				that.UpdateScroll();
				if (update) that.invalidate();
			}
			that.UpdateScroll=function()
			{
				if (that.project==null || that.cell==null) return;
				that.SetHScrollSize(that.convert(that.cell.ll1));
				that.SetVScrollSize(that.convert(that.cell.ll2));
				that.ReCalcScrollers();
			}
			that.convert=function(x) 
			{
				return x*that.zoom; 
			} 


			that.draw=function()
			{
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				var context=that._cnvContext;
				if (that.pixelCanvas.width!=that._cnvWidth || that.pixelCanvas.height!=that._cnvHeight)
				{
					that.pixelCanvas.width=that._cnvWidth;
					that.pixelCanvas.height=that._cnvHeight;
					that.imageData.SetImageData(that.pixelCanvasContext,that._cnvWidth,that._cnvHeight);
				}
				that.imageData.DrawBackGround(0,0,0,0);

				context.fillStyle="rgba(80,80,80,1.0)";
				context.fillRect(0,0,lx,ly);
				if (that.project==null || that.cell==null) return;
				var dotplot=that.project.dotplot;
				var xoo=Math.floor(-that.GetHScrollPos()); 
				var yoo=Math.floor(-that.GetVScrollPos());
				var l1=that.convert(that.cell.ll1);
				var l2=that.convert(that.cell.ll2);
				context.fillStyle="rgba(0,0,0,1.0)";
				context.fillRect(xoo,yoo,l1,l2);

				var r=255;var g=0;var b=0;var _x=0;var _y=0;
				var x1=0;var x2=0;var y1=0;var y2=0;var p=0;

				context.beginPath();
				context.strokeStyle="rgb(255,0,0)";
				x1=that.convert(that.xcurpos)+xoo;
				y1=that.convert(that.ycurpos)+yoo;
				context.moveTo(x1,0);
				context.lineTo(x1,l2);
				context.moveTo(0,y1);
				context.lineTo(l1,y1);
				context.closePath();
				context.stroke();
				
				var image=that.imageData;
				var zoom=that.zoom;
				var ll2=that.cell.ll2;
				var func=Math.floor;
				var _stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
				var i=dotplot.GetStretchCount();
				var data=image.data;
				var width=image.width;
				var length=image.length;
				while (i--)
				{
					dotplot.GetStretch(i,_stretch);
					x1=func(_stretch.x*zoom)+xoo;
					y1=func(_stretch.y*zoom)+yoo;
					if (_x!=x1 || _y!=y1)
					{
						x2=func((_stretch.x+_stretch.len)*zoom)+xoo;
						if (x2>=0 && x1<=width)
						{
							if (_stretch.ori>0) 
							{
//								y1=func(y*zoom)+yoo;
								r=100;g=162;b=255;
								if (_stretch.include==2) {r=150;g=255;b=150;} 
								if (_stretch.include==3) {r=255;g=255;b=255;} 
								r=that.r_colors[_stretch.include];
								g=that.g_colors[_stretch.include];
								b=that.b_colors[_stretch.include];
								if (_stretch.include<3) {r=255;g=255;b=255;} 
								while (x1<=x2) 
								{
									p=y1*width*4+x1*4;
									if (p>=0 && p+4<length)
									{
										data[p]=r;
										data[p+1]=g;
										data[p+2]=b;
										data[p+3]=255;
									}
									x1++;
									y1++;
								}
							}
							else 
							{
								y1=func((ll2-_stretch.y)*zoom)+yoo;
								r=255;g=50;b=50;
								if (_stretch.include==2) {r=150;g=255;b=150;}
								r=that.r_colors[_stretch.include];
								g=that.g_colors[_stretch.include];
								b=that.b_colors[_stretch.include];
								if (_stretch.include<3) {r=255;g=255;b=255;} 
								while (x1<=x2) 
								{
									p=y1*width*4+x1*4;
									if (p>=0 && p+4<length)
									{
										data[p]=r;
										data[p+1]=g;
										data[p+2]=b;
										data[p+3]=255;
									}
									x1++;
									y1--;
								}
							}
						}
					}
					_x=x1;
					_y=y1;
				}
				image.ResolveImageData();
				context.drawImage(that.pixelCanvas,0,0);
			}

			that.post_paint=function()
			{
			}
			
			that.zscrollposchanged=function(zscrollpos,zscrollmax,deltascroll)
			{
				if (that.project==null || that.cell==null) return;
				var old=that.zoom;
				var minzoom=400.0/that.cell.ll1;
				if (zscrollpos<zscrollmax/2)
				{
					var m=zscrollmax/2-zscrollpos;
					that.zoom=that.zoom-0.4*m/zscrollmax*that.zoom;
					if (that.zoom<=minzoom) that.zoom=minzoom;
				}
				if (zscrollpos>=zscrollmax/2)
				{
					var m=zscrollpos-zscrollmax/2;
					that.zoom=that.zoom+0.4*m/zscrollmax*that.zoom;
					if (that.zoom>=1.0) that.zoom=1.0;
				}
				if (that.zoom!=old)
				{
					that.oldzoom=that.zoom;
					that.UpdateFacts(false);
					that.scrolltoxypos();
				}
			}
			that.scrolltoxypos=function()
			{
				that.SetNewHVScrollPos(Math.max(that.xcurpos*that.zoom-100,0),Math.max(that.ycurpos*that.zoom-100,0));
			}
			that.MouseDown=function(posx,posy)  //overwrite
			{
				if (that.project==null || that.cell==null) return;
				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				var xcurpos=parseInt((posx-xoo)/that.zoom);
				var ycurpos=parseInt((posy-yoo)/that.zoom);
				var index=that.project.dotplot.GetStretchFromPos(xcurpos,ycurpos);
				parent.CellSelectStretch(index,xcurpos,false,true,true,true);
			}
			that.getXStart=function()
			{
				if (that.cell==null) return 0;
				var xoo=-that.GetHScrollPos(); 
				var p=parseInt((0-xoo)/that.zoom);
				p=Math.max(0,p);
				p=Math.min(p,that.cell.ll1);
				return p;
			}
			that.getXStop=function()
			{
				if (that.cell==null) return 0;
				var xoo=-that.GetHScrollPos(); 
				var p=parseInt((that._cnvWidth-xoo)/that.zoom);
				p=Math.max(0,p);
				p=Math.min(p,that.cell.ll1);
				return p;
			}
			that.getYStart=function()
			{
				if (that.cell==null) return 0;
				var yoo=-that.GetVScrollPos(); 
				var p=parseInt((0-yoo)/that.zoom);
				p=Math.max(0,p);
				p=Math.min(p,that.cell.ll2);
				return p;
			}
			that.getYStop=function()
			{
				if (that.cell==null) return 0;
				var yoo=-that.GetVScrollPos();
				var p=parseInt((that._cnvHeight-yoo)/that.zoom);
				p=Math.max(0,p);
				p=Math.min(p,that.cell.ll2);
				return p;
			}
			that.SetSelection=function(xcur,x1,x2,ycur,y1,y2,scroll)
			{
				that.xcurpos=xcur;
				that.xsel1=x1;
				that.xsel2=x2;
				that.ycurpos=ycur;
				that.ysel1=y1;
				that.ysel2=y2;
				if (scroll) that.scrolltoxypos();else that.render();
			}
			return that;
        };

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		DotPlotCell.StretchList=function(iParentRef,parent) 
		{
            var that=MScrollList.Init(iParentRef);
			that.parent=parent;
			that.cell=null;
			that.stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
			that.setCell=function(_cell)
			{
				that.cell=_cell;
				var dotplot=that.cell.project.dotplot;
				if (dotplot.StretchDataType=='synteny') that.SetDataCount(dotplot.GetStretchCount());
				else that.SetDataCount(dotplot.uniquestretches.length);
				that.render();
			}
			that.GetFieldText=function(rowindex,field)
			{
				if (that.cell==null) return 0;
				var dotplot=that.cell.project.dotplot;
				if (dotplot==null) return 0;
				if (dotplot.StretchDataType=='synteny')
				{
					that.stretch=dotplot.GetStretch(rowindex,that.stretch);
					if (field.fieldcode==='_index') return rowindex+1;
					if (field.fieldcode==='_x_position') return that.stretch.x;
					if (field.fieldcode==='_y_position') return that.stretch.y;
					if (field.fieldcode==='_length') return that.stretch.len;
					if (field.fieldcode==='_direction') return that.stretch.ori;
					if (field.fieldcode==='_score') return that.stretch.score;
					if (field.fieldcode==='_include') return that.stretch.include;
				}
				else
				{
					var stretch=dotplot.uniquestretches[rowindex];
					if (stretch==null) return 0;
					if (field.fieldcode==='_index') return rowindex+1;
					if (field.fieldcode==='_x_position') return stretch.start;
					if (field.fieldcode==='_y_position') return stretch.end;
					if (field.fieldcode==='_length') return stretch.end-stretch.start;
				}
				return 0;
			}
			that.SetFieldText=function(ctx,rowindex,colindex,field,text,px,py,sizex,sizey)
			{
				ctx.fillStyle='rgba(0,0,0,1.0)';
				ctx.fillText(that.GetFieldText(rowindex,field),px+2,py+2);
			}
			that.ListMouseDown=function(posx,posy)
			{
				var row=that.GetHitRow(posy);
				var _row=that.ConvertToIndices(row);
				var col=that.GetHitCol(posx);
				var colfield=that.GetFieldSel();
				if (that.cell==null) return;
				var dotplot=that.cell.project.dotplot;
				if (dotplot==null) return;
				if (dotplot.StretchDataType=='synteny')
				{
					parent.CellSelectStretch(_row,0,true,true,false,true);
				}
			}
			return that;
		}
		
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		DotPlotCell.StretchDetails=function(iParentRef,parent) 
		{
            var that=MScrollWindow.Init(iParentRef,false);
			that.parent=parent;
			that.cell=null;
			that.setCell=function(_cell)
			{
				that.cell=_cell;
				that.render();
			}
			that.draw=function()
			{
				var context=that._cnvContext;
				context.fillStyle="rgb(255,255,255)";
				context.fillRect(0,0,that._cnvWidth,that._cnvHeight);
				context.fillStyle="rgb(0,0,0)";
				if (that.cell==null) return;
				context.font = "14px serif";
				var seq1=that.cell.GetSeq1();
				var seq2=that.cell.GetSeq2();
				var yy=15;
				var spatie=15;
				context.fillText("Organism: "+seq1.organism,10,yy);yy+=spatie;
				context.fillText("Sequence:" +seq1.sequence+", "+seq1.length,10,yy);yy+=spatie;
				context.fillText("Coverage: " + "10%",10,yy);yy+=spatie;
				context.fillText("Overall identity (overlap only): " + "10%",10,yy);yy+=spatie;
				context.fillText("Overall identity (full sequence): " + "10%",10,yy);yy+=spatie;

				yy+=spatie;
				context.fillText("Organism: "+seq2.organism,10,yy);yy+=spatie;
				context.fillText("Sequence:" +seq2.sequence+", "+seq2.length,10,yy);yy+=spatie;
				context.fillText("Coverage: " + "10%",10,yy);yy+=spatie;
				context.fillText("Overall identity (overlap only): " + "10%",10,yy);yy+=spatie;
				context.fillText("Overall identity (full sequence): " + "10%",10,yy);yy+=spatie;
			}
			return that;
		}
		
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	
		DotPlotCell.SeqDetails=function(iParentRef,parent,_seqbox) 
		{
			var that=SeqPlot.Panel(iParentRef,_seqbox);
			that.parent=parent;
			that.diffbase1=0;
			that.diffbase2=0;
			that.multiline=0;
//			that.showNumberGrid=false;
			that.circular=false;
			that.invert=false;
			that.loaded=false;
			that.sequence1=null;
			that.sequence2=null;
			that.entry1=null;
			that.entry2=null;
			that.cell=null;
			that.dotplot=null;
			that.selectedStretch=null;
			that.variationchannel=null;
			that.stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
			that.colors_r=[];
			that.colors_g=[];
			that.colors_b=[];
			that.InitColors=function()
			{
				var k=0;
				var i=0;
				for (i=0;i<1022;i++) {that.colors_r.push(0);that.colors_g.push(0);that.colors_b.push(0);}
				for (i=0;i<128;i++) 
				{
					that.colors_r[k]=0;
					that.colors_g[k]=0;
					that.colors_b[k]=128+i;
					k++;
				}
				for (i=0;i<255;i++) 
				{
					that.colors_r[k]=0;
					that.colors_g[k]=i;
					that.colors_b[k]=255;
					k++;
				}
				for (i=0;i<255;i++) 
				{
					that.colors_r[k]=i;
					that.colors_g[k]=255;
					that.colors_b[k]=255-i;
					k++;
				}
				for (i=0;i<255;i++) 
				{
					that.colors_r[k]=255;
					that.colors_g[k]=255-i;
					that.colors_b[k]=0;
					k++;
				}
				for (i=0;i<128;i++) 
				{
					that.colors_r[k]=255-i;
					that.colors_g[k]=0;
					that.colors_b[k]=0;
					k++;
				}
			}
			that.getColor=function(k)
			{
				k=~~k;
				if (k>=0 && k<1022)
				{	
					return 'rgba('+that.colors_r[k]+','+that.colors_g[k]+','+that.colors_b[k]+',0.2)';
				}
				return 'rgba(0,0,0,1)';
			}
			that.InitColors();
			that.RequestCallBack=function()
			{
				that.loaded=false;
				that.cell=parent.DotPlotOverview.getSelectedCell();
				if (that.cell==null) return;
				that.sequence1=that.SeqBox.GetSequence(0);
				that.sequence2=that.SeqBox.GetSequence(1);
				if (that.sequence1!=null && that.sequence2!=null)
				{
					that.entry1=that.SeqBox.GetEntryFromCode(that.cell.getClassObjectCode1());
					that.entry2=that.SeqBox.GetEntryFromCode(that.cell.getClassObjectCode2());
					if (that.entry1==null || that.entry2==null) return;
					that.dotplot=parent.Project.dotplot;
					if (that.dotplot==null) return;
					if (that.dotplot.GetStretchCount()==0) return;
					that.loaded=true;
					var channel=that.SeqBox.FindChannel("stretches");
					if (channel==null) 
					{
						channel=new MPlotChannel.Sequence(that.SeqBox);
						channel.SetAccession("stretches");
						channel.SetLabel("stretches");
					}
					var seqchannel=that.sequence1.LinkChannel(channel);
					seqchannel.location=1;
					seqchannel.size=60;
					seqchannel.SetBackColor("rgba(255,255,255,1.0)");
					
					var seqchannel=that.sequence1.FindSeqChannel("Sequence data");
					seqchannel.location=-1;
					
/*					var channel=that.SeqBox.FindChannel("variations");
					if (channel==null) 
					{
						channel=new MPlotChannel.Sequence(that.SeqBox);
						channel.SetAccession("variations");
						channel.SetLabel("variations");
					}
					var seqchannel=that.sequence2.LinkChannel(channel);
					seqchannel.location=1;
					seqchannel.size=20;
					that.variationchannel=seqchannel;*/
					
					var channel=that.SeqBox.FindChannel("Sequence numbering");
					if (channel==null)
					{
						channel=new MPlotChannel.Numbering(this);
						channel.SetAccession("Sequence numbering");
						channel.SetLabel("Sequence numbering");
						channel.SetColor("rgb(180,180,180)");
						channel.SetViewState(true);
					}
					var seqchannel=that.sequence2.LinkChannel(channel);
					seqchannel.SetLocation(-1);

					var seqchannel=that.sequence1.FindSeqChannel("Sequence data");
					seqchannel.location=-1;

				}
				that.UpdateFacts(false);
				that.invalidate();
//				that.SeqBox.PerformFtsSearch("CDS",that.CallBackFtsSearch,"Coding features");
			}
			that.CallBackFtsSearch=function(ftschannel)
			{
				if (ftschannel!=null)
				{
					var seqchannel=that.sequence1.LinkChannel(ftschannel);
					seqchannel.location=-1;
					seqchannel.size=25;
					var seqchannel=that.sequence2.LinkChannel(ftschannel);
					seqchannel.location=+1;
					seqchannel.size=25;
					that.UpdateFacts(false);
					that.invalidate();
				}
			}
			that.SetSelection=function(x_,y_,stretch,update)
			{
				if (that.loaded==false) return;
				that.selectedStretch=stretch;
				var dd1=0;
				var dd2=0;
//				if (stretch.ori>0) dd2=x_-y_;else dd2=y_-(that.entry2.GetLength()-1)+x_;//2
				if (stretch.ori>0) dd2=x_-y_;else dd2=y_-(that.entry2.GetLength())+x_;//2
				if (dd2<0) dd1=-dd2;

				that.sequence1.ResetBlocks();
				that.sequence2.ResetBlocks();
				if (stretch.ori>0)
				{
					that.sequence1.AddBlock(that.entry1,dd1,0,that.entry1.GetLength(),false,0,false);
					that.sequence2.AddBlock(that.entry2,dd2+dd1,0,that.entry2.GetLength(),false,0,false);
					that.invert=false;
				}
				else
				{
					that.sequence1.AddBlock(that.entry1,dd1,0,that.entry1.GetLength(),false,0,false);
					that.sequence2.AddBlock(that.entry2,dd2+dd1,0,that.entry2.GetLength(),true,0,false);
					that.invert=true;
				}
				that.sequence1.LinkAllChannels();
				that.sequence2.LinkAllChannels();
				that.diffbase1=dd1;
				that.diffbase2=dd2+dd1;

				that.curpos=dd1+x_;//if (stretch.ori<0) that.curpos=y_;
				that.UpdateFacts(true);
			}
			that.pre_paintline=function(update)
			{
				if (that.loaded==false) return;
				var ctx=that._cnvContext;
				var xoo=Math.floor(-that.GetHScrollPos()); 
				var yoo=Math.floor(-that.GetVScrollPos());
				var offset=Math.floor(that.leftmargin+xoo);
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				if (that.line<0) return;
				var left=that.leftmargin;
				var right=left+that.xsize;
				var top=that.yoffset;
				var bottom=top+that.linespace;
				if (bottom<0) return;
				if (top>ly) return;
				var py1=top+that.sequence1.plotoffset;
				var py2=top+that.sequence2.plotoffset;
				var d=10;var r=0;var b=0;
				var px1=0;
				var px2=0;
				var px3=0;
				var px4=0;

				var data=that.imageData.data;
				var llx=that.imageData.width;
				var length=data.length;

				var diag1=0;
				var diag2=0;
				if (that.selectedStretch!=null)
				{
					diag1=that.selectedStretch.x-that.selectedStretch.y-that.dotplot.display_diagonal_cutoff;
					diag2=that.selectedStretch.x-that.selectedStretch.y+that.dotplot.display_diagonal_cutoff;
				}
				var x1=-1;
				var y1=-1;
				var dotplot=that.dotplot;
				var i=dotplot.GetStretchCount();
				var style=0;
				
				var floor=Math.floor;
				var max=Math.max;
				var min=Math.min;
				var abs=Math.abs;
				var convert=that.xconvert;
				var len1=that.entry1.GetLength();
				var len2=that.entry2.GetLength();
				var offsetdiff1=that.offsetbase-that.diffbase1;
				var offsetdiff2=that.offsetbase-that.diffbase2;
				var jj=0;var ii=0; var dist=0.0; var p=0; var aa=150;var len=0;var xxx1=0;var xxx2=0;
				var _stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
				var superstretches=dotplot.superstretches;
/*				if (superstretches!=null)
				{
					i=superstretches.oris.length-1;
					while (i--)
					{
						px1=floor(convert(superstretches.xpossen1[i]-offsetdiff1)+offset);
						px2=floor(convert(superstretches.xpossen2[i]-offsetdiff1)+offset);
						px3=floor(convert(superstretches.ypossen1[i]-offsetdiff2)+offset);
						px4=floor(convert(superstretches.ypossen2[i]-offsetdiff2)+offset);
						if (superstretches.oris[i]<0) 
						{
							px3=convert(superstretches.ypossen2[i]-offsetdiff2+1)+offset;//2
							px4=convert(superstretches.ypossen1[i]-offsetdiff2+1)+offset;//2
						}
						if (superstretches.oris[i]>0) ctx.fillStyle="rgba(100,100,255,0.4)";else ctx.fillStyle="rgba(255,100,100,0.4)";
						ctx.beginPath();
						ctx.moveTo(px1,py1);
						ctx.lineTo(px2,py1);
						ctx.lineTo(px4,py2);
						ctx.lineTo(px3,py2);
						ctx.lineTo(px1,py1);
						ctx.closePath();
						ctx.fill();
					}
					return;
				}*/
				while (i--)
				{
					dotplot.GetStretch(i,_stretch);
//					if (_stretch.include>1)
					{
						len=max(floor(convert(_stretch.len)),1);
						px1=floor(convert(_stretch.x-offsetdiff1)+offset);
						px2=px1+len;
						px3=convert(_stretch.y-offsetdiff2)+offset;
						if (that.invert==true && _stretch.ori<0) px3=convert(_stretch.y-offsetdiff2+1)+offset;//2
						if ((that.invert==true && _stretch.ori>0) || (that.invert==false && _stretch.ori<0)) px3=convert((len2-1)-(_stretch.y+_stretch.len)-offsetdiff2)+offset;//2
						px3=floor(px3);
						px4=px3+len;
						if (abs(px1-px3)>10000) continue;//te scheve lijnen worden niet getekend
						if (!(px2<left && px3+px2-px1<left) && !(px1>right && px3>right) && !(px1==x1 && px3==y1 && px1==px2))
						{
							if (px1!=x1 || px3!=y1)
							{
								if (dotplot.heapMapColors==true) ctx.fillStyle=that.getColor(1021*_stretch.x/len1);
								else 
								{
									if (_stretch.ori>0 && style!=1) {ctx.fillStyle="rgba(100,100,255,0.4)";style=1;}
									if (_stretch.ori<0 && style!=-1) {ctx.fillStyle="rgba(255,100,100,0.4)";style=-1;}
								}
//								if (_stretch.include==0) ctx.fillStyle="rgba(100,100,100,0.4)";
//								if (_stretch.include==2) ctx.fillStyle="rgba(255,100,100,0.4)";
//								if (_stretch.include==3) ctx.fillStyle="rgba(100,100,255,0.4)";
								ctx.beginPath();
								ctx.moveTo(px1,py1);
								ctx.lineTo(px2,py1);
								ctx.lineTo(px4,py2);
								ctx.lineTo(px3,py2);
								ctx.lineTo(px1,py1);
								ctx.closePath();
								ctx.fill();
							}

							x1=px1;
							y1=px3;
						}
					}
				}
			}
			that.post_paint=function()
			{
			}

			that.MouseDown=function(posx,posy)  //overwrite
			{
				if (that.loaded==false) return;
				that.curpos=that.getxpos(posx,posy);
				that.selpos1=that.curpos;
				that.selpos2=that.curpos;
				that.ycurpos=that.getypos(posx,posy);
				that.sequencesel=that.getsequencesel(posx,posy);
				that.channelsel=that.getchannelsel(posx,posy);
				var index=that.testHit(posx,posy);
				parent.CellSelectStretch(index,that.curpos,true,false,true,true);
				that.selectedStretch=that.dotplot.GetStretch(index,that.stretch);
				that.render();
			}
			that.testHit=function(posx,posy)
			{
				if (that.loaded==false) return -1;
 				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
				that.line=Math.floor((posy-yoo)/Math.max(that.linespace,1));
				that.yoffset=that.line*that.linespace+yoo;
				var ctx = that.getMyCanvasElement('main').getContext("2d");
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				if (that.line<0) return -1;
				var offset=that.leftmargin+xoo;
				var left=that.leftmargin;
				var right=left+that.xsize;
				var top=that.yoffset;
				var bottom=top+that.linespace;
				
				var py1=top+that.sequence1.plotoffset;
				var py2=top+that.sequence2.plotoffset;
				if (posy<py1 || posy>py2) return -1;
				var stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
				var ll=that.dotplot.GetStretchCount();
				for (var i=0;i<ll;i++)
				{
					that.dotplot.GetStretch(i,stretch);
					var px1=that.xconvert(stretch.x-that.offsetbase+that.diffbase1)+offset;
					var px2=that.xconvert(stretch.x+stretch.len-that.offsetbase+that.diffbase1)+offset;
					var px3=that.xconvert(stretch.y-that.offsetbase+that.diffbase2)+offset;
					if ((that.invert==true && stretch.ori>0) || (that.invert==false && stretch.ori<0))
					{
						px3=that.xconvert(that.entry2.GetLength()-(stretch.y+stretch.len)-that.offsetbase+that.diffbase2)+offset;
					}
					var hit=1;
					if (px1+px2-px1<left && px3+px2-px1<left) hit=0;
					if (px1>right && px3>right) hit=0;
					if (hit==0) continue;
					ctx.beginPath();
					ctx.moveTo(px1,py1);ctx.lineTo(px2,py1);
					ctx.lineTo(px3+px2-px1,py2);ctx.lineTo(px3,py2);
					ctx.lineTo(px1,py1);
					ctx.closePath();
					if (ctx.isPointInPath(posx,posy)) return i;
				}
 				return -1;
			}
			return that;
		}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	
		DotPlotCell.SeqCDetails=function(iParentRef,parent,_seqbox) 
		{
			var that=SeqPlot.Panel(iParentRef,_seqbox);
			that.parent=parent;
			that.diffbase1=0;
			that.diffbase2=0;
			that.multiline=0;
			that.showNumberGrid=false;
			that.invert=false;
			that.loaded=false;
			that.sequence=null;
			that.cell=null;
			that.dotplot=null;
			that.selectedStretch=null;
			that.stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
			that.variationchannel=null;
			that.circular=true;
			that.scrollcircular=true;
			that.basespercm=that.SeqBox.GetMaxSeqLen()/(2*200*Math.PI);


			that.RequestCallBack=function()
			{
				that.loaded=false;
				that.cell=parent.DotPlotOverview.getSelectedCell();
				if (that.cell==null) return;
				that.sequence=that.SeqBox.GetSequence(0);
				if (that.sequence!=null)
				{
					that.dotplot=parent.Project.dotplot;
					if (that.dotplot==null) return;
					if (that.dotplot.GetStretchCount()==0) return;
					that.loaded=true;
					that.basespercm=that.SeqBox.GetMaxSeqLen()/(2*10*Math.PI);
				
					var seqchannel=that.sequence.FindSeqChannel("Sequence data");
					seqchannel.location=-1;
					
/*					var channel=that.SeqBox.FindChannel("variations");
					if (channel==null) 
					{
						channel=new MPlotChannel.Sequence(that.SeqBox);
						channel.SetAccession("variations");
						channel.SetLabel("variations");
					}
					var seqchannel=that.sequence2.LinkChannel(channel);
					seqchannel.location=1;
					seqchannel.size=20;
					that.variationchannel=seqchannel;*/
					
					var seqchannel=that.sequence.FindSeqChannel("Sequence data");
					seqchannel.location=-1;
				}
				that.UpdateFacts(false);
				that.invalidate();
//				that.SeqBox.PerformFtsSearch("CDS",that.CallBackFtsSearch,"Coding features");
//				that.SeqBox.PerformSeqSearch("seq",that.CallBackSeqSearch,"Sequence search");
			}
			that.CallBackFtsSearch=function(channel)
			{
				if (channel!=null)
				{
					var seqchannel=that.sequence.LinkChannel(channel);
					seqchannel.location=+1;
					seqchannel.size=16;
					that.UpdateFacts(false);
					that.invalidate();
				}
			}
			that.CallBackSeqSearch=function(channel)
			{
				if (channel!=null)
				{
					var seqchannel=that.sequence.LinkChannel(channel);
					seqchannel.location=+1;
					seqchannel.size=16;
					that.UpdateFacts(false);
					that.invalidate();
				}
			}
			that.SetSelection=function(x_,y_,stretch,update)
			{
				if (that.loaded==false) return;
				that.selectedStretch=stretch;
			}
			that.pre_paint=function()
			{
				if (that.loaded==false) return;
				if (that.dotplot==null) return;
				var context=that._cnvContext;
				var xoo=Math.floor(-that.GetHScrollPos()); 
				var yoo=Math.floor(-that.GetVScrollPos());
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				var len=that.SeqBox.GetMaxSeqLen();
				var d=10;var r=0;var b=0;
				var px1=0;
				var px2=0;
				var px3=0;

				var rad=(200*that.maxbasespercm/(that.basespercm*2*Math.PI));
				rad=Math.max(rad,50);


				var ff=Math.min(1,2*that.basespercm/that.maxbasespercm);
				var cty=0;
				for (var i=0;i<that.SeqBox.GetSequenceCount();i++)
				{
					var sequence=that.SeqBox.GetSequence(i);
					if (sequence==null) continue;
//					sequence.UpdateSeqChannelOffsets();
					cty+=(1-ff)*sequence.GetLowerSeqChannelSpace();
				}
				rad=rad-cty;
				var image=that.imageData;

				context.lineWidth=1;
				context.strokeStyle='rgba(100,100,100,0.4)';
//				if (fts.dir<0) context.fillStyle='rgba(255,0,0,0.1)';else context.fillStyle='rgba(0,0,255,0.1)';
				context.fillStyle='rgba(0,0,255,0.1)';
				var x1=-1;
				var y1=-1;
				var pp1=0;var pp2=0;
				var floor=Math.floor;
				var max=Math.max;
				var min=Math.min;
				var convert=that.xconvert;
				var pt1x=0,pt1y=0,pt2x=0,pt2y=0,pt3x=0,pt3y=0;
				var _pt1x=0,_pt1y=0,_pt2x=0,_pt2y=0,_pt3x=0,_pt3y=0,__ptx1=0,__ptx2=0,__pty1=0,__pty2=0;
				var PI=Math.PI;
				var cx=that.centrumX;
				var cy=that.centrumY;
				var floor=Math.floor;
				var sin=Math.sin;
				var cos=Math.cos;
				
				var startbasegaps0=that.sequence.blocks[0].startbasegaps;
				var startbasegaps1=that.sequence.blocks[1].startbasegaps;
				var endbase1=that.sequence.blocks[1].endbase;
				
				var h1=0;
				var h2=0;
				var _h1=0;
				var _h2=0;

				pt3x=that.centrumX;
				pt3y=that.centrumY;
				
				var rr=0;
				var dotplot=that.dotplot;
				var i=dotplot.GetStretchCount();
				var _stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
				context.strokeStyle='rgba(100,100,255,0.2)';
//				context.beginPath();
				while (i--)
				{
					dotplot.GetStretch(i,_stretch);
					if (_stretch.include>1)
					{
						pp1=startbasegaps0+_stretch.x;
						_pp1=startbasegaps0+_stretch.x+_stretch.len;
						pp2=startbasegaps1+endbase1-_stretch.y;
						_pp2=startbasegaps1+endbase1-_stretch.y-_stretch.len;
						if (_stretch.ori<0) pp2=_stretch.y+startbasegaps1;
						if (_stretch.ori<0) _pp2=_stretch.y+_stretch.len+startbasegaps1;
						h1=360-360.0*pp1/len+that.C_offset;
						_h1=360-360.0*_pp1/len+that.C_offset;
						h2=360-360.0*pp2/len+that.C_offset;
						_h2=360-360.0*_pp2/len+that.C_offset;
//						var h11=360-360.0*(pp1+l)/len+that.C_offset;
//						var h22=360-360.0*(pp2+l)/len+that.C_offset;
						
						pt1x=floor(cx-sin(h1/180*PI)*rad);
						__pt1x=floor(cx-sin(_h1/180*PI)*rad);
						pt1y=floor(cy-cos(h1/180*PI)*rad);
						__pt1y=floor(cy-cos(_h1/180*PI)*rad);
						
						pt2x=floor(cx-sin(h2/180*PI)*rad);
						__pt2x=floor(cx-sin(_h2/180*PI)*rad);
						pt2y=floor(cy-cos(h2/180*PI)*rad);
						__pt2y=floor(cy-cos(_h2/180*PI)*rad);

						
						if (_stretch.ori<0 && rr==0)
						{
							context.stroke();
							context.closePath();
							context.beginPath();
							context.strokeStyle='rgba(255,100,100,0.2)';
							context.fillStyle='rgba(255,100,100,0.2)';
							rr=1;
						}
						if (_stretch.ori>0 && rr==1)
						{
							context.stroke();
							context.closePath();
							context.beginPath();
							context.strokeStyle='rgba(100,100,255,0.2)';
							context.fillStyle='rgba(100,100,255,0.2)';
							rr=0;
						}
						
						if (_pt1x!=pt1x || _pt1y!=pt1y || _pt2x!=pt2x || _pt2y!=pt2y)
						{
							if (pt1x==__pt1x)
							{
								context.moveTo(pt1x,pt1y);
								context.quadraticCurveTo(pt3x,pt3y,pt2x,pt2y);
							}
							else
							if (that.CheckQuadCurve(pt1x,pt1y,pt3x,pt3y,pt2x,pt2y)==true)
							{
								context.stroke();
								context.closePath();
								context.beginPath();
								
/*								context.moveTo(pt1x,pt1y);
								context.quadraticCurveTo(pt3x,pt3y,pt2x,pt2y);
								context.lineTo(__pt2x,__pt2y);
								context.quadraticCurveTo(pt3x,pt3y,__pt1x,__pt1y);*/
								
								context.moveTo(pt1x,pt1y);
								that.DrawQuadCurve(pt1x,pt1y,pt3x,pt3y,pt2x,pt2y);
								context.lineTo(pt2x,pt2y);
								that.DrawQuadCurve(__pt2x,__pt2y,pt3x,pt3y,__pt1x,__pt1y);
								context.lineTo(__pt1x,__pt1y);

								context.closePath();
								context.stroke();
								context.fill();
								context.beginPath();
							}
						}
						_pt1x=pt1x;
						_pt1y=pt1y;
						_pt2x=pt2x;
						_pt2y=pt2y;
					}
				}
				context.stroke();
				context.closePath();
				context.globalAlpha=1.0;
			}
			that.CheckQuadCurve=function(pt1x,pt1y,pt3x,pt3y,pt2x,pt2y)
			{
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				for (t=0;t<=1;t+=0.1)
				{
					x=Math.floor(pt1x*(1-t)*(1-t)+2*pt3x*t*(1-t)+pt2x*t*t);
					y=Math.floor(pt1y*(1-t)*(1-t)+2*pt3y*t*(1-t)+pt2y*t*t);
					if ((x>0 && x<lx) || (y>0 && y<ly)) return true;
				}
				return false;
			}
			that.DrawQuadCurve=function(pt1x,pt1y,pt3x,pt3y,pt2x,pt2y)
			{
				var context=that._cnvContext;
				var lx=that.GetXSize();
				var ly=that.GetYSize();
				var _x=-1;
				var _y=-1;
				for (t=0;t<=1;t+=0.01)
				{
					x=Math.floor(pt1x*(1-t)*(1-t)+2*pt3x*t*(1-t)+pt2x*t*t);
					y=Math.floor(pt1y*(1-t)*(1-t)+2*pt3y*t*(1-t)+pt2y*t*t);
					if (x<-100) x=-100;
					if (x>lx+100) x=lx+100;
					if (y<-100) y=-100;
					if (y>ly+100) y=ly+100;
					if (_x!=x || _y!=y) context.lineTo(x,y);
					_x=x;
					_y=y;
				}
			}
			that.FindQuadraticCurveMiddlePoint=function(pt1,pt2)
			{
				var midPoint={x:((pt1.x-that.centrumX)+(pt2.x-that.centrumX))/2,y:((pt1.y-that.centrumY)+(pt2.y-that.centrumY))/2};
				midPoint.x=Math.floor(midPoint.x*0.0+that.centrumX);
				midPoint.y=Math.floor(midPoint.y*0.0+that.centrumY);
				return midPoint;
			}
			that.post_paint=function()
			{
			}

			that.MouseDown=function(posx,posy)  //overwrite
			{
				if (that.loaded==false) return;
				that.curpos=that.getxpos(posx,posy);
				that.selpos1=that.curpos;
				that.selpos2=that.curpos;
				that.ycurpos=that.getypos(posx,posy);
				that.sequencesel=that.getsequencesel(posx,posy);
				that.channelsel=that.getchannelsel(posx,posy);
				var index=that.testHit(posx,posy);
				parent.CellSelectStretch(index,that.curpos,true,false,true,true);
				that.selectedStretch=that.dotplot.GetStretch(index,that.stretch);
				that.render();
			}
			that.testHit=function(posx,posy)
			{
				if (that.loaded==false) return -1;
 				var xoo=-that.GetHScrollPos(); 
				var yoo=-that.GetVScrollPos();
 				return -1;
			}
			return that;
		}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	
		DotPlotCell.Alignment=function(iParentRef,parent,_seqbox) 
		{
			var that=SeqPlot.Panel(iParentRef,_seqbox);
			that.aligned=true;
			that.parent=parent;
			that.diffbase1=0;
			that.diffbase2=0;
			that.multiline=1;
//			that.showNumberGrid=false;
			that.invert=false;
			that.loaded=false;
			that.sequence1=null;
			that.sequence2=null;
			that.entry1=null;
			that.entry2=null;
			that.cell=null;
			that.dotplot=null;
			that.selectedStretch=null;
			that.stretch={'x':0,'y':0,'len':0,'ori':0,'score':0,'include':0};
			that.variationchannel=null;
			that.ppy1=0;
			that.ppy2=0;
//			that.circular=true;
			
			that.RequestCallBack=function()
			{
				that.loaded=false;
				that.cell=that.parent.DotPlotOverview.getSelectedCell();
				if (that.cell==null) return;
				that.sequence1=that.SeqBox.GetSequence(0);
				that.sequence2=that.SeqBox.GetSequence(1);
				if (that.sequence1!=null && that.sequence2!=null)
				{
					that.entry1=that.SeqBox.GetEntryFromCode(that.cell.getClassObjectCode1());
					that.entry2=that.SeqBox.GetEntryFromCode(that.cell.getClassObjectCode2());
					if (that.entry1==null || that.entry2==null) return;
					that.dotplot=that.parent.Project.dotplot;
					if (that.dotplot==null) return;
					if (that.dotplot.GetStretchCount()==0) return;
					that.loaded=true;
					var channel=that.SeqBox.FindChannel("stretches");
					if (channel==null) 
					{
						channel=new MPlotChannel.Sequence(that.SeqBox);
						channel.SetAccession("stretches");
						channel.SetLabel("stretches");
					}
					var seqchannel=that.sequence1.LinkChannel(channel);
					seqchannel.location=1;
					seqchannel.size=0;
					seqchannel.SetBackColor("rgba(255,255,255,1.0)");
					
/*					var channel=that.SeqBox.FindChannel("variations");
					if (channel==null) 
					{
						channel=new MPlotChannel.VariationSearch(that.SeqBox);
						channel.SetAccession("variations");
						channel.SetLabel("variations");
					}
					var seqchannel=that.sequence2.LinkChannel(channel);
					seqchannel.location=1;
					seqchannel.size=20;
					that.variationchannel=seqchannel;*/
					
					var seqchannel=that.sequence1.FindSeqChannel("Sequence data");
//					seqchannel.location=-1;

					that.SeqBox.PerformFtsSearch("CDS",that.CallBackFtsSearch,"Coding features");
					that.SeqBox.PerformSeqSearch("seq",that.CallBackSeqSearch,"Sequence search");
				}
				that.UpdateFacts(false);
				that.invalidate();
			}
			that.CallBackFtsSearch=function(ftschannel)
			{
				if (ftschannel!=null)
				{
					var seqchannel=that.sequence1.LinkChannel(ftschannel);
					seqchannel.location=-1;
					seqchannel.size=20;
					var seqchannel=that.sequence2.LinkChannel(ftschannel);
					seqchannel.location=+1;
					seqchannel.size=20;
					that.UpdateFacts(false);
					that.invalidate();
				}
			}
			that.CallBackSeqSearch=function(channel)
			{
				if (channel!=null)
				{
					var seqchannel=that.sequence1.LinkChannel(channel);
					seqchannel.location=-1;
					seqchannel.size=10;
					var seqchannel=that.sequence2.LinkChannel(channel);
					seqchannel.location=+1;
					seqchannel.size=10;
					that.UpdateFacts(false);
					that.invalidate();
				}
			}
			that.CallBackVarSearch=function()
			{
				if (that.dotplot==null) return;
				var varchannel=that.SeqBox.FindChannel("variations");
				if (varchannel!=null)	
				{
//					varchannel.SetFoundHits(that.dotplot.variations);
				}
				if (varchannel!=null) console.log("valid channel");else  console.log("!!!! invalid channel !!!!");
				console.log("callback variations passed");
				that.UpdateFacts(false);
				that.invalidate();
			}
			that.SetSelection=function(x_,y_,stretch,update)
			{
				that.SetSel(x_,x_,x_);
			}
			that.pre_paintsequenceline=function(sequence)
			{
				if (that.loaded==false) return;
				var ppy=that.yoffset+sequence.plotoffset;
				if (ppy<0 || ppy>that.ysize) return;
				var blocks=sequence.blocks;
				var b1=0,b2=0;
				var block=null;
				var left=~~that.leftmargin;
				for (var j=0;j<that.xsize+10;j++) {that.bitdirect.setBit(j,0);that.bitinvert.setBit(j,0);}
				var inv=0;
				var j=blocks.length;
				while (j--)
				{
					block=blocks[j];
					if (block.invert==true) inv=1;else inv=0;
					if (block.startbasegaps>that.line_b2 || block.endbasegaps<that.line_b1) continue;
					b1=block.startbasegaps;if (b1<that.line_b1) b1=that.line_b1;
					b2=block.endbasegaps;if (b2>that.line_b2) b2=that.line_b2;
					b1=~~((b1-that.offsetbase)*that.cm2pix/that.basespercm+that.xscrollpos)-left;if (b1<0) b1=0;
					b2=~~((b2-that.offsetbase)*that.cm2pix/that.basespercm+that.xscrollpos)-left;if (b2>that.xsize) b2=that.xsize;
					if (inv==1) for (var k=b1;k<=b2;k++) that.bitinvert.setBit(k,1);
					else for (var k=b1;k<=b2;k++) that.bitdirect.setBit(k,1);
				}
				var context=that._cnvContext;
				context.fillStyle="rgba(0,0,255,1.0)";
				var k=0;var p=0;
				while (k<that.xsize) 
				{
					if (that.bitdirect.getBit(k)==1) p++;
					else {if (p>0) context.fillRect(left+k-p,ppy,p,2);p=0;}
					k++;
				}
				if (p>0) context.fillRect(left+k-p,ppy,p,2);
				context.fillStyle="rgba(255,0,0,1.0)";
				k=0;p=0;
				while (k<that.xsize) 
				{
					if (that.bitinvert.getBit(k)==1) p++;
					else {if (p>0) context.fillRect(left+k-p,ppy,p,2);p=0;}
					k++;
				}
				if (p>0) context.fillRect(left+k-p,ppy,p,2);
				if (sequence==that.sequence2) 
				{
					dd=-(that.sequence2.plotoffset-that.sequence1.plotoffset);
					context.fillStyle="rgba(0,0,255,0.2)";
					k=0;p=0;
					while (k<that.xsize) 
					{
						if (that.bitdirect.getBit(k)==1) p++;
						else {if (p>0) context.fillRect(left+k-p,ppy,p,dd);p=0;}
						k++;
					}
					if (p>0) context.fillRect(left+k-p,ppy,p,dd);
					context.fillStyle="rgba(255,0,0,0.2)";
					k=0;p=0;
					while (k<that.xsize) 
					{
						if (that.bitinvert.getBit(k)==1) p++;
						else {if (p>0) context.fillRect(left+k-p,ppy,p,dd);p=0;}
						k++;
					}
					if (p>0) context.fillRect(left+k-p,ppy,p,dd);
				}
			}
/*			that.pre_paintline=function(update)
			{
				if (that.loaded==false) return;
				that.ppy1=that.yoffset+that.sequence1.plotoffset;
				that.ppy2=that.yoffset+that.sequence2.plotoffset;
				if (that.ppy2<0) return;
				if (that.ppy1>that.ysize) return;
				var sequence=that.SeqBox.GetSequence(1);
				that.sequence=sequence;
				that.seqoffset=that.yoffset+sequence.plotoffset;
				var blocks=sequence.blocks;
				var cnt=blocks.length;
				var b1=0,b2=0;
				var block=null;
				var left=that.leftmargin;
				var right=that.leftmargin+that.xsize;
				for (var j=0;j<that.xsize+10;j++) {that.bitdirect.setBit(j,0);that.bitinvert.setBit(j,0);}
				var mmax=Math.max;
				var mmin=Math.min;
				var offbase=that.offsetbase;
				var scrollp=that.xscrollpos;
				var bb1=that.line_b1;
				var bb2=that.line_b2;
				var inv=0;
				var j=cnt;
				while (j--)
				{
					block=blocks[j];
					if (block.invert==true) inv=1;else inv=0;
					if (block.startbasegaps>bb2 || block.endbasegaps<bb1) continue;
					b1=block.startbasegaps;if (b1<bb1) b1=bb1;
					b2=block.endbasegaps;if (b2>bb2) b2=bb2;
					b1=~~((b1-offbase)*that.cm2pix/that.basespercm+scrollp)-left;if (b1<0) b1=0;
					b2=~~((b2-offbase)*that.cm2pix/that.basespercm+scrollp)-left;if (b2>right-left) b2=right-left;
					if (inv==1) for (var k=b1;k<=b2;k++) that.bitinvert.setBit(k,1);
					else for (var k=b1;k<=b2;k++) that.bitdirect.setBit(k,1);
				}
				var context=that._cnvContext;
				context.fillStyle="rgba(0,0,255,0.3)";
				var k=0;var p=0;
				while (k<that.xsize) 
				{
					if (that.bitdirect.getBit(k)==1) p++;
					else 
					{
						if (p>0) context.fillRect(left+k-p,that.ppy1,p,that.ppy2-that.ppy1);
						p=0;
					}
					k++;
				}
				if (p>0) context.fillRect(left+k-p,that.ppy1,p,that.ppy2-that.ppy1);
				
				context.fillStyle="rgba(255,0,0,0.3)";
				var k=0;var p=0;
				while (k<that.xsize) 
				{
					if (that.bitinvert.getBit(k)==1) p++;
					else 
					{
						if (p>0) context.fillRect(left+k-p,that.ppy1,p,that.ppy2-that.ppy1);
						p=0;
					}
					k++;
				}
				if (p>0) context.fillRect(left+k-p,that.ppy1,p,that.ppy2-that.ppy1);
			}*/

			that.MouseDown=function(posx,posy)  //overwrite
			{
				if (that.loaded==false) return;
				that.curpos=that.getxpos(posx,posy);
				that.selpos1=that.curpos;
				that.selpos2=that.curpos;
				that.ycurpos=that.getypos(posx,posy);
				that.sequencesel=that.getsequencesel(posx,posy);
				that.channelsel=that.getchannelsel(posx,posy);
				var index=that.testHit(posx,posy);
				parent.CellSelectStretch(index,that.curpos,true,true,true,false);
				that.selectedStretch=that.dotplot.GetStretch(index,that.stretch);
				that.render();
			}
			that.testHit=function(posx,posy)
			{
				if (that.loaded==false) return -1;
 				var ps=that.getxpos(posx,posy);
				var block=that.sequence2.GetBlock(ps,null);
				if (block!=null)
				{
					var ll=that.dotplot.GetStretchCount();
					for (var i=0;i<ll;i++)
					{
						that.dotplot.GetStretch(i,that.stretch);
						if (block.startbasegaps==that.stretch.x) return i;
					}
				}
 				return -1;
			}
			return that;
		}
		return DotPlotCell;
    });